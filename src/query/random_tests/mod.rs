//
// THESE TESTS WERE AUTO-GENERATED ON 2021-04-09 17:58:01 USING SEED 16418463664296661110
//

#[cfg(any(test, feature = "benchmark"))]
mod random_test01;

#[cfg(any(test, feature = "benchmark"))]
mod random_test02;

#[cfg(any(test, feature = "benchmark"))]
mod random_test03;

#[cfg(any(test, feature = "benchmark"))]
mod random_test04;

#[cfg(any(test, feature = "benchmark"))]
mod random_test05;

#[cfg(any(test, feature = "benchmark"))]
mod random_test06;

#[cfg(any(test, feature = "benchmark"))]
mod random_test07;

#[cfg(any(test, feature = "benchmark"))]
mod random_test08;

#[cfg(any(test, feature = "benchmark"))]
mod random_test09;

#[cfg(any(test, feature = "benchmark"))]
mod random_test10;

#[cfg(any(test, feature = "benchmark"))]
mod random_test11;

#[cfg(any(test, feature = "benchmark"))]
mod random_test12;

#[cfg(any(test, feature = "benchmark"))]
mod random_test13;

#[cfg(any(test, feature = "benchmark"))]
mod random_test14;

#[cfg(any(test, feature = "benchmark"))]
mod random_test15;

#[cfg(any(test, feature = "benchmark"))]
mod random_test16;

#[cfg(any(test, feature = "benchmark"))]
mod random_test17;

#[cfg(any(test, feature = "benchmark"))]
mod random_test18;

#[cfg(any(test, feature = "benchmark"))]
mod random_test19;

#[cfg(any(test, feature = "benchmark"))]
mod random_test20;

#[cfg(any(test, feature = "benchmark"))]
mod random_test21;

#[cfg(any(test, feature = "benchmark"))]
mod random_test22;

#[cfg(any(test, feature = "benchmark"))]
mod random_test23;

#[cfg(any(test, feature = "benchmark"))]
mod random_test24;

#[cfg(any(test, feature = "benchmark"))]
mod random_test25;

#[cfg(any(test, feature = "benchmark"))]
mod random_test26;

#[cfg(any(test, feature = "benchmark"))]
mod random_test27;

#[cfg(any(test, feature = "benchmark"))]
mod random_test28;

#[cfg(any(test, feature = "benchmark"))]
mod random_test29;

#[cfg(any(test, feature = "benchmark"))]
mod random_test30;

#[cfg(any(test, feature = "benchmark"))]
mod random_test31;

#[cfg(any(test, feature = "benchmark"))]
mod random_test32;

#[cfg(any(test, feature = "benchmark"))]
mod random_test33;

#[cfg(any(test, feature = "benchmark"))]
mod random_test34;

#[cfg(any(test, feature = "benchmark"))]
mod random_test35;

#[cfg(any(test, feature = "benchmark"))]
mod random_test36;

#[cfg(any(test, feature = "benchmark"))]
mod random_test37;

#[cfg(any(test, feature = "benchmark"))]
mod random_test38;

#[cfg(any(test, feature = "benchmark"))]
mod random_test39;

#[cfg(any(test, feature = "benchmark"))]
mod random_test40;

#[cfg(any(test, feature = "benchmark"))]
mod random_test41;

#[cfg(any(test, feature = "benchmark"))]
mod random_test42;

#[cfg(any(test, feature = "benchmark"))]
mod random_test43;

#[cfg(any(test, feature = "benchmark"))]
mod random_test44;

#[cfg(any(test, feature = "benchmark"))]
mod random_test45;

#[cfg(any(test, feature = "benchmark"))]
mod random_test46;

#[cfg(any(test, feature = "benchmark"))]
mod random_test47;

#[cfg(any(test, feature = "benchmark"))]
mod random_test48;

#[cfg(any(test, feature = "benchmark"))]
mod random_test49;

#[cfg(any(test, feature = "benchmark"))]
mod random_test50;

#[cfg(not(feature = "benchmark"))]
pub fn RunQueryTestBenchmarks()
{
}

#[cfg(feature = "benchmark")]
pub fn RunQueryTestBenchmarks()
{
    random_test01::Benchmark();
    random_test02::Benchmark();
    random_test03::Benchmark();
    random_test04::Benchmark();
    random_test05::Benchmark();
    random_test06::Benchmark();
    random_test07::Benchmark();
    random_test08::Benchmark();
    random_test09::Benchmark();
    random_test10::Benchmark();
    random_test11::Benchmark();
    random_test12::Benchmark();
    random_test13::Benchmark();
    random_test14::Benchmark();
    random_test15::Benchmark();
    random_test16::Benchmark();
    random_test17::Benchmark();
    random_test18::Benchmark();
    random_test19::Benchmark();
    random_test20::Benchmark();
    random_test21::Benchmark();
    random_test22::Benchmark();
    random_test23::Benchmark();
    random_test24::Benchmark();
    random_test25::Benchmark();
    random_test26::Benchmark();
    random_test27::Benchmark();
    random_test28::Benchmark();
    random_test29::Benchmark();
    random_test30::Benchmark();
    random_test31::Benchmark();
    random_test32::Benchmark();
    random_test33::Benchmark();
    random_test34::Benchmark();
    random_test35::Benchmark();
    random_test36::Benchmark();
    random_test37::Benchmark();
    random_test38::Benchmark();
    random_test39::Benchmark();
    random_test40::Benchmark();
    random_test41::Benchmark();
    random_test42::Benchmark();
    random_test43::Benchmark();
    random_test44::Benchmark();
    random_test45::Benchmark();
    random_test46::Benchmark();
    random_test47::Benchmark();
    random_test48::Benchmark();
    random_test49::Benchmark();
    random_test50::Benchmark();
}

