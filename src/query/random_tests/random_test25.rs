use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test25" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Unranked,
        statOrdering: [
            Dexterity,
            Charisma,
            Strength,
            Constitution,
            Intelligence,
            Wisdom,
        ],
        isReversed: true,
        availableRaces: vec![
            Changling,
            DragonBorn,
            Dwarf_MarkOfWarding,
            Elf_Wood,
            Firbolg,
            Genasi_Earth,
            Gnome_Rock,
            Goblin,
            HalfElf,
            HalfElf_MarkOfDetection,
            Halfling_MarkOfHospitality,
            HalfOrc_MarkOfFinding,
            Human,
            Kalashtar,
            Kenku,
            Kobold,
            Lizardfolk,
            Orc_Eberron,
            Shifter_WildHunt,
            Tiefling_Feral,
            Warforged,
        ],
        requiredFeats: vec![],
        constraints: vec![
            BinCmp_StatValue_Constant(
                BinaryCompare {
                    lhsValue: GetStatValue(
                        Wisdom,
                    ),
                    binCmp: GE,
                    rhsValue: GetConstant(
                        7,
                    ),
                },
            ),
            UnCmp_StatValue(
                UnaryCompare {
                    value: GetStatValue(
                        Dexterity,
                    ),
                    unCmp: IsMin,
                },
            ),
        ],
        numberOfASIs: 1,
        doAllRacesHaveFreeFeat: false,
        canRearrangeRaceModifiers: true,
        maxNumberOfResults: 24,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 7867783,
            numUniqueCombinationsPassedConstraints: 1627356,
            numStatBlocksEjected: 411,
            numUniqueCombinationsEjected: 584,
            numUniqueCombinationsBelowLowerBound: 1626915,
            numStatBlocksShiftedDuringInsertion: 6017,
            numDuplicateRaceStatBlocksRemoved: 3,
            numUniqueStatBlockAfterRacePass: 121,
            numUniqueStatBlockAfterFeatPass: 121,
            numFinalStatBlockCombinations: 24,
        };

    let expectedStatBlocks = 
        vec![
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 6, 15, 15, 19, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 15, 15, 15, 8],
                        myRaceModifiers:     [0, -2, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 6, 15, 16, 18, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 15, 15, 15, 8],
                        myRaceModifiers:     [0, -2, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 1, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 6, 15, 17, 17, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 15, 15, 15, 8],
                        myRaceModifiers:     [0, -2, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 6, 15, 18, 16, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 15, 15, 15, 8],
                        myRaceModifiers:     [0, -2, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 1, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 6, 15, 19, 15, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 15, 15, 15, 8],
                        myRaceModifiers:     [0, -2, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 6, 16, 15, 18, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 15, 15, 15, 8],
                        myRaceModifiers:     [0, -2, 0, 0, 2, 0],
                        myASIs:              [0, 0, 1, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 6, 16, 16, 17, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 15, 15, 15, 8],
                        myRaceModifiers:     [0, -2, 0, 0, 2, 0],
                        myASIs:              [0, 0, 1, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 6, 16, 17, 16, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 15, 15, 15, 8],
                        myRaceModifiers:     [0, -2, 0, 2, 0, 0],
                        myASIs:              [0, 0, 1, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 6, 16, 18, 15, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 15, 15, 15, 8],
                        myRaceModifiers:     [0, -2, 0, 2, 0, 0],
                        myASIs:              [0, 0, 1, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 6, 17, 15, 17, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 15, 15, 15, 8],
                        myRaceModifiers:     [0, -2, 0, 0, 2, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 6, 17, 16, 16, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 15, 15, 15, 8],
                        myRaceModifiers:     [0, -2, 2, 0, 0, 0],
                        myASIs:              [0, 0, 0, 1, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 6, 17, 17, 15, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 15, 15, 15, 8],
                        myRaceModifiers:     [0, -2, 0, 2, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 6, 18, 15, 16, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 15, 15, 15, 8],
                        myRaceModifiers:     [0, -2, 2, 0, 0, 0],
                        myASIs:              [0, 0, 1, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 6, 18, 16, 15, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 15, 15, 15, 8],
                        myRaceModifiers:     [0, -2, 2, 0, 0, 0],
                        myASIs:              [0, 0, 1, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 6, 19, 15, 15, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 15, 15, 15, 8],
                        myRaceModifiers:     [0, -2, 2, 0, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 6, 15, 15, 18, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 15, 15, 15, 8],
                        myRaceModifiers:     [0, -2, 0, 0, 2, 0],
                        myASIs:              [1, 0, 0, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 6, 15, 16, 17, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 15, 15, 15, 8],
                        myRaceModifiers:     [0, -2, 0, 0, 2, 0],
                        myASIs:              [1, 0, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 6, 15, 17, 16, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 15, 15, 15, 8],
                        myRaceModifiers:     [0, -2, 0, 2, 0, 0],
                        myASIs:              [1, 0, 0, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 6, 15, 18, 15, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 15, 15, 15, 8],
                        myRaceModifiers:     [0, -2, 0, 2, 0, 0],
                        myASIs:              [1, 0, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 6, 16, 15, 17, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 15, 15, 15, 8],
                        myRaceModifiers:     [0, -2, 0, 0, 2, 0],
                        myASIs:              [1, 0, 1, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 6, 16, 17, 15, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 15, 15, 15, 8],
                        myRaceModifiers:     [0, -2, 0, 2, 0, 0],
                        myASIs:              [1, 0, 1, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 6, 17, 15, 16, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 15, 15, 15, 8],
                        myRaceModifiers:     [0, -2, 2, 0, 0, 0],
                        myASIs:              [1, 0, 0, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 6, 17, 16, 15, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 15, 15, 15, 8],
                        myRaceModifiers:     [0, -2, 2, 0, 0, 0],
                        myASIs:              [1, 0, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 6, 18, 15, 15, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 15, 15, 15, 8],
                        myRaceModifiers:     [0, -2, 2, 0, 0, 0],
                        myASIs:              [1, 0, 1, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
        ];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

