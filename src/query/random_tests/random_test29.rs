use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test29" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Flat,
        statOrdering: [
            Strength,
            Dexterity,
            Constitution,
            Intelligence,
            Wisdom,
            Charisma,
        ],
        isReversed: true,
        availableRaces: vec![
            Aarakocra,
            Aasimar_Fallen,
            Aasimar_Protector,
            Aasimar_Scourge,
            Bugbear,
            CustomLineage,
            Changling,
            DragonBorn,
            Dwarf_Grey,
            Dwarf_Hill,
            Dwarf_MarkOfWarding,
            Dwarf_Mountain,
            Elf_Drow,
            Elf_High,
            Elf_MarkOfShadow,
            Elf_Wood,
            Firbolg,
            Genasi_Earth,
            Genasi_Fire,
            Genasi_Water,
            Gnome_Deep,
            Gnome_Forest,
            Gnome_MarkOfScribing,
            Gnome_Rock,
            Goblin,
            HalfElf,
            HalfElf_MarkOfDetection,
            HalfElf_MarkOfStorm,
            Halfling_Ghostwise,
            Halfling_Lightfoot,
            Halfling_MarkOfHealing,
            Halfling_MarkOfHospitality,
            Halfling_Stout,
            HalfOrc,
            HalfOrc_MarkOfFinding,
            Hobgoblin,
            Human,
            Human_MarkOfFinding,
            Human_MarkOfHandling,
            Human_MarkOfMaking,
            Human_MarkOfPassage,
            Human_MarkOfSentinel,
            Human_Variant,
            Kalashtar,
            Kenku,
            Kobold,
            Lizardfolk,
            Orc,
            Orc_Eberron,
            Shifter_BeastHide,
            Shifter_LongTooth,
            Shifter_SwiftStride,
            Shifter_WildHunt,
            Tabaxi,
            Tiefling,
            Tiefling_Feral,
            Tortle,
            Triton,
            Warforged,
            YuanTiPureblood,
        ],
        requiredFeats: vec![],
        constraints: vec![],
        numberOfASIs: 1,
        doAllRacesHaveFreeFeat: false,
        canRearrangeRaceModifiers: true,
        maxNumberOfResults: 23,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 19701969,
            numUniqueCombinationsPassedConstraints: 19701969,
            numStatBlocksEjected: 246,
            numUniqueCombinationsEjected: 3521,
            numUniqueCombinationsBelowLowerBound: 19701691,
            numStatBlocksShiftedDuringInsertion: 2994,
            numDuplicateRaceStatBlocksRemoved: 0,
            numUniqueStatBlockAfterRacePass: 297,
            numUniqueStatBlockAfterFeatPass: 303,
            numFinalStatBlockCombinations: 23,
        };

    let expectedStatBlocks = 
        vec![
            RankedStatBlock {
                rank: 3,
                statBlock: [6, 9, 9, 15, 15, 17],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 8, 15, 15, 15],
                        myRaceModifiers:     [-2, 0, 0, 0, 0, 2],
                        myASIs:              [0, 1, 1, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [6, 9, 9, 15, 17, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 8, 15, 15, 15],
                        myRaceModifiers:     [-2, 0, 0, 0, 2, 0],
                        myASIs:              [0, 1, 1, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [6, 9, 9, 17, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 8, 15, 15, 15],
                        myRaceModifiers:     [-2, 0, 0, 2, 0, 0],
                        myASIs:              [0, 1, 1, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [6, 9, 11, 15, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 8, 15, 15, 15],
                        myRaceModifiers:     [-2, 0, 2, 0, 0, 0],
                        myASIs:              [0, 1, 1, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [6, 9, 15, 9, 15, 17],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 15, 8, 15, 15],
                        myRaceModifiers:     [-2, 0, 0, 0, 0, 2],
                        myASIs:              [0, 1, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [6, 9, 15, 9, 17, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 15, 8, 15, 15],
                        myRaceModifiers:     [-2, 0, 0, 0, 2, 0],
                        myASIs:              [0, 1, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [6, 9, 15, 11, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 15, 8, 15, 15],
                        myRaceModifiers:     [-2, 0, 0, 2, 0, 0],
                        myASIs:              [0, 1, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [6, 9, 15, 15, 9, 17],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 15, 15, 8, 15],
                        myRaceModifiers:     [-2, 0, 0, 0, 0, 2],
                        myASIs:              [0, 1, 0, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [6, 9, 15, 15, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 15, 15, 8, 15],
                        myRaceModifiers:     [-2, 0, 0, 0, 2, 0],
                        myASIs:              [0, 1, 0, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [6, 9, 15, 15, 15, 11],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 15, 15, 15, 8],
                        myRaceModifiers:     [-2, 0, 0, 0, 0, 2],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [6, 9, 15, 15, 17, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 15, 15, 15, 8],
                        myRaceModifiers:     [-2, 0, 0, 0, 2, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [6, 9, 15, 17, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 15, 15, 8, 15],
                        myRaceModifiers:     [-2, 0, 0, 2, 0, 0],
                        myASIs:              [0, 1, 0, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [6, 9, 15, 17, 15, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 15, 15, 15, 8],
                        myRaceModifiers:     [-2, 0, 0, 2, 0, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [6, 9, 17, 9, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 15, 8, 15, 15],
                        myRaceModifiers:     [-2, 0, 2, 0, 0, 0],
                        myASIs:              [0, 1, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [6, 9, 17, 15, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 15, 15, 8, 15],
                        myRaceModifiers:     [-2, 0, 2, 0, 0, 0],
                        myASIs:              [0, 1, 0, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [6, 9, 17, 15, 15, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 15, 15, 15, 8],
                        myRaceModifiers:     [-2, 0, 2, 0, 0, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [6, 11, 9, 15, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 8, 15, 15, 15],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 1, 1, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [6, 11, 15, 9, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 15, 8, 15, 15],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 1, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [6, 11, 15, 15, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 15, 15, 8, 15],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 1, 0, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [6, 11, 15, 15, 15, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 15, 15, 15, 8],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [6, 15, 9, 9, 15, 17],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 8, 8, 15, 15],
                        myRaceModifiers:     [-2, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 1, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [6, 15, 9, 9, 17, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 8, 8, 15, 15],
                        myRaceModifiers:     [-2, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 1, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [6, 15, 9, 11, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 8, 8, 15, 15],
                        myRaceModifiers:     [-2, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 1, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
        ];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

