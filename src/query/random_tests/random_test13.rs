use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test13" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Unranked,
        statOrdering: [
            Intelligence,
            Constitution,
            Dexterity,
            Wisdom,
            Strength,
            Charisma,
        ],
        isReversed: true,
        availableRaces: vec![
            Aarakocra,
            Aasimar_Fallen,
            Aasimar_Protector,
            Aasimar_Scourge,
            Bugbear,
            Changling,
            CustomLineage,
            DragonBorn,
            Dwarf_Grey,
            Dwarf_Hill,
            Dwarf_MarkOfWarding,
            Dwarf_Mountain,
            Elf_Drow,
            Elf_High,
            Elf_MarkOfShadow,
            Elf_Wood,
            Firbolg,
            Genasi_Air,
            Genasi_Earth,
            Genasi_Fire,
            Genasi_Water,
            Gnome_Deep,
            Gnome_Forest,
            Gnome_MarkOfScribing,
            Gnome_Rock,
            Goblin,
            Goliath,
            HalfElf,
            HalfElf_MarkOfDetection,
            HalfElf_MarkOfStorm,
            Halfling_Ghostwise,
            Halfling_Lightfoot,
            Halfling_MarkOfHealing,
            Halfling_MarkOfHospitality,
            Halfling_Stout,
            HalfOrc,
            HalfOrc_MarkOfFinding,
            Hobgoblin,
            Human,
            Human_MarkOfFinding,
            Human_MarkOfHandling,
            Human_MarkOfMaking,
            Human_MarkOfPassage,
            Human_MarkOfSentinel,
            Human_Variant,
            Kalashtar,
            Kenku,
            Kobold,
            Lizardfolk,
            Orc,
            Orc_Eberron,
            Shifter_BeastHide,
            Shifter_LongTooth,
            Shifter_SwiftStride,
            Shifter_WildHunt,
            Tabaxi,
            Tiefling,
            Tiefling_Feral,
            Tortle,
            Triton,
            Warforged,
            YuanTiPureblood,
        ],
        requiredFeats: vec![
            Actor,
            Athlete,
            Mobile,
        ],
        constraints: vec![
            UnCmp_StatValue(
                UnaryCompare {
                    value: GetStatValue(
                        Dexterity,
                    ),
                    unCmp: IsMin,
                },
            ),
            BinCmp_StatModifier_StatModifier(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Constitution,
                    ),
                    binCmp: EQ,
                    rhsValue: GetStatModifier(
                        Constitution,
                    ),
                },
            ),
            BinCmp_StatModifier_StatModifier(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Strength,
                    ),
                    binCmp: LE,
                    rhsValue: GetStatModifier(
                        Wisdom,
                    ),
                },
            ),
        ],
        numberOfASIs: 6,
        doAllRacesHaveFreeFeat: false,
        canRearrangeRaceModifiers: false,
        maxNumberOfResults: 10,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 50190644,
            numUniqueCombinationsPassedConstraints: 4435050,
            numStatBlocksEjected: 580,
            numUniqueCombinationsEjected: 1143,
            numUniqueCombinationsBelowLowerBound: 4434412,
            numStatBlocksShiftedDuringInsertion: 3040,
            numDuplicateRaceStatBlocksRemoved: 30,
            numUniqueStatBlockAfterRacePass: 65,
            numUniqueStatBlockAfterFeatPass: 116,
            numFinalStatBlockCombinations: 64,
        };

    let expectedStatBlocks = 
        vec![
            RankedStatBlock {
                rank: 0,
                statBlock: [18, 8, 8, 8, 18, 20],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Aasimar_Fallen,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [1, 0, 0, 0, 3, 2],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Aasimar_Protector,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [2, 0, 0, 0, 2, 2],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [2, 0, 0, 0, 2, 2],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: DragonBorn,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 1],
                        myASIs:              [0, 0, 0, 0, 3, 3],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Firbolg,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 2, 0],
                        myASIs:              [1, 0, 0, 0, 1, 4],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [2, 0, 0, 0, 1, 3],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfHandling,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [2, 0, 0, 0, 1, 3],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Kalashtar,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [2, 0, 0, 0, 1, 3],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Tortle,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 1, 0],
                        myASIs:              [0, 0, 0, 0, 2, 4],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [19, 8, 8, 8, 18, 19],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Aasimar_Fallen,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [2, 0, 0, 0, 3, 1],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Aasimar_Protector,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [3, 0, 0, 0, 2, 1],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [3, 0, 0, 0, 2, 1],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: DragonBorn,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 1],
                        myASIs:              [1, 0, 0, 0, 3, 2],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Firbolg,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 2, 0],
                        myASIs:              [2, 0, 0, 0, 1, 3],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [3, 0, 0, 0, 1, 2],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfHandling,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [3, 0, 0, 0, 1, 2],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Kalashtar,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [3, 0, 0, 0, 1, 2],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Tortle,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 1, 0],
                        myASIs:              [1, 0, 0, 0, 2, 3],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [19, 8, 8, 8, 18, 20],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 0, 2],
                        myASIs:              [3, 0, 0, 0, 3, 2],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 1, 2],
                        myASIs:              [2, 0, 0, 0, 2, 2],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [3, 0, 0, 0, 2, 3],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [17, 8, 8, 8, 19, 20],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Aasimar_Fallen,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 4, 2],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Aasimar_Protector,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [1, 0, 0, 0, 3, 2],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [1, 0, 0, 0, 3, 2],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Firbolg,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 4],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [1, 0, 0, 0, 2, 3],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfHandling,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [1, 0, 0, 0, 2, 3],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Kalashtar,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [1, 0, 0, 0, 2, 3],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [18, 8, 8, 8, 19, 19],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Aasimar_Fallen,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [1, 0, 0, 0, 4, 1],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Aasimar_Protector,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [2, 0, 0, 0, 3, 1],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [2, 0, 0, 0, 3, 1],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: DragonBorn,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 1],
                        myASIs:              [0, 0, 0, 0, 4, 2],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Firbolg,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 2, 0],
                        myASIs:              [1, 0, 0, 0, 2, 3],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [2, 0, 0, 0, 2, 2],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfHandling,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [2, 0, 0, 0, 2, 2],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Kalashtar,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [2, 0, 0, 0, 2, 2],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Tortle,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 1, 0],
                        myASIs:              [0, 0, 0, 0, 3, 3],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [18, 8, 8, 8, 19, 20],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 0, 2],
                        myASIs:              [2, 0, 0, 0, 4, 2],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 1, 2],
                        myASIs:              [1, 0, 0, 0, 3, 2],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [2, 0, 0, 0, 3, 3],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [19, 8, 8, 8, 19, 18],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Aasimar_Fallen,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [2, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Aasimar_Protector,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [3, 0, 0, 0, 3, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [3, 0, 0, 0, 3, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: DragonBorn,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 1],
                        myASIs:              [1, 0, 0, 0, 4, 1],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Firbolg,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 2, 0],
                        myASIs:              [2, 0, 0, 0, 2, 2],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [3, 0, 0, 0, 2, 1],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfHandling,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [3, 0, 0, 0, 2, 1],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Kalashtar,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [3, 0, 0, 0, 2, 1],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Tortle,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 1, 0],
                        myASIs:              [1, 0, 0, 0, 3, 2],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [19, 8, 8, 8, 19, 19],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 0, 2],
                        myASIs:              [3, 0, 0, 0, 4, 1],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 1, 2],
                        myASIs:              [2, 0, 0, 0, 3, 1],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [3, 0, 0, 0, 3, 2],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 8, 8, 8, 20, 20],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Aasimar_Protector,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 4, 2],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 4, 2],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 0, 0, 3, 3],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfHandling,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 0, 0, 3, 3],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Kalashtar,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 0, 0, 3, 3],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [17, 8, 8, 8, 20, 19],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Aasimar_Fallen,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 5, 1],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Aasimar_Protector,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [1, 0, 0, 0, 4, 1],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [1, 0, 0, 0, 4, 1],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Firbolg,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 3, 3],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [1, 0, 0, 0, 3, 2],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfHandling,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [1, 0, 0, 0, 3, 2],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Kalashtar,
                        myFeats: vec![
                            Actor,
                            Athlete,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [1, 0, 0, 0, 3, 2],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
        ];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

