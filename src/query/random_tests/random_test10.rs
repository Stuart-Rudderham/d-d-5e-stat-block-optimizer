use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test10" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Unranked,
        statOrdering: [
            Wisdom,
            Strength,
            Constitution,
            Intelligence,
            Dexterity,
            Charisma,
        ],
        isReversed: true,
        availableRaces: vec![
            Aarakocra,
            Aasimar_Fallen,
            Aasimar_Protector,
            Aasimar_Scourge,
            Bugbear,
            Changling,
            CustomLineage,
            DragonBorn,
            Dwarf_Grey,
            Dwarf_Hill,
            Dwarf_MarkOfWarding,
            Dwarf_Mountain,
            Elf_Drow,
            Elf_High,
            Elf_MarkOfShadow,
            Elf_Wood,
            Firbolg,
            Genasi_Air,
            Genasi_Earth,
            Genasi_Fire,
            Genasi_Water,
            Gnome_Deep,
            Gnome_Forest,
            Gnome_MarkOfScribing,
            Gnome_Rock,
            Goblin,
            Goliath,
            HalfElf,
            HalfElf_MarkOfDetection,
            HalfElf_MarkOfStorm,
            Halfling_Ghostwise,
            Halfling_Lightfoot,
            Halfling_MarkOfHealing,
            Halfling_MarkOfHospitality,
            Halfling_Stout,
            HalfOrc,
            HalfOrc_MarkOfFinding,
            Hobgoblin,
            Human,
            Human_MarkOfFinding,
            Human_MarkOfHandling,
            Human_MarkOfMaking,
            Human_MarkOfPassage,
            Human_MarkOfSentinel,
            Human_Variant,
            Kalashtar,
            Kenku,
            Kobold,
            Lizardfolk,
            Orc,
            Orc_Eberron,
            Shifter_BeastHide,
            Shifter_LongTooth,
            Shifter_SwiftStride,
            Shifter_WildHunt,
            Tabaxi,
            Tiefling,
            Tiefling_Feral,
            Tortle,
            Triton,
            Warforged,
            YuanTiPureblood,
        ],
        requiredFeats: vec![
            Athlete,
        ],
        constraints: vec![
            BinCmp_StatModifier_Constant(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Strength,
                    ),
                    binCmp: NE,
                    rhsValue: GetConstant(
                        3,
                    ),
                },
            ),
            BinCmp_StatModifier_Constant(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Wisdom,
                    ),
                    binCmp: GT,
                    rhsValue: GetConstant(
                        0,
                    ),
                },
            ),
            BinCmp_StatModifier_Constant(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Constitution,
                    ),
                    binCmp: GT,
                    rhsValue: GetConstant(
                        1,
                    ),
                },
            ),
        ],
        numberOfASIs: 0,
        doAllRacesHaveFreeFeat: false,
        canRearrangeRaceModifiers: false,
        maxNumberOfResults: 27,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 442152,
            numUniqueCombinationsPassedConstraints: 86635,
            numStatBlocksEjected: 113,
            numUniqueCombinationsEjected: 140,
            numUniqueCombinationsBelowLowerBound: 86413,
            numStatBlocksShiftedDuringInsertion: 1685,
            numDuplicateRaceStatBlocksRemoved: 41,
            numUniqueStatBlockAfterRacePass: 65,
            numUniqueStatBlockAfterFeatPass: 36,
            numFinalStatBlockCombinations: 41,
        };

    let expectedStatBlocks = 
        vec![
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 15, 14, 8, 12, 17],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 14, 14, 8, 12, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 16, 14, 8, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 15, 14, 8, 10, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 16, 14, 8, 12, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 15, 14, 8, 12, 14],
                        myRaceModifiers:     [0, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 14, 14, 8, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 17, 14, 8, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 14, 14, 8, 12, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 15, 13, 8, 12, 15],
                        myRaceModifiers:     [0, 1, 1, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 18, 14, 8, 12, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 15, 14, 8, 12, 14],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 15, 14, 9, 12, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 14, 14, 8, 12, 15],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 16, 14, 9, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 15, 12, 9, 12, 15],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 14, 14, 8, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 1, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 17, 14, 9, 12, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 15, 14, 8, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 1, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 14, 14, 10, 12, 17],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 13, 14, 10, 12, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 15, 14, 10, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 14, 14, 8, 12, 15],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 14, 14, 9, 11, 15],
                        myRaceModifiers:     [0, 0, 0, 1, 1, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 15, 14, 10, 12, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 14, 14, 10, 12, 14],
                        myRaceModifiers:     [0, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 13, 14, 10, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 16, 14, 10, 12, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 15, 14, 8, 12, 14],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 15, 14, 9, 11, 14],
                        myRaceModifiers:     [0, 0, 0, 1, 1, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 16, 14, 10, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 13, 14, 10, 12, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 14, 13, 10, 12, 15],
                        myRaceModifiers:     [0, 1, 1, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 17, 14, 10, 12, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 14, 14, 10, 12, 14],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 15, 13, 10, 12, 14],
                        myRaceModifiers:     [0, 1, 1, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 18, 14, 10, 12, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 15, 14, 10, 12, 13],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 13, 14, 11, 12, 17],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 12, 14, 11, 12, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 14, 14, 11, 12, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 12, 14, 11, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 15, 14, 11, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 12, 14, 11, 12, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 13, 14, 10, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 1, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 16, 14, 11, 12, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 15, 12, 11, 12, 14],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 14, 14, 10, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 1, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 17, 14, 11, 12, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 15, 14, 10, 12, 13],
                        myRaceModifiers:     [0, 1, 0, 1, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 18, 14, 11, 12, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 15, 14, 11, 12, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 12, 14, 12, 12, 17],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 11, 14, 12, 12, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 13, 14, 12, 12, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 11, 14, 12, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 14, 14, 12, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 11, 14, 12, 12, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 12, 14, 11, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 1, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 14, 14, 12, 12, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 13, 14, 12, 12, 14],
                        myRaceModifiers:     [0, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 13, 13, 12, 12, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 15, 14, 12, 12, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 14, 14, 10, 12, 14],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 14, 14, 11, 11, 14],
                        myRaceModifiers:     [0, 0, 0, 1, 1, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 15, 14, 12, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 14, 14, 12, 12, 13],
                        myRaceModifiers:     [0, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 13, 13, 12, 12, 15],
                        myRaceModifiers:     [0, 1, 1, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
        ];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

