use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test41" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Weighted,
        statOrdering: [
            Wisdom,
            Charisma,
            Strength,
            Dexterity,
            Constitution,
            Intelligence,
        ],
        isReversed: false,
        availableRaces: vec![
            Aasimar_Fallen,
            Elf_MarkOfShadow,
            Firbolg,
            Gnome_MarkOfScribing,
            Goblin,
            Halfling_MarkOfHealing,
            HalfOrc_MarkOfFinding,
            Human_MarkOfMaking,
            Lizardfolk,
            Orc,
            YuanTiPureblood,
        ],
        requiredFeats: vec![
            FadeAway,
            GreatWeaponMaster,
        ],
        constraints: vec![
            UnCmp_StatModifier(
                UnaryCompare {
                    value: GetStatModifier(
                        Charisma,
                    ),
                    unCmp: IsMin,
                },
            ),
            UnCmp_StatValue(
                UnaryCompare {
                    value: GetStatValue(
                        Wisdom,
                    ),
                    unCmp: IsMax,
                },
            ),
            BinCmp_StatModifier_StatModifier(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Charisma,
                    ),
                    binCmp: LT,
                    rhsValue: GetStatModifier(
                        Constitution,
                    ),
                },
            ),
            UnCmp_StatValue(
                UnaryCompare {
                    value: GetStatValue(
                        Intelligence,
                    ),
                    unCmp: IsEven,
                },
            ),
            BinCmp_StatValue_Constant(
                BinaryCompare {
                    lhsValue: GetStatValue(
                        Dexterity,
                    ),
                    binCmp: GT,
                    rhsValue: GetConstant(
                        16,
                    ),
                },
            ),
        ],
        numberOfASIs: 1,
        doAllRacesHaveFreeFeat: true,
        canRearrangeRaceModifiers: true,
        maxNumberOfResults: 13,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 675510,
            numUniqueCombinationsPassedConstraints: 26,
            numStatBlocksEjected: 9,
            numUniqueCombinationsEjected: 9,
            numUniqueCombinationsBelowLowerBound: 4,
            numStatBlocksShiftedDuringInsertion: 75,
            numDuplicateRaceStatBlocksRemoved: 0,
            numUniqueStatBlockAfterRacePass: 150,
            numUniqueStatBlockAfterFeatPass: 55,
            numFinalStatBlockCombinations: 13,
        };

    let expectedStatBlocks = 
        vec![
            RankedStatBlock {
                rank: 21012,
                statBlock: [14, 17, 10, 8, 17, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_MarkOfScribing,
                        myFeats: vec![
                            FadeAway,
                            GreatWeaponMaster,
                        ],
                        myBasePointBuyArray: [14, 15, 10, 8, 15, 8],
                        myRaceModifiers:     [0, 1, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 21012,
                statBlock: [10, 17, 14, 8, 17, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_MarkOfScribing,
                        myFeats: vec![
                            FadeAway,
                            GreatWeaponMaster,
                        ],
                        myBasePointBuyArray: [10, 15, 14, 8, 15, 8],
                        myRaceModifiers:     [0, 1, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 21012,
                statBlock: [8, 17, 14, 10, 17, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_MarkOfScribing,
                        myFeats: vec![
                            FadeAway,
                            GreatWeaponMaster,
                        ],
                        myBasePointBuyArray: [8, 15, 14, 10, 15, 8],
                        myRaceModifiers:     [0, 1, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 21012,
                statBlock: [8, 17, 10, 14, 17, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_MarkOfScribing,
                        myFeats: vec![
                            FadeAway,
                            GreatWeaponMaster,
                        ],
                        myBasePointBuyArray: [8, 15, 10, 14, 15, 8],
                        myRaceModifiers:     [0, 1, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 21003,
                statBlock: [9, 17, 14, 8, 17, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_MarkOfScribing,
                        myFeats: vec![
                            FadeAway,
                            GreatWeaponMaster,
                        ],
                        myBasePointBuyArray: [9, 15, 14, 8, 15, 9],
                        myRaceModifiers:     [0, 1, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 21003,
                statBlock: [8, 17, 15, 8, 17, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_MarkOfScribing,
                        myFeats: vec![
                            FadeAway,
                            GreatWeaponMaster,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 8, 15, 8],
                        myRaceModifiers:     [0, 1, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 20202,
                statBlock: [12, 17, 12, 8, 17, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_MarkOfScribing,
                        myFeats: vec![
                            FadeAway,
                            GreatWeaponMaster,
                        ],
                        myBasePointBuyArray: [12, 15, 12, 8, 15, 9],
                        myRaceModifiers:     [0, 1, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 20202,
                statBlock: [8, 17, 12, 12, 17, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_MarkOfScribing,
                        myFeats: vec![
                            FadeAway,
                            GreatWeaponMaster,
                        ],
                        myBasePointBuyArray: [8, 15, 12, 12, 15, 9],
                        myRaceModifiers:     [0, 1, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 20202,
                statBlock: [13, 17, 12, 8, 17, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_MarkOfScribing,
                        myFeats: vec![
                            FadeAway,
                            GreatWeaponMaster,
                        ],
                        myBasePointBuyArray: [13, 15, 12, 8, 15, 8],
                        myRaceModifiers:     [0, 1, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 20202,
                statBlock: [12, 17, 13, 8, 17, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_MarkOfScribing,
                        myFeats: vec![
                            FadeAway,
                            GreatWeaponMaster,
                        ],
                        myBasePointBuyArray: [12, 15, 13, 8, 15, 8],
                        myRaceModifiers:     [0, 1, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 20202,
                statBlock: [9, 17, 12, 12, 17, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_MarkOfScribing,
                        myFeats: vec![
                            FadeAway,
                            GreatWeaponMaster,
                        ],
                        myBasePointBuyArray: [9, 15, 12, 12, 15, 8],
                        myRaceModifiers:     [0, 1, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 20202,
                statBlock: [8, 17, 13, 12, 17, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_MarkOfScribing,
                        myFeats: vec![
                            FadeAway,
                            GreatWeaponMaster,
                        ],
                        myBasePointBuyArray: [8, 15, 13, 12, 15, 8],
                        myRaceModifiers:     [0, 1, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 20121,
                statBlock: [12, 17, 10, 10, 17, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_MarkOfScribing,
                        myFeats: vec![
                            FadeAway,
                            GreatWeaponMaster,
                        ],
                        myBasePointBuyArray: [12, 15, 10, 10, 15, 9],
                        myRaceModifiers:     [0, 1, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
        ];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

