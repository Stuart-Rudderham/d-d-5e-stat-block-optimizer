use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test26" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Flat,
        statOrdering: [
            Intelligence,
            Constitution,
            Strength,
            Wisdom,
            Charisma,
            Dexterity,
        ],
        isReversed: true,
        availableRaces: vec![
            Elf_High,
            Elf_Wood,
            Genasi_Earth,
            Human_MarkOfFinding,
            Shifter_SwiftStride,
        ],
        requiredFeats: vec![],
        constraints: vec![
            UnCmp_StatValue(
                UnaryCompare {
                    value: GetStatValue(
                        Wisdom,
                    ),
                    unCmp: IsMin,
                },
            ),
            BinCmp_StatValue_Constant(
                BinaryCompare {
                    lhsValue: GetStatValue(
                        Wisdom,
                    ),
                    binCmp: GE,
                    rhsValue: GetConstant(
                        9,
                    ),
                },
            ),
            BinCmp_StatModifier_Constant(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Dexterity,
                    ),
                    binCmp: GT,
                    rhsValue: GetConstant(
                        -1,
                    ),
                },
            ),
            BinCmp_StatValue_StatValue(
                BinaryCompare {
                    lhsValue: GetStatValue(
                        Wisdom,
                    ),
                    binCmp: GE,
                    rhsValue: GetStatValue(
                        Constitution,
                    ),
                },
            ),
        ],
        numberOfASIs: 0,
        doAllRacesHaveFreeFeat: false,
        canRearrangeRaceModifiers: false,
        maxNumberOfResults: 17,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 61410,
            numUniqueCombinationsPassedConstraints: 1078,
            numStatBlocksEjected: 43,
            numUniqueCombinationsEjected: 43,
            numUniqueCombinationsBelowLowerBound: 1003,
            numStatBlocksShiftedDuringInsertion: 369,
            numDuplicateRaceStatBlocksRemoved: 0,
            numUniqueStatBlockAfterRacePass: 5,
            numUniqueStatBlockAfterFeatPass: 5,
            numFinalStatBlockCombinations: 32,
        };

    let expectedStatBlocks = 
        vec![
            RankedStatBlock {
                rank: 4,
                statBlock: [14, 17, 9, 9, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Elf_High,
                        myFeats: vec![],
                        myBasePointBuyArray: [14, 15, 9, 8, 9, 15],
                        myRaceModifiers:     [0, 2, 0, 1, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Elf_Wood,
                        myFeats: vec![],
                        myBasePointBuyArray: [14, 15, 9, 9, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 1, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [15, 17, 9, 9, 9, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Elf_High,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 15, 9, 8, 9, 14],
                        myRaceModifiers:     [0, 2, 0, 1, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Elf_Wood,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 15, 9, 9, 8, 14],
                        myRaceModifiers:     [0, 2, 0, 0, 1, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [15, 16, 9, 9, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Elf_High,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 14, 9, 8, 9, 15],
                        myRaceModifiers:     [0, 2, 0, 1, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Elf_Wood,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 14, 9, 9, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 1, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [13, 17, 9, 11, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Elf_High,
                        myFeats: vec![],
                        myBasePointBuyArray: [13, 15, 9, 10, 9, 15],
                        myRaceModifiers:     [0, 2, 0, 1, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Elf_Wood,
                        myFeats: vec![],
                        myBasePointBuyArray: [13, 15, 9, 11, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 1, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [15, 17, 9, 11, 9, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Elf_High,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 15, 9, 10, 9, 13],
                        myRaceModifiers:     [0, 2, 0, 1, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Elf_Wood,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 15, 9, 11, 8, 13],
                        myRaceModifiers:     [0, 2, 0, 0, 1, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Shifter_SwiftStride,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 15, 9, 11, 9, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [15, 15, 9, 11, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Elf_High,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 13, 9, 10, 9, 15],
                        myRaceModifiers:     [0, 2, 0, 1, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Elf_Wood,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 13, 9, 11, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 1, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [11, 15, 11, 11, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Genasi_Earth,
                        myFeats: vec![],
                        myBasePointBuyArray: [10, 15, 9, 11, 11, 15],
                        myRaceModifiers:     [1, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfFinding,
                        myFeats: vec![],
                        myBasePointBuyArray: [11, 15, 10, 11, 9, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [15, 15, 11, 11, 11, 11],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human_MarkOfFinding,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 15, 10, 11, 9, 11],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [15, 11, 11, 11, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Elf_High,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 9, 11, 10, 11, 15],
                        myRaceModifiers:     [0, 2, 0, 1, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Elf_Wood,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 9, 11, 11, 10, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 1, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfFinding,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 11, 10, 11, 9, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [11, 17, 9, 13, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Elf_High,
                        myFeats: vec![],
                        myBasePointBuyArray: [11, 15, 9, 12, 9, 15],
                        myRaceModifiers:     [0, 2, 0, 1, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Elf_Wood,
                        myFeats: vec![],
                        myBasePointBuyArray: [11, 15, 9, 13, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 1, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [15, 17, 9, 13, 9, 11],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Elf_High,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 15, 9, 12, 9, 11],
                        myRaceModifiers:     [0, 2, 0, 1, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Elf_Wood,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 15, 9, 13, 8, 11],
                        myRaceModifiers:     [0, 2, 0, 0, 1, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Shifter_SwiftStride,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 15, 9, 13, 9, 10],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [15, 13, 9, 13, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Elf_High,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 11, 9, 12, 9, 15],
                        myRaceModifiers:     [0, 2, 0, 1, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Elf_Wood,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 11, 9, 13, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 1, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [9, 17, 9, 14, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Elf_Wood,
                        myFeats: vec![],
                        myBasePointBuyArray: [9, 15, 9, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 1, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [15, 17, 9, 14, 9, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Elf_Wood,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 15, 9, 14, 8, 9],
                        myRaceModifiers:     [0, 2, 0, 0, 1, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Shifter_SwiftStride,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 15, 9, 14, 9, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [15, 11, 9, 14, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Elf_Wood,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 9, 9, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 1, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [9, 17, 9, 15, 9, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Elf_Wood,
                        myFeats: vec![],
                        myBasePointBuyArray: [9, 15, 9, 15, 8, 14],
                        myRaceModifiers:     [0, 2, 0, 0, 1, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [9, 16, 9, 15, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Elf_Wood,
                        myFeats: vec![],
                        myBasePointBuyArray: [9, 14, 9, 15, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 1, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
        ];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

