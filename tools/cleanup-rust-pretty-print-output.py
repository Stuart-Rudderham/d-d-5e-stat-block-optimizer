#
# The printed output that the Rust program outputs needs to be
# cleaned up for two reasons:
#
#    1) It doesn't actually compile. For example, enum entries
#       aren't fully qualified, so we fix that.
#
#    2) It takes up way too much space. Arrays are printed with
#       each entry on a single line, so stat blocks take up 6x
#       the room they need to. So we re-format it to put some
#       things on the same line.
#

import re
import sys
import fileinput
import os
import glob

globPattern = sys.argv[1]

for filename in glob.glob(globPattern):

    # Read all the data into a single string.
    with open(filename, 'r') as f:
        data = "".join( f.read() )

    # Some entries needs to be a vector, not fixed size array.
    data = re.sub(r'combinations: \['   , r'combinations: vec!['     , data)
    data = re.sub(r'availableRaces: \[' , r'availableRaces: vec!['   , data)
    data = re.sub(r'constraints: \['    , r'constraints: vec!['      , data)
    data = re.sub(r'requiredFeats: \['  , r'requiredFeats: vec!['    , data)
    data = re.sub(r'myFeats: \['        , r'myFeats: vec!['          , data)

    # Vertically align for easier reading/diffing.
    data = re.sub(r'myBasePointBuyArray: \[', r'myBasePointBuyArray: [', data)
    data = re.sub(r'myRaceModifiers: \['    , r'myRaceModifiers:     [', data)
    data = re.sub(r'myASIs: \['             , r'myASIs:              [', data)
    data = re.sub(r'myFeatModifiers: \['    , r'myFeatModifiers:     [', data)

    # Take up less vertical space.
    data = re.sub(r'\[\n\s*(-?\d+),\n\s*(-?\d+),\n\s*(-?\d+),\n\s*(-?\d+),\n\s*(-?\d+),\n\s*(-?\d+),\n\s*\]', r'[\1, \2, \3, \4, \5, \6]', data)

    # Disabling this regex for now since there is no way to have an arbitrary
    # number of capture groups for the feat list to put them on a single line.
    #
    # NOTE: If the data format changes and this regex stops matching everything
    #       it hits a bad case for runtime and the script will take forever to finish.
    #
    # data = re.sub(r'CharacterStatBlock \{\n\s*(.*)\n\s*(.*)\n\s*(.*)\n\s*(.*)\n\s*(.*)\n\s*\}', r'CharacterStatBlock { \1 \2 \3 \4 \5 }', data)

    # Write the formatted text back into the text file we read it from.
    with open(filename, 'w') as f:
        f.write(data)
