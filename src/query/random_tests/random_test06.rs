use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test06" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Flat,
        statOrdering: [
            Intelligence,
            Constitution,
            Charisma,
            Wisdom,
            Strength,
            Dexterity,
        ],
        isReversed: true,
        availableRaces: vec![],
        requiredFeats: vec![
            DragonHide,
            WeaponMaster,
        ],
        constraints: vec![
            BinCmp_StatValue_StatValue(
                BinaryCompare {
                    lhsValue: GetStatValue(
                        Strength,
                    ),
                    binCmp: LE,
                    rhsValue: GetStatValue(
                        Intelligence,
                    ),
                },
            ),
            BinCmp_StatModifier_Constant(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Charisma,
                    ),
                    binCmp: LT,
                    rhsValue: GetConstant(
                        -1,
                    ),
                },
            ),
            UnCmp_StatValue(
                UnaryCompare {
                    value: GetStatValue(
                        Constitution,
                    ),
                    unCmp: IsEven,
                },
            ),
        ],
        numberOfASIs: 3,
        doAllRacesHaveFreeFeat: false,
        canRearrangeRaceModifiers: true,
        maxNumberOfResults: 15,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 0,
            numUniqueCombinationsPassedConstraints: 0,
            numStatBlocksEjected: 0,
            numUniqueCombinationsEjected: 0,
            numUniqueCombinationsBelowLowerBound: 0,
            numStatBlocksShiftedDuringInsertion: 0,
            numDuplicateRaceStatBlocksRemoved: 0,
            numUniqueStatBlockAfterRacePass: 0,
            numUniqueStatBlockAfterFeatPass: 0,
            numFinalStatBlockCombinations: 0,
        };

    let expectedStatBlocks = 
        vec![];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

