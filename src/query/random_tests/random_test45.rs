use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test45" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Weighted,
        statOrdering: [
            Charisma,
            Constitution,
            Wisdom,
            Intelligence,
            Dexterity,
            Strength,
        ],
        isReversed: true,
        availableRaces: vec![
            Changling,
            Dwarf_Grey,
            Genasi_Earth,
            HalfElf,
            HalfElf_MarkOfStorm,
            Halfling_MarkOfHealing,
            Human_MarkOfMaking,
        ],
        requiredFeats: vec![],
        constraints: vec![
            UnCmp_StatValue(
                UnaryCompare {
                    value: GetStatValue(
                        Dexterity,
                    ),
                    unCmp: IsOdd,
                },
            ),
            BinCmp_StatModifier_StatModifier(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Strength,
                    ),
                    binCmp: GT,
                    rhsValue: GetStatModifier(
                        Dexterity,
                    ),
                },
            ),
            BinCmp_StatValue_StatValue(
                BinaryCompare {
                    lhsValue: GetStatValue(
                        Intelligence,
                    ),
                    binCmp: LE,
                    rhsValue: GetStatValue(
                        Constitution,
                    ),
                },
            ),
            BinCmp_StatValue_Constant(
                BinaryCompare {
                    lhsValue: GetStatValue(
                        Intelligence,
                    ),
                    binCmp: GT,
                    rhsValue: GetConstant(
                        11,
                    ),
                },
            ),
            UnCmp_StatModifier(
                UnaryCompare {
                    value: GetStatModifier(
                        Intelligence,
                    ),
                    unCmp: IsOdd,
                },
            ),
        ],
        numberOfASIs: 1,
        doAllRacesHaveFreeFeat: true,
        canRearrangeRaceModifiers: false,
        maxNumberOfResults: 13,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 6632346,
            numUniqueCombinationsPassedConstraints: 234013,
            numStatBlocksEjected: 97,
            numUniqueCombinationsEjected: 1529,
            numUniqueCombinationsBelowLowerBound: 233645,
            numStatBlocksShiftedDuringInsertion: 680,
            numDuplicateRaceStatBlocksRemoved: 924,
            numUniqueStatBlockAfterRacePass: 22,
            numUniqueStatBlockAfterFeatPass: 102,
            numFinalStatBlockCombinations: 374,
        };

    let expectedStatBlocks = 
        vec![
            RankedStatBlock {
                rank: 1410,
                statBlock: [15, 13, 13, 13, 13, 11],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 13, 13, 9],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 13, 13, 9],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 13, 10],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 11, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1410,
                statBlock: [15, 13, 13, 13, 11, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 13, 11, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 13, 11, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 11, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 11, 11, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1410,
                statBlock: [15, 11, 13, 13, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 13, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 13, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 8, 12, 13, 13, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 1, 1, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 11, 13, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1410,
                statBlock: [13, 11, 13, 13, 15, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [10, 11, 13, 13, 15, 11],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_Grey,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [10, 11, 11, 13, 15, 13],
                        myRaceModifiers:     [1, 0, 2, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Genasi_Earth,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [10, 11, 11, 13, 15, 13],
                        myRaceModifiers:     [1, 0, 2, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [11, 10, 13, 13, 15, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [11, 9, 13, 13, 15, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [10, 11, 13, 11, 15, 13],
                        myRaceModifiers:     [1, 0, 0, 2, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1410,
                statBlock: [13, 11, 15, 13, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [10, 11, 15, 13, 13, 11],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [11, 10, 15, 13, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [11, 9, 15, 13, 13, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [10, 11, 15, 11, 13, 13],
                        myRaceModifiers:     [1, 0, 0, 2, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1410,
                statBlock: [13, 11, 13, 13, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Dwarf_Grey,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [10, 11, 11, 13, 13, 15],
                        myRaceModifiers:     [1, 0, 2, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Genasi_Earth,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [10, 11, 11, 13, 13, 15],
                        myRaceModifiers:     [1, 0, 2, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [10, 11, 13, 11, 13, 15],
                        myRaceModifiers:     [1, 0, 0, 2, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1500,
                statBlock: [15, 13, 13, 13, 12, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 13, 12, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 13, 12, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 12, 11],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 11, 12, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1500,
                statBlock: [15, 13, 13, 12, 13, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 12, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 12, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 12, 13, 11],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 10, 13, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1500,
                statBlock: [15, 13, 13, 13, 13, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            AberrantDragonMark,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 13, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 13, 13, 9],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 13, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Crusher,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 13, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Durable,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 13, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 12, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Gunner,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            KeenMind,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 12, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 12, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            ModeratelyArmored,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 12, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Piercer,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 13, 13, 9],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Resilient_Con,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 13, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Resilient_Dex,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Resilient_Int,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 12, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Resilient_Wis,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 13, 12, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 12, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Slasher,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 13, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 12, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 12, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            WeaponMaster,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [14, 11, 13, 13, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [1, 1, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_Grey,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [14, 11, 11, 13, 13, 12],
                        myRaceModifiers:     [1, 0, 2, 0, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Genasi_Earth,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [14, 11, 11, 13, 13, 12],
                        myRaceModifiers:     [1, 0, 2, 0, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 13, 13, 10],
                        myRaceModifiers:     [0, 1, 1, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 13, 13, 9],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 13, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Crusher,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 13, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Durable,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 13, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            ElvenAccuracy,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 12, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Gunner,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            KeenMind,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 12, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 12, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            ModeratelyArmored,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 12, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Piercer,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 13, 13, 9],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Resilient_Con,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 13, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Resilient_Dex,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Resilient_Int,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 12, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Resilient_Wis,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 13, 12, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 12, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Slasher,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 13, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 12, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 12, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            WeaponMaster,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [14, 11, 13, 13, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [1, 1, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 13, 10],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 13, 13, 11],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 9, 12, 13, 13, 11],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Crusher,
                        ],
                        myBasePointBuyArray: [15, 9, 12, 13, 13, 11],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Durable,
                        ],
                        myBasePointBuyArray: [15, 9, 12, 13, 13, 11],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 12, 13, 11],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Gunner,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 13, 13, 11],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            KeenMind,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 12, 13, 11],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 13, 13, 11],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 12, 13, 11],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            ModeratelyArmored,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 13, 13, 11],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 12, 13, 11],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Piercer,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 13, 13, 11],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 13, 10],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Resilient_Con,
                        ],
                        myBasePointBuyArray: [15, 9, 12, 13, 13, 11],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Resilient_Dex,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 13, 13, 11],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Resilient_Int,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 12, 13, 11],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Resilient_Wis,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 12, 11],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            SecondChance,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 13, 13, 11],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 12, 13, 11],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 13, 13, 11],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Slasher,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 13, 13, 11],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            SquatNimbleness,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 13, 13, 11],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [15, 9, 12, 13, 13, 11],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 12, 13, 11],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 12, 13, 11],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            WeaponMaster,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 13, 13, 11],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [14, 10, 13, 13, 13, 11],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [1, 1, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 11, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 11, 13, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 11, 13, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Crusher,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 11, 13, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Durable,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 11, 13, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 10, 13, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Gunner,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 11, 13, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            KeenMind,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 10, 13, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 11, 13, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 10, 13, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            ModeratelyArmored,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 11, 13, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 10, 13, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Piercer,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 11, 13, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 11, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Resilient_Con,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 11, 13, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Resilient_Dex,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 11, 13, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Resilient_Int,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 10, 13, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Resilient_Wis,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 11, 12, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 10, 13, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 11, 13, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Slasher,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 11, 13, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 11, 13, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 10, 13, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 10, 13, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            WeaponMaster,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 11, 13, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [14, 11, 13, 11, 13, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [1, 1, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1500,
                statBlock: [15, 13, 12, 12, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 12, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 12, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 9, 12, 12, 13, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 10, 13, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1500,
                statBlock: [15, 13, 13, 12, 12, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 12, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 12, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 12, 12, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 10, 12, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1500,
                statBlock: [15, 13, 13, 13, 12, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            AberrantDragonMark,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 13, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 13, 12, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 13, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Crusher,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 13, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Durable,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 13, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 12, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Gunner,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            KeenMind,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 12, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 12, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            ModeratelyArmored,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 12, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Piercer,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 13, 12, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Resilient_Con,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 13, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Resilient_Dex,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Resilient_Int,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 12, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Resilient_Wis,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 13, 11, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 12, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Slasher,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 13, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 12, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 12, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            WeaponMaster,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [14, 11, 13, 13, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [1, 1, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_Grey,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [14, 11, 11, 13, 12, 13],
                        myRaceModifiers:     [1, 0, 2, 0, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Genasi_Earth,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [14, 11, 11, 13, 12, 13],
                        myRaceModifiers:     [1, 0, 2, 0, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 13, 12, 11],
                        myRaceModifiers:     [0, 1, 1, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 13, 12, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 13, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Crusher,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 13, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Durable,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 13, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            ElvenAccuracy,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 12, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Gunner,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            KeenMind,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 12, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 12, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            ModeratelyArmored,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 12, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Piercer,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 13, 12, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Resilient_Con,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 13, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Resilient_Dex,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Resilient_Int,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 12, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Resilient_Wis,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 13, 11, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 12, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Slasher,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 13, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 12, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 12, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            WeaponMaster,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [14, 11, 13, 13, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [1, 1, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 12, 11],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 13, 12, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 9, 12, 13, 12, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Crusher,
                        ],
                        myBasePointBuyArray: [15, 9, 12, 13, 12, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Durable,
                        ],
                        myBasePointBuyArray: [15, 9, 12, 13, 12, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 12, 12, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Gunner,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 13, 12, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            KeenMind,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 12, 12, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 13, 12, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 12, 12, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            ModeratelyArmored,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 13, 12, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 12, 12, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Piercer,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 13, 12, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 12, 11],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Resilient_Con,
                        ],
                        myBasePointBuyArray: [15, 9, 12, 13, 12, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Resilient_Dex,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 13, 12, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Resilient_Int,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 12, 12, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Resilient_Wis,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 13, 11, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            SecondChance,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 13, 12, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 12, 12, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 13, 12, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Slasher,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 13, 12, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            SquatNimbleness,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 13, 12, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [15, 9, 12, 13, 12, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 12, 12, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 12, 12, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            WeaponMaster,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 13, 12, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [14, 10, 13, 13, 12, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [1, 1, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 11, 12, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 11, 12, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 11, 12, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Crusher,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 11, 12, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Durable,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 11, 12, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 10, 12, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Gunner,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 11, 12, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            KeenMind,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 10, 12, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 11, 12, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 10, 12, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            ModeratelyArmored,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 11, 12, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 10, 12, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Piercer,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 11, 12, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 11, 12, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Resilient_Con,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 11, 12, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Resilient_Dex,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 11, 12, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Resilient_Int,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 10, 12, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Resilient_Wis,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 11, 11, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 10, 12, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 11, 12, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Slasher,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 11, 12, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 11, 12, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 10, 12, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 10, 12, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            WeaponMaster,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 11, 12, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [14, 11, 13, 11, 12, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [1, 1, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1500,
                statBlock: [15, 13, 13, 12, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            AberrantDragonMark,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 12, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 12, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 12, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 12, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Crusher,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 12, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Durable,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 12, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 11, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Gunner,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 12, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            KeenMind,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 11, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 12, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 11, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            ModeratelyArmored,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 12, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 11, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Piercer,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 12, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 12, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Resilient_Con,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 12, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Resilient_Dex,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 12, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Resilient_Int,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 11, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Resilient_Wis,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 12, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 11, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 12, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Slasher,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 12, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 12, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 11, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 11, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            WeaponMaster,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 12, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [14, 11, 13, 12, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [1, 1, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_Grey,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [14, 11, 11, 12, 13, 13],
                        myRaceModifiers:     [1, 0, 2, 0, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Genasi_Earth,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [14, 11, 11, 12, 13, 13],
                        myRaceModifiers:     [1, 0, 2, 0, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 12, 13, 11],
                        myRaceModifiers:     [0, 1, 1, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 12, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 12, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 12, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Crusher,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 12, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Durable,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 12, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            ElvenAccuracy,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 12, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 11, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Gunner,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 12, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            KeenMind,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 11, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 12, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 11, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            ModeratelyArmored,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 12, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 11, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Piercer,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 12, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 12, 13, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Resilient_Con,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 12, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Resilient_Dex,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 12, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Resilient_Int,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 11, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Resilient_Wis,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 12, 12, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 11, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 12, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Slasher,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 12, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 12, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 11, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 11, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            WeaponMaster,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 12, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [14, 11, 13, 12, 13, 11],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [1, 1, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 12, 13, 11],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 12, 13, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 9, 12, 12, 13, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Crusher,
                        ],
                        myBasePointBuyArray: [15, 9, 12, 12, 13, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Durable,
                        ],
                        myBasePointBuyArray: [15, 9, 12, 12, 13, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 11, 13, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Gunner,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 12, 13, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            KeenMind,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 11, 13, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 12, 13, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 11, 13, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            ModeratelyArmored,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 12, 13, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 11, 13, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Piercer,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 12, 13, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 12, 13, 11],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Resilient_Con,
                        ],
                        myBasePointBuyArray: [15, 9, 12, 12, 13, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Resilient_Dex,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 12, 13, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Resilient_Int,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 11, 13, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Resilient_Wis,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 12, 12, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            SecondChance,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 12, 13, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 11, 13, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 12, 13, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Slasher,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 12, 13, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            SquatNimbleness,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 12, 13, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [15, 9, 12, 12, 13, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 11, 13, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 11, 13, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            WeaponMaster,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 12, 13, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [14, 10, 13, 12, 13, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [1, 1, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 10, 13, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 10, 13, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 10, 13, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Crusher,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 10, 13, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Durable,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 10, 13, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 9, 13, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Gunner,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 10, 13, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            KeenMind,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 9, 13, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 10, 13, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 9, 13, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            ModeratelyArmored,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 10, 13, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 9, 13, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Piercer,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 10, 13, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 10, 13, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Resilient_Con,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 10, 13, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Resilient_Dex,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 10, 13, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Resilient_Int,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 9, 13, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Resilient_Wis,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 10, 12, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 9, 13, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 10, 13, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Slasher,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 10, 13, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 10, 13, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 9, 13, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 9, 13, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            WeaponMaster,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 10, 13, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfMaking,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [14, 11, 13, 10, 13, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [1, 1, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
        ];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

