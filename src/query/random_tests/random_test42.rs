use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test42" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Flat,
        statOrdering: [
            Constitution,
            Strength,
            Dexterity,
            Intelligence,
            Charisma,
            Wisdom,
        ],
        isReversed: true,
        availableRaces: vec![
            Aasimar_Fallen,
            Aasimar_Protector,
            Dwarf_Mountain,
            Elf_Drow,
            Elf_Wood,
            Genasi_Earth,
            Genasi_Water,
            Gnome_Forest,
            Gnome_MarkOfScribing,
            HalfElf_MarkOfDetection,
            HalfElf_MarkOfStorm,
            Human_MarkOfMaking,
            Human_MarkOfSentinel,
            Lizardfolk,
            Orc,
            Tabaxi,
            Triton,
            Warforged,
            YuanTiPureblood,
        ],
        requiredFeats: vec![
            DualWielder,
            Skulker,
        ],
        constraints: vec![],
        numberOfASIs: 2,
        doAllRacesHaveFreeFeat: false,
        canRearrangeRaceModifiers: false,
        maxNumberOfResults: 23,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 294768,
            numUniqueCombinationsPassedConstraints: 294768,
            numStatBlocksEjected: 195,
            numUniqueCombinationsEjected: 600,
            numUniqueCombinationsBelowLowerBound: 294550,
            numStatBlocksShiftedDuringInsertion: 2125,
            numDuplicateRaceStatBlocksRemoved: 0,
            numUniqueStatBlockAfterRacePass: 24,
            numUniqueStatBlockAfterFeatPass: 24,
            numFinalStatBlockCombinations: 23,
        };

    let expectedStatBlocks = 
        vec![
            RankedStatBlock {
                rank: 3,
                statBlock: [9, 15, 9, 15, 15, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Triton,
                        myFeats: vec![
                            DualWielder,
                            Skulker,
                        ],
                        myBasePointBuyArray: [8, 15, 8, 15, 15, 8],
                        myRaceModifiers:     [1, 0, 1, 0, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [10, 8, 9, 13, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            DualWielder,
                            Skulker,
                        ],
                        myBasePointBuyArray: [8, 8, 8, 15, 15, 15],
                        myRaceModifiers:     [2, 0, 1, -2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [10, 15, 9, 6, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            DualWielder,
                            Skulker,
                        ],
                        myBasePointBuyArray: [8, 15, 8, 8, 15, 15],
                        myRaceModifiers:     [2, 0, 1, -2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [10, 15, 9, 13, 15, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            DualWielder,
                            Skulker,
                        ],
                        myBasePointBuyArray: [8, 15, 8, 15, 15, 8],
                        myRaceModifiers:     [2, 0, 1, -2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [10, 15, 9, 13, 8, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            DualWielder,
                            Skulker,
                        ],
                        myBasePointBuyArray: [8, 15, 8, 15, 8, 15],
                        myRaceModifiers:     [2, 0, 1, -2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [11, 9, 9, 12, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            DualWielder,
                            Skulker,
                        ],
                        myBasePointBuyArray: [9, 9, 8, 14, 15, 15],
                        myRaceModifiers:     [2, 0, 1, -2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [11, 9, 9, 13, 15, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            DualWielder,
                            Skulker,
                        ],
                        myBasePointBuyArray: [9, 9, 8, 15, 15, 14],
                        myRaceModifiers:     [2, 0, 1, -2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [11, 9, 9, 13, 14, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            DualWielder,
                            Skulker,
                        ],
                        myBasePointBuyArray: [9, 9, 8, 15, 14, 15],
                        myRaceModifiers:     [2, 0, 1, -2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [11, 11, 9, 11, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            DualWielder,
                            Skulker,
                        ],
                        myBasePointBuyArray: [9, 11, 8, 13, 15, 15],
                        myRaceModifiers:     [2, 0, 1, -2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [11, 11, 9, 13, 15, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            DualWielder,
                            Skulker,
                        ],
                        myBasePointBuyArray: [9, 11, 8, 15, 15, 13],
                        myRaceModifiers:     [2, 0, 1, -2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [11, 11, 9, 13, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            DualWielder,
                            Skulker,
                        ],
                        myBasePointBuyArray: [9, 11, 8, 15, 13, 15],
                        myRaceModifiers:     [2, 0, 1, -2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [11, 13, 9, 9, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            DualWielder,
                            Skulker,
                        ],
                        myBasePointBuyArray: [9, 13, 8, 11, 15, 15],
                        myRaceModifiers:     [2, 0, 1, -2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [11, 13, 9, 13, 15, 11],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            DualWielder,
                            Skulker,
                        ],
                        myBasePointBuyArray: [9, 13, 8, 15, 15, 11],
                        myRaceModifiers:     [2, 0, 1, -2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [11, 13, 9, 13, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            DualWielder,
                            Skulker,
                        ],
                        myBasePointBuyArray: [9, 13, 8, 15, 11, 15],
                        myRaceModifiers:     [2, 0, 1, -2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [11, 14, 9, 7, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            DualWielder,
                            Skulker,
                        ],
                        myBasePointBuyArray: [9, 14, 8, 9, 15, 15],
                        myRaceModifiers:     [2, 0, 1, -2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [11, 14, 9, 13, 15, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            DualWielder,
                            Skulker,
                        ],
                        myBasePointBuyArray: [9, 14, 8, 15, 15, 9],
                        myRaceModifiers:     [2, 0, 1, -2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [11, 14, 9, 13, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            DualWielder,
                            Skulker,
                        ],
                        myBasePointBuyArray: [9, 14, 8, 15, 9, 15],
                        myRaceModifiers:     [2, 0, 1, -2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [11, 15, 9, 7, 15, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            DualWielder,
                            Skulker,
                        ],
                        myBasePointBuyArray: [9, 15, 8, 9, 15, 14],
                        myRaceModifiers:     [2, 0, 1, -2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [11, 15, 9, 7, 14, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            DualWielder,
                            Skulker,
                        ],
                        myBasePointBuyArray: [9, 15, 8, 9, 14, 15],
                        myRaceModifiers:     [2, 0, 1, -2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [11, 15, 9, 9, 15, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            DualWielder,
                            Skulker,
                        ],
                        myBasePointBuyArray: [9, 15, 8, 11, 15, 13],
                        myRaceModifiers:     [2, 0, 1, -2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [11, 15, 9, 9, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            DualWielder,
                            Skulker,
                        ],
                        myBasePointBuyArray: [9, 15, 8, 11, 13, 15],
                        myRaceModifiers:     [2, 0, 1, -2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [11, 15, 9, 11, 15, 11],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            DualWielder,
                            Skulker,
                        ],
                        myBasePointBuyArray: [9, 15, 8, 13, 15, 11],
                        myRaceModifiers:     [2, 0, 1, -2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [11, 15, 9, 11, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            DualWielder,
                            Skulker,
                        ],
                        myBasePointBuyArray: [9, 15, 8, 13, 11, 15],
                        myRaceModifiers:     [2, 0, 1, -2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
        ];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

