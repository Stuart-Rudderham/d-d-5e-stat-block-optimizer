#![allow(non_upper_case_globals)]

use std::cmp;

use crate::dnd::statblock::*;

pub fn GenerateAsiArrays(aNumASIs : u8) -> Vec<StatBlock>
{
    // Each ASI gives 2 stat points to be allocated.
    let aNumStatPoints = (aNumASIs as i8) * 2;

    // Could reserve space in advance.
    // https://en.wikipedia.org/wiki/Multiset#Counting_multisets
    let mut result = vec![];

    // .rev() is because we want to start with high strength and work down.
    for str in (0..=aNumStatPoints).rev()
    {
        for dex in (0..=aNumStatPoints - str).rev()
        {
            for con in (0..=aNumStatPoints - str - dex).rev()
            {
                for int in (0..=aNumStatPoints - str - dex - con).rev()
                {
                    for wis in (0..=aNumStatPoints - str - dex - con - int).rev()
                    {
                        let cha = aNumStatPoints - str - dex - con - int - wis;
                        if cha >= 0
                        {
                            let statBlock : StatBlock = [ cmp::max(0, str) ,
                                                          cmp::max(0, dex) ,
                                                          cmp::max(0, con) ,
                                                          cmp::max(0, int) ,
                                                          cmp::max(0, wis) ,
                                                          cmp::max(0, cha) ];
                            result.push(statBlock);
                        }
                    }
                }
            }
        }
    }

    result
}

type NumASIs = u8;
type NumFeats = u8;

pub fn GetNumRemainingAfterRequiredFeatPurchace(numASIs : NumASIs, numRequiredFeats : NumFeats, numFreeFeats : NumFeats) -> Option<(NumASIs, NumFeats)>
{
    // We don't have enough to buy all our required feats, even if we spend all of our ASIs.
    if numFreeFeats + numASIs < numRequiredFeats
    {
        None
    }
    // Need to spend all of our free feats, and maybe some of our ASIs.
    else if numFreeFeats <= numRequiredFeats
    {
        // Each of these feats will cost an ASI.
        let numFeatsNeedToBeCovered = numRequiredFeats - numFreeFeats;

        // Condition enforced above.
        assert!(numASIs >= numFeatsNeedToBeCovered);

        let numRemainingASIs = numASIs - numFeatsNeedToBeCovered;

        Some((numRemainingASIs, 0))
    }
    // We have some extra free feats we can spend on whatever, ASIs are untouched.
    else
    {
        let numRemainingFreeFeats = numFreeFeats - numRequiredFeats;

        Some((numASIs, numRemainingFreeFeats))
    }
}

#[cfg(test)]
mod tests
{
    use super::*;

    #[test]
    fn TestGetNumRemainingAfterRequiredFeatPurchace()
    {
        let TestEq = |(numASIs, numRequiredFeats, numFreeFeats), expectedResult|
        {
            assert_eq!( GetNumRemainingAfterRequiredFeatPurchace(numASIs, numRequiredFeats, numFreeFeats), expectedResult );
        };

        // 0 free feats                    |||| 1 free feat                        |||| 2 free feats
        TestEq( (0, 0, 0), Some((0, 0)) ); /**/ TestEq( (0, 0, 1), Some((0, 1)) ); /**/ TestEq( (0, 0, 2), Some((0, 2)) );
        TestEq( (0, 1, 0), None         ); /**/ TestEq( (0, 1, 1), Some((0, 0)) ); /**/ TestEq( (0, 1, 2), Some((0, 1)) );
        TestEq( (0, 2, 0), None         ); /**/ TestEq( (0, 2, 1), None         ); /**/ TestEq( (0, 2, 2), Some((0, 0)) );
        TestEq( (1, 0, 0), Some((1, 0)) ); /**/ TestEq( (1, 0, 1), Some((1, 1)) ); /**/ TestEq( (1, 0, 2), Some((1, 2)) );
        TestEq( (1, 1, 0), Some((0, 0)) ); /**/ TestEq( (1, 1, 1), Some((1, 0)) ); /**/ TestEq( (1, 1, 2), Some((1, 1)) );
        TestEq( (1, 2, 0), None         ); /**/ TestEq( (1, 2, 1), Some((0, 0)) ); /**/ TestEq( (1, 2, 2), Some((1, 0)) );
        TestEq( (2, 0, 0), Some((2, 0)) ); /**/ TestEq( (2, 0, 1), Some((2, 1)) ); /**/ TestEq( (2, 0, 2), Some((2, 2)) );
        TestEq( (2, 1, 0), Some((1, 0)) ); /**/ TestEq( (2, 1, 1), Some((2, 0)) ); /**/ TestEq( (2, 1, 2), Some((2, 1)) );
        TestEq( (2, 2, 0), Some((0, 0)) ); /**/ TestEq( (2, 2, 1), Some((1, 0)) ); /**/ TestEq( (2, 2, 2), Some((2, 0)) );
    }

    fn SanityCheckAsiArray(someAsiArrays : &Vec<StatBlock>)
    {
        println!("{:?}", someAsiArrays);

        if someAsiArrays.is_empty()
        {
            return
        }

        let mut someAsiArraysCopy = someAsiArrays.clone();

        // Test all unique.
        {
            let oldLen = someAsiArraysCopy.len();

            someAsiArraysCopy.sort();
            someAsiArraysCopy.dedup();

            let newLen = someAsiArraysCopy.len();

            assert_eq!(newLen, oldLen);
        }

        // Test all sum to the same value.
        let sums = someAsiArraysCopy.iter()
                                    .map( |arr| arr.iter().sum() )
                                    .collect::<Vec<_>>();
        let firstSum = sums[0];

        assert!( sums.iter().all(|sum : &i8| *sum == firstSum) );
    }

    #[test]
    fn AsiArray0()
    {
        let asiArray = GenerateAsiArrays(0);

        SanityCheckAsiArray(&asiArray);

        assert_eq!
        (
            asiArray,
            vec!
            [
                [0, 0, 0, 0, 0, 0],
            ]
        );
    }

    #[test]
    fn AsiArray1()
    {
        let asiArray = GenerateAsiArrays(1);

        SanityCheckAsiArray(&asiArray);

        assert_eq!
        (
            asiArray,
            vec!
            [
                [2, 0, 0, 0, 0, 0],
                [1, 1, 0, 0, 0, 0],
                [1, 0, 1, 0, 0, 0],
                [1, 0, 0, 1, 0, 0],
                [1, 0, 0, 0, 1, 0],
                [1, 0, 0, 0, 0, 1],
                [0, 2, 0, 0, 0, 0],
                [0, 1, 1, 0, 0, 0],
                [0, 1, 0, 1, 0, 0],
                [0, 1, 0, 0, 1, 0],
                [0, 1, 0, 0, 0, 1],
                [0, 0, 2, 0, 0, 0],
                [0, 0, 1, 1, 0, 0],
                [0, 0, 1, 0, 1, 0],
                [0, 0, 1, 0, 0, 1],
                [0, 0, 0, 2, 0, 0],
                [0, 0, 0, 1, 1, 0],
                [0, 0, 0, 1, 0, 1],
                [0, 0, 0, 0, 2, 0],
                [0, 0, 0, 0, 1, 1],
                [0, 0, 0, 0, 0, 2],
            ]
        );
    }

    #[test]
    fn AsiArray2()
    {
        let asiArray = GenerateAsiArrays(2);

        SanityCheckAsiArray(&asiArray);

        assert_eq!
        (
            asiArray,
            vec!
            [
                [4, 0, 0, 0, 0, 0],
                [3, 1, 0, 0, 0, 0],
                [3, 0, 1, 0, 0, 0],
                [3, 0, 0, 1, 0, 0],
                [3, 0, 0, 0, 1, 0],
                [3, 0, 0, 0, 0, 1],
                [2, 2, 0, 0, 0, 0],
                [2, 1, 1, 0, 0, 0],
                [2, 1, 0, 1, 0, 0],
                [2, 1, 0, 0, 1, 0],
                [2, 1, 0, 0, 0, 1],
                [2, 0, 2, 0, 0, 0],
                [2, 0, 1, 1, 0, 0],
                [2, 0, 1, 0, 1, 0],
                [2, 0, 1, 0, 0, 1],
                [2, 0, 0, 2, 0, 0],
                [2, 0, 0, 1, 1, 0],
                [2, 0, 0, 1, 0, 1],
                [2, 0, 0, 0, 2, 0],
                [2, 0, 0, 0, 1, 1],
                [2, 0, 0, 0, 0, 2],
                [1, 3, 0, 0, 0, 0],
                [1, 2, 1, 0, 0, 0],
                [1, 2, 0, 1, 0, 0],
                [1, 2, 0, 0, 1, 0],
                [1, 2, 0, 0, 0, 1],
                [1, 1, 2, 0, 0, 0],
                [1, 1, 1, 1, 0, 0],
                [1, 1, 1, 0, 1, 0],
                [1, 1, 1, 0, 0, 1],
                [1, 1, 0, 2, 0, 0],
                [1, 1, 0, 1, 1, 0],
                [1, 1, 0, 1, 0, 1],
                [1, 1, 0, 0, 2, 0],
                [1, 1, 0, 0, 1, 1],
                [1, 1, 0, 0, 0, 2],
                [1, 0, 3, 0, 0, 0],
                [1, 0, 2, 1, 0, 0],
                [1, 0, 2, 0, 1, 0],
                [1, 0, 2, 0, 0, 1],
                [1, 0, 1, 2, 0, 0],
                [1, 0, 1, 1, 1, 0],
                [1, 0, 1, 1, 0, 1],
                [1, 0, 1, 0, 2, 0],
                [1, 0, 1, 0, 1, 1],
                [1, 0, 1, 0, 0, 2],
                [1, 0, 0, 3, 0, 0],
                [1, 0, 0, 2, 1, 0],
                [1, 0, 0, 2, 0, 1],
                [1, 0, 0, 1, 2, 0],
                [1, 0, 0, 1, 1, 1],
                [1, 0, 0, 1, 0, 2],
                [1, 0, 0, 0, 3, 0],
                [1, 0, 0, 0, 2, 1],
                [1, 0, 0, 0, 1, 2],
                [1, 0, 0, 0, 0, 3],
                [0, 4, 0, 0, 0, 0],
                [0, 3, 1, 0, 0, 0],
                [0, 3, 0, 1, 0, 0],
                [0, 3, 0, 0, 1, 0],
                [0, 3, 0, 0, 0, 1],
                [0, 2, 2, 0, 0, 0],
                [0, 2, 1, 1, 0, 0],
                [0, 2, 1, 0, 1, 0],
                [0, 2, 1, 0, 0, 1],
                [0, 2, 0, 2, 0, 0],
                [0, 2, 0, 1, 1, 0],
                [0, 2, 0, 1, 0, 1],
                [0, 2, 0, 0, 2, 0],
                [0, 2, 0, 0, 1, 1],
                [0, 2, 0, 0, 0, 2],
                [0, 1, 3, 0, 0, 0],
                [0, 1, 2, 1, 0, 0],
                [0, 1, 2, 0, 1, 0],
                [0, 1, 2, 0, 0, 1],
                [0, 1, 1, 2, 0, 0],
                [0, 1, 1, 1, 1, 0],
                [0, 1, 1, 1, 0, 1],
                [0, 1, 1, 0, 2, 0],
                [0, 1, 1, 0, 1, 1],
                [0, 1, 1, 0, 0, 2],
                [0, 1, 0, 3, 0, 0],
                [0, 1, 0, 2, 1, 0],
                [0, 1, 0, 2, 0, 1],
                [0, 1, 0, 1, 2, 0],
                [0, 1, 0, 1, 1, 1],
                [0, 1, 0, 1, 0, 2],
                [0, 1, 0, 0, 3, 0],
                [0, 1, 0, 0, 2, 1],
                [0, 1, 0, 0, 1, 2],
                [0, 1, 0, 0, 0, 3],
                [0, 0, 4, 0, 0, 0],
                [0, 0, 3, 1, 0, 0],
                [0, 0, 3, 0, 1, 0],
                [0, 0, 3, 0, 0, 1],
                [0, 0, 2, 2, 0, 0],
                [0, 0, 2, 1, 1, 0],
                [0, 0, 2, 1, 0, 1],
                [0, 0, 2, 0, 2, 0],
                [0, 0, 2, 0, 1, 1],
                [0, 0, 2, 0, 0, 2],
                [0, 0, 1, 3, 0, 0],
                [0, 0, 1, 2, 1, 0],
                [0, 0, 1, 2, 0, 1],
                [0, 0, 1, 1, 2, 0],
                [0, 0, 1, 1, 1, 1],
                [0, 0, 1, 1, 0, 2],
                [0, 0, 1, 0, 3, 0],
                [0, 0, 1, 0, 2, 1],
                [0, 0, 1, 0, 1, 2],
                [0, 0, 1, 0, 0, 3],
                [0, 0, 0, 4, 0, 0],
                [0, 0, 0, 3, 1, 0],
                [0, 0, 0, 3, 0, 1],
                [0, 0, 0, 2, 2, 0],
                [0, 0, 0, 2, 1, 1],
                [0, 0, 0, 2, 0, 2],
                [0, 0, 0, 1, 3, 0],
                [0, 0, 0, 1, 2, 1],
                [0, 0, 0, 1, 1, 2],
                [0, 0, 0, 1, 0, 3],
                [0, 0, 0, 0, 4, 0],
                [0, 0, 0, 0, 3, 1],
                [0, 0, 0, 0, 2, 2],
                [0, 0, 0, 0, 1, 3],
                [0, 0, 0, 0, 0, 4],
            ]
        );
    }
}
