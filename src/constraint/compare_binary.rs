#![allow(non_snake_case)]
#![allow(deprecated)]
#![allow(non_camel_case_types)]

use crate::dnd::statblock::*;
use crate::constraint::value::*;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum BinCmp
{
    EQ,
    NE,
    GT,
    GE,
    LT,
    LE,
}

#[derive(Debug, Eq, PartialEq, Copy, Clone)]
pub struct BinaryCompare<V1: GetConstraintValue, V2: GetConstraintValue>
{
    pub lhsValue    : V1     ,
    pub binCmp      : BinCmp ,
    pub rhsValue    : V2     ,
}

impl<V1: GetConstraintValue, V2: GetConstraintValue> BinaryCompare<V1, V2>
{
    pub fn IsStatBlockValid(&self, aStatBlock : StatBlock) -> bool
    {
        let lhs = self.lhsValue.GetValue(aStatBlock);
        let rhs = self.rhsValue.GetValue(aStatBlock);

        match self.binCmp
        {
            BinCmp::EQ => lhs == rhs,
            BinCmp::NE => lhs != rhs,
            BinCmp::GT => lhs >  rhs,
            BinCmp::GE => lhs >= rhs,
            BinCmp::LT => lhs <  rhs,
            BinCmp::LE => lhs <= rhs,
        }
    }

    pub fn New(aLhsValue : V1, aBinCmp : BinCmp, aRhsValue : V2) -> BinaryCompare<V1, V2>
    {
        BinaryCompare
        {
            lhsValue    : aLhsValue ,
            binCmp      : aBinCmp   ,
            rhsValue    : aRhsValue ,
        }
    }
}
