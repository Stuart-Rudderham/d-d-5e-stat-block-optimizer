extern crate rand;
extern crate time;
extern crate glob;

use crate::dnd::stat::*;
use crate::dnd::race::*;
use crate::dnd::feat::*;

use crate::constraint::compare_binary::*;
use crate::constraint::compare_unary::*;
use crate::constraint::value::*;

use crate::query::*;

use rand::Rng;

use std::fs::File;
use std::fs::OpenOptions;
use std::fs;
use std::io;
use std::io::prelude::*;
use std::path::PathBuf;

use self::glob::glob;

fn ChooseRandomStat<R : rand::Rng>(rng : &mut R) -> Stat
{
    let choices =
        [
            Stat::Strength    ,
            Stat::Dexterity   ,
            Stat::Constitution,
            Stat::Intelligence,
            Stat::Wisdom      ,
            Stat::Charisma    ,
        ];

    *rng.choose(&choices).unwrap()
}

fn ChooseRandomBinCmp<R : rand::Rng>(rng : &mut R) -> BinCmp
{
    let choices =
        [
            BinCmp::EQ,
            BinCmp::NE,
            BinCmp::GT,
            BinCmp::GE,
            BinCmp::LT,
            BinCmp::LE,
        ];

    *rng.choose(&choices).unwrap()
}

fn ChooseRandomUnCmp<R : rand::Rng>(rng : &mut R) -> UnCmp
{
    let choices =
        [
            UnCmp::IsEven,
            UnCmp::IsOdd ,
            UnCmp::IsMax ,
            UnCmp::IsMin ,
        ];

    *rng.choose(&choices).unwrap()
}

fn CreateRandomConstraint<R : rand::Rng>(rng : &mut R) -> Constraint
{
    // Some helper functions.
    fn ChooseRandomStatValue   <R : rand::Rng>(rng : &mut R) -> GetStatValue    { GetStatValue    ( ChooseRandomStat(rng) ) }
    fn ChooseRandomStatModifier<R : rand::Rng>(rng : &mut R) -> GetStatModifier { GetStatModifier ( ChooseRandomStat(rng) ) }

    fn ChooseRandomStatValueConstant   <R : rand::Rng>(rng : &mut R) -> GetConstant { GetConstant( rng.gen_range( 6, 19) ) }
    fn ChooseRandomStatModifierConstant<R : rand::Rng>(rng : &mut R) -> GetConstant { GetConstant( rng.gen_range(-2,  4) ) }

    let choices =
    [
        Constraint::BinCmp_StatValue_Constant       ( BinaryCompare::New(ChooseRandomStatValue   (rng), ChooseRandomBinCmp(rng), ChooseRandomStatValueConstant   (rng) ) ),
        Constraint::BinCmp_StatModifier_Constant    ( BinaryCompare::New(ChooseRandomStatModifier(rng), ChooseRandomBinCmp(rng), ChooseRandomStatModifierConstant(rng) ) ),
        Constraint::BinCmp_StatValue_StatValue      ( BinaryCompare::New(ChooseRandomStatValue   (rng), ChooseRandomBinCmp(rng), ChooseRandomStatValue           (rng) ) ),
        Constraint::BinCmp_StatModifier_StatModifier( BinaryCompare::New(ChooseRandomStatModifier(rng), ChooseRandomBinCmp(rng), ChooseRandomStatModifier        (rng) ) ),
        Constraint::UnCmp_StatValue                 ( UnaryCompare ::New(ChooseRandomStatValue   (rng), ChooseRandomUnCmp (rng)                                        ) ),
        Constraint::UnCmp_StatModifier              ( UnaryCompare ::New(ChooseRandomStatModifier(rng), ChooseRandomUnCmp (rng)                                        ) ),
    ];

    *rng.choose(&choices).unwrap()
}

fn CreateRandomConstraintList<R : rand::Rng>(length : usize, rng : &mut R) -> Vec<Constraint>
{
    let mut ret = vec![];

    ret.reserve(length);

    for _ in 0..length
    {
        ret.push( CreateRandomConstraint(rng) );
    }

    ret
}

fn CreateRandomRaceList<R : rand::Rng>(rng : &mut R) -> Vec<CharacterRace>
{
    let raceList = RaceList.to_vec();

    let length : usize = rng.gen_range(1, raceList.len());

    let mut result = rand::sample( rng, raceList.iter().cloned(), length );

    result.sort();

    result
}

fn CreateRandomFeatList<R : rand::Rng>(rng : &mut R) -> Vec<Feat>
{
    let featList = FeatList.to_vec();

    let length : usize =
        if rng.gen::<bool>()
        {
            rng.gen_range(1, 3)
        }
        else
        {
            0
        };

    let mut result = rand::sample( rng, featList.iter().cloned(), length );

    result.sort();

    result
}

fn ChooseRandomRankingStyle<R : rand::Rng>(rng : &mut R) -> RankingStyle
{
    let choices =
        [
            RankingStyle::Flat      ,
            RankingStyle::Weighted  ,
            //RankingStyle::Variance  , // I don't really care about this one.
            RankingStyle::Unranked  ,
        ];

    *rng.choose(&choices).unwrap()
}

fn ChooseRandomStatOrdering<R : rand::Rng>(rng : &mut R) -> StatOrdering
{
    let mut statOrdering = DefaultStatOrdering;

    rng.shuffle(&mut statOrdering);

    statOrdering
}

fn CreateRandomQueryParameters<R : rand::Rng>(rng : &mut R) -> QueryParameters
{
    let featList = CreateRandomFeatList(rng);

    let numFeats = featList.len() as u8;

    QueryParameters
    {
        rankingStyle              : ChooseRandomRankingStyle(rng)                        ,
        statOrdering              : ChooseRandomStatOrdering(rng)                        ,
        isReversed                : rng.gen::<bool>()                                    ,
        availableRaces            : CreateRandomRaceList(rng)                            ,
        requiredFeats             : featList                                             ,
        constraints               : CreateRandomConstraintList(rng.gen_range(0, 6), rng) ,
        numberOfASIs              : rng.gen_range(0, numFeats + 2)                       ,
        doAllRacesHaveFreeFeat    : rng.gen::<bool>()                                    ,
        canRearrangeRaceModifiers : rng.gen::<bool>()                                    ,
        maxNumberOfResults        : rng.gen_range(10, 30)                                ,
        optCustomPointBuyArray    : None                                                 , // Never set by default, can set manually if desired.
    }
}

fn GenerateQueryTest(dirRoot : &PathBuf, testFilenameRoot : &str, queryParameters : &QueryParameters, allowZeroResults : bool) -> Result<(), &'static str>
{
    let (outputStatBlocks, outputQueryStats, _) = GenerateAllValidCharacterStatBlocks( queryParameters );

    if !allowZeroResults && outputStatBlocks.is_empty()
    {
        // This is not a good query, don't write it out to a file.
        return Err("Empty query");
    }

    if outputQueryStats.numFinalStatBlockCombinations > 1000
    {
        // If we don't put a cap on the result size we can end up with test
        // files that are many MB in size, which messes up the Rust compiler.
        return Err("Too many combinations in query result");
    }

    let queryParametersStr = format!("    {:#?}", queryParameters ).replace("\n", "\n    ");

    let outputStatBlocksStr = format!("        vec!{:#?};", outputStatBlocks ).replace("\n", "\n        ");

    let outputQueryStatsStr = format!("        {:#?};", outputQueryStats ).replace("\n", "\n        ");

    let testFilename = format!("{}.rs", testFilenameRoot);

    let mut testFile = File::create( dirRoot.join(testFilename) ).unwrap();

    // Write out test file.
    writeln!( &mut testFile, "use crate::query::*;"                                                                                                                     ).unwrap();
    writeln!( &mut testFile, "use crate::query::random_tests_harness::*;"                                                                                               ).unwrap();
    writeln!( &mut testFile,                                                                                                                                            ).unwrap();
    writeln!( &mut testFile, "#[allow(unused_imports)] use crate::dnd::race::*;"                                                                                        ).unwrap();
    writeln!( &mut testFile, "#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;"                                                                         ).unwrap();
    writeln!( &mut testFile,                                                                                                                                            ).unwrap();
    writeln!( &mut testFile, "#[allow(unused_imports)] use crate::dnd::feat::*;"                                                                                        ).unwrap();
    writeln!( &mut testFile, "#[allow(unused_imports)] use crate::dnd::feat::Feat::*;"                                                                                  ).unwrap();
    writeln!( &mut testFile,                                                                                                                                            ).unwrap();
    writeln!( &mut testFile, "#[allow(unused_imports)] use crate::dnd::statblock::*;"                                                                                   ).unwrap();
    writeln!( &mut testFile, "#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;"                                                                     ).unwrap();
    writeln!( &mut testFile,                                                                                                                                            ).unwrap();
    writeln!( &mut testFile, "#[allow(unused_imports)] use crate::dnd::stat::*;"                                                                                        ).unwrap();
    writeln!( &mut testFile, "#[allow(unused_imports)] use crate::dnd::stat::Stat::*;"                                                                                  ).unwrap();
    writeln!( &mut testFile,                                                                                                                                            ).unwrap();
    writeln!( &mut testFile, "#[allow(unused_imports)] use crate::constraint::*;"                                                                                       ).unwrap();
    writeln!( &mut testFile, "#[allow(unused_imports)] use crate::constraint::Constraint::*;"                                                                           ).unwrap();
    writeln!( &mut testFile,                                                                                                                                            ).unwrap();
    writeln!( &mut testFile, "#[allow(unused_imports)] use crate::constraint::compare_binary::*;"                                                                       ).unwrap();
    writeln!( &mut testFile, "#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;"                                                               ).unwrap();
    writeln!( &mut testFile,                                                                                                                                            ).unwrap();
    writeln!( &mut testFile, "#[allow(unused_imports)] use crate::constraint::compare_unary::*;"                                                                        ).unwrap();
    writeln!( &mut testFile, "#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;"                                                                 ).unwrap();
    writeln!( &mut testFile,                                                                                                                                            ).unwrap();
    writeln!( &mut testFile, "#[allow(unused_imports)] use crate::constraint::value::*;"                                                                                ).unwrap();
    writeln!( &mut testFile,                                                                                                                                            ).unwrap();

    writeln!( &mut testFile, "#[test]"                                                                                                                                  ).unwrap();
    writeln!( &mut testFile, "fn OffByOne()"                                                                                                                            ).unwrap();
    writeln!( &mut testFile, "{{"                                                                                                                                       ).unwrap();
    writeln!( &mut testFile, "    OffByOneTestHarness( GetQueryParameters )"                                                                                            ).unwrap();
    writeln!( &mut testFile, "}}"                                                                                                                                       ).unwrap();
    writeln!( &mut testFile,                                                                                                                                            ).unwrap();

    writeln!( &mut testFile, "#[test]"                                                                                                                                  ).unwrap();
    writeln!( &mut testFile, "fn ExpectedOutput()"                                                                                                                      ).unwrap();
    writeln!( &mut testFile, "{{"                                                                                                                                       ).unwrap();
    writeln!( &mut testFile, "    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )"                                                             ).unwrap();
    writeln!( &mut testFile, "}}"                                                                                                                                       ).unwrap();
    writeln!( &mut testFile,                                                                                                                                            ).unwrap();

    writeln!( &mut testFile, "#[cfg(feature = \"benchmark\")]"                                                                                                          ).unwrap();
    writeln!( &mut testFile, "pub fn Benchmark()"                                                                                                                       ).unwrap();
    writeln!( &mut testFile, "{{"                                                                                                                                       ).unwrap();
    writeln!( &mut testFile, "    BenchmarkTestHarness( GetQueryParameters, \"{}\" )", testFilenameRoot                                                                 ).unwrap();
    writeln!( &mut testFile, "}}"                                                                                                                                       ).unwrap();
    writeln!( &mut testFile,                                                                                                                                            ).unwrap();

    writeln!( &mut testFile, "fn GetQueryParameters() -> QueryParameters"                                                                                               ).unwrap();
    writeln!( &mut testFile, "{{"                                                                                                                                       ).unwrap();
    writeln!( &mut testFile, "{}", queryParametersStr                                                                                                                   ).unwrap();
    writeln!( &mut testFile, "}}"                                                                                                                                       ).unwrap();
    writeln!( &mut testFile,                                                                                                                                            ).unwrap();

    writeln!( &mut testFile, "#[cfg(test)]"                                                                                                                             ).unwrap();
    writeln!( &mut testFile, "fn GetExpectedQueryResults() -> QueryResult"                                                                                              ).unwrap();
    writeln!( &mut testFile, "{{"                                                                                                                                       ).unwrap();
    writeln!( &mut testFile, "    let expectedQueryStats = "                                                                                                            ).unwrap();
    writeln!( &mut testFile, "{}", outputQueryStatsStr                                                                                                                  ).unwrap();
    writeln!( &mut testFile,                                                                                                                                            ).unwrap();
    writeln!( &mut testFile, "    let expectedStatBlocks = "                                                                                                            ).unwrap();
    writeln!( &mut testFile, "{}", outputStatBlocksStr                                                                                                                  ).unwrap();
    writeln!( &mut testFile,                                                                                                                                            ).unwrap();
    writeln!( &mut testFile, "    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(\"\"))"                                                                   ).unwrap();
    writeln!( &mut testFile, "}}"                                                                                                                                       ).unwrap();
    writeln!( &mut testFile,                                                                                                                                            ).unwrap();

    Ok(())
}

fn GenerateRandomQueryTest(dirRoot : &PathBuf, testFilenameRoot : &str, seed : u64, testNum : u32)
{
    println!();

    let seedArray: &[_] = &[ seed ];
    let mut rng : rand::Isaac64Rng = rand::SeedableRng::from_seed( seedArray );

    loop
    {
        print!("Generating {} ... ", testFilenameRoot);

        io::stdout().flush().unwrap();

        let mut queryParameters = CreateRandomQueryParameters(&mut rng);

        // By default a query must have at least one result.
        let mut allowZeroResults = false;

        // These are random tests, but it is still good to force specific branches.
        match testNum
        {
            // No results.
            1 =>
            {
                queryParameters.maxNumberOfResults = 0;

                allowZeroResults = true;
            },

            // One result.
            2 =>
            {
                queryParameters.maxNumberOfResults = 1;
            },

            // Two result.
            3 =>
            {
                queryParameters.maxNumberOfResults = 2;
            },

            // No constraints.
            4 =>
            {
                queryParameters.constraints.clear();
            },

            // All races.
            5 =>
            {
                queryParameters.availableRaces = RaceList.iter()
                                                         .cloned()
                                                         .collect();
            },

            // No races.
            6 =>
            {
                queryParameters.availableRaces.clear();

                allowZeroResults = true;
            },

            // A slow test. Many useless constraints, all the races, some feats and ASIs.
            7 =>
            {
                queryParameters.numberOfASIs = 3;

                queryParameters.availableRaces = RaceList.iter()
                                                         .cloned()
                                                         .collect();

                queryParameters.constraints =
                    vec!
                    [
                        // Stat values.
                        Constraint::BinCmp_StatValue_Constant           ( BinaryCompare::New( GetStatValue    (Stat::Strength    ), BinCmp::GE, GetConstant     (-100              ) ) ),
                        Constraint::BinCmp_StatValue_Constant           ( BinaryCompare::New( GetStatValue    (Stat::Dexterity   ), BinCmp::GE, GetConstant     (- 99              ) ) ),
                        Constraint::BinCmp_StatValue_Constant           ( BinaryCompare::New( GetStatValue    (Stat::Constitution), BinCmp::GE, GetConstant     (- 98              ) ) ),
                        Constraint::BinCmp_StatValue_Constant           ( BinaryCompare::New( GetStatValue    (Stat::Intelligence), BinCmp::GE, GetConstant     (- 97              ) ) ),
                        Constraint::BinCmp_StatValue_Constant           ( BinaryCompare::New( GetStatValue    (Stat::Wisdom      ), BinCmp::GE, GetConstant     (- 96              ) ) ),
                        Constraint::BinCmp_StatValue_Constant           ( BinaryCompare::New( GetStatValue    (Stat::Charisma    ), BinCmp::GE, GetConstant     (- 95              ) ) ),

                        Constraint::BinCmp_StatValue_Constant           ( BinaryCompare::New( GetStatValue    (Stat::Strength    ), BinCmp::LT, GetConstant     ( 100              ) ) ),
                        Constraint::BinCmp_StatValue_Constant           ( BinaryCompare::New( GetStatValue    (Stat::Dexterity   ), BinCmp::LT, GetConstant     (  99              ) ) ),
                        Constraint::BinCmp_StatValue_Constant           ( BinaryCompare::New( GetStatValue    (Stat::Constitution), BinCmp::LT, GetConstant     (  98              ) ) ),
                        Constraint::BinCmp_StatValue_Constant           ( BinaryCompare::New( GetStatValue    (Stat::Intelligence), BinCmp::LT, GetConstant     (  97              ) ) ),
                        Constraint::BinCmp_StatValue_Constant           ( BinaryCompare::New( GetStatValue    (Stat::Wisdom      ), BinCmp::LT, GetConstant     (  96              ) ) ),
                        Constraint::BinCmp_StatValue_Constant           ( BinaryCompare::New( GetStatValue    (Stat::Charisma    ), BinCmp::LT, GetConstant     (  96              ) ) ),

                        Constraint::BinCmp_StatValue_StatValue          ( BinaryCompare::New( GetStatValue    (Stat::Strength    ), BinCmp::EQ, GetStatValue    (Stat::Strength    ) ) ),
                        Constraint::BinCmp_StatValue_StatValue          ( BinaryCompare::New( GetStatValue    (Stat::Dexterity   ), BinCmp::EQ, GetStatValue    (Stat::Dexterity   ) ) ),
                        Constraint::BinCmp_StatValue_StatValue          ( BinaryCompare::New( GetStatValue    (Stat::Constitution), BinCmp::EQ, GetStatValue    (Stat::Constitution) ) ),
                        Constraint::BinCmp_StatValue_StatValue          ( BinaryCompare::New( GetStatValue    (Stat::Intelligence), BinCmp::EQ, GetStatValue    (Stat::Intelligence) ) ),
                        Constraint::BinCmp_StatValue_StatValue          ( BinaryCompare::New( GetStatValue    (Stat::Wisdom      ), BinCmp::EQ, GetStatValue    (Stat::Wisdom      ) ) ),
                        Constraint::BinCmp_StatValue_StatValue          ( BinaryCompare::New( GetStatValue    (Stat::Charisma    ), BinCmp::EQ, GetStatValue    (Stat::Charisma    ) ) ),

                        // Stat Modifiers.
                        Constraint::BinCmp_StatModifier_Constant        ( BinaryCompare::New( GetStatModifier (Stat::Strength    ), BinCmp::GE, GetConstant     (-100              ) ) ),
                        Constraint::BinCmp_StatModifier_Constant        ( BinaryCompare::New( GetStatModifier (Stat::Dexterity   ), BinCmp::GE, GetConstant     (- 99              ) ) ),
                        Constraint::BinCmp_StatModifier_Constant        ( BinaryCompare::New( GetStatModifier (Stat::Constitution), BinCmp::GE, GetConstant     (- 98              ) ) ),
                        Constraint::BinCmp_StatModifier_Constant        ( BinaryCompare::New( GetStatModifier (Stat::Intelligence), BinCmp::GE, GetConstant     (- 97              ) ) ),
                        Constraint::BinCmp_StatModifier_Constant        ( BinaryCompare::New( GetStatModifier (Stat::Wisdom      ), BinCmp::GE, GetConstant     (- 96              ) ) ),
                        Constraint::BinCmp_StatModifier_Constant        ( BinaryCompare::New( GetStatModifier (Stat::Charisma    ), BinCmp::GE, GetConstant     (- 95              ) ) ),

                        Constraint::BinCmp_StatModifier_Constant        ( BinaryCompare::New( GetStatModifier (Stat::Strength    ), BinCmp::LT, GetConstant     ( 100              ) ) ),
                        Constraint::BinCmp_StatModifier_Constant        ( BinaryCompare::New( GetStatModifier (Stat::Dexterity   ), BinCmp::LT, GetConstant     (  99              ) ) ),
                        Constraint::BinCmp_StatModifier_Constant        ( BinaryCompare::New( GetStatModifier (Stat::Constitution), BinCmp::LT, GetConstant     (  98              ) ) ),
                        Constraint::BinCmp_StatModifier_Constant        ( BinaryCompare::New( GetStatModifier (Stat::Intelligence), BinCmp::LT, GetConstant     (  97              ) ) ),
                        Constraint::BinCmp_StatModifier_Constant        ( BinaryCompare::New( GetStatModifier (Stat::Wisdom      ), BinCmp::LT, GetConstant     (  96              ) ) ),
                        Constraint::BinCmp_StatModifier_Constant        ( BinaryCompare::New( GetStatModifier (Stat::Charisma    ), BinCmp::LT, GetConstant     (  96              ) ) ),

                        Constraint::BinCmp_StatModifier_StatModifier    ( BinaryCompare::New( GetStatModifier (Stat::Strength    ), BinCmp::EQ, GetStatModifier (Stat::Strength    ) ) ),
                        Constraint::BinCmp_StatModifier_StatModifier    ( BinaryCompare::New( GetStatModifier (Stat::Dexterity   ), BinCmp::EQ, GetStatModifier (Stat::Dexterity   ) ) ),
                        Constraint::BinCmp_StatModifier_StatModifier    ( BinaryCompare::New( GetStatModifier (Stat::Constitution), BinCmp::EQ, GetStatModifier (Stat::Constitution) ) ),
                        Constraint::BinCmp_StatModifier_StatModifier    ( BinaryCompare::New( GetStatModifier (Stat::Intelligence), BinCmp::EQ, GetStatModifier (Stat::Intelligence) ) ),
                        Constraint::BinCmp_StatModifier_StatModifier    ( BinaryCompare::New( GetStatModifier (Stat::Wisdom      ), BinCmp::EQ, GetStatModifier (Stat::Wisdom      ) ) ),
                        Constraint::BinCmp_StatModifier_StatModifier    ( BinaryCompare::New( GetStatModifier (Stat::Charisma    ), BinCmp::EQ, GetStatModifier (Stat::Charisma    ) ) ),
                    ];

                queryParameters.requiredFeats = vec![ Feat::Observant, Feat::TavernBrawler ];
                queryParameters.doAllRacesHaveFreeFeat = false;
            },

            // Too many feats, with Variant Human.
            8 =>
            {
                queryParameters.availableRaces = RaceList.iter()
                                                         .cloned()
                                                         .collect();

                queryParameters.requiredFeats = vec![ Feat::Actor, Feat::Athlete ];
                queryParameters.doAllRacesHaveFreeFeat = false;

                queryParameters.numberOfASIs = 0;

                allowZeroResults = true;
            },

            // Too many feats, with no Variant Human.
            9 =>
            {
                queryParameters.availableRaces = RaceList.iter()
                                                         .cloned()
                                                         .collect();

                queryParameters.availableRaces.retain(|&aRace| aRace != CharacterRace::Human_Variant);

                queryParameters.requiredFeats = vec![ Feat::Athlete ];
                queryParameters.doAllRacesHaveFreeFeat = false;

                queryParameters.numberOfASIs = 0;

                allowZeroResults = true;
            },

            // Enough feats for Variant Human, with no ASIs.
            10 =>
            {
                queryParameters.availableRaces = RaceList.iter()
                                                         .cloned()
                                                         .collect();

                queryParameters.requiredFeats = vec![ Feat::Athlete ];
                queryParameters.doAllRacesHaveFreeFeat = false;

                queryParameters.numberOfASIs = 0;
            },

            // Enough feats for Variant Human, with ASIs
            11 =>
            {
                queryParameters.availableRaces = RaceList.iter()
                                                         .cloned()
                                                         .collect();

                queryParameters.requiredFeats = vec![ Feat::Actor, Feat::Athlete ];
                queryParameters.doAllRacesHaveFreeFeat = false;

                queryParameters.numberOfASIs = 1;
            },

            // Enough feats for everyone, Variant Human gets extra ASI
            12 =>
            {
                queryParameters.availableRaces = RaceList.iter()
                                                         .cloned()
                                                         .collect();

                queryParameters.requiredFeats = vec![ Feat::Athlete ];
                queryParameters.doAllRacesHaveFreeFeat = false;

                queryParameters.numberOfASIs = 1;
            },

            // Lots of feats and ASIs.
            13 =>
            {
                queryParameters.availableRaces = RaceList.iter()
                                                         .cloned()
                                                         .collect();

                queryParameters.requiredFeats = vec![ Feat::Actor, Feat::Athlete, Feat::Mobile ];
                queryParameters.doAllRacesHaveFreeFeat = false;

                queryParameters.numberOfASIs = 6;
            },

            // Lots of ASIs and no feats. Mostly affected by combining point buy and asi arrays.
            14 =>
            {
                queryParameters.numberOfASIs = 4;

                queryParameters.availableRaces = RaceList.iter()
                                                         .cloned()
                                                         .collect();

                queryParameters.constraints = vec![];
                queryParameters.requiredFeats = vec![];
                queryParameters.doAllRacesHaveFreeFeat = false;
            },

            // Enough feats for everyone, with no ASIs.
            15 =>
            {
                queryParameters.availableRaces = RaceList.iter()
                                                         .cloned()
                                                         .collect();

                queryParameters.requiredFeats = vec![ Feat::Athlete ];
                queryParameters.doAllRacesHaveFreeFeat = true;

                queryParameters.numberOfASIs = 0;
            },

            // Enough feats for everyone, with ASIs.
            16 =>
            {
                queryParameters.availableRaces = RaceList.iter()
                                                         .cloned()
                                                         .collect();

                queryParameters.requiredFeats = vec![ Feat::Actor, Feat::Athlete ];
                queryParameters.doAllRacesHaveFreeFeat = true;

                queryParameters.numberOfASIs = 1;
            },

            // Custom point buy array.
            17 =>
            {
                queryParameters.optCustomPointBuyArray = Some([10, 10, 20, 10, 10, 6]);
            }

            // Large query result.
            18 =>
            {
                queryParameters.maxNumberOfResults = 500;
            }

            // Make sure there is at least one test with the flag set to true.
            19 =>
            {
                queryParameters.canRearrangeRaceModifiers = true;
            }

            // Make sure there is at least one test with the flag set to true.
            20 =>
            {
                queryParameters.canRearrangeRaceModifiers = false;
            }

            // Otherwise keep the random.
            _ =>
            {
            },
        }

        match GenerateQueryTest(dirRoot, testFilenameRoot, &queryParameters, allowZeroResults)
        {
            Ok(_) =>
            {
                println!("Done!");
                return;
            },
            Err(errorMessage) =>
            {
                println!("Failed ({})", errorMessage);
                continue;
            },
        }
    }
}

pub fn GenerateRandomQueryTests(numTests : u32, dirRoot : &PathBuf, seed : u64)
{
    let seedArray: &[_] = &[ seed ];
    let mut rng : rand::Isaac64Rng = rand::SeedableRng::from_seed( seedArray );

    // Remove any old test files.
    for path in glob( &dirRoot.join("random_test*.rs").to_string_lossy() ).unwrap().filter_map(Result::ok)
    {
        println!("Deleting {}", path.display());
        fs::remove_file(&path).unwrap_or_else(|_| panic!("Unable to delete file {}", path.to_string_lossy()));
    }

    // Create or clear the test module file.
    let mut testModFile = OpenOptions::new()
                              .write(true)
                              .truncate(true)
                              .create(true)
                              .open( dirRoot.join("mod.rs") )
                              .unwrap();

    let currentDateAndTimeStr = time::strftime("%F %T", &time::now()).unwrap();

    writeln!( &mut testModFile, "//"                                                                                        ).unwrap();
    writeln!( &mut testModFile, "// THESE TESTS WERE AUTO-GENERATED ON {} USING SEED {:?}", currentDateAndTimeStr, seed     ).unwrap();
    writeln!( &mut testModFile, "//"                                                                                        ).unwrap();
    writeln!( &mut testModFile,                                                                                             ).unwrap();

    // Create each unit test file.
    for i in 1..=numTests
    {
        let testFileameRoot = format!("random_test{:02}", i);

        // Give each test its own seed to help keep things more deterministic. This way generating
        // each test is isolated and can't affect the result of generating the other tests.
        let testSeed = rng.gen::<u64>();

        GenerateRandomQueryTest(dirRoot, &testFileameRoot, testSeed, i);

        // Add test file to test module.
        writeln!( &mut testModFile, "#[cfg(any(test, feature = \"benchmark\"))]"    ).unwrap();
        writeln!( &mut testModFile, "mod {};", testFileameRoot                      ).unwrap();
        writeln!( &mut testModFile,                                                 ).unwrap();
    }

    // Generate the benchmarking function.
    {
        writeln!( &mut testModFile, "#[cfg(not(feature = \"benchmark\"))]"          ).unwrap();
        writeln!( &mut testModFile, "pub fn RunQueryTestBenchmarks()"               ).unwrap();
        writeln!( &mut testModFile, "{{"                                            ).unwrap();
        writeln!( &mut testModFile, "}}"                                            ).unwrap();
        writeln!( &mut testModFile,                                                 ).unwrap();

        writeln!( &mut testModFile, "#[cfg(feature = \"benchmark\")]"               ).unwrap();
        writeln!( &mut testModFile, "pub fn RunQueryTestBenchmarks()"               ).unwrap();
        writeln!( &mut testModFile, "{{"                                            ).unwrap();

        for i in 1..=numTests
        {
            writeln!( &mut testModFile, "    random_test{:02}::Benchmark();", i     ).unwrap();
        }

        writeln!( &mut testModFile, "}}"                                            ).unwrap();
        writeln!( &mut testModFile,                                                 ).unwrap();
    }
}
