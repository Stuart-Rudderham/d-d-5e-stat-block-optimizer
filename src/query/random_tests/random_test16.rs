use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test16" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Weighted,
        statOrdering: [
            Charisma,
            Strength,
            Constitution,
            Dexterity,
            Intelligence,
            Wisdom,
        ],
        isReversed: true,
        availableRaces: vec![
            Aarakocra,
            Aasimar_Fallen,
            Aasimar_Protector,
            Aasimar_Scourge,
            Bugbear,
            Changling,
            CustomLineage,
            DragonBorn,
            Dwarf_Grey,
            Dwarf_Hill,
            Dwarf_MarkOfWarding,
            Dwarf_Mountain,
            Elf_Drow,
            Elf_High,
            Elf_MarkOfShadow,
            Elf_Wood,
            Firbolg,
            Genasi_Air,
            Genasi_Earth,
            Genasi_Fire,
            Genasi_Water,
            Gnome_Deep,
            Gnome_Forest,
            Gnome_MarkOfScribing,
            Gnome_Rock,
            Goblin,
            Goliath,
            HalfElf,
            HalfElf_MarkOfDetection,
            HalfElf_MarkOfStorm,
            Halfling_Ghostwise,
            Halfling_Lightfoot,
            Halfling_MarkOfHealing,
            Halfling_MarkOfHospitality,
            Halfling_Stout,
            HalfOrc,
            HalfOrc_MarkOfFinding,
            Hobgoblin,
            Human,
            Human_MarkOfFinding,
            Human_MarkOfHandling,
            Human_MarkOfMaking,
            Human_MarkOfPassage,
            Human_MarkOfSentinel,
            Human_Variant,
            Kalashtar,
            Kenku,
            Kobold,
            Lizardfolk,
            Orc,
            Orc_Eberron,
            Shifter_BeastHide,
            Shifter_LongTooth,
            Shifter_SwiftStride,
            Shifter_WildHunt,
            Tabaxi,
            Tiefling,
            Tiefling_Feral,
            Tortle,
            Triton,
            Warforged,
            YuanTiPureblood,
        ],
        requiredFeats: vec![
            Actor,
            Athlete,
        ],
        constraints: vec![
            UnCmp_StatValue(
                UnaryCompare {
                    value: GetStatValue(
                        Charisma,
                    ),
                    unCmp: IsOdd,
                },
            ),
            BinCmp_StatValue_Constant(
                BinaryCompare {
                    lhsValue: GetStatValue(
                        Wisdom,
                    ),
                    binCmp: GE,
                    rhsValue: GetConstant(
                        11,
                    ),
                },
            ),
            BinCmp_StatModifier_Constant(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Dexterity,
                    ),
                    binCmp: GE,
                    rhsValue: GetConstant(
                        -2,
                    ),
                },
            ),
            BinCmp_StatValue_StatValue(
                BinaryCompare {
                    lhsValue: GetStatValue(
                        Strength,
                    ),
                    binCmp: EQ,
                    rhsValue: GetStatValue(
                        Constitution,
                    ),
                },
            ),
        ],
        numberOfASIs: 1,
        doAllRacesHaveFreeFeat: true,
        canRearrangeRaceModifiers: true,
        maxNumberOfResults: 23,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 8395854,
            numUniqueCombinationsPassedConstraints: 291918,
            numStatBlocksEjected: 239,
            numUniqueCombinationsEjected: 1768,
            numUniqueCombinationsBelowLowerBound: 289812,
            numStatBlocksShiftedDuringInsertion: 2846,
            numDuplicateRaceStatBlocksRemoved: 1342,
            numUniqueStatBlockAfterRacePass: 297,
            numUniqueStatBlockAfterFeatPass: 529,
            numFinalStatBlockCombinations: 31,
        };

    let expectedStatBlocks = 
        vec![
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 12, 13, 13, 13, 11],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [10, 12, 13, 13, 15, 10],
                        myRaceModifiers:     [2, 0, 0, 0, -2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 13, 13, 12, 13, 11],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [10, 13, 13, 12, 15, 10],
                        myRaceModifiers:     [2, 0, 0, 0, -2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 13, 13, 13, 12, 11],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [10, 13, 13, 15, 12, 10],
                        myRaceModifiers:     [2, 0, 0, -2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 13, 13, 13, 13, 11],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [11, 14, 13, 13, 13, 10],
                        myRaceModifiers:     [2, -2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [10, 12, 13, 13, 15, 10],
                        myRaceModifiers:     [2, 1, 0, 0, -2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 10, 13, 13, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [10, 10, 13, 13, 15, 12],
                        myRaceModifiers:     [2, 0, 0, 0, -2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 11, 13, 12, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [10, 11, 13, 12, 15, 12],
                        myRaceModifiers:     [2, 0, 0, 0, -2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 11, 13, 13, 12, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [10, 11, 13, 15, 12, 12],
                        myRaceModifiers:     [2, 0, 0, -2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 11, 13, 13, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [10, 11, 13, 13, 13, 14],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [10, 10, 13, 13, 15, 12],
                        myRaceModifiers:     [2, 1, 0, 0, -2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 12, 13, 11, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [10, 12, 13, 11, 15, 12],
                        myRaceModifiers:     [2, 0, 0, 0, -2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 12, 13, 13, 11, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [10, 12, 13, 15, 11, 12],
                        myRaceModifiers:     [2, 0, 0, -2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 13, 13, 10, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [10, 13, 13, 10, 15, 12],
                        myRaceModifiers:     [2, 0, 0, 0, -2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 13, 13, 11, 12, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [10, 13, 15, 11, 12, 12],
                        myRaceModifiers:     [2, 0, -2, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 13, 13, 11, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [10, 13, 13, 11, 13, 14],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [10, 12, 13, 11, 15, 12],
                        myRaceModifiers:     [2, 1, 0, 0, -2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 13, 13, 12, 11, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [10, 13, 15, 12, 11, 12],
                        myRaceModifiers:     [2, 0, -2, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 13, 13, 13, 11, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [10, 13, 13, 13, 11, 14],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [10, 12, 13, 15, 11, 12],
                        myRaceModifiers:     [2, 1, 0, -2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 600,
                statBlock: [12, 12, 12, 13, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [9, 12, 12, 13, 15, 12],
                        myRaceModifiers:     [2, 0, 0, 0, -2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 600,
                statBlock: [12, 13, 12, 12, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [9, 13, 12, 12, 15, 12],
                        myRaceModifiers:     [2, 0, 0, 0, -2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 600,
                statBlock: [12, 13, 12, 13, 12, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [9, 13, 12, 15, 12, 12],
                        myRaceModifiers:     [2, 0, 0, -2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 600,
                statBlock: [12, 13, 12, 13, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [9, 13, 12, 13, 13, 14],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [9, 12, 12, 13, 15, 12],
                        myRaceModifiers:     [2, 1, 0, 0, -2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 600,
                statBlock: [13, 12, 13, 12, 12, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [10, 12, 15, 12, 12, 12],
                        myRaceModifiers:     [2, 0, -2, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 600,
                statBlock: [13, 12, 13, 12, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [10, 12, 13, 12, 13, 14],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [10, 11, 13, 12, 15, 12],
                        myRaceModifiers:     [2, 1, 0, 0, -2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 600,
                statBlock: [13, 12, 13, 13, 12, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [10, 12, 13, 13, 12, 14],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [10, 11, 13, 15, 12, 12],
                        myRaceModifiers:     [2, 1, 0, -2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 600,
                statBlock: [13, 12, 13, 13, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [11, 13, 13, 13, 13, 12],
                        myRaceModifiers:     [2, -2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [10, 11, 13, 13, 13, 14],
                        myRaceModifiers:     [2, 1, 0, 0, 0, -2],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
        ];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

