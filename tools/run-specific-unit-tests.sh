#!/bin/bash

TEST_OUTPUT_FILE="test-output.txt"
TEST_OUTPUT_ACTUAL_FILE="test-output-actual.txt"
TEST_OUTPUT_EXPECTED_FILE="test-output-expected.txt"

# Clear out files from previous runs.
rm -f "$TEST_OUTPUT_FILE"
rm -f "$TEST_OUTPUT_ACTUAL_FILE"
rm -f "$TEST_OUTPUT_EXPECTED_FILE"

# Run the specific test.
cargo test --release $1 > "$TEST_OUTPUT_FILE"

# Cleanup the output for easier diff-ing in case of failure.
python cleanup-rust-pretty-print-output.py "$TEST_OUTPUT_FILE"

# Split expected and actual output into seperate files for easy diff-ing.
grep --only-matching --perl-regexp --null-data "(?s)Actual Begin.*?Actual End"     "$TEST_OUTPUT_FILE" > "$TEST_OUTPUT_ACTUAL_FILE"
grep --only-matching --perl-regexp --null-data "(?s)Expected Begin.*?Expected End" "$TEST_OUTPUT_FILE" > "$TEST_OUTPUT_EXPECTED_FILE"
