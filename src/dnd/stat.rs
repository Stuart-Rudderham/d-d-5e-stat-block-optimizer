#![allow(non_upper_case_globals)]

#[derive(Debug, Eq, PartialEq, Copy, Clone)]
pub enum Stat
{
    Strength            ,
    Dexterity           ,
    Constitution        ,
    Intelligence        ,
    Wisdom              ,
    Charisma            ,
}
