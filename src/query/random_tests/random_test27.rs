use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test27" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Weighted,
        statOrdering: [
            Intelligence,
            Constitution,
            Wisdom,
            Strength,
            Dexterity,
            Charisma,
        ],
        isReversed: false,
        availableRaces: vec![
            Dwarf_MarkOfWarding,
            Elf_Wood,
            Genasi_Air,
            Goliath,
            HalfElf_MarkOfDetection,
            Human,
            Human_MarkOfFinding,
            Kalashtar,
            Kobold,
            Lizardfolk,
            Shifter_BeastHide,
            Shifter_LongTooth,
            Triton,
        ],
        requiredFeats: vec![],
        constraints: vec![
            BinCmp_StatValue_Constant(
                BinaryCompare {
                    lhsValue: GetStatValue(
                        Intelligence,
                    ),
                    binCmp: GT,
                    rhsValue: GetConstant(
                        11,
                    ),
                },
            ),
            BinCmp_StatModifier_Constant(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Dexterity,
                    ),
                    binCmp: LT,
                    rhsValue: GetConstant(
                        0,
                    ),
                },
            ),
        ],
        numberOfASIs: 1,
        doAllRacesHaveFreeFeat: true,
        canRearrangeRaceModifiers: false,
        maxNumberOfResults: 20,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 5396909,
            numUniqueCombinationsPassedConstraints: 447763,
            numStatBlocksEjected: 133,
            numUniqueCombinationsEjected: 1066,
            numUniqueCombinationsBelowLowerBound: 447610,
            numStatBlocksShiftedDuringInsertion: 1215,
            numDuplicateRaceStatBlocksRemoved: 0,
            numUniqueStatBlockAfterRacePass: 15,
            numUniqueStatBlockAfterFeatPass: 83,
            numFinalStatBlockCombinations: 172,
        };

    let expectedStatBlocks = 
        vec![
            RankedStatBlock {
                rank: 1011012,
                statBlock: [10, 8, 20, 16, 14, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Dwarf_MarkOfWarding,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 15, 14, 8],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_MarkOfWarding,
                        myFeats: vec![
                            Crusher,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 15, 14, 8],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_MarkOfWarding,
                        myFeats: vec![
                            Durable,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 15, 14, 8],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_MarkOfWarding,
                        myFeats: vec![
                            DwarvenFortitude,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 15, 14, 8],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_MarkOfWarding,
                        myFeats: vec![
                            Resilient_Con,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 15, 14, 8],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_MarkOfWarding,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 15, 14, 8],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_MarkOfWarding,
                        myFeats: vec![
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 15, 14, 8],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1011012,
                statBlock: [8, 8, 20, 16, 14, 10],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Dwarf_MarkOfWarding,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 15, 14, 10],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_MarkOfWarding,
                        myFeats: vec![
                            Crusher,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 15, 14, 10],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_MarkOfWarding,
                        myFeats: vec![
                            Durable,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 15, 14, 10],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_MarkOfWarding,
                        myFeats: vec![
                            DwarvenFortitude,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 15, 14, 10],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_MarkOfWarding,
                        myFeats: vec![
                            Resilient_Con,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 15, 14, 10],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_MarkOfWarding,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 15, 14, 10],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_MarkOfWarding,
                        myFeats: vec![
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 15, 14, 10],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1011012,
                statBlock: [14, 8, 20, 16, 10, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Dwarf_MarkOfWarding,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [14, 8, 15, 15, 10, 8],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_MarkOfWarding,
                        myFeats: vec![
                            Crusher,
                        ],
                        myBasePointBuyArray: [14, 8, 15, 15, 10, 8],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_MarkOfWarding,
                        myFeats: vec![
                            Durable,
                        ],
                        myBasePointBuyArray: [14, 8, 15, 15, 10, 8],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_MarkOfWarding,
                        myFeats: vec![
                            DwarvenFortitude,
                        ],
                        myBasePointBuyArray: [14, 8, 15, 15, 10, 8],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_MarkOfWarding,
                        myFeats: vec![
                            Resilient_Con,
                        ],
                        myBasePointBuyArray: [14, 8, 15, 15, 10, 8],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_MarkOfWarding,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [14, 8, 15, 15, 10, 8],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_MarkOfWarding,
                        myFeats: vec![
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [14, 8, 15, 15, 10, 8],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1011012,
                statBlock: [8, 8, 20, 16, 10, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Dwarf_MarkOfWarding,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 15, 10, 14],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_MarkOfWarding,
                        myFeats: vec![
                            Crusher,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 15, 10, 14],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_MarkOfWarding,
                        myFeats: vec![
                            Durable,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 15, 10, 14],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_MarkOfWarding,
                        myFeats: vec![
                            DwarvenFortitude,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 15, 10, 14],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_MarkOfWarding,
                        myFeats: vec![
                            Resilient_Con,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 15, 10, 14],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_MarkOfWarding,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 15, 10, 14],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_MarkOfWarding,
                        myFeats: vec![
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 15, 10, 14],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1011012,
                statBlock: [14, 8, 20, 16, 8, 10],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Dwarf_MarkOfWarding,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [14, 8, 15, 15, 8, 10],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_MarkOfWarding,
                        myFeats: vec![
                            Crusher,
                        ],
                        myBasePointBuyArray: [14, 8, 15, 15, 8, 10],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_MarkOfWarding,
                        myFeats: vec![
                            Durable,
                        ],
                        myBasePointBuyArray: [14, 8, 15, 15, 8, 10],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_MarkOfWarding,
                        myFeats: vec![
                            DwarvenFortitude,
                        ],
                        myBasePointBuyArray: [14, 8, 15, 15, 8, 10],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_MarkOfWarding,
                        myFeats: vec![
                            Resilient_Con,
                        ],
                        myBasePointBuyArray: [14, 8, 15, 15, 8, 10],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_MarkOfWarding,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [14, 8, 15, 15, 8, 10],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_MarkOfWarding,
                        myFeats: vec![
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [14, 8, 15, 15, 8, 10],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1011012,
                statBlock: [10, 8, 20, 16, 8, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Dwarf_MarkOfWarding,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 15, 8, 14],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_MarkOfWarding,
                        myFeats: vec![
                            Crusher,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 15, 8, 14],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_MarkOfWarding,
                        myFeats: vec![
                            Durable,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 15, 8, 14],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_MarkOfWarding,
                        myFeats: vec![
                            DwarvenFortitude,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 15, 8, 14],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_MarkOfWarding,
                        myFeats: vec![
                            Resilient_Con,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 15, 8, 14],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_MarkOfWarding,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 15, 8, 14],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_MarkOfWarding,
                        myFeats: vec![
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 15, 8, 14],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1011012,
                statBlock: [10, 8, 14, 16, 20, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            ElvenAccuracy,
                        ],
                        myBasePointBuyArray: [10, 8, 14, 15, 15, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [10, 8, 14, 15, 15, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [10, 8, 14, 15, 15, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            Resilient_Wis,
                        ],
                        myBasePointBuyArray: [10, 8, 14, 15, 15, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [10, 8, 14, 15, 15, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [10, 8, 14, 15, 15, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [10, 8, 14, 15, 15, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [10, 8, 14, 15, 15, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1011012,
                statBlock: [8, 8, 14, 16, 20, 10],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            ElvenAccuracy,
                        ],
                        myBasePointBuyArray: [8, 8, 14, 15, 15, 10],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [8, 8, 14, 15, 15, 10],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [8, 8, 14, 15, 15, 10],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            Resilient_Wis,
                        ],
                        myBasePointBuyArray: [8, 8, 14, 15, 15, 10],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [8, 8, 14, 15, 15, 10],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 8, 14, 15, 15, 10],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [8, 8, 14, 15, 15, 10],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [8, 8, 14, 15, 15, 10],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1011012,
                statBlock: [14, 8, 10, 16, 20, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            ElvenAccuracy,
                        ],
                        myBasePointBuyArray: [14, 8, 10, 15, 15, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [14, 8, 10, 15, 15, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [14, 8, 10, 15, 15, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            Resilient_Wis,
                        ],
                        myBasePointBuyArray: [14, 8, 10, 15, 15, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [14, 8, 10, 15, 15, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [14, 8, 10, 15, 15, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [14, 8, 10, 15, 15, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [14, 8, 10, 15, 15, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1011012,
                statBlock: [8, 8, 10, 16, 20, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            ElvenAccuracy,
                        ],
                        myBasePointBuyArray: [8, 8, 10, 15, 15, 14],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [8, 8, 10, 15, 15, 14],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [8, 8, 10, 15, 15, 14],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            Resilient_Wis,
                        ],
                        myBasePointBuyArray: [8, 8, 10, 15, 15, 14],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [8, 8, 10, 15, 15, 14],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 8, 10, 15, 15, 14],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [8, 8, 10, 15, 15, 14],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [8, 8, 10, 15, 15, 14],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1011012,
                statBlock: [14, 8, 8, 16, 20, 10],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            ElvenAccuracy,
                        ],
                        myBasePointBuyArray: [14, 8, 8, 15, 15, 10],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [14, 8, 8, 15, 15, 10],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [14, 8, 8, 15, 15, 10],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            Resilient_Wis,
                        ],
                        myBasePointBuyArray: [14, 8, 8, 15, 15, 10],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [14, 8, 8, 15, 15, 10],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [14, 8, 8, 15, 15, 10],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [14, 8, 8, 15, 15, 10],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [14, 8, 8, 15, 15, 10],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1011012,
                statBlock: [10, 8, 8, 16, 20, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            ElvenAccuracy,
                        ],
                        myBasePointBuyArray: [10, 8, 8, 15, 15, 14],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [10, 8, 8, 15, 15, 14],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [10, 8, 8, 15, 15, 14],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            Resilient_Wis,
                        ],
                        myBasePointBuyArray: [10, 8, 8, 15, 15, 14],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [10, 8, 8, 15, 15, 14],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [10, 8, 8, 15, 15, 14],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [10, 8, 8, 15, 15, 14],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [10, 8, 8, 15, 15, 14],
                        myRaceModifiers:     [0, 0, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1011012,
                statBlock: [10, 8, 20, 14, 16, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Lizardfolk,
                        myFeats: vec![
                            AberrantDragonMark,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 14, 15, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 1, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Lizardfolk,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 14, 15, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 1, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Lizardfolk,
                        myFeats: vec![
                            Crusher,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 14, 15, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 1, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Lizardfolk,
                        myFeats: vec![
                            Durable,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 14, 15, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 1, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Lizardfolk,
                        myFeats: vec![
                            Resilient_Con,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 14, 15, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 1, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Lizardfolk,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 14, 15, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 1, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Lizardfolk,
                        myFeats: vec![
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 14, 15, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 1, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1011012,
                statBlock: [8, 8, 20, 14, 16, 10],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Lizardfolk,
                        myFeats: vec![
                            AberrantDragonMark,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 14, 15, 10],
                        myRaceModifiers:     [0, 0, 2, 0, 1, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Lizardfolk,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 14, 15, 10],
                        myRaceModifiers:     [0, 0, 2, 0, 1, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Lizardfolk,
                        myFeats: vec![
                            Crusher,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 14, 15, 10],
                        myRaceModifiers:     [0, 0, 2, 0, 1, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Lizardfolk,
                        myFeats: vec![
                            Durable,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 14, 15, 10],
                        myRaceModifiers:     [0, 0, 2, 0, 1, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Lizardfolk,
                        myFeats: vec![
                            Resilient_Con,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 14, 15, 10],
                        myRaceModifiers:     [0, 0, 2, 0, 1, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Lizardfolk,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 14, 15, 10],
                        myRaceModifiers:     [0, 0, 2, 0, 1, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Lizardfolk,
                        myFeats: vec![
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 14, 15, 10],
                        myRaceModifiers:     [0, 0, 2, 0, 1, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1011012,
                statBlock: [16, 8, 20, 14, 10, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Shifter_BeastHide,
                        myFeats: vec![
                            AberrantDragonMark,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 14, 10, 8],
                        myRaceModifiers:     [1, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Shifter_BeastHide,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 14, 10, 8],
                        myRaceModifiers:     [1, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Shifter_BeastHide,
                        myFeats: vec![
                            Crusher,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 14, 10, 8],
                        myRaceModifiers:     [1, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Shifter_BeastHide,
                        myFeats: vec![
                            Durable,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 14, 10, 8],
                        myRaceModifiers:     [1, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Shifter_BeastHide,
                        myFeats: vec![
                            Resilient_Con,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 14, 10, 8],
                        myRaceModifiers:     [1, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Shifter_BeastHide,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 14, 10, 8],
                        myRaceModifiers:     [1, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Shifter_BeastHide,
                        myFeats: vec![
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 14, 10, 8],
                        myRaceModifiers:     [1, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1011012,
                statBlock: [16, 8, 20, 14, 8, 10],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Shifter_BeastHide,
                        myFeats: vec![
                            AberrantDragonMark,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 14, 8, 10],
                        myRaceModifiers:     [1, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Shifter_BeastHide,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 14, 8, 10],
                        myRaceModifiers:     [1, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Shifter_BeastHide,
                        myFeats: vec![
                            Crusher,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 14, 8, 10],
                        myRaceModifiers:     [1, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Shifter_BeastHide,
                        myFeats: vec![
                            Durable,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 14, 8, 10],
                        myRaceModifiers:     [1, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Shifter_BeastHide,
                        myFeats: vec![
                            Resilient_Con,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 14, 8, 10],
                        myRaceModifiers:     [1, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Shifter_BeastHide,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 14, 8, 10],
                        myRaceModifiers:     [1, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Shifter_BeastHide,
                        myFeats: vec![
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 14, 8, 10],
                        myRaceModifiers:     [1, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1011012,
                statBlock: [10, 8, 16, 14, 20, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            ElvenAccuracy,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 14, 15, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 14, 15, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 14, 15, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            Resilient_Wis,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 14, 15, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 14, 15, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 14, 15, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 14, 15, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 14, 15, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfFinding,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 14, 15, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfFinding,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 14, 15, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfFinding,
                        myFeats: vec![
                            Resilient_Wis,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 14, 15, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfFinding,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 14, 15, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfFinding,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 14, 15, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfFinding,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 14, 15, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfFinding,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 14, 15, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1011012,
                statBlock: [8, 8, 16, 14, 20, 10],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            ElvenAccuracy,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 14, 15, 10],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 14, 15, 10],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 14, 15, 10],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            Resilient_Wis,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 14, 15, 10],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 14, 15, 10],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 14, 15, 10],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 14, 15, 10],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 14, 15, 10],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfFinding,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 14, 15, 10],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfFinding,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 14, 15, 10],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfFinding,
                        myFeats: vec![
                            Resilient_Wis,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 14, 15, 10],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfFinding,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 14, 15, 10],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfFinding,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 14, 15, 10],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfFinding,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 14, 15, 10],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfFinding,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 14, 15, 10],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1011012,
                statBlock: [20, 8, 16, 14, 10, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Goliath,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 14, 10, 8],
                        myRaceModifiers:     [2, 0, 1, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Goliath,
                        myFeats: vec![
                            Crusher,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 14, 10, 8],
                        myRaceModifiers:     [2, 0, 1, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Goliath,
                        myFeats: vec![
                            HeavilyArmored,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 14, 10, 8],
                        myRaceModifiers:     [2, 0, 1, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Goliath,
                        myFeats: vec![
                            HeavyArmorMaster,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 14, 10, 8],
                        myRaceModifiers:     [2, 0, 1, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Goliath,
                        myFeats: vec![
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 14, 10, 8],
                        myRaceModifiers:     [2, 0, 1, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Goliath,
                        myFeats: vec![
                            ModeratelyArmored,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 14, 10, 8],
                        myRaceModifiers:     [2, 0, 1, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Goliath,
                        myFeats: vec![
                            Piercer,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 14, 10, 8],
                        myRaceModifiers:     [2, 0, 1, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Goliath,
                        myFeats: vec![
                            Resilient_Str,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 14, 10, 8],
                        myRaceModifiers:     [2, 0, 1, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Goliath,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 14, 10, 8],
                        myRaceModifiers:     [2, 0, 1, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Goliath,
                        myFeats: vec![
                            Slasher,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 14, 10, 8],
                        myRaceModifiers:     [2, 0, 1, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Goliath,
                        myFeats: vec![
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 14, 10, 8],
                        myRaceModifiers:     [2, 0, 1, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Goliath,
                        myFeats: vec![
                            WeaponMaster,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 14, 10, 8],
                        myRaceModifiers:     [2, 0, 1, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1011012,
                statBlock: [20, 8, 16, 14, 8, 10],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Goliath,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 14, 8, 10],
                        myRaceModifiers:     [2, 0, 1, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Goliath,
                        myFeats: vec![
                            Crusher,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 14, 8, 10],
                        myRaceModifiers:     [2, 0, 1, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Goliath,
                        myFeats: vec![
                            HeavilyArmored,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 14, 8, 10],
                        myRaceModifiers:     [2, 0, 1, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Goliath,
                        myFeats: vec![
                            HeavyArmorMaster,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 14, 8, 10],
                        myRaceModifiers:     [2, 0, 1, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Goliath,
                        myFeats: vec![
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 14, 8, 10],
                        myRaceModifiers:     [2, 0, 1, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Goliath,
                        myFeats: vec![
                            ModeratelyArmored,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 14, 8, 10],
                        myRaceModifiers:     [2, 0, 1, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Goliath,
                        myFeats: vec![
                            Piercer,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 14, 8, 10],
                        myRaceModifiers:     [2, 0, 1, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Goliath,
                        myFeats: vec![
                            Resilient_Str,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 14, 8, 10],
                        myRaceModifiers:     [2, 0, 1, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Goliath,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 14, 8, 10],
                        myRaceModifiers:     [2, 0, 1, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Goliath,
                        myFeats: vec![
                            Slasher,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 14, 8, 10],
                        myRaceModifiers:     [2, 0, 1, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Goliath,
                        myFeats: vec![
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 14, 8, 10],
                        myRaceModifiers:     [2, 0, 1, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Goliath,
                        myFeats: vec![
                            WeaponMaster,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 14, 8, 10],
                        myRaceModifiers:     [2, 0, 1, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
        ];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

