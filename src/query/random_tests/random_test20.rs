use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test20" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Weighted,
        statOrdering: [
            Dexterity,
            Intelligence,
            Constitution,
            Strength,
            Wisdom,
            Charisma,
        ],
        isReversed: true,
        availableRaces: vec![
            Aasimar_Fallen,
            Aasimar_Protector,
            Aasimar_Scourge,
            Bugbear,
            CustomLineage,
            Changling,
            DragonBorn,
            Dwarf_Hill,
            Elf_Drow,
            Firbolg,
            Genasi_Air,
            Genasi_Earth,
            Gnome_Forest,
            Gnome_Rock,
            Goliath,
            HalfElf,
            HalfElf_MarkOfStorm,
            Halfling_Ghostwise,
            Halfling_MarkOfHospitality,
            HalfOrc,
            Human,
            Human_MarkOfFinding,
            Human_MarkOfHandling,
            Human_MarkOfMaking,
            Human_MarkOfPassage,
            Human_MarkOfSentinel,
            Kalashtar,
            Kenku,
            Kobold,
            Lizardfolk,
            Orc,
            Orc_Eberron,
            Shifter_SwiftStride,
            Shifter_WildHunt,
            Triton,
            Warforged,
            YuanTiPureblood,
        ],
        requiredFeats: vec![],
        constraints: vec![
            BinCmp_StatValue_Constant(
                BinaryCompare {
                    lhsValue: GetStatValue(
                        Charisma,
                    ),
                    binCmp: LT,
                    rhsValue: GetConstant(
                        17,
                    ),
                },
            ),
            BinCmp_StatModifier_Constant(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Charisma,
                    ),
                    binCmp: LT,
                    rhsValue: GetConstant(
                        0,
                    ),
                },
            ),
        ],
        numberOfASIs: 0,
        doAllRacesHaveFreeFeat: true,
        canRearrangeRaceModifiers: false,
        maxNumberOfResults: 27,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 2677476,
            numUniqueCombinationsPassedConstraints: 266835,
            numStatBlocksEjected: 376,
            numUniqueCombinationsEjected: 12338,
            numUniqueCombinationsBelowLowerBound: 266402,
            numStatBlocksShiftedDuringInsertion: 5701,
            numDuplicateRaceStatBlocksRemoved: 42,
            numUniqueStatBlockAfterRacePass: 48,
            numUniqueStatBlockAfterFeatPass: 218,
            numFinalStatBlockCombinations: 126,
        };

    let expectedStatBlocks = 
        vec![
            RankedStatBlock {
                rank: 501,
                statBlock: [13, 12, 13, 13, 13, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 13, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 501,
                statBlock: [13, 13, 13, 12, 13, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 11, 13, 12, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 501,
                statBlock: [13, 13, 12, 13, 13, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 11, 12, 13, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 501,
                statBlock: [13, 13, 13, 13, 12, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 11, 13, 13, 12, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 501,
                statBlock: [13, 13, 13, 13, 13, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 11, 13, 13, 13, 8],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 501,
                statBlock: [13, 13, 13, 13, 13, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            AberrantDragonMark,
                        ],
                        myBasePointBuyArray: [15, 11, 12, 13, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [15, 11, 13, 13, 13, 8],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 13, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 11, 12, 13, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Crusher,
                        ],
                        myBasePointBuyArray: [15, 11, 12, 13, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Durable,
                        ],
                        myBasePointBuyArray: [15, 11, 12, 13, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 11, 13, 12, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Gunner,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 13, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            KeenMind,
                        ],
                        myBasePointBuyArray: [15, 11, 13, 12, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 13, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [15, 11, 13, 12, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            ModeratelyArmored,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 13, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [15, 11, 13, 12, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Piercer,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 13, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [15, 11, 13, 13, 13, 8],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Resilient_Con,
                        ],
                        myBasePointBuyArray: [15, 11, 12, 13, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Resilient_Dex,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 13, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Resilient_Int,
                        ],
                        myBasePointBuyArray: [15, 11, 13, 12, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Resilient_Wis,
                        ],
                        myBasePointBuyArray: [15, 11, 13, 13, 12, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 11, 13, 12, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 13, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Slasher,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 13, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            SquatNimbleness,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 13, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [15, 11, 12, 13, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 11, 13, 12, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 11, 13, 12, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            WeaponMaster,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 13, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [11, 13, 12, 15, 13, 9],
                        myRaceModifiers:     [2, 0, 1, -2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1221,
                statBlock: [13, 10, 13, 11, 15, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 11, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1221,
                statBlock: [13, 10, 15, 11, 13, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 11, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1221,
                statBlock: [13, 10, 11, 13, 15, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 8, 11, 13, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1221,
                statBlock: [13, 10, 15, 13, 11, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 13, 11, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1221,
                statBlock: [13, 10, 11, 15, 13, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 8, 11, 15, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1221,
                statBlock: [13, 10, 13, 15, 11, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 15, 11, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1221,
                statBlock: [13, 11, 13, 10, 15, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 10, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1221,
                statBlock: [13, 11, 15, 10, 13, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 9, 15, 10, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1221,
                statBlock: [13, 11, 12, 11, 15, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 9, 12, 11, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1221,
                statBlock: [13, 11, 13, 11, 15, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 11, 15, 8],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1221,
                statBlock: [13, 11, 13, 11, 15, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            AberrantDragonMark,
                        ],
                        myBasePointBuyArray: [15, 9, 12, 11, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 11, 15, 8],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 11, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 9, 12, 11, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Crusher,
                        ],
                        myBasePointBuyArray: [15, 9, 12, 11, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Durable,
                        ],
                        myBasePointBuyArray: [15, 9, 12, 11, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 10, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Gunner,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 11, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            KeenMind,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 10, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 11, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 10, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            ModeratelyArmored,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 11, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 10, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Piercer,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 11, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 11, 15, 8],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Resilient_Con,
                        ],
                        myBasePointBuyArray: [15, 9, 12, 11, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Resilient_Dex,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 11, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Resilient_Int,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 10, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 10, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 11, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Slasher,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 11, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            SquatNimbleness,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 11, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [15, 9, 12, 11, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 10, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 10, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            WeaponMaster,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 11, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1221,
                statBlock: [13, 11, 15, 11, 12, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 9, 15, 11, 12, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1221,
                statBlock: [13, 11, 15, 11, 13, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 9, 15, 11, 13, 8],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1221,
                statBlock: [13, 11, 15, 11, 13, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [15, 9, 15, 11, 13, 8],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 11, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 9, 15, 11, 13, 8],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 9, 15, 10, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Gunner,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 11, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            KeenMind,
                        ],
                        myBasePointBuyArray: [15, 9, 15, 10, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 11, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [15, 9, 15, 10, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            ModeratelyArmored,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 11, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [15, 9, 15, 10, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Piercer,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 11, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [15, 9, 15, 11, 13, 8],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Resilient_Dex,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 11, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Resilient_Int,
                        ],
                        myBasePointBuyArray: [15, 9, 15, 10, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Resilient_Wis,
                        ],
                        myBasePointBuyArray: [15, 9, 15, 11, 12, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 9, 15, 10, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 11, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Slasher,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 11, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            SquatNimbleness,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 11, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 9, 15, 10, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 9, 15, 10, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            WeaponMaster,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 11, 13, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1221,
                statBlock: [13, 11, 11, 12, 15, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 9, 11, 12, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1221,
                statBlock: [13, 11, 15, 12, 11, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 9, 15, 12, 11, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1221,
                statBlock: [13, 11, 10, 13, 15, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 9, 10, 13, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1221,
                statBlock: [13, 11, 11, 13, 15, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 9, 11, 13, 15, 8],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1221,
                statBlock: [13, 11, 11, 13, 15, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            AberrantDragonMark,
                        ],
                        myBasePointBuyArray: [15, 9, 10, 13, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [15, 9, 11, 13, 15, 8],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 8, 11, 13, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 9, 10, 13, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Crusher,
                        ],
                        myBasePointBuyArray: [15, 9, 10, 13, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Durable,
                        ],
                        myBasePointBuyArray: [15, 9, 10, 13, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 9, 11, 12, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Gunner,
                        ],
                        myBasePointBuyArray: [15, 8, 11, 13, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            KeenMind,
                        ],
                        myBasePointBuyArray: [15, 9, 11, 12, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 8, 11, 13, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [15, 9, 11, 12, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            ModeratelyArmored,
                        ],
                        myBasePointBuyArray: [15, 8, 11, 13, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [15, 9, 11, 12, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Piercer,
                        ],
                        myBasePointBuyArray: [15, 8, 11, 13, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [15, 9, 11, 13, 15, 8],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Resilient_Con,
                        ],
                        myBasePointBuyArray: [15, 9, 10, 13, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Resilient_Dex,
                        ],
                        myBasePointBuyArray: [15, 8, 11, 13, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Resilient_Int,
                        ],
                        myBasePointBuyArray: [15, 9, 11, 12, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 9, 11, 12, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 8, 11, 13, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Slasher,
                        ],
                        myBasePointBuyArray: [15, 8, 11, 13, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            SquatNimbleness,
                        ],
                        myBasePointBuyArray: [15, 8, 11, 13, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [15, 9, 10, 13, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 9, 11, 12, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 9, 11, 12, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            WeaponMaster,
                        ],
                        myBasePointBuyArray: [15, 8, 11, 13, 15, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [11, 11, 10, 15, 15, 9],
                        myRaceModifiers:     [2, 0, 1, -2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1221,
                statBlock: [11, 11, 13, 13, 15, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [9, 11, 12, 15, 15, 9],
                        myRaceModifiers:     [2, 0, 1, -2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1221,
                statBlock: [13, 11, 15, 13, 10, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 9, 15, 13, 10, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
        ];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

