use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test05" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Weighted,
        statOrdering: [
            Dexterity,
            Constitution,
            Strength,
            Wisdom,
            Intelligence,
            Charisma,
        ],
        isReversed: true,
        availableRaces: vec![
            Aarakocra,
            Aasimar_Fallen,
            Aasimar_Protector,
            Aasimar_Scourge,
            Bugbear,
            Changling,
            CustomLineage,
            DragonBorn,
            Dwarf_Grey,
            Dwarf_Hill,
            Dwarf_MarkOfWarding,
            Dwarf_Mountain,
            Elf_Drow,
            Elf_High,
            Elf_MarkOfShadow,
            Elf_Wood,
            Firbolg,
            Genasi_Air,
            Genasi_Earth,
            Genasi_Fire,
            Genasi_Water,
            Gnome_Deep,
            Gnome_Forest,
            Gnome_MarkOfScribing,
            Gnome_Rock,
            Goblin,
            Goliath,
            HalfElf,
            HalfElf_MarkOfDetection,
            HalfElf_MarkOfStorm,
            Halfling_Ghostwise,
            Halfling_Lightfoot,
            Halfling_MarkOfHealing,
            Halfling_MarkOfHospitality,
            Halfling_Stout,
            HalfOrc,
            HalfOrc_MarkOfFinding,
            Hobgoblin,
            Human,
            Human_MarkOfFinding,
            Human_MarkOfHandling,
            Human_MarkOfMaking,
            Human_MarkOfPassage,
            Human_MarkOfSentinel,
            Human_Variant,
            Kalashtar,
            Kenku,
            Kobold,
            Lizardfolk,
            Orc,
            Orc_Eberron,
            Shifter_BeastHide,
            Shifter_LongTooth,
            Shifter_SwiftStride,
            Shifter_WildHunt,
            Tabaxi,
            Tiefling,
            Tiefling_Feral,
            Tortle,
            Triton,
            Warforged,
            YuanTiPureblood,
        ],
        requiredFeats: vec![],
        constraints: vec![
            UnCmp_StatValue(
                UnaryCompare {
                    value: GetStatValue(
                        Dexterity,
                    ),
                    unCmp: IsMin,
                },
            ),
        ],
        numberOfASIs: 0,
        doAllRacesHaveFreeFeat: false,
        canRearrangeRaceModifiers: false,
        maxNumberOfResults: 27,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 1117662,
            numUniqueCombinationsPassedConstraints: 235829,
            numStatBlocksEjected: 269,
            numUniqueCombinationsEjected: 1915,
            numUniqueCombinationsBelowLowerBound: 235509,
            numStatBlocksShiftedDuringInsertion: 3922,
            numDuplicateRaceStatBlocksRemoved: 0,
            numUniqueStatBlockAfterRacePass: 65,
            numUniqueStatBlockAfterFeatPass: 91,
            numFinalStatBlockCombinations: 28,
        };

    let expectedStatBlocks = 
        vec![
            RankedStatBlock {
                rank: 420,
                statBlock: [13, 10, 11, 13, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 8, 11, 13, 13, 13],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 420,
                statBlock: [13, 10, 13, 13, 11, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 8, 13, 13, 11, 13],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 420,
                statBlock: [13, 10, 13, 11, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 8, 13, 11, 13, 13],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 420,
                statBlock: [13, 10, 13, 13, 13, 11],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 8, 13, 13, 13, 11],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 420,
                statBlock: [13, 11, 11, 13, 12, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 9, 11, 13, 12, 13],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 420,
                statBlock: [13, 11, 11, 12, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 9, 11, 12, 13, 13],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 420,
                statBlock: [13, 11, 11, 13, 13, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 9, 11, 13, 13, 12],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 420,
                statBlock: [13, 11, 11, 13, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![],
                        myBasePointBuyArray: [11, 11, 10, 15, 13, 13],
                        myRaceModifiers:     [2, 0, 1, -2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 420,
                statBlock: [13, 11, 12, 13, 11, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 9, 12, 13, 11, 13],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 420,
                statBlock: [13, 11, 12, 11, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 9, 12, 11, 13, 13],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 420,
                statBlock: [13, 11, 12, 13, 13, 11],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 9, 12, 13, 13, 11],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 420,
                statBlock: [11, 11, 13, 13, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![],
                        myBasePointBuyArray: [9, 11, 12, 15, 13, 13],
                        myRaceModifiers:     [2, 0, 1, -2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 420,
                statBlock: [13, 11, 13, 12, 11, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 9, 13, 12, 11, 13],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 420,
                statBlock: [13, 11, 13, 13, 11, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 9, 13, 13, 11, 12],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 420,
                statBlock: [13, 11, 13, 13, 11, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![],
                        myBasePointBuyArray: [11, 11, 12, 15, 11, 13],
                        myRaceModifiers:     [2, 0, 1, -2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 420,
                statBlock: [13, 11, 13, 11, 12, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 9, 13, 11, 12, 13],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 420,
                statBlock: [13, 11, 13, 13, 12, 11],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 9, 13, 13, 12, 11],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 420,
                statBlock: [13, 11, 13, 11, 13, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 9, 13, 11, 13, 12],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 420,
                statBlock: [13, 11, 13, 12, 13, 11],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 9, 13, 12, 13, 11],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 420,
                statBlock: [13, 11, 13, 13, 13, 11],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![],
                        myBasePointBuyArray: [11, 11, 12, 15, 13, 11],
                        myRaceModifiers:     [2, 0, 1, -2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 501,
                statBlock: [13, 9, 13, 13, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![],
                        myBasePointBuyArray: [11, 9, 12, 15, 13, 13],
                        myRaceModifiers:     [2, 0, 1, -2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 10, 12, 13, 12, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 8, 12, 13, 12, 13],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 10, 12, 12, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 8, 12, 12, 13, 13],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 10, 12, 13, 13, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 8, 12, 13, 13, 12],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 10, 12, 13, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![],
                        myBasePointBuyArray: [11, 10, 11, 15, 13, 13],
                        myRaceModifiers:     [2, 0, 1, -2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [12, 10, 13, 13, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [14, 8, 13, 13, 13, 13],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![],
                        myBasePointBuyArray: [10, 10, 12, 15, 13, 13],
                        myRaceModifiers:     [2, 0, 1, -2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 10, 13, 12, 12, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 8, 13, 12, 12, 13],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
        ];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

