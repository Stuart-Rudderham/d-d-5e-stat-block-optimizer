use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test04" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Flat,
        statOrdering: [
            Wisdom,
            Intelligence,
            Dexterity,
            Charisma,
            Strength,
            Constitution,
        ],
        isReversed: false,
        availableRaces: vec![
            Aarakocra,
            Aasimar_Fallen,
            Aasimar_Protector,
            Aasimar_Scourge,
            Bugbear,
            CustomLineage,
            Changling,
            DragonBorn,
            Dwarf_Grey,
            Dwarf_Hill,
            Dwarf_MarkOfWarding,
            Dwarf_Mountain,
            Elf_Drow,
            Elf_High,
            Elf_MarkOfShadow,
            Elf_Wood,
            Firbolg,
            Genasi_Air,
            Genasi_Earth,
            Genasi_Fire,
            Genasi_Water,
            Gnome_Deep,
            Gnome_Forest,
            Gnome_MarkOfScribing,
            Gnome_Rock,
            Goliath,
            HalfElf,
            HalfElf_MarkOfDetection,
            HalfElf_MarkOfStorm,
            Halfling_Ghostwise,
            Halfling_Lightfoot,
            Halfling_MarkOfHealing,
            Halfling_MarkOfHospitality,
            Halfling_Stout,
            HalfOrc,
            HalfOrc_MarkOfFinding,
            Hobgoblin,
            Human,
            Human_MarkOfFinding,
            Human_MarkOfHandling,
            Human_MarkOfMaking,
            Human_MarkOfPassage,
            Human_MarkOfSentinel,
            Human_Variant,
            Kalashtar,
            Kobold,
            Lizardfolk,
            Orc,
            Orc_Eberron,
            Shifter_BeastHide,
            Shifter_LongTooth,
            Shifter_SwiftStride,
            Shifter_WildHunt,
            Tabaxi,
            Tiefling,
            Tiefling_Feral,
            Tortle,
            Triton,
            Warforged,
            YuanTiPureblood,
        ],
        requiredFeats: vec![
            SkillExpert,
        ],
        constraints: vec![],
        numberOfASIs: 2,
        doAllRacesHaveFreeFeat: false,
        canRearrangeRaceModifiers: false,
        maxNumberOfResults: 11,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 20626277,
            numUniqueCombinationsPassedConstraints: 20526041,
            numStatBlocksEjected: 241,
            numUniqueCombinationsEjected: 267,
            numUniqueCombinationsBelowLowerBound: 20525708,
            numStatBlocksShiftedDuringInsertion: 1328,
            numDuplicateRaceStatBlocksRemoved: 55,
            numUniqueStatBlockAfterRacePass: 65,
            numUniqueStatBlockAfterFeatPass: 227,
            numFinalStatBlockCombinations: 11,
        };

    let expectedStatBlocks = 
        vec![
            RankedStatBlock {
                rank: 12,
                statBlock: [14, 14, 12, 14, 16, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [12, 13, 11, 13, 13, 13],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 12,
                statBlock: [12, 14, 14, 14, 16, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [10, 13, 13, 13, 13, 13],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 12,
                statBlock: [14, 14, 14, 14, 16, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [12, 13, 13, 13, 13, 11],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 12,
                statBlock: [14, 12, 14, 14, 16, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [12, 11, 13, 13, 13, 13],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 12,
                statBlock: [14, 14, 14, 12, 16, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [12, 13, 13, 11, 13, 13],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 12,
                statBlock: [14, 14, 12, 16, 14, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [12, 13, 11, 13, 13, 13],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 12,
                statBlock: [12, 14, 14, 16, 14, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [10, 13, 13, 13, 13, 13],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 12,
                statBlock: [14, 14, 14, 16, 14, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [12, 13, 13, 13, 13, 11],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 12,
                statBlock: [14, 12, 14, 16, 14, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [12, 11, 13, 13, 13, 13],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 12,
                statBlock: [14, 16, 12, 14, 14, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [12, 13, 11, 13, 13, 13],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 12,
                statBlock: [12, 16, 14, 14, 14, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [10, 13, 13, 13, 13, 13],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
        ];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

