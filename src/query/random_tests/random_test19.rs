use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test19" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Flat,
        statOrdering: [
            Intelligence,
            Charisma,
            Dexterity,
            Wisdom,
            Strength,
            Constitution,
        ],
        isReversed: false,
        availableRaces: vec![
            HalfElf_MarkOfStorm,
            Halfling_MarkOfHealing,
            Human_MarkOfSentinel,
        ],
        requiredFeats: vec![],
        constraints: vec![],
        numberOfASIs: 1,
        doAllRacesHaveFreeFeat: false,
        canRearrangeRaceModifiers: true,
        maxNumberOfResults: 19,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 1950690,
            numUniqueCombinationsPassedConstraints: 1950690,
            numStatBlocksEjected: 264,
            numUniqueCombinationsEjected: 888,
            numUniqueCombinationsBelowLowerBound: 1950326,
            numStatBlocksShiftedDuringInsertion: 2754,
            numDuplicateRaceStatBlocksRemoved: 147,
            numUniqueStatBlockAfterRacePass: 30,
            numUniqueStatBlockAfterFeatPass: 30,
            numFinalStatBlockCombinations: 57,
        };

    let expectedStatBlocks = 
        vec![
            RankedStatBlock {
                rank: 10,
                statBlock: [12, 14, 12, 16, 12, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![],
                        myBasePointBuyArray: [12, 13, 12, 13, 12, 13],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 1],
                        myASIs:              [0, 1, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![],
                        myBasePointBuyArray: [12, 13, 12, 13, 12, 13],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 1],
                        myASIs:              [0, 1, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfSentinel,
                        myFeats: vec![],
                        myBasePointBuyArray: [12, 13, 12, 13, 12, 13],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 1],
                        myASIs:              [0, 1, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [12, 12, 12, 16, 14, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![],
                        myBasePointBuyArray: [12, 12, 12, 13, 13, 13],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 1],
                        myASIs:              [0, 0, 0, 1, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![],
                        myBasePointBuyArray: [12, 12, 12, 13, 13, 13],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 1],
                        myASIs:              [0, 0, 0, 1, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfSentinel,
                        myFeats: vec![],
                        myBasePointBuyArray: [12, 12, 12, 13, 13, 13],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 1],
                        myASIs:              [0, 0, 0, 1, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [14, 12, 12, 16, 12, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![],
                        myBasePointBuyArray: [13, 12, 12, 13, 12, 13],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 1],
                        myASIs:              [1, 0, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![],
                        myBasePointBuyArray: [13, 12, 12, 13, 12, 13],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 1],
                        myASIs:              [1, 0, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfSentinel,
                        myFeats: vec![],
                        myBasePointBuyArray: [13, 12, 12, 13, 12, 13],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 1],
                        myASIs:              [1, 0, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [12, 12, 14, 16, 12, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![],
                        myBasePointBuyArray: [12, 12, 13, 13, 12, 13],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 1],
                        myASIs:              [0, 0, 1, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![],
                        myBasePointBuyArray: [12, 12, 13, 13, 12, 13],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 1],
                        myASIs:              [0, 0, 1, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfSentinel,
                        myFeats: vec![],
                        myBasePointBuyArray: [12, 12, 13, 13, 12, 13],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 1],
                        myASIs:              [0, 0, 1, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [12, 14, 12, 16, 14, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![],
                        myBasePointBuyArray: [12, 13, 12, 13, 13, 12],
                        myRaceModifiers:     [0, 0, 0, 2, 1, 0],
                        myASIs:              [0, 1, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![],
                        myBasePointBuyArray: [12, 13, 12, 13, 13, 12],
                        myRaceModifiers:     [0, 0, 0, 2, 1, 0],
                        myASIs:              [0, 1, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfSentinel,
                        myFeats: vec![],
                        myBasePointBuyArray: [12, 13, 12, 13, 13, 12],
                        myRaceModifiers:     [0, 0, 0, 2, 1, 0],
                        myASIs:              [0, 1, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [14, 14, 12, 16, 12, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![],
                        myBasePointBuyArray: [13, 13, 12, 13, 12, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [1, 0, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![],
                        myBasePointBuyArray: [13, 13, 12, 13, 12, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [1, 0, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfSentinel,
                        myFeats: vec![],
                        myBasePointBuyArray: [13, 13, 12, 13, 12, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [1, 0, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [12, 14, 14, 16, 12, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![],
                        myBasePointBuyArray: [12, 13, 13, 13, 12, 12],
                        myRaceModifiers:     [0, 0, 1, 2, 0, 0],
                        myASIs:              [0, 1, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![],
                        myBasePointBuyArray: [12, 13, 13, 13, 12, 12],
                        myRaceModifiers:     [0, 0, 1, 2, 0, 0],
                        myASIs:              [0, 1, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfSentinel,
                        myFeats: vec![],
                        myBasePointBuyArray: [12, 13, 13, 13, 12, 12],
                        myRaceModifiers:     [0, 0, 1, 2, 0, 0],
                        myASIs:              [0, 1, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [14, 12, 12, 16, 14, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![],
                        myBasePointBuyArray: [13, 12, 12, 13, 13, 12],
                        myRaceModifiers:     [0, 0, 0, 2, 1, 0],
                        myASIs:              [1, 0, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![],
                        myBasePointBuyArray: [13, 12, 12, 13, 13, 12],
                        myRaceModifiers:     [0, 0, 0, 2, 1, 0],
                        myASIs:              [1, 0, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfSentinel,
                        myFeats: vec![],
                        myBasePointBuyArray: [13, 12, 12, 13, 13, 12],
                        myRaceModifiers:     [0, 0, 0, 2, 1, 0],
                        myASIs:              [1, 0, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [12, 12, 14, 16, 14, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![],
                        myBasePointBuyArray: [12, 12, 13, 13, 13, 12],
                        myRaceModifiers:     [0, 0, 0, 2, 1, 0],
                        myASIs:              [0, 0, 1, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![],
                        myBasePointBuyArray: [12, 12, 13, 13, 13, 12],
                        myRaceModifiers:     [0, 0, 0, 2, 1, 0],
                        myASIs:              [0, 0, 1, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfSentinel,
                        myFeats: vec![],
                        myBasePointBuyArray: [12, 12, 13, 13, 13, 12],
                        myRaceModifiers:     [0, 0, 0, 2, 1, 0],
                        myASIs:              [0, 0, 1, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [14, 12, 14, 16, 12, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![],
                        myBasePointBuyArray: [13, 12, 13, 13, 12, 12],
                        myRaceModifiers:     [0, 0, 1, 2, 0, 0],
                        myASIs:              [1, 0, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![],
                        myBasePointBuyArray: [13, 12, 13, 13, 12, 12],
                        myRaceModifiers:     [0, 0, 1, 2, 0, 0],
                        myASIs:              [1, 0, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfSentinel,
                        myFeats: vec![],
                        myBasePointBuyArray: [13, 12, 13, 13, 12, 12],
                        myRaceModifiers:     [0, 0, 1, 2, 0, 0],
                        myASIs:              [1, 0, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [12, 14, 12, 14, 12, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![],
                        myBasePointBuyArray: [12, 13, 12, 13, 12, 13],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 2],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![],
                        myBasePointBuyArray: [12, 13, 12, 13, 12, 13],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 2],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfSentinel,
                        myFeats: vec![],
                        myBasePointBuyArray: [12, 13, 12, 13, 12, 13],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 2],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [12, 12, 12, 14, 14, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![],
                        myBasePointBuyArray: [12, 12, 12, 13, 13, 13],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![],
                        myBasePointBuyArray: [12, 12, 12, 13, 13, 13],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfSentinel,
                        myFeats: vec![],
                        myBasePointBuyArray: [12, 12, 12, 13, 13, 13],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [14, 12, 12, 14, 12, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![],
                        myBasePointBuyArray: [13, 12, 12, 13, 12, 13],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 2],
                        myASIs:              [1, 0, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![],
                        myBasePointBuyArray: [13, 12, 12, 13, 12, 13],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 2],
                        myASIs:              [1, 0, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfSentinel,
                        myFeats: vec![],
                        myBasePointBuyArray: [13, 12, 12, 13, 12, 13],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 2],
                        myASIs:              [1, 0, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [12, 12, 14, 14, 12, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![],
                        myBasePointBuyArray: [12, 12, 13, 13, 12, 13],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 1, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![],
                        myBasePointBuyArray: [12, 12, 13, 13, 12, 13],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 1, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfSentinel,
                        myFeats: vec![],
                        myBasePointBuyArray: [12, 12, 13, 13, 12, 13],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 1, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [12, 16, 12, 14, 12, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![],
                        myBasePointBuyArray: [12, 13, 12, 13, 12, 13],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 1, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![],
                        myBasePointBuyArray: [12, 13, 12, 13, 12, 13],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 1, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfSentinel,
                        myFeats: vec![],
                        myBasePointBuyArray: [12, 13, 12, 13, 12, 13],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 1, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [12, 14, 12, 14, 14, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![],
                        myBasePointBuyArray: [12, 12, 12, 13, 13, 13],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 0, 0, 1, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![],
                        myBasePointBuyArray: [12, 12, 12, 13, 13, 13],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 0, 0, 1, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfSentinel,
                        myFeats: vec![],
                        myBasePointBuyArray: [12, 12, 12, 13, 13, 13],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 0, 0, 1, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [14, 14, 12, 14, 12, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![],
                        myBasePointBuyArray: [12, 13, 12, 13, 12, 13],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 1],
                        myASIs:              [0, 1, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![],
                        myBasePointBuyArray: [12, 13, 12, 13, 12, 13],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 1],
                        myASIs:              [0, 1, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfSentinel,
                        myFeats: vec![],
                        myBasePointBuyArray: [12, 13, 12, 13, 12, 13],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 1],
                        myASIs:              [0, 1, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [12, 14, 14, 14, 12, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![],
                        myBasePointBuyArray: [12, 12, 13, 13, 12, 13],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 0, 1, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![],
                        myBasePointBuyArray: [12, 12, 13, 13, 12, 13],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 0, 1, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfSentinel,
                        myFeats: vec![],
                        myBasePointBuyArray: [12, 12, 13, 13, 12, 13],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 0, 1, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [12, 12, 12, 14, 16, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfStorm,
                        myFeats: vec![],
                        myBasePointBuyArray: [12, 12, 12, 13, 13, 13],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 0, 1, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![],
                        myBasePointBuyArray: [12, 12, 12, 13, 13, 13],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 0, 1, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfSentinel,
                        myFeats: vec![],
                        myBasePointBuyArray: [12, 12, 12, 13, 13, 13],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 0, 1, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
        ];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

