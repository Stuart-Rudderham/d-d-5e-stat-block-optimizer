use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test09" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Flat,
        statOrdering: [
            Dexterity,
            Constitution,
            Wisdom,
            Charisma,
            Strength,
            Intelligence,
        ],
        isReversed: true,
        availableRaces: vec![
            Aarakocra,
            Aasimar_Fallen,
            Aasimar_Protector,
            Aasimar_Scourge,
            Bugbear,
            Changling,
            CustomLineage,
            DragonBorn,
            Dwarf_Grey,
            Dwarf_Hill,
            Dwarf_MarkOfWarding,
            Dwarf_Mountain,
            Elf_Drow,
            Elf_High,
            Elf_MarkOfShadow,
            Elf_Wood,
            Firbolg,
            Genasi_Air,
            Genasi_Earth,
            Genasi_Fire,
            Genasi_Water,
            Gnome_Deep,
            Gnome_Forest,
            Gnome_MarkOfScribing,
            Gnome_Rock,
            Goblin,
            Goliath,
            HalfElf,
            HalfElf_MarkOfDetection,
            HalfElf_MarkOfStorm,
            Halfling_Ghostwise,
            Halfling_Lightfoot,
            Halfling_MarkOfHealing,
            Halfling_MarkOfHospitality,
            Halfling_Stout,
            HalfOrc,
            HalfOrc_MarkOfFinding,
            Hobgoblin,
            Human,
            Human_MarkOfFinding,
            Human_MarkOfHandling,
            Human_MarkOfMaking,
            Human_MarkOfPassage,
            Human_MarkOfSentinel,
            Kalashtar,
            Kenku,
            Kobold,
            Lizardfolk,
            Orc,
            Orc_Eberron,
            Shifter_BeastHide,
            Shifter_LongTooth,
            Shifter_SwiftStride,
            Shifter_WildHunt,
            Tabaxi,
            Tiefling,
            Tiefling_Feral,
            Tortle,
            Triton,
            Warforged,
            YuanTiPureblood,
        ],
        requiredFeats: vec![
            Athlete,
        ],
        constraints: vec![
            UnCmp_StatValue(
                UnaryCompare {
                    value: GetStatValue(
                        Intelligence,
                    ),
                    unCmp: IsEven,
                },
            ),
        ],
        numberOfASIs: 0,
        doAllRacesHaveFreeFeat: false,
        canRearrangeRaceModifiers: true,
        maxNumberOfResults: 23,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 147384,
            numUniqueCombinationsPassedConstraints: 70644,
            numStatBlocksEjected: 96,
            numUniqueCombinationsEjected: 99,
            numUniqueCombinationsBelowLowerBound: 70519,
            numStatBlocksShiftedDuringInsertion: 1052,
            numDuplicateRaceStatBlocksRemoved: 3,
            numUniqueStatBlockAfterRacePass: 282,
            numUniqueStatBlockAfterFeatPass: 12,
            numFinalStatBlockCombinations: 23,
        };

    let expectedStatBlocks = 
        vec![
            RankedStatBlock {
                rank: 4,
                statBlock: [9, 8, 15, 10, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [11, 8, 15, 8, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 8, 15, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [9, 8, 15, 8, 15, 17],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [9, 8, 15, 8, 17, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [9, 8, 17, 8, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [15, 9, 8, 10, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [17, 9, 8, 8, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [15, 9, 8, 8, 15, 17],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [15, 9, 8, 8, 17, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [15, 9, 9, 16, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 8, 9, 14, 9, 15],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [17, 9, 9, 14, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 8, 9, 14, 9, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [15, 9, 9, 14, 9, 17],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 8, 9, 14, 9, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [15, 9, 9, 14, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 8, 9, 14, 9, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [15, 9, 9, 16, 15, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 8, 9, 14, 15, 9],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [17, 9, 9, 14, 15, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 8, 9, 14, 15, 9],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [15, 9, 9, 14, 15, 11],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 8, 9, 14, 15, 9],
                        myRaceModifiers:     [0, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [9, 9, 9, 16, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 9, 9, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [11, 9, 9, 14, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 9, 9, 14, 15, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [15, 9, 9, 10, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [14, 9, 9, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [17, 9, 9, 8, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [14, 9, 9, 8, 15, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [9, 9, 9, 14, 15, 17],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 9, 9, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [15, 9, 9, 8, 15, 17],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [14, 9, 9, 8, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [15, 9, 9, 14, 17, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 8, 9, 14, 15, 9],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
        ];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

