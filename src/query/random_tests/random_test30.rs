use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test30" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Weighted,
        statOrdering: [
            Intelligence,
            Wisdom,
            Constitution,
            Charisma,
            Dexterity,
            Strength,
        ],
        isReversed: false,
        availableRaces: vec![
            Aasimar_Fallen,
            Aasimar_Protector,
            CustomLineage,
            Changling,
            Dwarf_Hill,
            Dwarf_MarkOfWarding,
            Dwarf_Mountain,
            Elf_MarkOfShadow,
            Elf_Wood,
            Genasi_Earth,
            Gnome_Deep,
            Gnome_MarkOfScribing,
            Goblin,
            Goliath,
            Halfling_Ghostwise,
            Halfling_MarkOfHealing,
            HalfOrc,
            HalfOrc_MarkOfFinding,
            Hobgoblin,
            Human_MarkOfFinding,
            Human_MarkOfHandling,
            Human_MarkOfMaking,
            Human_MarkOfSentinel,
            Human_Variant,
            Kenku,
            Lizardfolk,
            Orc,
            Orc_Eberron,
            Shifter_LongTooth,
            Shifter_SwiftStride,
            Tiefling,
            Tiefling_Feral,
            Tortle,
            YuanTiPureblood,
        ],
        requiredFeats: vec![
            SkillExpert,
        ],
        constraints: vec![
            BinCmp_StatValue_StatValue(
                BinaryCompare {
                    lhsValue: GetStatValue(
                        Charisma,
                    ),
                    binCmp: NE,
                    rhsValue: GetStatValue(
                        Wisdom,
                    ),
                },
            ),
            UnCmp_StatValue(
                UnaryCompare {
                    value: GetStatValue(
                        Intelligence,
                    ),
                    unCmp: IsEven,
                },
            ),
            BinCmp_StatModifier_Constant(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Charisma,
                    ),
                    binCmp: NE,
                    rhsValue: GetConstant(
                        1,
                    ),
                },
            ),
            UnCmp_StatModifier(
                UnaryCompare {
                    value: GetStatModifier(
                        Intelligence,
                    ),
                    unCmp: IsMax,
                },
            ),
        ],
        numberOfASIs: 1,
        doAllRacesHaveFreeFeat: false,
        canRearrangeRaceModifiers: false,
        maxNumberOfResults: 25,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 4992308,
            numUniqueCombinationsPassedConstraints: 492446,
            numStatBlocksEjected: 320,
            numUniqueCombinationsEjected: 419,
            numUniqueCombinationsBelowLowerBound: 492073,
            numStatBlocksShiftedDuringInsertion: 4073,
            numDuplicateRaceStatBlocksRemoved: 0,
            numUniqueStatBlockAfterRacePass: 48,
            numUniqueStatBlockAfterFeatPass: 166,
            numFinalStatBlockCombinations: 25,
        };

    let expectedStatBlocks = 
        vec![
            RankedStatBlock {
                rank: 1002102,
                statBlock: [8, 12, 14, 20, 14, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 12, 14, 15, 14, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1002102,
                statBlock: [12, 8, 14, 20, 14, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [12, 8, 14, 15, 14, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1002102,
                statBlock: [8, 14, 12, 20, 14, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 14, 12, 15, 14, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1002102,
                statBlock: [14, 8, 12, 20, 14, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [14, 8, 12, 15, 14, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1002102,
                statBlock: [12, 14, 8, 20, 14, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [12, 14, 8, 15, 14, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1002102,
                statBlock: [14, 12, 8, 20, 14, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [14, 12, 8, 15, 14, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1002102,
                statBlock: [8, 8, 14, 20, 12, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 8, 14, 15, 12, 14],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1002102,
                statBlock: [8, 14, 14, 20, 12, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 14, 14, 15, 12, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1002102,
                statBlock: [14, 8, 14, 20, 12, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [14, 8, 14, 15, 12, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1002102,
                statBlock: [8, 14, 8, 20, 12, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 14, 8, 15, 12, 14],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1002102,
                statBlock: [14, 8, 8, 20, 12, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [14, 8, 8, 15, 12, 14],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1002102,
                statBlock: [14, 14, 8, 20, 12, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [14, 14, 8, 15, 12, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1002102,
                statBlock: [8, 12, 14, 20, 8, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 12, 14, 15, 8, 14],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1002102,
                statBlock: [12, 8, 14, 20, 8, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [12, 8, 14, 15, 8, 14],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1002102,
                statBlock: [8, 14, 12, 20, 8, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 14, 12, 15, 8, 14],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1002102,
                statBlock: [14, 8, 12, 20, 8, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [14, 8, 12, 15, 8, 14],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1002102,
                statBlock: [12, 14, 8, 20, 8, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [12, 14, 8, 15, 8, 14],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1002102,
                statBlock: [14, 12, 8, 20, 8, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [14, 12, 8, 15, 8, 14],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1002021,
                statBlock: [8, 10, 14, 20, 14, 10],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 10, 14, 15, 14, 10],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1002021,
                statBlock: [10, 8, 14, 20, 14, 10],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [10, 8, 14, 15, 14, 10],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1002021,
                statBlock: [10, 10, 14, 20, 14, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [10, 10, 14, 15, 14, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1002021,
                statBlock: [8, 14, 10, 20, 14, 10],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 14, 10, 15, 14, 10],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1002021,
                statBlock: [14, 8, 10, 20, 14, 10],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [14, 8, 10, 15, 14, 10],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1002021,
                statBlock: [10, 14, 10, 20, 14, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [10, 14, 10, 15, 14, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1002021,
                statBlock: [14, 10, 10, 20, 14, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [14, 10, 10, 15, 14, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
        ];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

