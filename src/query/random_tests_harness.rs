#[cfg(any(test, feature = "benchmark"))]
use crate::query::*;

#[cfg(test)]
pub fn ExpectedOutputTestHarness<QP, QR>( GetQueryParameters : QP, GetExpectedQueryResults : QR )
    where QP : Fn() -> QueryParameters,
          QR : Fn() -> QueryResult,
{
    // We don't care about the profiler output for testing correctness.
    let (actualOutput  , actualStats  , _) = GenerateAllValidCharacterStatBlocks(&GetQueryParameters());
    let (expectedOutput, expectedStats, _) = GetExpectedQueryResults();

    println!();

    // Make sure that everything is as expected.
    assert!( actualOutput == expectedOutput, "\nActual Begin\n{:#?}\nActual End\n\n!=\n\nExpected Begin\n{:#?}\nExpected End\n\n", actualOutput, expectedOutput );
    assert!( actualStats  == expectedStats , "\nActual Begin\n{:#?}\nActual End\n\n!=\n\nExpected Begin\n{:#?}\nExpected End\n\n", actualStats , expectedStats  );
}

#[cfg(test)]
pub fn OffByOneTestHarness<QP>( GetQueryParameters : QP )
    where QP : Fn() -> QueryParameters,
{
    let     queryParametersNormal = GetQueryParameters();
    let mut queryParametersExtra  = GetQueryParameters();

    // Displaying more entries shouldn't change the previous
    // ones since the output is sorted best to worst.
    queryParametersExtra.maxNumberOfResults += 100;

    let outputNormal = GenerateAllValidCharacterStatBlocks(&queryParametersNormal);
    let outputExtra  = GenerateAllValidCharacterStatBlocks(&queryParametersExtra );

    println!();

    // Iterate over both outputs, making sure they are the same up to the difference in length.
    for (ref statBlockExtra, ref statBlockNormal) in outputExtra.0.iter().zip( outputNormal.0.iter() )
    {
        assert!( statBlockExtra == statBlockNormal, "\nActual Begin\n{:#?}\nActual End\n\n!=\n\nExpected Begin\n{:#?}\nExpected End\n\n", outputExtra.0, outputNormal.0 );
    }
}

#[cfg(feature = "benchmark")]
pub fn BenchmarkTestHarness<QP>( GetQueryParameters : QP, testName : &str )
    where QP : Fn() -> QueryParameters,
{
    use std::io::{self, Write};

    let TimeQuery = ||
    {
        let (_, _, profiler) = GenerateAllValidCharacterStatBlocks( &GetQueryParameters() );

        GetTotalTimeNS( &profiler )
    };

    print!("{} |", testName);

    let results =
        [
            // Flush the stream between runs so we get an update after every run.
            { print!(" "); io::stdout().flush().unwrap(); let result = TimeQuery(); print!("1"); result },
            { print!("."); io::stdout().flush().unwrap(); let result = TimeQuery(); print!("2"); result },
            { print!("."); io::stdout().flush().unwrap(); let result = TimeQuery(); print!("3"); result },
            { print!("."); io::stdout().flush().unwrap(); let result = TimeQuery(); print!("4"); result },
            { print!("."); io::stdout().flush().unwrap(); let result = TimeQuery(); print!("5"); result },
            { print!("."); io::stdout().flush().unwrap(); let result = TimeQuery(); print!("6"); result },
            { print!("."); io::stdout().flush().unwrap(); let result = TimeQuery(); print!("7"); result },
        ];

    // Sum the values.
    let sum = results.iter().fold(0, |acc, &x| acc + x);

    // Remove outliers.
    let adjustedSum = sum - results.iter().max().unwrap()
                          - results.iter().min().unwrap();

    // Return the calculate average runtime.
    let averageTime = adjustedSum as f64 / (results.len() - 2) as f64;

    println!( " | {:.2}s", averageTime / 1_000_000_000f64 );
}
