// Rust's hash map/sets use SipHash by default, which makes tradeoffs that aren't
// important for our usecase (DOS protection + slower performance for small keys).
//
// Additionally, the DOS protection means that the hasher/key iteration order is
// different at each program run, which makes testing more annoying since the
// query output is no longer completely deterministic.
//
// The default FNV hasher uses a fixed key internally which means that it is
// deterministic.
pub type DeterministicHashMap<K, V> = fnv::FnvHashMap<K, V>;
pub type DeterministicHashSet<K   > = fnv::FnvHashSet<K,  >;
