#!/bin/bash

# This script expects to be run from the tools directory.
ROOT_DIR="../src/query/random_tests"
GLOB_PATTERN="$ROOT_DIR/random_test*.rs"

# Run the Rust program to generate the output files into the specified directory.
cargo run --release --features "generate-query-tests" -- --generate-query-tests $1 --generate-query-tests-root-dir "$ROOT_DIR"

# Clean it up with the Python program so it actually compiles.
python cleanup-rust-pretty-print-output.py "$GLOB_PATTERN"
