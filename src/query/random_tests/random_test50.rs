use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test50" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Flat,
        statOrdering: [
            Wisdom,
            Constitution,
            Strength,
            Intelligence,
            Dexterity,
            Charisma,
        ],
        isReversed: true,
        availableRaces: vec![
            Aarakocra,
            Aasimar_Scourge,
            Dwarf_MarkOfWarding,
            Genasi_Air,
            Genasi_Fire,
            Gnome_Rock,
            Halfling_Ghostwise,
            Halfling_Stout,
            Hobgoblin,
            Human,
            Kalashtar,
            Kobold,
            Shifter_LongTooth,
            Shifter_SwiftStride,
            Tortle,
        ],
        requiredFeats: vec![],
        constraints: vec![],
        numberOfASIs: 0,
        doAllRacesHaveFreeFeat: false,
        canRearrangeRaceModifiers: true,
        maxNumberOfResults: 17,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 749202,
            numUniqueCombinationsPassedConstraints: 749202,
            numStatBlocksEjected: 249,
            numUniqueCombinationsEjected: 251,
            numUniqueCombinationsBelowLowerBound: 748934,
            numStatBlocksShiftedDuringInsertion: 2122,
            numDuplicateRaceStatBlocksRemoved: 0,
            numUniqueStatBlockAfterRacePass: 61,
            numUniqueStatBlockAfterFeatPass: 61,
            numFinalStatBlockCombinations: 17,
        };

    let expectedStatBlocks = 
        vec![
            RankedStatBlock {
                rank: 3,
                statBlock: [8, 15, 8, 15, 6, 17],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 8, 15, 8, 15],
                        myRaceModifiers:     [0, 0, 0, 0, -2, 2],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [8, 17, 8, 15, 6, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 8, 15, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, -2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [8, 15, 8, 17, 6, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 8, 15, 8, 15],
                        myRaceModifiers:     [0, 0, 0, 2, -2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [10, 15, 8, 15, 6, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 8, 15, 8, 15],
                        myRaceModifiers:     [2, 0, 0, 0, -2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [15, 15, 8, 8, 6, 17],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 15, 8, 8, 8, 15],
                        myRaceModifiers:     [0, 0, 0, 0, -2, 2],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [15, 17, 8, 8, 6, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 15, 8, 8, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, -2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [15, 15, 8, 10, 6, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 15, 8, 8, 8, 15],
                        myRaceModifiers:     [0, 0, 0, 2, -2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [15, 8, 8, 15, 6, 17],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [0, 0, 0, 0, -2, 2],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [15, 10, 8, 15, 6, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, -2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [15, 15, 8, 15, 6, 10],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 15, 8, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, -2, 2],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [15, 17, 8, 15, 6, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 15, 8, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, -2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [15, 8, 8, 17, 6, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [0, 0, 0, 2, -2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [15, 15, 8, 17, 6, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 15, 8, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, -2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [17, 15, 8, 8, 6, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 15, 8, 8, 8, 15],
                        myRaceModifiers:     [2, 0, 0, 0, -2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [17, 8, 8, 15, 6, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [2, 0, 0, 0, -2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [17, 15, 8, 15, 6, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 15, 8, 15, 8, 8],
                        myRaceModifiers:     [2, 0, 0, 0, -2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [9, 15, 9, 14, 6, 17],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [9, 15, 9, 14, 8, 15],
                        myRaceModifiers:     [0, 0, 0, 0, -2, 2],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
        ];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

