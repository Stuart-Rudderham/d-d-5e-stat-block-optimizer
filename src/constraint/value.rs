#![allow(non_snake_case)]
#![allow(deprecated)]
#![allow(non_camel_case_types)]

use crate::dnd::stat::*;
use crate::dnd::statblock::*;

pub trait GetConstraintValue
{
    fn GetValue(&self, aStatBlock : StatBlock) -> i8;

    fn GetMaxValue(aStatBlock : StatBlock) -> i8;
    fn GetMinValue(aStatBlock : StatBlock) -> i8;
}

#[derive(Debug, Eq, PartialEq, Copy, Clone)]
pub struct GetConstant(pub i8);

impl GetConstraintValue for GetConstant
{
    fn GetValue(&self, _ : StatBlock) -> i8
    {
        let &GetConstant(aNum) = self;

        aNum
    }

    fn GetMaxValue(_ : StatBlock) -> i8
    {
        // This function doesn't really make sense for this struct.
        i8::max_value()
    }

    // This function doesn't really make sense for this struct.
    fn GetMinValue(_ : StatBlock) -> i8
    {
        // This function doesn't really make sense for this struct.
        i8::min_value()
    }
}

#[derive(Debug, Eq, PartialEq, Copy, Clone)]
pub struct GetStatValue(pub Stat);

impl GetConstraintValue for GetStatValue
{
    fn GetValue(&self, aStatBlock : StatBlock) -> i8
    {
        let &GetStatValue(aStat) = self;

        aStatBlock[ StatToIndex(aStat) ]
    }

    fn GetMaxValue(aStatBlock : StatBlock) -> i8
    {
        *aStatBlock.iter().max().unwrap()
    }

    fn GetMinValue(aStatBlock : StatBlock) -> i8
    {
        *aStatBlock.iter().min().unwrap()
    }
}

#[derive(Debug, Eq, PartialEq, Copy, Clone)]
pub struct GetStatModifier(pub Stat);

impl GetConstraintValue for GetStatModifier
{
    fn GetValue(&self, aStatBlock : StatBlock) -> i8
    {
        let &GetStatModifier(aStat) = self;

        let statValue = GetStatValue(aStat).GetValue(aStatBlock);

        StatValueToModifier( statValue )
    }

    fn GetMaxValue(aStatBlock : StatBlock) -> i8
    {
        aStatBlock.iter().map( |&aStatValue| StatValueToModifier(aStatValue) ).max().unwrap()
    }

    fn GetMinValue(aStatBlock : StatBlock) -> i8
    {
        aStatBlock.iter().map( |&aStatValue| StatValueToModifier(aStatValue) ).min().unwrap()
    }
}

//
// These add a lot of complexity and extra cases for a fairly uncommon use case.
// Disabling until I really decide I want them.
//
/*
#[derive(Debug, Eq, PartialEq)]
pub struct GetMetaStatValueBinCmp(pub BinCmp, pub i8);

impl GetConstraintValue for GetMetaStatValueBinCmp
{
    fn GetValue(&self, aStatBlock : &StatBlock) -> i8
    {
        let &GetMetaStatValueBinCmp(aBinCmp, aConstant) = self;

        let strConstraint = BinaryCompare { lhsValue : GetStatValue(Stat::Strength    ), binCmp : aBinCmp, rhsValue : GetConstant(aConstant), };
        let dexConstraint = BinaryCompare { lhsValue : GetStatValue(Stat::Dexterity   ), binCmp : aBinCmp, rhsValue : GetConstant(aConstant), };
        let conConstraint = BinaryCompare { lhsValue : GetStatValue(Stat::Constitution), binCmp : aBinCmp, rhsValue : GetConstant(aConstant), };
        let intConstraint = BinaryCompare { lhsValue : GetStatValue(Stat::Intelligence), binCmp : aBinCmp, rhsValue : GetConstant(aConstant), };
        let wisConstraint = BinaryCompare { lhsValue : GetStatValue(Stat::Wisdom      ), binCmp : aBinCmp, rhsValue : GetConstant(aConstant), };
        let chaConstraint = BinaryCompare { lhsValue : GetStatValue(Stat::Charisma    ), binCmp : aBinCmp, rhsValue : GetConstant(aConstant), };

        0
        + if strConstraint.IsStatBlockValid(aStatBlock) { 1 } else { 0 }
        + if dexConstraint.IsStatBlockValid(aStatBlock) { 1 } else { 0 }
        + if conConstraint.IsStatBlockValid(aStatBlock) { 1 } else { 0 }
        + if intConstraint.IsStatBlockValid(aStatBlock) { 1 } else { 0 }
        + if wisConstraint.IsStatBlockValid(aStatBlock) { 1 } else { 0 }
        + if chaConstraint.IsStatBlockValid(aStatBlock) { 1 } else { 0 }

        /*
        [ Stat::Strength    ,
         Stat::Dexterity    ,
         Stat::Constitution ,
         Stat::Intelligence ,
         Stat::Wisdom       ,
         Stat::Charisma     ].iter().map()
         */
    }
}

#[derive(Debug, Eq, PartialEq)]
pub struct GetMetaStatValueUnCmp(pub UnCmp);

impl GetConstraintValue for GetMetaStatValueUnCmp
{
    fn GetValue(&self, aStatBlock : &StatBlock) -> i8
    {
        let &GetMetaStatValueUnCmp(anUnCmp) = self;

        let strConstraint = UnaryCompare { value : GetStatValue(Stat::Strength    ), unCmp : anUnCmp, };
        let dexConstraint = UnaryCompare { value : GetStatValue(Stat::Dexterity   ), unCmp : anUnCmp, };
        let conConstraint = UnaryCompare { value : GetStatValue(Stat::Constitution), unCmp : anUnCmp, };
        let intConstraint = UnaryCompare { value : GetStatValue(Stat::Intelligence), unCmp : anUnCmp, };
        let wisConstraint = UnaryCompare { value : GetStatValue(Stat::Wisdom      ), unCmp : anUnCmp, };
        let chaConstraint = UnaryCompare { value : GetStatValue(Stat::Charisma    ), unCmp : anUnCmp, };

        0
        + if strConstraint.IsStatBlockValid(aStatBlock) { 1 } else { 0 }
        + if dexConstraint.IsStatBlockValid(aStatBlock) { 1 } else { 0 }
        + if conConstraint.IsStatBlockValid(aStatBlock) { 1 } else { 0 }
        + if intConstraint.IsStatBlockValid(aStatBlock) { 1 } else { 0 }
        + if wisConstraint.IsStatBlockValid(aStatBlock) { 1 } else { 0 }
        + if chaConstraint.IsStatBlockValid(aStatBlock) { 1 } else { 0 }
    }
}

#[derive(Debug, Eq, PartialEq)]
pub struct GetMetaStatModifierBinCmp(pub BinCmp, pub i8);

impl GetConstraintValue for GetMetaStatModifierBinCmp
{
    fn GetValue(&self, aStatBlock : &StatBlock) -> i8
    {
        let &GetMetaStatModifierBinCmp(aBinCmp, aConstant) = self;

        let strConstraint = BinaryCompare { lhsValue : GetStatModifier(Stat::Strength    ), binCmp : aBinCmp, rhsValue : GetConstant(aConstant), };
        let dexConstraint = BinaryCompare { lhsValue : GetStatModifier(Stat::Dexterity   ), binCmp : aBinCmp, rhsValue : GetConstant(aConstant), };
        let conConstraint = BinaryCompare { lhsValue : GetStatModifier(Stat::Constitution), binCmp : aBinCmp, rhsValue : GetConstant(aConstant), };
        let intConstraint = BinaryCompare { lhsValue : GetStatModifier(Stat::Intelligence), binCmp : aBinCmp, rhsValue : GetConstant(aConstant), };
        let wisConstraint = BinaryCompare { lhsValue : GetStatModifier(Stat::Wisdom      ), binCmp : aBinCmp, rhsValue : GetConstant(aConstant), };
        let chaConstraint = BinaryCompare { lhsValue : GetStatModifier(Stat::Charisma    ), binCmp : aBinCmp, rhsValue : GetConstant(aConstant), };

        0
        + if strConstraint.IsStatBlockValid(aStatBlock) { 1 } else { 0 }
        + if dexConstraint.IsStatBlockValid(aStatBlock) { 1 } else { 0 }
        + if conConstraint.IsStatBlockValid(aStatBlock) { 1 } else { 0 }
        + if intConstraint.IsStatBlockValid(aStatBlock) { 1 } else { 0 }
        + if wisConstraint.IsStatBlockValid(aStatBlock) { 1 } else { 0 }
        + if chaConstraint.IsStatBlockValid(aStatBlock) { 1 } else { 0 }
    }
}

#[derive(Debug, Eq, PartialEq)]
pub struct GetMetaStatModifierUnCmp(pub UnCmp);

impl GetConstraintValue for GetMetaStatModifierUnCmp
{
    fn GetValue(&self, aStatBlock : &StatBlock) -> i8
    {
        let &GetMetaStatModifierUnCmp(anUnCmp) = self;

        let strConstraint = UnaryCompare { value : GetStatModifier(Stat::Strength    ), unCmp : anUnCmp, };
        let dexConstraint = UnaryCompare { value : GetStatModifier(Stat::Dexterity   ), unCmp : anUnCmp, };
        let conConstraint = UnaryCompare { value : GetStatModifier(Stat::Constitution), unCmp : anUnCmp, };
        let intConstraint = UnaryCompare { value : GetStatModifier(Stat::Intelligence), unCmp : anUnCmp, };
        let wisConstraint = UnaryCompare { value : GetStatModifier(Stat::Wisdom      ), unCmp : anUnCmp, };
        let chaConstraint = UnaryCompare { value : GetStatModifier(Stat::Charisma    ), unCmp : anUnCmp, };

        0
        + if strConstraint.IsStatBlockValid(aStatBlock) { 1 } else { 0 }
        + if dexConstraint.IsStatBlockValid(aStatBlock) { 1 } else { 0 }
        + if conConstraint.IsStatBlockValid(aStatBlock) { 1 } else { 0 }
        + if intConstraint.IsStatBlockValid(aStatBlock) { 1 } else { 0 }
        + if wisConstraint.IsStatBlockValid(aStatBlock) { 1 } else { 0 }
        + if chaConstraint.IsStatBlockValid(aStatBlock) { 1 } else { 0 }
    }
}
*/
