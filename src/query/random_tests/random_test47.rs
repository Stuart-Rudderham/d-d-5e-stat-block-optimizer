use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test47" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Weighted,
        statOrdering: [
            Charisma,
            Wisdom,
            Intelligence,
            Dexterity,
            Strength,
            Constitution,
        ],
        isReversed: false,
        availableRaces: vec![
            Aasimar_Scourge,
            Bugbear,
            CustomLineage,
            Dwarf_Grey,
            Genasi_Air,
            Genasi_Water,
            Gnome_Deep,
            Gnome_Forest,
            Gnome_Rock,
            HalfElf,
            Halfling_Ghostwise,
            Hobgoblin,
            Human,
            Human_Variant,
            Kenku,
            Kobold,
            Lizardfolk,
            Shifter_SwiftStride,
            Tabaxi,
            Tortle,
            Triton,
            Warforged,
        ],
        requiredFeats: vec![
            Linguist,
        ],
        constraints: vec![
            BinCmp_StatModifier_StatModifier(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Wisdom,
                    ),
                    binCmp: GE,
                    rhsValue: GetStatModifier(
                        Wisdom,
                    ),
                },
            ),
            BinCmp_StatModifier_StatModifier(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Charisma,
                    ),
                    binCmp: GE,
                    rhsValue: GetStatModifier(
                        Intelligence,
                    ),
                },
            ),
            UnCmp_StatModifier(
                UnaryCompare {
                    value: GetStatModifier(
                        Intelligence,
                    ),
                    unCmp: IsMax,
                },
            ),
        ],
        numberOfASIs: 1,
        doAllRacesHaveFreeFeat: false,
        canRearrangeRaceModifiers: true,
        maxNumberOfResults: 29,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 3097245,
            numUniqueCombinationsPassedConstraints: 212925,
            numStatBlocksEjected: 163,
            numUniqueCombinationsEjected: 224,
            numUniqueCombinationsBelowLowerBound: 212512,
            numStatBlocksShiftedDuringInsertion: 2220,
            numDuplicateRaceStatBlocksRemoved: 120,
            numUniqueStatBlockAfterRacePass: 162,
            numUniqueStatBlockAfterFeatPass: 162,
            numFinalStatBlockCombinations: 69,
        };

    let expectedStatBlocks = 
        vec![
            RankedStatBlock {
                rank: 30102,
                statBlock: [8, 12, 8, 16, 16, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [8, 12, 8, 13, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 1, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [8, 12, 8, 13, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 2, 1, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [8, 12, 8, 13, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 30102,
                statBlock: [12, 8, 8, 16, 16, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [12, 8, 8, 13, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 1, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [12, 8, 8, 13, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 2, 1, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [12, 8, 8, 13, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 30102,
                statBlock: [8, 8, 12, 16, 16, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [8, 8, 12, 13, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 1, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [8, 8, 12, 13, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 2, 1, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [8, 8, 12, 13, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 30102,
                statBlock: [8, 16, 8, 16, 12, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [8, 13, 8, 15, 12, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [8, 14, 8, 14, 12, 15],
                        myRaceModifiers:     [0, 2, 0, 1, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [8, 13, 8, 15, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 30102,
                statBlock: [16, 8, 8, 16, 12, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [13, 8, 8, 15, 12, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [1, 0, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [14, 8, 8, 14, 12, 15],
                        myRaceModifiers:     [2, 0, 0, 1, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [13, 8, 8, 15, 12, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 1],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 30102,
                statBlock: [8, 8, 16, 16, 12, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [8, 8, 13, 15, 12, 15],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 1, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [8, 8, 14, 14, 12, 15],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [8, 8, 13, 15, 12, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 1],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 30102,
                statBlock: [12, 16, 8, 16, 8, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [12, 13, 8, 15, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [12, 14, 8, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 1, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [12, 13, 8, 15, 8, 15],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 30102,
                statBlock: [8, 16, 12, 16, 8, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [8, 13, 12, 15, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [8, 14, 12, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 1, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [8, 13, 12, 15, 8, 15],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 30102,
                statBlock: [16, 12, 8, 16, 8, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [13, 12, 8, 15, 8, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [1, 0, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [14, 12, 8, 14, 8, 15],
                        myRaceModifiers:     [2, 0, 0, 1, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [13, 12, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 1],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 30102,
                statBlock: [8, 12, 16, 16, 8, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [8, 12, 13, 15, 8, 15],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 1, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [8, 12, 14, 14, 8, 15],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [8, 12, 13, 15, 8, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 1],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 30102,
                statBlock: [16, 8, 12, 16, 8, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [13, 8, 12, 15, 8, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [1, 0, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [14, 8, 12, 14, 8, 15],
                        myRaceModifiers:     [2, 0, 0, 1, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [13, 8, 12, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 1],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 30102,
                statBlock: [12, 8, 16, 16, 8, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [12, 8, 13, 15, 8, 15],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 1, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [12, 8, 14, 14, 8, 15],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [12, 8, 13, 15, 8, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 1],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 30021,
                statBlock: [10, 10, 9, 16, 16, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [9, 9, 8, 14, 15, 15],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 30021,
                statBlock: [10, 10, 8, 16, 16, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [10, 10, 8, 13, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 1, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [10, 10, 8, 13, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 2, 1, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [10, 10, 8, 13, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 30021,
                statBlock: [9, 10, 10, 16, 16, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [8, 9, 9, 14, 15, 15],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 30021,
                statBlock: [8, 10, 10, 16, 16, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [8, 10, 10, 13, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 1, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [8, 10, 10, 13, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 2, 1, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [8, 10, 10, 13, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 30021,
                statBlock: [10, 9, 10, 16, 16, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [9, 8, 9, 14, 15, 15],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 30021,
                statBlock: [10, 8, 10, 16, 16, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [10, 8, 10, 13, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 1, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [10, 8, 10, 13, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 2, 1, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [10, 8, 10, 13, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 30021,
                statBlock: [10, 16, 9, 16, 10, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [9, 15, 8, 14, 9, 15],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 30021,
                statBlock: [10, 16, 8, 16, 10, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [10, 13, 8, 15, 10, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [10, 14, 8, 14, 10, 15],
                        myRaceModifiers:     [0, 2, 0, 1, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [10, 13, 8, 15, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 30021,
                statBlock: [9, 16, 10, 16, 10, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [8, 15, 9, 14, 9, 15],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 30021,
                statBlock: [8, 16, 10, 16, 10, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [8, 13, 10, 15, 10, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [8, 14, 10, 14, 10, 15],
                        myRaceModifiers:     [0, 2, 0, 1, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [8, 13, 10, 15, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 30021,
                statBlock: [16, 10, 9, 16, 10, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [15, 9, 8, 14, 9, 15],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 30021,
                statBlock: [16, 10, 8, 16, 10, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [13, 10, 8, 15, 10, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [1, 0, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [14, 10, 8, 14, 10, 15],
                        myRaceModifiers:     [2, 0, 0, 1, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [13, 10, 8, 15, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 1],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 30021,
                statBlock: [9, 10, 16, 16, 10, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [8, 9, 15, 14, 9, 15],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 30021,
                statBlock: [8, 10, 16, 16, 10, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [8, 10, 13, 15, 10, 15],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 1, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [8, 10, 14, 14, 10, 15],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [8, 10, 13, 15, 10, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 1],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 30021,
                statBlock: [16, 9, 10, 16, 10, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [15, 8, 9, 14, 9, 15],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 30021,
                statBlock: [10, 9, 16, 16, 10, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [9, 8, 15, 14, 9, 15],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 30021,
                statBlock: [16, 8, 10, 16, 10, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [13, 8, 10, 15, 10, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [1, 0, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [14, 8, 10, 14, 10, 15],
                        myRaceModifiers:     [2, 0, 0, 1, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [13, 8, 10, 15, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 1],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
        ];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

