#![allow(non_snake_case)]
#![allow(deprecated)]
#![allow(non_camel_case_types)]

use crate::dnd::statblock::*;
use crate::constraint::value::*;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum UnCmp
{
    IsEven,
    IsOdd,
    IsMax,
    IsMin,
}

#[derive(Debug, Eq, PartialEq, Copy, Clone)]
pub struct UnaryCompare<V: GetConstraintValue>
{
    pub value   : V         ,
    pub unCmp   : UnCmp     ,
}

impl<V: GetConstraintValue> UnaryCompare<V>
{
    pub fn IsStatBlockValid(&self, aStatBlock : StatBlock) -> bool
    {
        let value = self.value.GetValue(aStatBlock);

        match self.unCmp
        {
            UnCmp::IsEven => value % 2 == 0,
            UnCmp::IsOdd  => value % 2 == 1,
            UnCmp::IsMax  => value == V::GetMaxValue(aStatBlock),
            UnCmp::IsMin  => value == V::GetMinValue(aStatBlock),
        }
    }

    pub fn New(aValue : V, anUnCmp : UnCmp) -> UnaryCompare<V>
    {
        UnaryCompare
        {
            value       : aValue    ,
            unCmp       : anUnCmp   ,
        }
    }
}
