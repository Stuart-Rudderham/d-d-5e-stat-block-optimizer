#![allow(non_upper_case_globals)]

use std::cmp::Ordering;

use crate::dnd::race::*;
use crate::dnd::feat::*;
use crate::dnd::stat::*;

pub type StatValue = i8;
pub type StatModifier = i8;

pub const MaxStatValue : i8 = 20;

pub fn StatValueToModifier( aStatValue : StatValue ) -> StatModifier
{
    match aStatValue
    {
         0 |  1 => -5,
         2 |  3 => -4,
         4 |  5 => -3,
         6 |  7 => -2,
         8 |  9 => -1,
        10 | 11 =>  0,
        12 | 13 =>  1,
        14 | 15 =>  2,
        16 | 17 =>  3,
        18 | 19 =>  4,
        20 | 21 =>  5,
        22 | 23 =>  6,
        24 | 25 =>  7,
        26 | 27 =>  8,
        28 | 29 =>  9,
        _ => panic!("Unable to convert stat value [{}] to modifier, add entry to match statement", aStatValue),
    }

    // Explicit match statement was faster than doing float math.
    //(((aStatValue as f32) - 10f32) / 2f32).floor() as i8
}

#[test]
fn StatValueToStatModifierTest()
{
    assert_eq!( StatValueToModifier(0 ), -5 );
    assert_eq!( StatValueToModifier(1 ), -5 );
    assert_eq!( StatValueToModifier(2 ), -4 );
    assert_eq!( StatValueToModifier(3 ), -4 );
    assert_eq!( StatValueToModifier(4 ), -3 );
    assert_eq!( StatValueToModifier(5 ), -3 );
    assert_eq!( StatValueToModifier(6 ), -2 );
    assert_eq!( StatValueToModifier(7 ), -2 );
    assert_eq!( StatValueToModifier(8 ), -1 );
    assert_eq!( StatValueToModifier(9 ), -1 );
    assert_eq!( StatValueToModifier(10),  0 );
    assert_eq!( StatValueToModifier(11),  0 );
    assert_eq!( StatValueToModifier(12),  1 );
    assert_eq!( StatValueToModifier(13),  1 );
    assert_eq!( StatValueToModifier(14),  2 );
    assert_eq!( StatValueToModifier(15),  2 );
    assert_eq!( StatValueToModifier(16),  3 );
    assert_eq!( StatValueToModifier(17),  3 );
    assert_eq!( StatValueToModifier(18),  4 );
    assert_eq!( StatValueToModifier(19),  4 );
    assert_eq!( StatValueToModifier(20),  5 );
}

pub type StatBlock = [StatValue; 6];

pub fn IsZeroStatBlock( aStatBlock : StatBlock ) -> bool
{
    aStatBlock.iter().all(|&x| x == 0)
}

pub fn IsStatBlockAboveMaxValue( aStatBlock : StatBlock ) -> bool
{
    aStatBlock.iter().any( |&x| x > MaxStatValue )
}

#[test]
fn IsZeroStatBlockTest()
{
    assert_eq!(IsZeroStatBlock( [0, 0, 0, 0, 0, 0] ), true  );

    assert_eq!(IsZeroStatBlock( [1, 0, 0, 0, 0, 0] ), false );
    assert_eq!(IsZeroStatBlock( [0, 1, 0, 0, 0, 0] ), false );
    assert_eq!(IsZeroStatBlock( [0, 0, 1, 0, 0, 0] ), false );
    assert_eq!(IsZeroStatBlock( [0, 0, 0, 1, 0, 0] ), false );
    assert_eq!(IsZeroStatBlock( [0, 0, 0, 0, 1, 0] ), false );
    assert_eq!(IsZeroStatBlock( [0, 0, 0, 0, 0, 1] ), false );
}

pub fn CombineStatBlocks2( statBlock1 : StatBlock, statBlock2 : StatBlock ) -> StatBlock
{
    [ statBlock1[0] + statBlock2[0] ,
      statBlock1[1] + statBlock2[1] ,
      statBlock1[2] + statBlock2[2] ,
      statBlock1[3] + statBlock2[3] ,
      statBlock1[4] + statBlock2[4] ,
      statBlock1[5] + statBlock2[5] ]
}

// This function is useful to have but isn't currently used anywhere outside of tests.
#[cfg(test)]
pub fn CombineStatBlocks3( statBlock1 : StatBlock, statBlock2 : StatBlock, statBlock3 : StatBlock ) -> StatBlock
{
    CombineStatBlocks2( CombineStatBlocks2(statBlock1, statBlock2), statBlock3)
}

pub fn CombineStatBlocks4( statBlock1 : StatBlock, statBlock2 : StatBlock, statBlock3 : StatBlock, statBlock4 : StatBlock ) -> StatBlock
{
    CombineStatBlocks2( CombineStatBlocks2(statBlock1, statBlock2), CombineStatBlocks2(statBlock3, statBlock4) )
}

#[test]
fn CombineStatBlocksTest()
{
    assert_eq!
    (
        CombineStatBlocks2
        (
            [1, 2, 3, 4, 5, 6],
            [2, 3, 4, 5, 6, 7]
        ),
        [3, 5, 7, 9, 11, 13]
    );

    assert_eq!
    (
        CombineStatBlocks2
        (
            [ 1,  2,  3,  4,  5,  6],
            [-1, -2, -3, -4, -5, -6],
        ),
        [0, 0, 0, 0, 0, 0]
    );

    assert_eq!
    (
        CombineStatBlocks3
        (
            [1, 2, 3, 4, 5, 6],
            [2, 3, 4, 5, 6, 7],
            [3, 4, 5, 6, 7, 8]
        ),
        [6, 9, 12, 15, 18, 21]
    );

    assert_eq!
    (
        CombineStatBlocks4
        (
            [1, 2, 3, 4, 5, 6],
            [2, 3, 4, 5, 6, 7],
            [3, 4, 5, 6, 7, 8],
            [4, 5, 6, 7, 8, 9],
        ),
        [10, 14, 18, 22, 26, 30]
    );
}

pub fn GenerateAllUniquePermutationsOfStatBlock( mut aStatBlock : StatBlock) -> Vec<StatBlock>
{
    use superslice::Ext;

    // There are 6 different stats, so there are at most 6! = 720 unique permutations.
    let mut result = Vec::with_capacity(6 * 5 * 4 * 3 * 2 * 1);

    // We need to start at the "first" permutation lexicographically so that we know when
    // when next_permutation() has wrapped around and covered all permutations.
    aStatBlock.sort();

    loop
    {
        result.push(aStatBlock);

        let isFirstPermutation = aStatBlock.next_permutation() == false;
        if isFirstPermutation
        {
            break;
        }
    }

    result
}

#[test]
fn TestGenerateAllUniquePermutationsOfStatBlock()
{
    let IsEveryEntryUnique = |aStatBlockList : &[StatBlock]|
    {
        let mut statBlockVector = aStatBlockList.to_vec();

        let oldSize = statBlockVector.len();

        // Remove all the duplicate elements.
        statBlockVector.sort();
        statBlockVector.dedup();

        let newSize = statBlockVector.len();

        // Check if any elements were actually removed.
        return oldSize == newSize;
    };

    // Tests for the tests.
    assert!( IsEveryEntryUnique(&vec![ [1, 2, 3, 4, 5, 6], [6, 5, 4, 3, 2, 1], [6, 1, 5, 4, 3, 2] ]) == true );
    assert!( IsEveryEntryUnique(&vec![ [1, 2, 3, 4, 5, 6], [6, 5, 4, 3, 2, 1], [1, 2, 3, 4, 5, 6] ]) == false );

    let TestPermutation = |aStatBlock : StatBlock, expectedPermutationLen : usize|
    {
        let         input = aStatBlock;
        let reversedInput = { let mut x = aStatBlock; x.reverse(); x };

        let perm1 = GenerateAllUniquePermutationsOfStatBlock( input );

        // It's a permutation so we know how many elements there should be.
        assert_eq!(perm1.len(), expectedPermutationLen);

        // It's a permutation so every element should be unique.
        assert!( IsEveryEntryUnique(&perm1) );

        // Make sure the reverse input gives the same permutation.
        let perm2 = GenerateAllUniquePermutationsOfStatBlock( reversedInput );
        assert_eq!(perm1, perm2);

        // Make sure the original inputs exist somewhere in the permutation.
        assert!( perm1.contains(&        input) );
        assert!( perm1.contains(&reversedInput) );
        assert!( perm2.contains(&        input) );
        assert!( perm2.contains(&reversedInput) );
    };

    TestPermutation( [1, 2, 3, 4, 5, 6], 720 / (                    1) );
    TestPermutation( [1, 1, 3, 4, 5, 6], 720 / (                2 * 1) );
    TestPermutation( [1, 1, 1, 4, 5, 6], 720 / (            3 * 2 * 1) );
    TestPermutation( [1, 1, 1, 1, 5, 6], 720 / (        4 * 3 * 2 * 1) );
    TestPermutation( [1, 1, 1, 1, 1, 6], 720 / (    5 * 4 * 3 * 2 * 1) );
    TestPermutation( [1, 1, 1, 1, 1, 1], 720 / (6 * 5 * 4 * 3 * 2 * 1) );

    // Make sure the "lowest" stat block is always first.
    assert_eq!( GenerateAllUniquePermutationsOfStatBlock([6, 5, 4, 1, 2, 3]).first().unwrap(), &[1, 2, 3, 4, 5, 6] );
}

// This function is useful to have but isn't currently used anywhere outside of tests.
#[cfg(test)]
pub fn IndexToStat(anIndex : usize) -> Stat
{
    match anIndex
    {
        0 => Stat::Strength         ,
        1 => Stat::Dexterity        ,
        2 => Stat::Constitution     ,
        3 => Stat::Intelligence     ,
        4 => Stat::Wisdom           ,
        5 => Stat::Charisma         ,
        _ => panic!(),
    }
}

pub fn StatToIndex(aStat : Stat) -> usize
{
    match aStat
    {
        Stat::Strength      => 0    ,
        Stat::Dexterity     => 1    ,
        Stat::Constitution  => 2    ,
        Stat::Intelligence  => 3    ,
        Stat::Wisdom        => 4    ,
        Stat::Charisma      => 5    ,
    }
}

#[test]
fn StatIndexRoundTrip()
{
    assert_eq!( Stat::Strength    , IndexToStat(StatToIndex(Stat::Strength    )) );
    assert_eq!( Stat::Dexterity   , IndexToStat(StatToIndex(Stat::Dexterity   )) );
    assert_eq!( Stat::Constitution, IndexToStat(StatToIndex(Stat::Constitution)) );
    assert_eq!( Stat::Intelligence, IndexToStat(StatToIndex(Stat::Intelligence)) );
    assert_eq!( Stat::Wisdom      , IndexToStat(StatToIndex(Stat::Wisdom      )) );
    assert_eq!( Stat::Charisma    , IndexToStat(StatToIndex(Stat::Charisma    )) );
}

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq, Clone)]
pub struct CharacterStatBlock
{
    pub myRace                      : CharacterRace     ,   // What race was used.
    pub myFeats                     : Vec<Feat>         ,   // What feats (if any) were used.

    pub myBasePointBuyArray         : StatBlock         ,   // What point buy array was used.
    pub myRaceModifiers             : StatBlock         ,   // Racial stat modifiers (includes choices that Human/Half-Elf get to make).
    pub myASIs                      : StatBlock         ,   // What ASIs were used.
    pub myFeatModifiers             : StatBlock         ,   // What stat points all feats gave.
}

pub type StatBlockRank = i32;

#[derive(Debug, Eq, PartialEq)]
pub struct RankedStatBlock
{
    pub rank                : StatBlockRank             ,   // The stat block's rank.
    pub statBlock           : StatBlock                 ,   // The stat block itself.
    pub combinations        : Vec<CharacterStatBlock>   ,   // The list of different ways that stat block can be created.
}

pub type StatOrdering = [Stat; 6];

pub const DefaultStatOrdering : StatOrdering =
    [
        Stat::Strength      ,
        Stat::Dexterity     ,
        Stat::Constitution  ,
        Stat::Intelligence  ,
        Stat::Wisdom        ,
        Stat::Charisma      ,
    ];

fn OrderStatBlock( aStatBlock : StatBlock, statOrdering : StatOrdering ) -> StatBlock
{
    [
        aStatBlock[ StatToIndex(statOrdering[0]) ],
        aStatBlock[ StatToIndex(statOrdering[1]) ],
        aStatBlock[ StatToIndex(statOrdering[2]) ],
        aStatBlock[ StatToIndex(statOrdering[3]) ],
        aStatBlock[ StatToIndex(statOrdering[4]) ],
        aStatBlock[ StatToIndex(statOrdering[5]) ],
    ]
}

#[test]
fn TestOrderStatBlock()
{
    use crate::dnd::stat::Stat::*;

    assert_eq!( OrderStatBlock([1, 2, 3, 4, 5, 6], [Strength    , Dexterity   , Constitution, Intelligence, Wisdom      , Charisma    ]), [1, 2, 3, 4, 5, 6] ); // Identity transform.
    assert_eq!( OrderStatBlock([1, 2, 3, 4, 5, 6], [Strength    , Charisma    , Intelligence, Dexterity   , Wisdom      , Constitution]), [1, 6, 4, 2, 5, 3] ); // 6 random permutations
    assert_eq!( OrderStatBlock([1, 2, 3, 4, 5, 6], [Dexterity   , Strength    , Constitution, Charisma    , Wisdom      , Intelligence]), [2, 1, 3, 6, 5, 4] );
    assert_eq!( OrderStatBlock([1, 2, 3, 4, 5, 6], [Constitution, Dexterity   , Intelligence, Strength    , Charisma    , Wisdom      ]), [3, 2, 4, 1, 6, 5] );
    assert_eq!( OrderStatBlock([1, 2, 3, 4, 5, 6], [Intelligence, Strength    , Wisdom      , Charisma    , Constitution, Dexterity   ]), [4, 1, 5, 6, 3, 2] );
    assert_eq!( OrderStatBlock([1, 2, 3, 4, 5, 6], [Wisdom      , Charisma    , Dexterity   , Constitution, Strength    , Intelligence]), [5, 6, 2, 3, 1, 4] );
    assert_eq!( OrderStatBlock([1, 2, 3, 4, 5, 6], [Charisma    , Strength    , Wisdom      , Dexterity   , Intelligence, Constitution]), [6, 1, 5, 2, 4, 3] );
}

// We pass in the rank separately so it can be cached outside this function
// and not recalculate it every time we compare the same stat block.
pub fn CompareStatBlock( lhs : (StatBlockRank, StatBlock), rhs : (StatBlockRank, StatBlock), statOrdering : StatOrdering ) -> Ordering
{
    let (lhsRank, lhsStatBlock) = lhs;
    let (rhsRank, rhsStatBlock) = rhs;

    match lhsRank.cmp(&rhsRank)
    {
        Ordering::Less    => Ordering::Less,
        Ordering::Greater => Ordering::Greater,
        Ordering::Equal   =>
        {
            let lhsOrderedStatBlock = OrderStatBlock(lhsStatBlock, statOrdering);
            let rhsOrderedStatBlock = OrderStatBlock(rhsStatBlock, statOrdering);

            lhsOrderedStatBlock.cmp(&rhsOrderedStatBlock)
        },
    }
}

#[test]
fn TestCompareStatBlock()
{
    use crate::dnd::stat::Stat::*;

    let statOrdering1 = [Strength, Dexterity, Constitution, Intelligence, Wisdom  , Charisma ];
    let statOrdering2 = [Strength, Dexterity, Constitution, Intelligence, Charisma, Wisdom  ];

    // Different ranks.
    assert_eq!( Ordering::Less, CompareStatBlock
    (
        (1, [1, 2, 3, 4, 5, 6]),
        (2, [1, 2, 3, 4, 5, 6]),
        statOrdering1
    ));

    assert_eq!( Ordering::Greater, CompareStatBlock
    (
        (2, [1, 2, 3, 4, 5, 6]),
        (1, [1, 2, 3, 4, 5, 6]),
        statOrdering1
    ));

    // Same rank, different stat blocks
    assert_eq!( Ordering::Greater, CompareStatBlock
    (
        (1, [1, 1, 1, 1, 2, 1]),
        (1, [1, 1, 1, 1, 1, 2]),
        statOrdering1
    ));

    assert_eq!( Ordering::Less, CompareStatBlock
    (
        (1, [1, 1, 1, 1, 2, 1]),
        (1, [1, 1, 1, 1, 1, 2]),
        statOrdering2
    ));

    assert_eq!( Ordering::Greater, CompareStatBlock
    (
        (1, [1, 0, 0, 0, 0, 0]),
        (1, [0, 9, 9, 9, 9, 9]),
        statOrdering1
    ));

    // Same everything.
    assert_eq!( Ordering::Equal, CompareStatBlock
    (
        (1, [1, 2, 3, 4, 5, 6]),
        (1, [1, 2, 3, 4, 5, 6]),
        statOrdering1
    ));

    assert_eq!( Ordering::Equal, CompareStatBlock
    (
        (1, [1, 2, 3, 4, 5, 6]),
        (1, [1, 2, 3, 4, 5, 6]),
        statOrdering2
    ));
}

#[derive(Debug, Copy, Clone)]
pub enum RankingStyle
{
    Flat        ,       // (+1, +1, +1) == (+3, +0, +0)
    Weighted    ,       // (+2, +2, +2) <  (+3, +0, +0)
    Variance    ,       // (+1, +3, +3) <  (+2, +2, +2)
    Unranked    ,       // (+0, +0, +0) == (+3, +3, +3)
}

fn RankStatBlock_Flat(aStatBlock : StatBlock) -> StatBlockRank
{
    // Sum the stat modifiers.
    let s = aStatBlock.iter()
                      .map( |stat| StatValueToModifier(*stat) )
                      .sum::<StatModifier>();

    StatBlockRank::from(s)
}

fn RankStatBlock_Weighted(aStatBlock : StatBlock) -> StatBlockRank
{
// TODO: Having an explicit lookup table is way faster than calling .pow(x)
/*
    let Foo = |aStatModifer|
    {
        match aStatModifer
        {
            -2 =>          0,
            -1 =>         10,
             0 =>        100,
             1 =>      1_000,
             2 =>     10_000,
             3 =>    100_000,
             4 =>  1_000_000,
             5 => 10_000_000,
            _ => panic!("Unable to convert stat value [{}] to modifier, add entry to match statement", aStatModifer),
        }
    };

    // Sum the exponential stat modifiers, so higher modifiers have much much more weight.
    aStatBlock.iter()
              .map( |stat| Foo(StatValueToModifier(*stat))  )
              .sum()
              //.fold(0, |sum, x| sum + x )
*/
    // Sum the exponential stat modifiers, so higher modifiers have much much more weight.
    aStatBlock.iter()
              .map( |stat| 1 + StatValueToModifier(*stat)  )
              .fold(0, |sum, x| sum + 10i32.pow(x as u32) )
}

fn RankStatBlock_Variance(aStatBlock : StatBlock) -> StatBlockRank
{
    // Larger extremes between stat modifiers are penalized.
    let min = aStatBlock.iter()
                        .min()
                        .unwrap();

    let minMod = StatBlockRank::from(StatValueToModifier(*min));

    let max = aStatBlock.iter()
                        .max()
                        .unwrap();

    let maxMod = StatBlockRank::from(StatValueToModifier(*max));

    let diff = maxMod - minMod;

    -(diff * diff)
}

fn RankStatBlock_Unranked(_ : StatBlock) -> StatBlockRank
{
    // Every statblock has the same rank.
    0
}

pub fn RankStatBlock(aStatBlock : StatBlock, aRankingStyle : RankingStyle) -> StatBlockRank
{
    match aRankingStyle
    {
        RankingStyle::Flat      => RankStatBlock_Flat    (aStatBlock),
        RankingStyle::Weighted  => RankStatBlock_Weighted(aStatBlock),
        RankingStyle::Variance  => RankStatBlock_Variance(aStatBlock),
        RankingStyle::Unranked  => RankStatBlock_Unranked(aStatBlock),
    }
}
