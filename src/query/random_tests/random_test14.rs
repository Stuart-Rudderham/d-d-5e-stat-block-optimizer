use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test14" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Weighted,
        statOrdering: [
            Constitution,
            Strength,
            Dexterity,
            Wisdom,
            Charisma,
            Intelligence,
        ],
        isReversed: true,
        availableRaces: vec![
            Aarakocra,
            Aasimar_Fallen,
            Aasimar_Protector,
            Aasimar_Scourge,
            Bugbear,
            Changling,
            CustomLineage,
            DragonBorn,
            Dwarf_Grey,
            Dwarf_Hill,
            Dwarf_MarkOfWarding,
            Dwarf_Mountain,
            Elf_Drow,
            Elf_High,
            Elf_MarkOfShadow,
            Elf_Wood,
            Firbolg,
            Genasi_Air,
            Genasi_Earth,
            Genasi_Fire,
            Genasi_Water,
            Gnome_Deep,
            Gnome_Forest,
            Gnome_MarkOfScribing,
            Gnome_Rock,
            Goblin,
            Goliath,
            HalfElf,
            HalfElf_MarkOfDetection,
            HalfElf_MarkOfStorm,
            Halfling_Ghostwise,
            Halfling_Lightfoot,
            Halfling_MarkOfHealing,
            Halfling_MarkOfHospitality,
            Halfling_Stout,
            HalfOrc,
            HalfOrc_MarkOfFinding,
            Hobgoblin,
            Human,
            Human_MarkOfFinding,
            Human_MarkOfHandling,
            Human_MarkOfMaking,
            Human_MarkOfPassage,
            Human_MarkOfSentinel,
            Human_Variant,
            Kalashtar,
            Kenku,
            Kobold,
            Lizardfolk,
            Orc,
            Orc_Eberron,
            Shifter_BeastHide,
            Shifter_LongTooth,
            Shifter_SwiftStride,
            Shifter_WildHunt,
            Tabaxi,
            Tiefling,
            Tiefling_Feral,
            Tortle,
            Triton,
            Warforged,
            YuanTiPureblood,
        ],
        requiredFeats: vec![],
        constraints: vec![],
        numberOfASIs: 4,
        doAllRacesHaveFreeFeat: false,
        canRearrangeRaceModifiers: false,
        maxNumberOfResults: 24,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 56637399,
            numUniqueCombinationsPassedConstraints: 44250298,
            numStatBlocksEjected: 317,
            numUniqueCombinationsEjected: 2310,
            numUniqueCombinationsBelowLowerBound: 44249931,
            numStatBlocksShiftedDuringInsertion: 3764,
            numDuplicateRaceStatBlocksRemoved: 0,
            numUniqueStatBlockAfterRacePass: 65,
            numUniqueStatBlockAfterFeatPass: 91,
            numFinalStatBlockCombinations: 26,
        };

    let expectedStatBlocks = 
        vec![
            RankedStatBlock {
                rank: 1500,
                statBlock: [13, 13, 12, 15, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 8, 8, 15, 12, 13],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 3, 4, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1500,
                statBlock: [13, 13, 12, 13, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 8, 8, 12, 13, 15],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 3, 4, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1500,
                statBlock: [13, 13, 12, 13, 15, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 8, 8, 12, 15, 13],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 3, 4, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1500,
                statBlock: [13, 12, 13, 15, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 8, 8, 15, 12, 13],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 2, 5, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1500,
                statBlock: [13, 12, 13, 13, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 8, 8, 12, 13, 15],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 2, 5, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1500,
                statBlock: [13, 12, 13, 13, 15, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 8, 8, 12, 15, 13],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 2, 5, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1500,
                statBlock: [13, 13, 13, 15, 12, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 8, 8, 15, 12, 13],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 3, 5, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1500,
                statBlock: [13, 13, 13, 13, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 8, 8, 13, 12, 15],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 3, 5, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1500,
                statBlock: [13, 13, 13, 15, 13, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 8, 8, 15, 13, 12],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 3, 5, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1500,
                statBlock: [13, 13, 13, 15, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [14, 8, 9, 15, 13, 13],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [1, 3, 4, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1500,
                statBlock: [13, 13, 13, 12, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 8, 8, 12, 13, 15],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 3, 5, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1500,
                statBlock: [13, 13, 13, 13, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [14, 8, 9, 13, 13, 15],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [1, 3, 4, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 12, 15, 13, 15],
                        myRaceModifiers:     [2, 0, 1, -2, 0, 0],
                        myASIs:              [3, 5, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1500,
                statBlock: [13, 13, 13, 13, 15, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 8, 8, 13, 15, 12],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 3, 5, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1500,
                statBlock: [13, 13, 13, 12, 15, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 8, 8, 12, 15, 13],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 3, 5, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1500,
                statBlock: [13, 13, 13, 13, 15, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [14, 8, 9, 13, 15, 13],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [1, 3, 4, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 12, 15, 15, 13],
                        myRaceModifiers:     [2, 0, 1, -2, 0, 0],
                        myASIs:              [3, 5, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1500,
                statBlock: [13, 15, 13, 13, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 8, 15, 12, 13],
                        myRaceModifiers:     [2, 0, 1, -2, 0, 0],
                        myASIs:              [3, 0, 4, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1500,
                statBlock: [13, 12, 15, 13, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 8, 15, 8, 12, 13],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 2, 0, 5, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1500,
                statBlock: [13, 13, 15, 13, 12, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 8, 15, 8, 12, 13],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 3, 0, 5, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1500,
                statBlock: [13, 13, 15, 13, 13, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 8, 15, 8, 13, 12],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 3, 0, 5, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1500,
                statBlock: [13, 13, 15, 12, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 8, 15, 8, 12, 13],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 3, 0, 4, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1500,
                statBlock: [13, 13, 15, 13, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [14, 8, 15, 9, 13, 13],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [1, 3, 0, 4, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 2220,
                statBlock: [13, 11, 10, 15, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 1, 2, 0, 5, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 2220,
                statBlock: [13, 11, 10, 15, 15, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 8, 8, 15, 15, 8],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 1, 2, 0, 0, 5],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 2220,
                statBlock: [13, 11, 10, 13, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 1, 2, 5, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
        ];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

