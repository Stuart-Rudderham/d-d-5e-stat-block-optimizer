# README #

### What is this repository for? ###

Find, given a set of constraints, the best possible D&D 5e character stat blocks.

### How do I get set up? ###

###### Pre-built binary ######
[ Download the binary ](https://bitbucket.org/Stuart_Rudderham/d-d-5e-stat-block-optimizer/downloads/d-d-5e-stat-block-optimizer.exe)

```
#!bash

./d-d-5e-stat-block-optimizer.exe
./d-d-5e-stat-block-optimizer.exe -h
./d-d-5e-stat-block-optimizer.exe --num 10 --asi 2 --weighted --reverse
```

###### Build from source ######
[ Download Rust ](https://www.rust-lang.org/)
 
```
#!bash

cargo build --release
cargo run --release
cargo run --release -- --help
cargo run --release -- --num 10 --asi 2 --weighted --reverse
```

###### Data files ######

There are 3 files that you can use to configure your query.

* `data/constraints.txt` to control what stat possibilities are available.
For example, "dex == 16".
* `data/races.txt` to control what race possibilities are available.
* `data/feats.txt` to control what feats you require the character to have. You must have enough Ability Skill Increases (ASIs) to be able to have each feat.

###### Output ######

Once you setup the desired constraints, run the program and see something like below.

The table header shows you the stat block that was calculated, each table row shows a possible way that the stat block could have been built while respecting the constraints. The first number is the stat value chosen through pointbuy, any modifiers come from the race, feat, or ASI selected.

The "best" stat block is the one at the bottom.

```
-------------------------------------------------------------------------------------------------------------
[ Str: 14         | Dex:  7         | Con: 14         | Int: 12         | Wis: 12         | Cha: 13         ] #05 | ValueFn() = 5
-------------------------------------------------------------------------------------------------------------
[ Str: 13 + 1     | Dex:  7         | Con: 14         | Int: 12         | Wis: 12         | Cha: 11 + 2     ] Race: Aasimar_Fallen
[ Str: 14         | Dex:  7         | Con: 13 + 1     | Int: 12         | Wis: 12         | Cha: 11 + 2     ] Race: Aasimar_Scourge
[ Str: 13 + 1     | Dex:  7         | Con: 14         | Int: 12         | Wis: 12         | Cha: 11 + 2     ] Race: Changling
[ Str: 14         | Dex:  7         | Con: 12 + 2     | Int: 12         | Wis: 11 + 1     | Cha: 13         ] Race: Dwarf_Hill
[ Str: 14         | Dex:  7         | Con: 12 + 2     | Int: 11 + 1     | Wis: 12         | Cha: 13         ] Race: Dwarf_MarkOfWarding
[ Str: 14         | Dex:  7         | Con: 12 + 2     | Int: 11 + 1     | Wis: 12         | Cha: 13         ] Race: Genasi_Fire
[ Str: 14         | Dex:  7         | Con: 12 + 2     | Int: 12         | Wis: 11 + 1     | Cha: 13         ] Race: Genasi_Water
[ Str: 14         | Dex:  7         | Con: 12 + 2     | Int: 11 + 1     | Wis: 12         | Cha: 13         ] Race: Hobgoblin
[ Str: 14         | Dex:  7         | Con: 12 + 2     | Int: 12         | Wis: 11 + 1     | Cha: 13         ] Race: Human_MarkOfSentinel
[ Str: 14         | Dex:  7         | Con: 12 + 1 + 1 | Int: 11 + 1     | Wis: 12         | Cha: 13         ] Race: Human_Variant              | Feats [AberrantDragonMark]
[ Str: 13 + 1     | Dex:  7         | Con: 14         | Int: 11 + 1     | Wis: 12         | Cha: 12     + 1 ] Race: Human_Variant              | Feats [Actor]
[ Str: 12 + 1 + 1 | Dex:  7         | Con: 14         | Int: 11 + 1     | Wis: 12         | Cha: 13         ] Race: Human_Variant              | Feats [Athlete]
[ Str: 14         | Dex:  7         | Con: 12 + 1 + 1 | Int: 11 + 1     | Wis: 12         | Cha: 13         ] Race: Human_Variant              | Feats [Durable]
[ Str: 12 + 1 + 1 | Dex:  7         | Con: 14         | Int: 11 + 1     | Wis: 12         | Cha: 13         ] Race: Human_Variant              | Feats [HeavilyArmored]
[ Str: 12 + 1 + 1 | Dex:  7         | Con: 14         | Int: 11 + 1     | Wis: 12         | Cha: 13         ] Race: Human_Variant              | Feats [HeavyArmorMaster]
[ Str: 13 + 1     | Dex:  7         | Con: 14         | Int: 11     + 1 | Wis: 12         | Cha: 12 + 1     ] Race: Human_Variant              | Feats [KeenMind]
[ Str: 12 + 1 + 1 | Dex:  7         | Con: 14         | Int: 11 + 1     | Wis: 12         | Cha: 13         ] Race: Human_Variant              | Feats [LightlyArmored]
[ Str: 13 + 1     | Dex:  7         | Con: 14         | Int: 11     + 1 | Wis: 12         | Cha: 12 + 1     ] Race: Human_Variant              | Feats [Linguist]
[ Str: 12 + 1 + 1 | Dex:  7         | Con: 14         | Int: 11 + 1     | Wis: 12         | Cha: 13         ] Race: Human_Variant              | Feats [ModeratelyArmored]
[ Str: 13 + 1     | Dex:  7         | Con: 14         | Int: 11     + 1 | Wis: 12         | Cha: 12 + 1     ] Race: Human_Variant              | Feats [Observant]
[ Str: 13 + 1     | Dex:  7         | Con: 14         | Int: 11 + 1     | Wis: 12         | Cha: 12     + 1 ] Race: Human_Variant              | Feats [Resilient_Cha]
[ Str: 14         | Dex:  7         | Con: 12 + 1 + 1 | Int: 11 + 1     | Wis: 12         | Cha: 13         ] Race: Human_Variant              | Feats [Resilient_Con]
[ Str: 13 + 1     | Dex:  7         | Con: 14         | Int: 11     + 1 | Wis: 12         | Cha: 12 + 1     ] Race: Human_Variant              | Feats [Resilient_Int]
[ Str: 12 + 1 + 1 | Dex:  7         | Con: 14         | Int: 11 + 1     | Wis: 12         | Cha: 13         ] Race: Human_Variant              | Feats [Resilient_Str]
[ Str: 13 + 1     | Dex:  7         | Con: 14         | Int: 12         | Wis: 11     + 1 | Cha: 12 + 1     ] Race: Human_Variant              | Feats [Resilient_Wis]
[ Str: 12 + 1 + 1 | Dex:  7         | Con: 14         | Int: 11 + 1     | Wis: 12         | Cha: 13         ] Race: Human_Variant              | Feats [TavernBrawler]
[ Str: 12 + 1 + 1 | Dex:  7         | Con: 14         | Int: 11 + 1     | Wis: 12         | Cha: 13         ] Race: Human_Variant              | Feats [WeaponMaster]
[ Str: 14         | Dex:  7         | Con: 12 + 2     | Int: 12         | Wis: 11 + 1     | Cha: 13         ] Race: Lizardfolk
[ Str: 12 + 2     | Dex:  7         | Con: 14         | Int: 12         | Wis: 11 + 1     | Cha: 13         ] Race: Tortle
[ Str: 14         | Dex:  7         | Con: 12 + 2     | Int: 11 + 1     | Wis: 12         | Cha: 13         ] Race: Warforged


-------------------------------------------------------------------------------------------------------------
[ Str: 15         | Dex:  7         | Con: 14         | Int: 12         | Wis: 12         | Cha: 13         ] #04 | ValueFn() = 5
-------------------------------------------------------------------------------------------------------------
[ Str: 14 + 1     | Dex:  7         | Con: 13 + 1     | Int: 12         | Wis: 12         | Cha: 11 + 2     ] Race: HalfElf


-------------------------------------------------------------------------------------------------------------
[ Str: 14         | Dex:  8         | Con: 14         | Int: 12         | Wis: 12         | Cha: 12         ] #03 | ValueFn() = 6
-------------------------------------------------------------------------------------------------------------
[ Str: 14         | Dex:  7 + 1     | Con: 13     + 1 | Int: 11 + 1     | Wis: 12         | Cha: 12         ] Race: Human_Variant              | Feats [AberrantDragonMark]
[ Str: 13 + 1     | Dex:  7 + 1     | Con: 14         | Int: 12         | Wis: 12         | Cha: 11     + 1 ] Race: Human_Variant              | Feats [Actor]
[ Str: 13     + 1 | Dex:  7 + 1     | Con: 14         | Int: 11 + 1     | Wis: 12         | Cha: 12         ] Race: Human_Variant              | Feats [Athlete]
[ Str: 14         | Dex:  7 + 1     | Con: 13     + 1 | Int: 11 + 1     | Wis: 12         | Cha: 12         ] Race: Human_Variant              | Feats [Durable]
[ Str: 13     + 1 | Dex:  7 + 1     | Con: 14         | Int: 11 + 1     | Wis: 12         | Cha: 12         ] Race: Human_Variant              | Feats [HeavilyArmored]
[ Str: 13     + 1 | Dex:  7 + 1     | Con: 14         | Int: 11 + 1     | Wis: 12         | Cha: 12         ] Race: Human_Variant              | Feats [HeavyArmorMaster]
[ Str: 13 + 1     | Dex:  7 + 1     | Con: 14         | Int: 11     + 1 | Wis: 12         | Cha: 12         ] Race: Human_Variant              | Feats [KeenMind]
[ Str: 13     + 1 | Dex:  7 + 1     | Con: 14         | Int: 11 + 1     | Wis: 12         | Cha: 12         ] Race: Human_Variant              | Feats [LightlyArmored]
[ Str: 13 + 1     | Dex:  7 + 1     | Con: 14         | Int: 11     + 1 | Wis: 12         | Cha: 12         ] Race: Human_Variant              | Feats [Linguist]
[ Str: 13     + 1 | Dex:  7 + 1     | Con: 14         | Int: 11 + 1     | Wis: 12         | Cha: 12         ] Race: Human_Variant              | Feats [ModeratelyArmored]
[ Str: 13 + 1     | Dex:  7 + 1     | Con: 14         | Int: 11     + 1 | Wis: 12         | Cha: 12         ] Race: Human_Variant              | Feats [Observant]
[ Str: 13 + 1     | Dex:  7 + 1     | Con: 14         | Int: 12         | Wis: 12         | Cha: 11     + 1 ] Race: Human_Variant              | Feats [Resilient_Cha]
[ Str: 14         | Dex:  7 + 1     | Con: 13     + 1 | Int: 11 + 1     | Wis: 12         | Cha: 12         ] Race: Human_Variant              | Feats [Resilient_Con]
[ Str: 13 + 1     | Dex:  7     + 1 | Con: 14         | Int: 11 + 1     | Wis: 12         | Cha: 12         ] Race: Human_Variant              | Feats [Resilient_Dex]
[ Str: 13 + 1     | Dex:  7 + 1     | Con: 14         | Int: 11     + 1 | Wis: 12         | Cha: 12         ] Race: Human_Variant              | Feats [Resilient_Int]
[ Str: 13     + 1 | Dex:  7 + 1     | Con: 14         | Int: 11 + 1     | Wis: 12         | Cha: 12         ] Race: Human_Variant              | Feats [Resilient_Str]
[ Str: 13 + 1     | Dex:  7 + 1     | Con: 14         | Int: 12         | Wis: 11     + 1 | Cha: 12         ] Race: Human_Variant              | Feats [Resilient_Wis]
[ Str: 13     + 1 | Dex:  7 + 1     | Con: 14         | Int: 11 + 1     | Wis: 12         | Cha: 12         ] Race: Human_Variant              | Feats [TavernBrawler]
[ Str: 13     + 1 | Dex:  7 + 1     | Con: 14         | Int: 11 + 1     | Wis: 12         | Cha: 12         ] Race: Human_Variant              | Feats [WeaponMaster]


-------------------------------------------------------------------------------------------------------------
[ Str: 15         | Dex:  8         | Con: 14         | Int: 13         | Wis: 13         | Cha: 12         ] #02 | ValueFn() = 6
-------------------------------------------------------------------------------------------------------------
[ Str: 14 + 1     | Dex:  7 + 1     | Con: 13 + 1     | Int: 12 + 1     | Wis: 12 + 1     | Cha: 11 + 1     ] Race: Human


-------------------------------------------------------------------------------------------------------------
[ Str: 14         | Dex:  8         | Con: 14         | Int: 12         | Wis: 12         | Cha: 13         ] #01 | ValueFn() = 6
-------------------------------------------------------------------------------------------------------------
[ Str: 13 + 1     | Dex:  7 + 1     | Con: 14         | Int: 12         | Wis: 12         | Cha: 11 + 2     ] Race: HalfElf


Final Result

    20 unique stat blocks displayed
    195 different combinations

Parameters

    Flat ranking
    5 constraints
    61 races
    Sorted Best-To-Worst
    All Races Free Feat: False
    Custom Base StatBlock: Some([7, 11, 12, 12, 13, 14])

Stats

    174240 different combinations seen
    22.958 ns per combination
    4.592 ns per constraint
    43557822 combinations per second

    1/30/9.8 combinations per stat block (min/max/average)
    360 combinations removed for duplicate race
    184.62% of total

    556 combinations passed all constraints
    7194.604 ns per combination
    138993 combinations per second
    0.32% of total

    1 combinations passed all constraints but were below the current lower bound
    0.18% of total that passed all constraints

    0 unique stat blocks ejected for a better one
    0.00% of stat blocks stored at some point
    0 combinations ejected
    NaN combinations per stat block, on average

    76 stat blocks shifted during insertion
    3.8 stat blocks shifted per insertion, on average

Timing information for :
    Run Query              - 1 * 4.0ms = 4.0ms (100.0%)
      Setup                    - 1 * 0ns = 0ns (0.0%)
        Point Buy                  - 1 * 0ns = 0ns (NaN%)
        Normal ASIs                - 1 * 0ns = 0ns (NaN%)
          Generate                     - 1 * 0ns = 0ns (NaN%)
          Combine                      - 1 * 0ns = 0ns (NaN%)
        Free Feat ASIs             - 1 * 0ns = 0ns (NaN%)
        Race Modifiers             - 1 * 0ns = 0ns (NaN%)
        Feat Modifiers             - 1 * 0ns = 0ns (NaN%)
      Loops                    - 1 * 4.0ms = 4.0ms (100.0%)
      Remove Duplicates        - 1 * 0ns = 0ns (0.0%)
    Sanity Check           - 1 * 0ns = 0ns (0.0%)
```