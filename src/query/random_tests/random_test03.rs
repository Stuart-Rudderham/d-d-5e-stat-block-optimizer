use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test03" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Weighted,
        statOrdering: [
            Constitution,
            Intelligence,
            Wisdom,
            Dexterity,
            Strength,
            Charisma,
        ],
        isReversed: true,
        availableRaces: vec![
            Bugbear,
            Dwarf_Grey,
            Dwarf_MarkOfWarding,
            Dwarf_Mountain,
            Elf_MarkOfShadow,
            Elf_Wood,
            Genasi_Air,
            Genasi_Fire,
            Gnome_Deep,
            Gnome_Forest,
            Goblin,
            HalfElf,
            Halfling_Ghostwise,
            Halfling_Lightfoot,
            Halfling_Stout,
            HalfOrc,
            Hobgoblin,
            Human_MarkOfFinding,
            Human_MarkOfSentinel,
            Human_Variant,
            Kalashtar,
            Kenku,
            Lizardfolk,
            Orc_Eberron,
            Shifter_SwiftStride,
            Tiefling_Feral,
            Tortle,
            Warforged,
        ],
        requiredFeats: vec![],
        constraints: vec![
            BinCmp_StatValue_Constant(
                BinaryCompare {
                    lhsValue: GetStatValue(
                        Wisdom,
                    ),
                    binCmp: LE,
                    rhsValue: GetConstant(
                        8,
                    ),
                },
            ),
            BinCmp_StatValue_Constant(
                BinaryCompare {
                    lhsValue: GetStatValue(
                        Strength,
                    ),
                    binCmp: EQ,
                    rhsValue: GetConstant(
                        16,
                    ),
                },
            ),
        ],
        numberOfASIs: 0,
        doAllRacesHaveFreeFeat: true,
        canRearrangeRaceModifiers: false,
        maxNumberOfResults: 2,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 2837142,
            numUniqueCombinationsPassedConstraints: 15159,
            numStatBlocksEjected: 22,
            numUniqueCombinationsEjected: 2417,
            numUniqueCombinationsBelowLowerBound: 15125,
            numStatBlocksShiftedDuringInsertion: 11,
            numDuplicateRaceStatBlocksRemoved: 6,
            numUniqueStatBlockAfterRacePass: 41,
            numUniqueStatBlockAfterFeatPass: 231,
            numFinalStatBlockCombinations: 2,
        };

    let expectedStatBlocks = 
        vec![
            RankedStatBlock {
                rank: 10401,
                statBlock: [16, 13, 12, 13, 8, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            NonStatGivingFeat,
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 12, 12, 13, 8, 13],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10401,
                statBlock: [16, 13, 13, 12, 8, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            NonStatGivingFeat,
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 12, 13, 12, 8, 13],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
        ];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

