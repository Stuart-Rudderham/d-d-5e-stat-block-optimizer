use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test31" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Flat,
        statOrdering: [
            Constitution,
            Dexterity,
            Charisma,
            Wisdom,
            Intelligence,
            Strength,
        ],
        isReversed: true,
        availableRaces: vec![
            Aarakocra,
            Aasimar_Protector,
            Bugbear,
            CustomLineage,
            Changling,
            DragonBorn,
            Dwarf_Grey,
            Dwarf_Hill,
            Dwarf_MarkOfWarding,
            Dwarf_Mountain,
            Elf_Drow,
            Elf_MarkOfShadow,
            Elf_Wood,
            Firbolg,
            Genasi_Air,
            Genasi_Earth,
            Genasi_Water,
            Gnome_Deep,
            Gnome_MarkOfScribing,
            Gnome_Rock,
            Goliath,
            HalfElf_MarkOfDetection,
            Halfling_Ghostwise,
            Halfling_Lightfoot,
            Halfling_MarkOfHealing,
            Halfling_MarkOfHospitality,
            HalfOrc,
            HalfOrc_MarkOfFinding,
            Hobgoblin,
            Human,
            Human_MarkOfHandling,
            Human_MarkOfMaking,
            Human_MarkOfPassage,
            Human_MarkOfSentinel,
            Human_Variant,
            Kalashtar,
            Kenku,
            Lizardfolk,
            Orc,
            Shifter_BeastHide,
            Shifter_SwiftStride,
            Shifter_WildHunt,
            Tiefling,
            Tiefling_Feral,
            Warforged,
            YuanTiPureblood,
        ],
        requiredFeats: vec![],
        constraints: vec![
            BinCmp_StatValue_Constant(
                BinaryCompare {
                    lhsValue: GetStatValue(
                        Charisma,
                    ),
                    binCmp: GE,
                    rhsValue: GetConstant(
                        12,
                    ),
                },
            ),
            UnCmp_StatValue(
                UnaryCompare {
                    value: GetStatValue(
                        Constitution,
                    ),
                    unCmp: IsMax,
                },
            ),
            UnCmp_StatModifier(
                UnaryCompare {
                    value: GetStatModifier(
                        Strength,
                    ),
                    unCmp: IsMax,
                },
            ),
            BinCmp_StatModifier_StatModifier(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Strength,
                    ),
                    binCmp: LE,
                    rhsValue: GetStatModifier(
                        Strength,
                    ),
                },
            ),
        ],
        numberOfASIs: 1,
        doAllRacesHaveFreeFeat: true,
        canRearrangeRaceModifiers: true,
        maxNumberOfResults: 24,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 58520700,
            numUniqueCombinationsPassedConstraints: 906004,
            numStatBlocksEjected: 247,
            numUniqueCombinationsEjected: 3788,
            numUniqueCombinationsBelowLowerBound: 905520,
            numStatBlocksShiftedDuringInsertion: 3344,
            numDuplicateRaceStatBlocksRemoved: 50,
            numUniqueStatBlockAfterRacePass: 187,
            numUniqueStatBlockAfterFeatPass: 900,
            numFinalStatBlockCombinations: 98,
        };

    let expectedStatBlocks = 
        vec![
            RankedStatBlock {
                rank: 3,
                statBlock: [15, 7, 15, 11, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 8, 15],
                        myRaceModifiers:     [0, -2, 0, 2, 1, 0],
                        myASIs:              [0, 1, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [15, 7, 15, 9, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 8, 15],
                        myRaceModifiers:     [0, -2, 0, 1, 2, 0],
                        myASIs:              [0, 1, 0, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [15, 9, 15, 11, 9, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 8, 15],
                        myRaceModifiers:     [0, 0, 0, 2, 1, -2],
                        myASIs:              [0, 1, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [15, 9, 15, 9, 11, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 8, 15],
                        myRaceModifiers:     [0, 0, 0, 1, 2, -2],
                        myASIs:              [0, 1, 0, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [15, 9, 15, 11, 7, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 8, 15],
                        myRaceModifiers:     [0, 1, 0, 2, -2, 0],
                        myASIs:              [0, 0, 0, 1, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [15, 9, 15, 7, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 8, 15],
                        myRaceModifiers:     [0, 1, 0, -2, 2, 0],
                        myASIs:              [0, 0, 0, 1, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [15, 11, 15, 9, 9, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 1, -2],
                        myASIs:              [0, 1, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [15, 11, 15, 9, 7, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 1, -2, 0],
                        myASIs:              [0, 1, 0, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [15, 11, 15, 7, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 8, 15],
                        myRaceModifiers:     [0, 2, 0, -2, 1, 0],
                        myASIs:              [0, 1, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [15, 6, 15, 15, 9, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 15, 8, 8],
                        myRaceModifiers:     [0, -2, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [15, 6, 15, 9, 15, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 15, 8],
                        myRaceModifiers:     [0, -2, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [15, 6, 15, 15, 9, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 15, 8, 8],
                        myRaceModifiers:     [0, -2, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 15, 8, 8],
                        myRaceModifiers:     [0, -2, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 15, 8, 8],
                        myRaceModifiers:     [0, -2, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 15, 8, 8],
                        myRaceModifiers:     [0, -2, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 15, 8, 8],
                        myRaceModifiers:     [0, -2, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 15, 8, 8],
                        myRaceModifiers:     [0, -2, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 15, 8, 8],
                        myRaceModifiers:     [0, -2, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 15, 8, 8],
                        myRaceModifiers:     [0, -2, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [14, 8, 15, 15, 8, 10],
                        myRaceModifiers:     [0, -2, 0, 0, 1, 2],
                        myASIs:              [1, 0, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [15, 6, 15, 9, 15, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 15, 8],
                        myRaceModifiers:     [0, -2, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 15, 8],
                        myRaceModifiers:     [0, -2, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 15, 8],
                        myRaceModifiers:     [0, -2, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 15, 8],
                        myRaceModifiers:     [0, -2, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 15, 8],
                        myRaceModifiers:     [0, -2, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 15, 8],
                        myRaceModifiers:     [0, -2, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 15, 8],
                        myRaceModifiers:     [0, -2, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 15, 8],
                        myRaceModifiers:     [0, -2, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [14, 8, 15, 8, 15, 10],
                        myRaceModifiers:     [0, -2, 0, 1, 0, 2],
                        myASIs:              [1, 0, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [15, 6, 15, 12, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 8, 15],
                        myRaceModifiers:     [0, -2, 0, 2, 1, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [15, 6, 15, 13, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 8, 15],
                        myRaceModifiers:     [0, -2, 0, 2, 1, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            KeenMind,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 8, 15],
                        myRaceModifiers:     [0, -2, 0, 2, 1, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 8, 15],
                        myRaceModifiers:     [0, -2, 0, 2, 1, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 8, 15],
                        myRaceModifiers:     [0, -2, 0, 2, 1, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Resilient_Int,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 8, 15],
                        myRaceModifiers:     [0, -2, 0, 2, 1, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 8, 15],
                        myRaceModifiers:     [0, -2, 0, 2, 1, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 8, 15],
                        myRaceModifiers:     [0, -2, 0, 2, 1, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 8, 15],
                        myRaceModifiers:     [0, -2, 0, 2, 1, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 8, 15],
                        myRaceModifiers:     [0, -2, 0, 2, 1, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [14, 8, 15, 9, 9, 15],
                        myRaceModifiers:     [1, -2, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [15, 6, 15, 11, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 8, 15],
                        myRaceModifiers:     [0, -2, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [15, 6, 15, 10, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 8, 15],
                        myRaceModifiers:     [0, -2, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 1, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [15, 6, 15, 11, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 8, 15],
                        myRaceModifiers:     [0, -2, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 1, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            KeenMind,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 8, 15],
                        myRaceModifiers:     [0, -2, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 1, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 8, 15],
                        myRaceModifiers:     [0, -2, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 1, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 8, 15],
                        myRaceModifiers:     [0, -2, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 1, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Resilient_Int,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 8, 15],
                        myRaceModifiers:     [0, -2, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 1, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Resilient_Wis,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 8, 15],
                        myRaceModifiers:     [0, -2, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 8, 15],
                        myRaceModifiers:     [0, -2, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 1, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 8, 15],
                        myRaceModifiers:     [0, -2, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 1, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 8, 15],
                        myRaceModifiers:     [0, -2, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 1, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 8, 15],
                        myRaceModifiers:     [0, -2, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 1, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [14, 8, 15, 8, 10, 15],
                        myRaceModifiers:     [0, -2, 0, 2, 1, 0],
                        myASIs:              [1, 0, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [15, 6, 15, 9, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 8, 15],
                        myRaceModifiers:     [0, -2, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [15, 6, 15, 9, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 8, 15],
                        myRaceModifiers:     [0, -2, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 8, 15],
                        myRaceModifiers:     [0, -2, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Resilient_Wis,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 8, 15],
                        myRaceModifiers:     [0, -2, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 8, 15],
                        myRaceModifiers:     [0, -2, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 8, 15],
                        myRaceModifiers:     [0, -2, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 8, 15],
                        myRaceModifiers:     [0, -2, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 8, 15],
                        myRaceModifiers:     [0, -2, 0, 1, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [14, 8, 15, 8, 10, 15],
                        myRaceModifiers:     [0, -2, 0, 1, 2, 0],
                        myASIs:              [1, 0, 0, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [15, 7, 15, 15, 9, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 15, 8, 8],
                        myRaceModifiers:     [0, -2, 0, 0, 1, 2],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 15, 8, 8],
                        myRaceModifiers:     [0, -2, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 15, 8, 8],
                        myRaceModifiers:     [0, -2, 0, 0, 1, 2],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 15, 8, 8],
                        myRaceModifiers:     [0, -2, 0, 0, 1, 2],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Gunner,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 15, 8, 8],
                        myRaceModifiers:     [0, -2, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 15, 8, 8],
                        myRaceModifiers:     [0, -2, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            ModeratelyArmored,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 15, 8, 8],
                        myRaceModifiers:     [0, -2, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Piercer,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 15, 8, 8],
                        myRaceModifiers:     [0, -2, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 15, 8, 8],
                        myRaceModifiers:     [0, -2, 0, 0, 1, 2],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Resilient_Dex,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 15, 8, 8],
                        myRaceModifiers:     [0, -2, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 15, 8, 8],
                        myRaceModifiers:     [0, -2, 0, 0, 1, 2],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 15, 8, 8],
                        myRaceModifiers:     [0, -2, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Slasher,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 15, 8, 8],
                        myRaceModifiers:     [0, -2, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 15, 8, 8],
                        myRaceModifiers:     [0, -2, 0, 0, 1, 2],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 15, 8, 8],
                        myRaceModifiers:     [0, -2, 0, 0, 1, 2],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            WeaponMaster,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 15, 8, 8],
                        myRaceModifiers:     [0, -2, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [14, 8, 15, 15, 8, 10],
                        myRaceModifiers:     [0, -2, 0, 0, 1, 2],
                        myASIs:              [1, 1, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [15, 7, 15, 9, 15, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 15, 8],
                        myRaceModifiers:     [0, -2, 0, 1, 0, 2],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 15, 8],
                        myRaceModifiers:     [0, -2, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 15, 8],
                        myRaceModifiers:     [0, -2, 0, 1, 0, 2],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 15, 8],
                        myRaceModifiers:     [0, -2, 0, 1, 0, 2],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Gunner,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 15, 8],
                        myRaceModifiers:     [0, -2, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 15, 8],
                        myRaceModifiers:     [0, -2, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            ModeratelyArmored,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 15, 8],
                        myRaceModifiers:     [0, -2, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Piercer,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 15, 8],
                        myRaceModifiers:     [0, -2, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 15, 8],
                        myRaceModifiers:     [0, -2, 0, 1, 0, 2],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Resilient_Dex,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 15, 8],
                        myRaceModifiers:     [0, -2, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 15, 8],
                        myRaceModifiers:     [0, -2, 0, 1, 0, 2],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 15, 8],
                        myRaceModifiers:     [0, -2, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Slasher,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 15, 8],
                        myRaceModifiers:     [0, -2, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 15, 8],
                        myRaceModifiers:     [0, -2, 0, 1, 0, 2],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 15, 8],
                        myRaceModifiers:     [0, -2, 0, 1, 0, 2],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            WeaponMaster,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 15, 8],
                        myRaceModifiers:     [0, -2, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [14, 8, 15, 8, 15, 10],
                        myRaceModifiers:     [0, -2, 0, 1, 0, 2],
                        myASIs:              [1, 1, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [15, 7, 15, 15, 8, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [14, 8, 15, 15, 8, 10],
                        myRaceModifiers:     [1, -2, 0, 0, 0, 2],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 4,
                statBlock: [15, 7, 15, 14, 9, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 14, 8, 10],
                        myRaceModifiers:     [0, -2, 0, 0, 1, 2],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
        ];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

