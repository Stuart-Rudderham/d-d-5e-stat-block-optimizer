use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test37" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Weighted,
        statOrdering: [
            Dexterity,
            Strength,
            Intelligence,
            Constitution,
            Charisma,
            Wisdom,
        ],
        isReversed: true,
        availableRaces: vec![
            Aasimar_Protector,
            Human_Variant,
            Shifter_LongTooth,
        ],
        requiredFeats: vec![],
        constraints: vec![],
        numberOfASIs: 1,
        doAllRacesHaveFreeFeat: false,
        canRearrangeRaceModifiers: false,
        maxNumberOfResults: 14,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 4226495,
            numUniqueCombinationsPassedConstraints: 4226495,
            numStatBlocksEjected: 98,
            numUniqueCombinationsEjected: 181,
            numUniqueCombinationsBelowLowerBound: 4226247,
            numStatBlocksShiftedDuringInsertion: 639,
            numDuplicateRaceStatBlocksRemoved: 126,
            numUniqueStatBlockAfterRacePass: 17,
            numUniqueStatBlockAfterFeatPass: 65,
            numFinalStatBlockCombinations: 14,
        };

    let expectedStatBlocks = 
        vec![
            RankedStatBlock {
                rank: 1410,
                statBlock: [13, 10, 13, 13, 15, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [10, 9, 13, 13, 15, 13],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1410,
                statBlock: [13, 10, 13, 13, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [10, 9, 13, 13, 13, 15],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1410,
                statBlock: [13, 10, 15, 13, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [10, 9, 15, 13, 13, 13],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1410,
                statBlock: [13, 10, 13, 15, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [10, 9, 13, 15, 13, 13],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1410,
                statBlock: [15, 10, 13, 13, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [15, 8, 11, 13, 13, 13],
                        myRaceModifiers:     [0, 1, 1, 0, 0, 0],
                        myASIs:              [0, 1, 1, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1410,
                statBlock: [12, 11, 13, 13, 15, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [9, 10, 13, 13, 15, 13],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1410,
                statBlock: [12, 11, 13, 13, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [9, 10, 13, 13, 13, 15],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1410,
                statBlock: [12, 11, 15, 13, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [9, 10, 15, 13, 13, 13],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1410,
                statBlock: [12, 11, 13, 15, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [9, 10, 13, 15, 13, 13],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1410,
                statBlock: [13, 11, 13, 12, 15, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [10, 10, 13, 12, 15, 13],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1410,
                statBlock: [13, 11, 13, 12, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [10, 10, 13, 12, 13, 15],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1410,
                statBlock: [13, 11, 15, 12, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [10, 10, 15, 12, 13, 13],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1410,
                statBlock: [13, 11, 12, 13, 15, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [10, 10, 12, 13, 15, 13],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1410,
                statBlock: [13, 11, 12, 13, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [10, 10, 12, 13, 13, 15],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
        ];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

