use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test34" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Flat,
        statOrdering: [
            Charisma,
            Wisdom,
            Constitution,
            Intelligence,
            Strength,
            Dexterity,
        ],
        isReversed: true,
        availableRaces: vec![
            Aarakocra,
            Aasimar_Fallen,
            Aasimar_Protector,
            Aasimar_Scourge,
            Bugbear,
            CustomLineage,
            Changling,
            DragonBorn,
            Dwarf_Grey,
            Dwarf_Hill,
            Dwarf_MarkOfWarding,
            Dwarf_Mountain,
            Elf_High,
            Elf_MarkOfShadow,
            Elf_Wood,
            Firbolg,
            Genasi_Air,
            Genasi_Earth,
            Genasi_Fire,
            Genasi_Water,
            Gnome_Deep,
            Gnome_Forest,
            Gnome_MarkOfScribing,
            Gnome_Rock,
            Goblin,
            Goliath,
            HalfElf,
            HalfElf_MarkOfDetection,
            HalfElf_MarkOfStorm,
            Halfling_Ghostwise,
            Halfling_MarkOfHealing,
            Halfling_MarkOfHospitality,
            HalfOrc,
            HalfOrc_MarkOfFinding,
            Hobgoblin,
            Human,
            Human_MarkOfFinding,
            Human_MarkOfHandling,
            Human_MarkOfSentinel,
            Human_Variant,
            Kalashtar,
            Kenku,
            Shifter_BeastHide,
            Shifter_LongTooth,
            Shifter_WildHunt,
            Tabaxi,
            Tiefling,
            Tiefling_Feral,
            Tortle,
            Warforged,
            YuanTiPureblood,
        ],
        requiredFeats: vec![],
        constraints: vec![
            BinCmp_StatModifier_Constant(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Strength,
                    ),
                    binCmp: GE,
                    rhsValue: GetConstant(
                        -1,
                    ),
                },
            ),
            BinCmp_StatValue_StatValue(
                BinaryCompare {
                    lhsValue: GetStatValue(
                        Charisma,
                    ),
                    binCmp: GT,
                    rhsValue: GetStatValue(
                        Strength,
                    ),
                },
            ),
            BinCmp_StatModifier_StatModifier(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Dexterity,
                    ),
                    binCmp: LE,
                    rhsValue: GetStatModifier(
                        Dexterity,
                    ),
                },
            ),
            UnCmp_StatValue(
                UnaryCompare {
                    value: GetStatValue(
                        Wisdom,
                    ),
                    unCmp: IsEven,
                },
            ),
            BinCmp_StatModifier_StatModifier(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Strength,
                    ),
                    binCmp: GE,
                    rhsValue: GetStatModifier(
                        Charisma,
                    ),
                },
            ),
        ],
        numberOfASIs: 1,
        doAllRacesHaveFreeFeat: false,
        canRearrangeRaceModifiers: false,
        maxNumberOfResults: 25,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 5787047,
            numUniqueCombinationsPassedConstraints: 126626,
            numStatBlocksEjected: 218,
            numUniqueCombinationsEjected: 1528,
            numUniqueCombinationsBelowLowerBound: 126274,
            numStatBlocksShiftedDuringInsertion: 2797,
            numDuplicateRaceStatBlocksRemoved: 348,
            numUniqueStatBlockAfterRacePass: 59,
            numUniqueStatBlockAfterFeatPass: 89,
            numFinalStatBlockCombinations: 380,
        };

    let expectedStatBlocks = 
        vec![
            RankedStatBlock {
                rank: 5,
                statBlock: [8, 18, 15, 15, 8, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 5,
                statBlock: [8, 19, 15, 15, 8, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Gunner,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            ModeratelyArmored,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Piercer,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Resilient_Dex,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Slasher,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            WeaponMaster,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Elf_MarkOfShadow,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Gunner,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            ModeratelyArmored,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Piercer,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Resilient_Dex,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Slasher,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            WeaponMaster,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 5,
                statBlock: [8, 17, 15, 16, 8, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 5,
                statBlock: [8, 16, 15, 17, 8, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 1, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 5,
                statBlock: [8, 17, 15, 17, 8, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Gunner,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            KeenMind,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            ModeratelyArmored,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Piercer,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Resilient_Dex,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Resilient_Int,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Slasher,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            WeaponMaster,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Elf_High,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 1, 0, 0],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Elf_MarkOfShadow,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_MarkOfScribing,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 1, 0, 1, 0, 0],
                        myASIs:              [0, 1, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 1, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 1, 0, 1, 0, 0],
                        myASIs:              [0, 1, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Gunner,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 1, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            KeenMind,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 1, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            ModeratelyArmored,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 1, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Piercer,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 1, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 1, 0, 1, 0, 0],
                        myASIs:              [0, 1, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Resilient_Dex,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 1, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Resilient_Int,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 1, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Slasher,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 1, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            WeaponMaster,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 1, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tiefling_Feral,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 1, 0, 0],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 5,
                statBlock: [8, 15, 15, 18, 8, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 5,
                statBlock: [8, 15, 15, 19, 8, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            KeenMind,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Resilient_Int,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_MarkOfScribing,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 1],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            KeenMind,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Resilient_Int,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 5,
                statBlock: [8, 17, 16, 15, 8, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 1, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 5,
                statBlock: [8, 15, 16, 17, 8, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 1, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 0, 1, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 5,
                statBlock: [8, 16, 17, 15, 8, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 1],
                        myASIs:              [0, 1, 1, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 5,
                statBlock: [8, 17, 17, 15, 8, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            AberrantDragonMark,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 1, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Crusher,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 1, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Durable,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 1, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Gunner,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            ModeratelyArmored,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Piercer,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Resilient_Con,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 1, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Resilient_Dex,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Slasher,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 1, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            WeaponMaster,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Elf_MarkOfShadow,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Genasi_Air,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 1, 2, 0, 0, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Goblin,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 1, 0, 0, 0],
                        myASIs:              [0, 0, 1, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            AberrantDragonMark,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 1, 1, 0, 0, 0],
                        myASIs:              [0, 1, 1, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 1],
                        myASIs:              [0, 1, 1, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Crusher,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Durable,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 1, 1, 0, 0, 0],
                        myASIs:              [0, 1, 1, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Gunner,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 1],
                        myASIs:              [0, 1, 1, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 1],
                        myASIs:              [0, 1, 1, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            ModeratelyArmored,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 1],
                        myASIs:              [0, 1, 1, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Piercer,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 1],
                        myASIs:              [0, 1, 1, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 1, 1, 0, 0, 0],
                        myASIs:              [0, 1, 1, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Resilient_Con,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Resilient_Dex,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 1],
                        myASIs:              [0, 1, 1, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 1, 1, 0, 0, 0],
                        myASIs:              [0, 1, 1, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 1],
                        myASIs:              [0, 1, 1, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Slasher,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 1],
                        myASIs:              [0, 1, 1, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 1, 1, 0, 0, 0],
                        myASIs:              [0, 1, 1, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 1, 1, 0, 0, 0],
                        myASIs:              [0, 1, 1, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            WeaponMaster,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 1],
                        myASIs:              [0, 1, 1, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Warforged,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 5,
                statBlock: [8, 15, 17, 16, 8, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 5,
                statBlock: [8, 15, 17, 17, 8, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            AberrantDragonMark,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 1, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 1, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Crusher,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 1, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Durable,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 1, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            KeenMind,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Resilient_Con,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 1, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Resilient_Int,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 1, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 1, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_MarkOfWarding,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Genasi_Fire,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_MarkOfScribing,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 1],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Rock,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 1, 2, 0, 0],
                        myASIs:              [0, 0, 1, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Hobgoblin,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 1, 0, 0],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            AberrantDragonMark,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 0, 1, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 1, 1, 0, 0],
                        myASIs:              [0, 0, 1, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 0, 1, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Crusher,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 0, 1, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Durable,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 0, 1, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            KeenMind,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 1, 1, 0, 0],
                        myASIs:              [0, 0, 1, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Resilient_Con,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 0, 1, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Resilient_Int,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 0, 1, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 0, 1, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 0, 1],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Warforged,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 1],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 5,
                statBlock: [8, 15, 18, 15, 8, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 1, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 1],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 5,
                statBlock: [8, 15, 19, 15, 8, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            AberrantDragonMark,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 1, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 1, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Crusher,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 1, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Durable,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 1, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Resilient_Con,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 1, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 1, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 1, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            AberrantDragonMark,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 1],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 1],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Crusher,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 1],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Durable,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 1],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Resilient_Con,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 1],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 1],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 1],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Warforged,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 1],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 5,
                statBlock: [8, 16, 15, 15, 10, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 1, 0, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 5,
                statBlock: [8, 17, 15, 15, 10, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Aarakocra,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 1, 0],
                        myASIs:              [0, 0, 0, 0, 1, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Gunner,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            ModeratelyArmored,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 1, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Piercer,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Resilient_Dex,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Resilient_Wis,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 1, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Slasher,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            WeaponMaster,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [8, 14, 15, 15, 9, 9],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 1, 0, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Elf_MarkOfShadow,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Elf_Wood,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 1, 0],
                        myASIs:              [0, 0, 0, 0, 1, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_Ghostwise,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 1, 0],
                        myASIs:              [0, 0, 0, 0, 1, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHealing,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHospitality,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 1, 0],
                        myASIs:              [0, 0, 0, 0, 1, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfHandling,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 1, 0, 0, 1, 0],
                        myASIs:              [0, 1, 0, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 1, 0, 0, 1, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 1, 0, 0, 1, 0],
                        myASIs:              [0, 1, 0, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Gunner,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 1, 0, 0, 1, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 1, 0, 0, 1, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            ModeratelyArmored,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 1, 0, 0, 1, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Piercer,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 1, 0, 0, 1, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 1, 0, 0, 1, 0],
                        myASIs:              [0, 1, 0, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Resilient_Dex,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 1, 0, 0, 1, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Resilient_Wis,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 1, 0, 0, 1, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Slasher,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 1, 0, 0, 1, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            WeaponMaster,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 1, 0, 0, 1, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [8, 14, 15, 15, 9, 9],
                        myRaceModifiers:     [0, 1, 0, 0, 1, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kalashtar,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kenku,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 1, 0],
                        myASIs:              [0, 0, 0, 0, 1, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Shifter_WildHunt,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 1, 0, 0, 2, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 5,
                statBlock: [8, 15, 15, 16, 10, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 0, 0, 1, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 5,
                statBlock: [8, 15, 15, 17, 10, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            KeenMind,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Resilient_Int,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Resilient_Wis,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 1, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [8, 14, 15, 15, 9, 9],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 1, 0, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_MarkOfScribing,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfHandling,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 1, 0],
                        myASIs:              [0, 0, 0, 1, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 1, 0],
                        myASIs:              [0, 0, 0, 1, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 0, 0, 1, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            KeenMind,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 0, 0, 1, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 0, 0, 1, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 0, 0, 1, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 1, 0],
                        myASIs:              [0, 0, 0, 1, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Resilient_Int,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 0, 0, 1, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Resilient_Wis,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 0, 0, 1, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 0, 0, 1, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 0, 0, 1, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 0, 0, 1, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [8, 14, 15, 15, 9, 9],
                        myRaceModifiers:     [0, 0, 0, 1, 1, 0],
                        myASIs:              [0, 1, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kalashtar,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 5,
                statBlock: [8, 15, 16, 15, 10, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 1, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 0, 1, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 5,
                statBlock: [8, 15, 17, 15, 10, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            AberrantDragonMark,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 1, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 1, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Crusher,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 1, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Durable,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 1, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 1, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Resilient_Con,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 1, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Resilient_Wis,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 1, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 1, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 1, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [8, 14, 15, 15, 9, 9],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 1, 0, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_Hill,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 1, 0],
                        myASIs:              [0, 0, 0, 0, 1, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Genasi_Water,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 1, 0],
                        myASIs:              [0, 0, 0, 0, 1, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfOrc_MarkOfFinding,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 1, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfFinding,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 1, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfHandling,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfSentinel,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 1, 0],
                        myASIs:              [0, 0, 0, 0, 1, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            AberrantDragonMark,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 0, 1, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 1, 0],
                        myASIs:              [0, 0, 1, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 0, 1, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Crusher,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 0, 1, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Durable,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 0, 1, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 1, 0],
                        myASIs:              [0, 0, 1, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Resilient_Con,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 0, 1, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Resilient_Wis,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 0, 1, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 0, 1, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [8, 14, 15, 15, 9, 9],
                        myRaceModifiers:     [0, 0, 1, 0, 1, 0],
                        myASIs:              [0, 1, 1, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kalashtar,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 2, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Warforged,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 5,
                statBlock: [8, 15, 15, 15, 12, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 1, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 1, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Resilient_Wis,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 1, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 1, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 1, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 1, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 1, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [8, 14, 15, 15, 9, 9],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 0],
                        myASIs:              [0, 1, 0, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf_MarkOfDetection,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfHandling,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Resilient_Wis,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [8, 14, 15, 15, 9, 9],
                        myRaceModifiers:     [0, 1, 0, 0, 1, 0],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kalashtar,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 5,
                statBlock: [8, 19, 9, 15, 14, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [8, 15, 9, 15, 14, 9],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 5,
                statBlock: [8, 17, 9, 17, 14, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [8, 15, 9, 15, 14, 9],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [8, 15, 9, 15, 14, 9],
                        myRaceModifiers:     [0, 1, 0, 1, 0, 0],
                        myASIs:              [0, 1, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 5,
                statBlock: [8, 15, 9, 19, 14, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [8, 15, 9, 15, 14, 9],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
        ];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

