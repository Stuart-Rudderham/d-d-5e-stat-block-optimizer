use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test22" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Flat,
        statOrdering: [
            Intelligence,
            Wisdom,
            Strength,
            Charisma,
            Dexterity,
            Constitution,
        ],
        isReversed: true,
        availableRaces: vec![
            CustomLineage,
            Changling,
            Dwarf_Grey,
            Elf_High,
            Gnome_Rock,
            HalfOrc_MarkOfFinding,
            Hobgoblin,
            Kalashtar,
            Triton,
            YuanTiPureblood,
        ],
        requiredFeats: vec![],
        constraints: vec![],
        numberOfASIs: 0,
        doAllRacesHaveFreeFeat: false,
        canRearrangeRaceModifiers: true,
        maxNumberOfResults: 16,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 761484,
            numUniqueCombinationsPassedConstraints: 761484,
            numStatBlocksEjected: 169,
            numUniqueCombinationsEjected: 1620,
            numUniqueCombinationsBelowLowerBound: 761295,
            numStatBlocksShiftedDuringInsertion: 1374,
            numDuplicateRaceStatBlocksRemoved: 0,
            numUniqueStatBlockAfterRacePass: 56,
            numUniqueStatBlockAfterFeatPass: 62,
            numFinalStatBlockCombinations: 16,
        };

    let expectedStatBlocks = 
        vec![
            RankedStatBlock {
                rank: 3,
                statBlock: [9, 15, 15, 9, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Triton,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 8, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 1, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [15, 15, 15, 9, 9, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Triton,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 15, 15, 8, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 1, 1, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [15, 9, 15, 9, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Triton,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 8, 15, 8, 8, 15],
                        myRaceModifiers:     [0, 1, 0, 1, 1, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [15, 15, 9, 9, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Triton,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 15, 8, 8, 8, 15],
                        myRaceModifiers:     [0, 0, 1, 1, 1, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [9, 15, 15, 9, 15, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Triton,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 8, 15, 8],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [9, 9, 15, 9, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Triton,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 15, 8, 15, 15],
                        myRaceModifiers:     [1, 1, 0, 1, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [9, 15, 9, 9, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Triton,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 8, 8, 15, 15],
                        myRaceModifiers:     [1, 0, 1, 1, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [15, 9, 15, 9, 15, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Triton,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 8, 15, 8, 15, 8],
                        myRaceModifiers:     [0, 1, 0, 1, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [15, 15, 9, 9, 15, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Triton,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 15, 8, 8, 15, 8],
                        myRaceModifiers:     [0, 0, 1, 1, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [15, 9, 9, 9, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Triton,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 8, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 1, 1, 1, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [9, 15, 15, 15, 9, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Triton,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [1, 0, 0, 0, 1, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [9, 9, 15, 15, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Triton,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 15, 15, 8, 15],
                        myRaceModifiers:     [1, 1, 0, 0, 1, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [9, 15, 9, 15, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Triton,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 1, 0, 1, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [15, 9, 15, 15, 9, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Triton,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 8, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 1, 0, 0, 1, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [15, 15, 9, 15, 9, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Triton,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 15, 8, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 1, 0, 1, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [15, 9, 9, 15, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Triton,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [0, 1, 1, 0, 1, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
        ];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

