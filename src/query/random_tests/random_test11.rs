use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test11" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Weighted,
        statOrdering: [
            Intelligence,
            Dexterity,
            Constitution,
            Strength,
            Wisdom,
            Charisma,
        ],
        isReversed: true,
        availableRaces: vec![
            Aarakocra,
            Aasimar_Fallen,
            Aasimar_Protector,
            Aasimar_Scourge,
            Bugbear,
            Changling,
            CustomLineage,
            DragonBorn,
            Dwarf_Grey,
            Dwarf_Hill,
            Dwarf_MarkOfWarding,
            Dwarf_Mountain,
            Elf_Drow,
            Elf_High,
            Elf_MarkOfShadow,
            Elf_Wood,
            Firbolg,
            Genasi_Air,
            Genasi_Earth,
            Genasi_Fire,
            Genasi_Water,
            Gnome_Deep,
            Gnome_Forest,
            Gnome_MarkOfScribing,
            Gnome_Rock,
            Goblin,
            Goliath,
            HalfElf,
            HalfElf_MarkOfDetection,
            HalfElf_MarkOfStorm,
            Halfling_Ghostwise,
            Halfling_Lightfoot,
            Halfling_MarkOfHealing,
            Halfling_MarkOfHospitality,
            Halfling_Stout,
            HalfOrc,
            HalfOrc_MarkOfFinding,
            Hobgoblin,
            Human,
            Human_MarkOfFinding,
            Human_MarkOfHandling,
            Human_MarkOfMaking,
            Human_MarkOfPassage,
            Human_MarkOfSentinel,
            Human_Variant,
            Kalashtar,
            Kenku,
            Kobold,
            Lizardfolk,
            Orc,
            Orc_Eberron,
            Shifter_BeastHide,
            Shifter_LongTooth,
            Shifter_SwiftStride,
            Shifter_WildHunt,
            Tabaxi,
            Tiefling,
            Tiefling_Feral,
            Tortle,
            Triton,
            Warforged,
            YuanTiPureblood,
        ],
        requiredFeats: vec![
            Actor,
            Athlete,
        ],
        constraints: vec![],
        numberOfASIs: 1,
        doAllRacesHaveFreeFeat: false,
        canRearrangeRaceModifiers: true,
        maxNumberOfResults: 26,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 442152,
            numUniqueCombinationsPassedConstraints: 442152,
            numStatBlocksEjected: 239,
            numUniqueCombinationsEjected: 256,
            numUniqueCombinationsBelowLowerBound: 441399,
            numStatBlocksShiftedDuringInsertion: 3195,
            numDuplicateRaceStatBlocksRemoved: 474,
            numUniqueStatBlockAfterRacePass: 297,
            numUniqueStatBlockAfterFeatPass: 36,
            numFinalStatBlockCombinations: 50,
        };

    let expectedStatBlocks = 
        vec![
            RankedStatBlock {
                rank: 1410,
                statBlock: [13, 13, 13, 10, 15, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [10, 13, 13, 10, 15, 12],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [11, 12, 13, 10, 15, 12],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1410,
                statBlock: [15, 13, 13, 10, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 10, 13, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 11, 12, 10, 13, 12],
                        myRaceModifiers:     [0, 1, 1, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1410,
                statBlock: [13, 13, 15, 10, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [10, 13, 15, 10, 13, 12],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [11, 12, 15, 10, 13, 12],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1410,
                statBlock: [13, 15, 13, 10, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [10, 15, 13, 10, 13, 12],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [11, 15, 12, 10, 13, 12],
                        myRaceModifiers:     [1, 0, 1, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1410,
                statBlock: [13, 12, 13, 11, 15, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [10, 12, 13, 11, 15, 12],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [11, 11, 13, 11, 15, 12],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1410,
                statBlock: [15, 12, 13, 11, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 11, 13, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 11, 13, 12],
                        myRaceModifiers:     [0, 1, 1, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1410,
                statBlock: [13, 12, 15, 11, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [10, 12, 15, 11, 13, 12],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [11, 11, 15, 11, 13, 12],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1410,
                statBlock: [13, 13, 12, 11, 15, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [10, 13, 12, 11, 15, 12],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [11, 12, 12, 11, 15, 12],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1410,
                statBlock: [15, 13, 12, 11, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 11, 13, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 11, 11, 11, 13, 12],
                        myRaceModifiers:     [0, 1, 1, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1410,
                statBlock: [12, 13, 13, 11, 15, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [9, 13, 13, 11, 15, 12],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [10, 12, 13, 11, 15, 12],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1410,
                statBlock: [13, 13, 13, 11, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [10, 13, 13, 11, 13, 14],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [11, 12, 13, 11, 13, 14],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1410,
                statBlock: [13, 13, 13, 11, 15, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [10, 13, 13, 11, 15, 11],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [11, 12, 13, 11, 15, 11],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1410,
                statBlock: [13, 13, 13, 11, 15, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [11, 13, 13, 11, 14, 12],
                        myRaceModifiers:     [1, 0, 0, 0, 1, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1410,
                statBlock: [15, 13, 13, 11, 12, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 11, 12, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 11, 12, 11, 12, 12],
                        myRaceModifiers:     [0, 1, 1, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1410,
                statBlock: [15, 13, 13, 11, 13, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 11, 13, 11],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 11, 12, 11, 13, 11],
                        myRaceModifiers:     [0, 1, 1, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1410,
                statBlock: [15, 13, 13, 11, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [14, 11, 13, 11, 13, 12],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [14, 11, 13, 11, 13, 12],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1410,
                statBlock: [12, 13, 15, 11, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [9, 13, 15, 11, 13, 12],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [10, 12, 15, 11, 13, 12],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1410,
                statBlock: [13, 13, 15, 11, 12, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [10, 13, 15, 11, 12, 12],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [11, 12, 15, 11, 12, 12],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1410,
                statBlock: [13, 13, 15, 11, 13, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [10, 13, 15, 11, 13, 11],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [11, 12, 15, 11, 13, 11],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1410,
                statBlock: [13, 13, 15, 11, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [11, 13, 14, 11, 13, 12],
                        myRaceModifiers:     [1, 0, 1, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1410,
                statBlock: [13, 15, 12, 11, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [10, 15, 12, 11, 13, 12],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [11, 15, 11, 11, 13, 12],
                        myRaceModifiers:     [1, 0, 1, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1410,
                statBlock: [12, 15, 13, 11, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [9, 15, 13, 11, 13, 12],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [10, 15, 12, 11, 13, 12],
                        myRaceModifiers:     [1, 0, 1, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1410,
                statBlock: [13, 15, 13, 11, 12, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [10, 15, 13, 11, 12, 12],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [11, 15, 12, 11, 12, 12],
                        myRaceModifiers:     [1, 0, 1, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1410,
                statBlock: [13, 15, 13, 11, 13, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [10, 15, 13, 11, 13, 11],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [11, 15, 12, 11, 13, 11],
                        myRaceModifiers:     [1, 0, 1, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1410,
                statBlock: [13, 15, 13, 11, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [11, 14, 13, 11, 13, 12],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [11, 14, 13, 11, 13, 12],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1410,
                statBlock: [13, 11, 13, 12, 15, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [10, 11, 13, 12, 15, 12],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            Actor,
                            Athlete,
                        ],
                        myBasePointBuyArray: [11, 10, 13, 12, 15, 12],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 1],
                    },
                ],
            },
        ];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

