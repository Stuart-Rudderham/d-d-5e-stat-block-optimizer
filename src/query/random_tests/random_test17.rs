use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test17" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Weighted,
        statOrdering: [
            Wisdom,
            Constitution,
            Dexterity,
            Intelligence,
            Strength,
            Charisma,
        ],
        isReversed: false,
        availableRaces: vec![
            Aarakocra,
            Dwarf_Grey,
            Genasi_Water,
            Gnome_Forest,
            Gnome_Rock,
            Halfling_MarkOfHospitality,
            Shifter_SwiftStride,
            Tabaxi,
            Tiefling_Feral,
        ],
        requiredFeats: vec![
            MagicInitiate,
        ],
        constraints: vec![
            BinCmp_StatModifier_StatModifier(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Wisdom,
                    ),
                    binCmp: GE,
                    rhsValue: GetStatModifier(
                        Strength,
                    ),
                },
            ),
            UnCmp_StatModifier(
                UnaryCompare {
                    value: GetStatModifier(
                        Intelligence,
                    ),
                    unCmp: IsMin,
                },
            ),
            BinCmp_StatModifier_Constant(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Dexterity,
                    ),
                    binCmp: LT,
                    rhsValue: GetConstant(
                        2,
                    ),
                },
            ),
            UnCmp_StatValue(
                UnaryCompare {
                    value: GetStatValue(
                        Intelligence,
                    ),
                    unCmp: IsEven,
                },
            ),
        ],
        numberOfASIs: 2,
        doAllRacesHaveFreeFeat: true,
        canRearrangeRaceModifiers: true,
        maxNumberOfResults: 18,
        optCustomPointBuyArray: Some(
            [10, 10, 20, 10, 10, 6],
        ),
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 112680,
            numUniqueCombinationsPassedConstraints: 2194,
            numStatBlocksEjected: 103,
            numUniqueCombinationsEjected: 1152,
            numUniqueCombinationsBelowLowerBound: 2048,
            numStatBlocksShiftedDuringInsertion: 1063,
            numDuplicateRaceStatBlocksRemoved: 0,
            numUniqueStatBlockAfterRacePass: 30,
            numUniqueStatBlockAfterFeatPass: 30,
            numFinalStatBlockCombinations: 162,
        };

    let expectedStatBlocks = 
        vec![
            RankedStatBlock {
                rank: 1010030,
                statBlock: [10, 11, 16, 6, 20, 10],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Aarakocra,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [0, 1, 2, 0, 0, 0],
                        myASIs:              [0, 0, 4, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_Grey,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [0, 1, 2, 0, 0, 0],
                        myASIs:              [0, 0, 4, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Genasi_Water,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [0, 1, 2, 0, 0, 0],
                        myASIs:              [0, 0, 4, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [0, 1, 2, 0, 0, 0],
                        myASIs:              [0, 0, 4, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Rock,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [0, 1, 2, 0, 0, 0],
                        myASIs:              [0, 0, 4, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHospitality,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [0, 1, 2, 0, 0, 0],
                        myASIs:              [0, 0, 4, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Shifter_SwiftStride,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [0, 1, 2, 0, 0, 0],
                        myASIs:              [0, 0, 4, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [0, 1, 2, 0, 0, 0],
                        myASIs:              [0, 0, 4, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tiefling_Feral,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [0, 1, 2, 0, 0, 0],
                        myASIs:              [0, 0, 4, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1010030,
                statBlock: [11, 10, 16, 6, 20, 10],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Aarakocra,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [1, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 4, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_Grey,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [1, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 4, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Genasi_Water,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [1, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 4, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [1, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 4, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Rock,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [1, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 4, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHospitality,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [1, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 4, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Shifter_SwiftStride,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [1, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 4, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [1, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 4, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tiefling_Feral,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [1, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 4, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1010030,
                statBlock: [10, 10, 16, 6, 20, 11],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Aarakocra,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 1],
                        myASIs:              [0, 0, 4, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_Grey,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 1],
                        myASIs:              [0, 0, 4, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Genasi_Water,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 1],
                        myASIs:              [0, 0, 4, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 1],
                        myASIs:              [0, 0, 4, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Rock,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 1],
                        myASIs:              [0, 0, 4, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHospitality,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 1],
                        myASIs:              [0, 0, 4, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Shifter_SwiftStride,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 1],
                        myASIs:              [0, 0, 4, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 1],
                        myASIs:              [0, 0, 4, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tiefling_Feral,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 1],
                        myASIs:              [0, 0, 4, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1010030,
                statBlock: [16, 10, 11, 6, 20, 10],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Aarakocra,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [2, 0, 1, 0, 0, 0],
                        myASIs:              [4, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_Grey,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [2, 0, 1, 0, 0, 0],
                        myASIs:              [4, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Genasi_Water,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [2, 0, 1, 0, 0, 0],
                        myASIs:              [4, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [2, 0, 1, 0, 0, 0],
                        myASIs:              [4, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Rock,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [2, 0, 1, 0, 0, 0],
                        myASIs:              [4, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHospitality,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [2, 0, 1, 0, 0, 0],
                        myASIs:              [4, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Shifter_SwiftStride,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [2, 0, 1, 0, 0, 0],
                        myASIs:              [4, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [2, 0, 1, 0, 0, 0],
                        myASIs:              [4, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tiefling_Feral,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [2, 0, 1, 0, 0, 0],
                        myASIs:              [4, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1010030,
                statBlock: [10, 10, 11, 6, 20, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Aarakocra,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_Grey,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Genasi_Water,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Rock,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHospitality,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Shifter_SwiftStride,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tiefling_Feral,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1010030,
                statBlock: [16, 11, 10, 6, 20, 10],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Aarakocra,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [2, 1, 0, 0, 0, 0],
                        myASIs:              [4, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_Grey,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [2, 1, 0, 0, 0, 0],
                        myASIs:              [4, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Genasi_Water,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [2, 1, 0, 0, 0, 0],
                        myASIs:              [4, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [2, 1, 0, 0, 0, 0],
                        myASIs:              [4, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Rock,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [2, 1, 0, 0, 0, 0],
                        myASIs:              [4, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHospitality,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [2, 1, 0, 0, 0, 0],
                        myASIs:              [4, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Shifter_SwiftStride,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [2, 1, 0, 0, 0, 0],
                        myASIs:              [4, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [2, 1, 0, 0, 0, 0],
                        myASIs:              [4, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tiefling_Feral,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [2, 1, 0, 0, 0, 0],
                        myASIs:              [4, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1010030,
                statBlock: [10, 11, 10, 6, 20, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Aarakocra,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_Grey,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Genasi_Water,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Rock,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHospitality,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Shifter_SwiftStride,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tiefling_Feral,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1010030,
                statBlock: [16, 10, 10, 6, 20, 11],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Aarakocra,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 1],
                        myASIs:              [4, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_Grey,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 1],
                        myASIs:              [4, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Genasi_Water,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 1],
                        myASIs:              [4, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 1],
                        myASIs:              [4, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Rock,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 1],
                        myASIs:              [4, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHospitality,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 1],
                        myASIs:              [4, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Shifter_SwiftStride,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 1],
                        myASIs:              [4, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 1],
                        myASIs:              [4, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tiefling_Feral,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 1],
                        myASIs:              [4, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1010030,
                statBlock: [11, 10, 10, 6, 20, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Aarakocra,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_Grey,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Genasi_Water,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Rock,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHospitality,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Shifter_SwiftStride,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tiefling_Feral,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 20, 10],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1010030,
                statBlock: [10, 11, 20, 6, 16, 10],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Aarakocra,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_Grey,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Genasi_Water,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Rock,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHospitality,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Shifter_SwiftStride,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tiefling_Feral,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1010030,
                statBlock: [11, 10, 20, 6, 16, 10],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Aarakocra,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [1, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_Grey,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [1, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Genasi_Water,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [1, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [1, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Rock,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [1, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHospitality,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [1, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Shifter_SwiftStride,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [1, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [1, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tiefling_Feral,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [1, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1010030,
                statBlock: [10, 10, 20, 6, 16, 11],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Aarakocra,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_Grey,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Genasi_Water,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Rock,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHospitality,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Shifter_SwiftStride,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tiefling_Feral,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1010030,
                statBlock: [10, 10, 11, 6, 16, 20],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Aarakocra,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 10, 20],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_Grey,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 10, 20],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Genasi_Water,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 10, 20],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 10, 20],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Rock,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 10, 20],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHospitality,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 10, 20],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Shifter_SwiftStride,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 10, 20],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 10, 20],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tiefling_Feral,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 10, 20],
                        myRaceModifiers:     [0, 0, 1, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1010030,
                statBlock: [10, 11, 10, 6, 16, 20],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Aarakocra,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 10, 20],
                        myRaceModifiers:     [0, 1, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_Grey,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 10, 20],
                        myRaceModifiers:     [0, 1, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Genasi_Water,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 10, 20],
                        myRaceModifiers:     [0, 1, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 10, 20],
                        myRaceModifiers:     [0, 1, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Rock,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 10, 20],
                        myRaceModifiers:     [0, 1, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHospitality,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 10, 20],
                        myRaceModifiers:     [0, 1, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Shifter_SwiftStride,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 10, 20],
                        myRaceModifiers:     [0, 1, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 10, 20],
                        myRaceModifiers:     [0, 1, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tiefling_Feral,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 10, 20],
                        myRaceModifiers:     [0, 1, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1010030,
                statBlock: [11, 10, 10, 6, 16, 20],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Aarakocra,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 10, 20],
                        myRaceModifiers:     [1, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_Grey,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 10, 20],
                        myRaceModifiers:     [1, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Genasi_Water,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 10, 20],
                        myRaceModifiers:     [1, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 10, 20],
                        myRaceModifiers:     [1, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Rock,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 10, 20],
                        myRaceModifiers:     [1, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHospitality,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 10, 20],
                        myRaceModifiers:     [1, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Shifter_SwiftStride,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 10, 20],
                        myRaceModifiers:     [1, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 10, 20],
                        myRaceModifiers:     [1, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tiefling_Feral,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 10, 20],
                        myRaceModifiers:     [1, 0, 0, 0, 2, 0],
                        myASIs:              [0, 0, 0, 0, 4, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1010030,
                statBlock: [10, 10, 20, 6, 11, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Aarakocra,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_Grey,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Genasi_Water,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Rock,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHospitality,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Shifter_SwiftStride,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tiefling_Feral,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1010030,
                statBlock: [10, 10, 16, 6, 11, 20],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Aarakocra,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 10, 20],
                        myRaceModifiers:     [0, 0, 2, 0, 1, 0],
                        myASIs:              [0, 0, 4, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_Grey,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 10, 20],
                        myRaceModifiers:     [0, 0, 2, 0, 1, 0],
                        myASIs:              [0, 0, 4, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Genasi_Water,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 10, 20],
                        myRaceModifiers:     [0, 0, 2, 0, 1, 0],
                        myASIs:              [0, 0, 4, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 10, 20],
                        myRaceModifiers:     [0, 0, 2, 0, 1, 0],
                        myASIs:              [0, 0, 4, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Rock,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 10, 20],
                        myRaceModifiers:     [0, 0, 2, 0, 1, 0],
                        myASIs:              [0, 0, 4, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHospitality,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 10, 20],
                        myRaceModifiers:     [0, 0, 2, 0, 1, 0],
                        myASIs:              [0, 0, 4, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Shifter_SwiftStride,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 10, 20],
                        myRaceModifiers:     [0, 0, 2, 0, 1, 0],
                        myASIs:              [0, 0, 4, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 10, 20],
                        myRaceModifiers:     [0, 0, 2, 0, 1, 0],
                        myASIs:              [0, 0, 4, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tiefling_Feral,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 10, 6, 10, 20],
                        myRaceModifiers:     [0, 0, 2, 0, 1, 0],
                        myASIs:              [0, 0, 4, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1010030,
                statBlock: [10, 11, 20, 6, 10, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Aarakocra,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Dwarf_Grey,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Genasi_Water,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Rock,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHospitality,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Shifter_SwiftStride,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tiefling_Feral,
                        myFeats: vec![
                            MagicInitiate,
                        ],
                        myBasePointBuyArray: [10, 10, 20, 6, 10, 10],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 4],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
        ];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

