use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test02" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Weighted,
        statOrdering: [
            Constitution,
            Strength,
            Charisma,
            Wisdom,
            Intelligence,
            Dexterity,
        ],
        isReversed: true,
        availableRaces: vec![
            Aarakocra,
            Aasimar_Fallen,
            Aasimar_Protector,
            Bugbear,
            CustomLineage,
            Changling,
            DragonBorn,
            Dwarf_Grey,
            Dwarf_Hill,
            Dwarf_MarkOfWarding,
            Dwarf_Mountain,
            Elf_Drow,
            Elf_High,
            Elf_MarkOfShadow,
            Elf_Wood,
            Firbolg,
            Genasi_Earth,
            Genasi_Fire,
            Genasi_Water,
            Gnome_Deep,
            Gnome_Forest,
            Gnome_MarkOfScribing,
            Gnome_Rock,
            Goblin,
            Goliath,
            HalfElf,
            HalfElf_MarkOfDetection,
            Halfling_Ghostwise,
            Halfling_Lightfoot,
            Halfling_MarkOfHealing,
            Halfling_MarkOfHospitality,
            Halfling_Stout,
            HalfOrc,
            HalfOrc_MarkOfFinding,
            Hobgoblin,
            Human,
            Human_MarkOfFinding,
            Human_MarkOfMaking,
            Human_MarkOfPassage,
            Human_MarkOfSentinel,
            Human_Variant,
            Kalashtar,
            Kenku,
            Kobold,
            Lizardfolk,
            Orc,
            Orc_Eberron,
            Shifter_BeastHide,
            Shifter_LongTooth,
            Shifter_SwiftStride,
            Shifter_WildHunt,
            Tabaxi,
            Tiefling,
            Tiefling_Feral,
            Tortle,
            Triton,
            Warforged,
            YuanTiPureblood,
        ],
        requiredFeats: vec![
            MountedCombatant,
        ],
        constraints: vec![
            UnCmp_StatModifier(
                UnaryCompare {
                    value: GetStatModifier(
                        Intelligence,
                    ),
                    unCmp: IsEven,
                },
            ),
            UnCmp_StatModifier(
                UnaryCompare {
                    value: GetStatModifier(
                        Dexterity,
                    ),
                    unCmp: IsEven,
                },
            ),
            BinCmp_StatValue_StatValue(
                BinaryCompare {
                    lhsValue: GetStatValue(
                        Wisdom,
                    ),
                    binCmp: GE,
                    rhsValue: GetStatValue(
                        Strength,
                    ),
                },
            ),
        ],
        numberOfASIs: 0,
        doAllRacesHaveFreeFeat: false,
        canRearrangeRaceModifiers: false,
        maxNumberOfResults: 1,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 257922,
            numUniqueCombinationsPassedConstraints: 39947,
            numStatBlocksEjected: 13,
            numUniqueCombinationsEjected: 13,
            numUniqueCombinationsBelowLowerBound: 39919,
            numStatBlocksShiftedDuringInsertion: 0,
            numDuplicateRaceStatBlocksRemoved: 13,
            numUniqueStatBlockAfterRacePass: 65,
            numUniqueStatBlockAfterFeatPass: 21,
            numFinalStatBlockCombinations: 2,
        };

    let expectedStatBlocks = 
        vec![
            RankedStatBlock {
                rank: 1320,
                statBlock: [13, 15, 10, 11, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            MountedCombatant,
                        ],
                        myBasePointBuyArray: [11, 15, 10, 11, 13, 13],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            MountedCombatant,
                        ],
                        myBasePointBuyArray: [12, 15, 9, 11, 13, 13],
                        myRaceModifiers:     [1, 0, 1, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
        ];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

