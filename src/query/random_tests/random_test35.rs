use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test35" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Unranked,
        statOrdering: [
            Strength,
            Intelligence,
            Wisdom,
            Dexterity,
            Charisma,
            Constitution,
        ],
        isReversed: false,
        availableRaces: vec![
            Aarakocra,
            Aasimar_Fallen,
            Aasimar_Protector,
            CustomLineage,
            Changling,
            DragonBorn,
            Elf_High,
            Elf_MarkOfShadow,
            Elf_Wood,
            Genasi_Earth,
            Genasi_Water,
            Gnome_MarkOfScribing,
            Goliath,
            HalfElf_MarkOfDetection,
            HalfElf_MarkOfStorm,
            Halfling_Ghostwise,
            Halfling_MarkOfHealing,
            Halfling_MarkOfHospitality,
            Halfling_Stout,
            HalfOrc,
            Hobgoblin,
            Human_MarkOfHandling,
            Human_Variant,
            Kalashtar,
            Kenku,
            Kobold,
            Orc,
            Orc_Eberron,
            Shifter_SwiftStride,
            Shifter_WildHunt,
            Tortle,
            Warforged,
        ],
        requiredFeats: vec![
            RevenantBlade,
        ],
        constraints: vec![
            BinCmp_StatValue_StatValue(
                BinaryCompare {
                    lhsValue: GetStatValue(
                        Constitution,
                    ),
                    binCmp: GE,
                    rhsValue: GetStatValue(
                        Dexterity,
                    ),
                },
            ),
        ],
        numberOfASIs: 2,
        doAllRacesHaveFreeFeat: false,
        canRearrangeRaceModifiers: true,
        maxNumberOfResults: 12,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 3576265,
            numUniqueCombinationsPassedConstraints: 1796981,
            numStatBlocksEjected: 503,
            numUniqueCombinationsEjected: 1551,
            numUniqueCombinationsBelowLowerBound: 1796452,
            numStatBlocksShiftedDuringInsertion: 3130,
            numDuplicateRaceStatBlocksRemoved: 0,
            numUniqueStatBlockAfterRacePass: 201,
            numUniqueStatBlockAfterFeatPass: 55,
            numFinalStatBlockCombinations: 36,
        };

    let expectedStatBlocks = 
        vec![
            RankedStatBlock {
                rank: 0,
                statBlock: [20, 8, 8, 16, 15, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Elf_High,
                        myFeats: vec![
                            RevenantBlade,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 15, 8],
                        myRaceModifiers:     [2, 0, 0, 1, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Elf_MarkOfShadow,
                        myFeats: vec![
                            RevenantBlade,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 15, 8],
                        myRaceModifiers:     [2, 0, 0, 1, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Elf_Wood,
                        myFeats: vec![
                            RevenantBlade,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 15, 8],
                        myRaceModifiers:     [2, 0, 0, 1, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [20, 9, 9, 16, 14, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Elf_High,
                        myFeats: vec![
                            RevenantBlade,
                        ],
                        myBasePointBuyArray: [15, 9, 9, 15, 14, 8],
                        myRaceModifiers:     [2, 0, 0, 1, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Elf_MarkOfShadow,
                        myFeats: vec![
                            RevenantBlade,
                        ],
                        myBasePointBuyArray: [15, 9, 9, 15, 14, 8],
                        myRaceModifiers:     [2, 0, 0, 1, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Elf_Wood,
                        myFeats: vec![
                            RevenantBlade,
                        ],
                        myBasePointBuyArray: [15, 9, 9, 15, 14, 8],
                        myRaceModifiers:     [2, 0, 0, 1, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [20, 8, 8, 16, 14, 10],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Elf_High,
                        myFeats: vec![
                            RevenantBlade,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 14, 10],
                        myRaceModifiers:     [2, 0, 0, 1, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Elf_MarkOfShadow,
                        myFeats: vec![
                            RevenantBlade,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 14, 10],
                        myRaceModifiers:     [2, 0, 0, 1, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Elf_Wood,
                        myFeats: vec![
                            RevenantBlade,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 14, 10],
                        myRaceModifiers:     [2, 0, 0, 1, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [20, 8, 9, 16, 14, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Elf_High,
                        myFeats: vec![
                            RevenantBlade,
                        ],
                        myBasePointBuyArray: [15, 8, 9, 15, 14, 9],
                        myRaceModifiers:     [2, 0, 0, 1, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Elf_MarkOfShadow,
                        myFeats: vec![
                            RevenantBlade,
                        ],
                        myBasePointBuyArray: [15, 8, 9, 15, 14, 9],
                        myRaceModifiers:     [2, 0, 0, 1, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Elf_Wood,
                        myFeats: vec![
                            RevenantBlade,
                        ],
                        myBasePointBuyArray: [15, 8, 9, 15, 14, 9],
                        myRaceModifiers:     [2, 0, 0, 1, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [20, 8, 10, 16, 14, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Elf_High,
                        myFeats: vec![
                            RevenantBlade,
                        ],
                        myBasePointBuyArray: [15, 8, 10, 15, 14, 8],
                        myRaceModifiers:     [2, 0, 0, 1, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Elf_MarkOfShadow,
                        myFeats: vec![
                            RevenantBlade,
                        ],
                        myBasePointBuyArray: [15, 8, 10, 15, 14, 8],
                        myRaceModifiers:     [2, 0, 0, 1, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Elf_Wood,
                        myFeats: vec![
                            RevenantBlade,
                        ],
                        myBasePointBuyArray: [15, 8, 10, 15, 14, 8],
                        myRaceModifiers:     [2, 0, 0, 1, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [20, 10, 10, 16, 13, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Elf_High,
                        myFeats: vec![
                            RevenantBlade,
                        ],
                        myBasePointBuyArray: [15, 10, 10, 15, 13, 8],
                        myRaceModifiers:     [2, 0, 0, 1, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Elf_MarkOfShadow,
                        myFeats: vec![
                            RevenantBlade,
                        ],
                        myBasePointBuyArray: [15, 10, 10, 15, 13, 8],
                        myRaceModifiers:     [2, 0, 0, 1, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Elf_Wood,
                        myFeats: vec![
                            RevenantBlade,
                        ],
                        myBasePointBuyArray: [15, 10, 10, 15, 13, 8],
                        myRaceModifiers:     [2, 0, 0, 1, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [20, 9, 9, 16, 13, 10],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Elf_High,
                        myFeats: vec![
                            RevenantBlade,
                        ],
                        myBasePointBuyArray: [15, 9, 9, 15, 13, 10],
                        myRaceModifiers:     [2, 0, 0, 1, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Elf_MarkOfShadow,
                        myFeats: vec![
                            RevenantBlade,
                        ],
                        myBasePointBuyArray: [15, 9, 9, 15, 13, 10],
                        myRaceModifiers:     [2, 0, 0, 1, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Elf_Wood,
                        myFeats: vec![
                            RevenantBlade,
                        ],
                        myBasePointBuyArray: [15, 9, 9, 15, 13, 10],
                        myRaceModifiers:     [2, 0, 0, 1, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [20, 9, 10, 16, 13, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Elf_High,
                        myFeats: vec![
                            RevenantBlade,
                        ],
                        myBasePointBuyArray: [15, 9, 10, 15, 13, 9],
                        myRaceModifiers:     [2, 0, 0, 1, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Elf_MarkOfShadow,
                        myFeats: vec![
                            RevenantBlade,
                        ],
                        myBasePointBuyArray: [15, 9, 10, 15, 13, 9],
                        myRaceModifiers:     [2, 0, 0, 1, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Elf_Wood,
                        myFeats: vec![
                            RevenantBlade,
                        ],
                        myBasePointBuyArray: [15, 9, 10, 15, 13, 9],
                        myRaceModifiers:     [2, 0, 0, 1, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [20, 9, 11, 16, 13, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Elf_High,
                        myFeats: vec![
                            RevenantBlade,
                        ],
                        myBasePointBuyArray: [15, 9, 11, 15, 13, 8],
                        myRaceModifiers:     [2, 0, 0, 1, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Elf_MarkOfShadow,
                        myFeats: vec![
                            RevenantBlade,
                        ],
                        myBasePointBuyArray: [15, 9, 11, 15, 13, 8],
                        myRaceModifiers:     [2, 0, 0, 1, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Elf_Wood,
                        myFeats: vec![
                            RevenantBlade,
                        ],
                        myBasePointBuyArray: [15, 9, 11, 15, 13, 8],
                        myRaceModifiers:     [2, 0, 0, 1, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [20, 8, 8, 16, 13, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Elf_High,
                        myFeats: vec![
                            RevenantBlade,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 13, 12],
                        myRaceModifiers:     [2, 0, 0, 1, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Elf_MarkOfShadow,
                        myFeats: vec![
                            RevenantBlade,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 13, 12],
                        myRaceModifiers:     [2, 0, 0, 1, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Elf_Wood,
                        myFeats: vec![
                            RevenantBlade,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 13, 12],
                        myRaceModifiers:     [2, 0, 0, 1, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [20, 8, 9, 16, 13, 11],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Elf_High,
                        myFeats: vec![
                            RevenantBlade,
                        ],
                        myBasePointBuyArray: [15, 8, 9, 15, 13, 11],
                        myRaceModifiers:     [2, 0, 0, 1, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Elf_MarkOfShadow,
                        myFeats: vec![
                            RevenantBlade,
                        ],
                        myBasePointBuyArray: [15, 8, 9, 15, 13, 11],
                        myRaceModifiers:     [2, 0, 0, 1, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Elf_Wood,
                        myFeats: vec![
                            RevenantBlade,
                        ],
                        myBasePointBuyArray: [15, 8, 9, 15, 13, 11],
                        myRaceModifiers:     [2, 0, 0, 1, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [20, 8, 10, 16, 13, 10],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Elf_High,
                        myFeats: vec![
                            RevenantBlade,
                        ],
                        myBasePointBuyArray: [15, 8, 10, 15, 13, 10],
                        myRaceModifiers:     [2, 0, 0, 1, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Elf_MarkOfShadow,
                        myFeats: vec![
                            RevenantBlade,
                        ],
                        myBasePointBuyArray: [15, 8, 10, 15, 13, 10],
                        myRaceModifiers:     [2, 0, 0, 1, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Elf_Wood,
                        myFeats: vec![
                            RevenantBlade,
                        ],
                        myBasePointBuyArray: [15, 8, 10, 15, 13, 10],
                        myRaceModifiers:     [2, 0, 0, 1, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
        ];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

