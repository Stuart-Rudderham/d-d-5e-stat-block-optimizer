use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test24" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Weighted,
        statOrdering: [
            Constitution,
            Charisma,
            Intelligence,
            Dexterity,
            Strength,
            Wisdom,
        ],
        isReversed: false,
        availableRaces: vec![
            Changling,
            Dwarf_Grey,
            Elf_Wood,
            Genasi_Air,
            Gnome_MarkOfScribing,
            Halfling_Lightfoot,
            HalfOrc,
            Human,
            Human_MarkOfHandling,
            Shifter_BeastHide,
            Shifter_LongTooth,
            Tabaxi,
        ],
        requiredFeats: vec![],
        constraints: vec![
            UnCmp_StatModifier(
                UnaryCompare {
                    value: GetStatModifier(
                        Charisma,
                    ),
                    unCmp: IsOdd,
                },
            ),
            BinCmp_StatModifier_Constant(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Intelligence,
                    ),
                    binCmp: EQ,
                    rhsValue: GetConstant(
                        2,
                    ),
                },
            ),
            BinCmp_StatValue_StatValue(
                BinaryCompare {
                    lhsValue: GetStatValue(
                        Dexterity,
                    ),
                    binCmp: GE,
                    rhsValue: GetStatValue(
                        Dexterity,
                    ),
                },
            ),
        ],
        numberOfASIs: 1,
        doAllRacesHaveFreeFeat: true,
        canRearrangeRaceModifiers: false,
        maxNumberOfResults: 20,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 6372254,
            numUniqueCombinationsPassedConstraints: 651289,
            numStatBlocksEjected: 425,
            numUniqueCombinationsEjected: 4242,
            numUniqueCombinationsBelowLowerBound: 650837,
            numStatBlocksShiftedDuringInsertion: 4312,
            numDuplicateRaceStatBlocksRemoved: 0,
            numUniqueStatBlockAfterRacePass: 18,
            numUniqueStatBlockAfterFeatPass: 98,
            numFinalStatBlockCombinations: 193,
        };

    let expectedStatBlocks = 
        vec![
            RankedStatBlock {
                rank: 1011012,
                statBlock: [8, 10, 16, 14, 8, 20],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [8, 10, 15, 14, 8, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [8, 10, 15, 14, 8, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [8, 10, 15, 14, 8, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [8, 10, 15, 14, 8, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [8, 10, 15, 14, 8, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 10, 15, 14, 8, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [8, 10, 15, 14, 8, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [8, 10, 15, 14, 8, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1011012,
                statBlock: [10, 8, 16, 14, 8, 20],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 14, 8, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 14, 8, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 14, 8, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 14, 8, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 14, 8, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 14, 8, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 14, 8, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 14, 8, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1011012,
                statBlock: [8, 8, 16, 14, 10, 20],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 14, 10, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 14, 10, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 14, 10, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 14, 10, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 14, 10, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 14, 10, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 14, 10, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 14, 10, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1011012,
                statBlock: [8, 16, 10, 14, 8, 20],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [8, 15, 10, 14, 8, 15],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [8, 15, 10, 14, 8, 15],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [8, 15, 10, 14, 8, 15],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [8, 15, 10, 14, 8, 15],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [8, 15, 10, 14, 8, 15],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 15, 10, 14, 8, 15],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [8, 15, 10, 14, 8, 15],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [8, 15, 10, 14, 8, 15],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1011012,
                statBlock: [16, 8, 10, 14, 8, 20],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [15, 8, 10, 14, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 8, 10, 14, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 10, 14, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [15, 8, 10, 14, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 10, 14, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 8, 10, 14, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 8, 10, 14, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 8, 10, 14, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1011012,
                statBlock: [8, 8, 10, 14, 16, 20],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [8, 8, 10, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [8, 8, 10, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [8, 8, 10, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [8, 8, 10, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [8, 8, 10, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 8, 10, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [8, 8, 10, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [8, 8, 10, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1011012,
                statBlock: [8, 20, 10, 14, 8, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 15, 10, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            Gunner,
                        ],
                        myBasePointBuyArray: [8, 15, 10, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 15, 10, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            ModeratelyArmored,
                        ],
                        myBasePointBuyArray: [8, 15, 10, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            Piercer,
                        ],
                        myBasePointBuyArray: [8, 15, 10, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            Resilient_Dex,
                        ],
                        myBasePointBuyArray: [8, 15, 10, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            SecondChance,
                        ],
                        myBasePointBuyArray: [8, 15, 10, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 15, 10, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            Slasher,
                        ],
                        myBasePointBuyArray: [8, 15, 10, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            SquatNimbleness,
                        ],
                        myBasePointBuyArray: [8, 15, 10, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            WeaponMaster,
                        ],
                        myBasePointBuyArray: [8, 15, 10, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 15, 10, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            Gunner,
                        ],
                        myBasePointBuyArray: [8, 15, 10, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 15, 10, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            ModeratelyArmored,
                        ],
                        myBasePointBuyArray: [8, 15, 10, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            Piercer,
                        ],
                        myBasePointBuyArray: [8, 15, 10, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            Resilient_Dex,
                        ],
                        myBasePointBuyArray: [8, 15, 10, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 15, 10, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            Slasher,
                        ],
                        myBasePointBuyArray: [8, 15, 10, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            WeaponMaster,
                        ],
                        myBasePointBuyArray: [8, 15, 10, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1011012,
                statBlock: [8, 8, 10, 14, 20, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human_MarkOfHandling,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [8, 8, 10, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfHandling,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [8, 8, 10, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfHandling,
                        myFeats: vec![
                            Resilient_Wis,
                        ],
                        myBasePointBuyArray: [8, 8, 10, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfHandling,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [8, 8, 10, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfHandling,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 8, 10, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfHandling,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [8, 8, 10, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfHandling,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [8, 8, 10, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1011012,
                statBlock: [10, 16, 8, 14, 8, 20],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [10, 15, 8, 14, 8, 15],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [10, 15, 8, 14, 8, 15],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [10, 15, 8, 14, 8, 15],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [10, 15, 8, 14, 8, 15],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [10, 15, 8, 14, 8, 15],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [10, 15, 8, 14, 8, 15],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [10, 15, 8, 14, 8, 15],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [10, 15, 8, 14, 8, 15],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1011012,
                statBlock: [8, 16, 8, 14, 10, 20],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [8, 15, 8, 14, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [8, 15, 8, 14, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [8, 15, 8, 14, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [8, 15, 8, 14, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [8, 15, 8, 14, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 15, 8, 14, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [8, 15, 8, 14, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [8, 15, 8, 14, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1011012,
                statBlock: [16, 10, 8, 14, 8, 20],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [15, 10, 8, 14, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 10, 8, 14, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 10, 8, 14, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [15, 10, 8, 14, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 10, 8, 14, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 10, 8, 14, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 10, 8, 14, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 10, 8, 14, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1011012,
                statBlock: [8, 10, 8, 14, 16, 20],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [8, 10, 8, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [8, 10, 8, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [8, 10, 8, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [8, 10, 8, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [8, 10, 8, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 10, 8, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [8, 10, 8, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [8, 10, 8, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1011012,
                statBlock: [16, 8, 8, 14, 10, 20],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 14, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 14, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 14, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 14, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 14, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 14, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 14, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 14, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1011012,
                statBlock: [10, 8, 8, 14, 16, 20],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [10, 8, 8, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [10, 8, 8, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [10, 8, 8, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [10, 8, 8, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [10, 8, 8, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [10, 8, 8, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [10, 8, 8, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [10, 8, 8, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1011012,
                statBlock: [10, 20, 8, 14, 8, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [10, 15, 8, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            Gunner,
                        ],
                        myBasePointBuyArray: [10, 15, 8, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 15, 8, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            ModeratelyArmored,
                        ],
                        myBasePointBuyArray: [10, 15, 8, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            Piercer,
                        ],
                        myBasePointBuyArray: [10, 15, 8, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            Resilient_Dex,
                        ],
                        myBasePointBuyArray: [10, 15, 8, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            SecondChance,
                        ],
                        myBasePointBuyArray: [10, 15, 8, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [10, 15, 8, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            Slasher,
                        ],
                        myBasePointBuyArray: [10, 15, 8, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            SquatNimbleness,
                        ],
                        myBasePointBuyArray: [10, 15, 8, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            WeaponMaster,
                        ],
                        myBasePointBuyArray: [10, 15, 8, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [10, 15, 8, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            Gunner,
                        ],
                        myBasePointBuyArray: [10, 15, 8, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 15, 8, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            ModeratelyArmored,
                        ],
                        myBasePointBuyArray: [10, 15, 8, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            Piercer,
                        ],
                        myBasePointBuyArray: [10, 15, 8, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            Resilient_Dex,
                        ],
                        myBasePointBuyArray: [10, 15, 8, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [10, 15, 8, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            Slasher,
                        ],
                        myBasePointBuyArray: [10, 15, 8, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            WeaponMaster,
                        ],
                        myBasePointBuyArray: [10, 15, 8, 14, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1011012,
                statBlock: [8, 20, 8, 14, 10, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 15, 8, 14, 10, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            Gunner,
                        ],
                        myBasePointBuyArray: [8, 15, 8, 14, 10, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 15, 8, 14, 10, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            ModeratelyArmored,
                        ],
                        myBasePointBuyArray: [8, 15, 8, 14, 10, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            Piercer,
                        ],
                        myBasePointBuyArray: [8, 15, 8, 14, 10, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            Resilient_Dex,
                        ],
                        myBasePointBuyArray: [8, 15, 8, 14, 10, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            SecondChance,
                        ],
                        myBasePointBuyArray: [8, 15, 8, 14, 10, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 15, 8, 14, 10, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            Slasher,
                        ],
                        myBasePointBuyArray: [8, 15, 8, 14, 10, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            SquatNimbleness,
                        ],
                        myBasePointBuyArray: [8, 15, 8, 14, 10, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            WeaponMaster,
                        ],
                        myBasePointBuyArray: [8, 15, 8, 14, 10, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 15, 8, 14, 10, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            Gunner,
                        ],
                        myBasePointBuyArray: [8, 15, 8, 14, 10, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 15, 8, 14, 10, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            ModeratelyArmored,
                        ],
                        myBasePointBuyArray: [8, 15, 8, 14, 10, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            Piercer,
                        ],
                        myBasePointBuyArray: [8, 15, 8, 14, 10, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            Resilient_Dex,
                        ],
                        myBasePointBuyArray: [8, 15, 8, 14, 10, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 15, 8, 14, 10, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            Slasher,
                        ],
                        myBasePointBuyArray: [8, 15, 8, 14, 10, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Tabaxi,
                        myFeats: vec![
                            WeaponMaster,
                        ],
                        myBasePointBuyArray: [8, 15, 8, 14, 10, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1011012,
                statBlock: [8, 10, 8, 14, 20, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human_MarkOfHandling,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [8, 10, 8, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfHandling,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [8, 10, 8, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfHandling,
                        myFeats: vec![
                            Resilient_Wis,
                        ],
                        myBasePointBuyArray: [8, 10, 8, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfHandling,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [8, 10, 8, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfHandling,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 10, 8, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfHandling,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [8, 10, 8, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfHandling,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [8, 10, 8, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1011012,
                statBlock: [10, 8, 8, 14, 20, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human_MarkOfHandling,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [10, 8, 8, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfHandling,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [10, 8, 8, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfHandling,
                        myFeats: vec![
                            Resilient_Wis,
                        ],
                        myBasePointBuyArray: [10, 8, 8, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfHandling,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [10, 8, 8, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfHandling,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [10, 8, 8, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfHandling,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [10, 8, 8, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_MarkOfHandling,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [10, 8, 8, 14, 15, 15],
                        myRaceModifiers:     [0, 0, 0, 0, 2, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1011003,
                statBlock: [8, 8, 16, 15, 8, 20],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 15, 8, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 15, 8, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 15, 8, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 15, 8, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 15, 8, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 15, 8, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 15, 8, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 15, 8, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1011003,
                statBlock: [9, 9, 16, 14, 8, 20],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [9, 9, 15, 14, 8, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [9, 9, 15, 14, 8, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [9, 9, 15, 14, 8, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [9, 9, 15, 14, 8, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [9, 9, 15, 14, 8, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [9, 9, 15, 14, 8, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [9, 9, 15, 14, 8, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [9, 9, 15, 14, 8, 15],
                        myRaceModifiers:     [0, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                ],
            },
        ];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

