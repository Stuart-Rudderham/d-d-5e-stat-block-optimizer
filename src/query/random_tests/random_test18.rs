use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test18" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Unranked,
        statOrdering: [
            Wisdom,
            Dexterity,
            Constitution,
            Strength,
            Intelligence,
            Charisma,
        ],
        isReversed: false,
        availableRaces: vec![
            Aasimar_Protector,
            Aasimar_Scourge,
            Bugbear,
            DragonBorn,
            Dwarf_MarkOfWarding,
            Dwarf_Mountain,
            Elf_High,
            Elf_Wood,
            Genasi_Air,
            Genasi_Water,
            Gnome_Deep,
            Gnome_Forest,
            HalfElf,
            HalfElf_MarkOfDetection,
            HalfElf_MarkOfStorm,
            Halfling_Ghostwise,
            Halfling_Lightfoot,
            Halfling_MarkOfHospitality,
            Halfling_Stout,
            HalfOrc,
            HalfOrc_MarkOfFinding,
            Lizardfolk,
            Shifter_LongTooth,
            Shifter_SwiftStride,
            Shifter_WildHunt,
            Tabaxi,
            Tiefling_Feral,
        ],
        requiredFeats: vec![
            FadeAway,
            LightlyArmored,
        ],
        constraints: vec![
            BinCmp_StatValue_StatValue(
                BinaryCompare {
                    lhsValue: GetStatValue(
                        Charisma,
                    ),
                    binCmp: EQ,
                    rhsValue: GetStatValue(
                        Dexterity,
                    ),
                },
            ),
            BinCmp_StatModifier_StatModifier(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Charisma,
                    ),
                    binCmp: GT,
                    rhsValue: GetStatModifier(
                        Intelligence,
                    ),
                },
            ),
            BinCmp_StatValue_StatValue(
                BinaryCompare {
                    lhsValue: GetStatValue(
                        Intelligence,
                    ),
                    binCmp: LE,
                    rhsValue: GetStatValue(
                        Dexterity,
                    ),
                },
            ),
        ],
        numberOfASIs: 2,
        doAllRacesHaveFreeFeat: false,
        canRearrangeRaceModifiers: false,
        maxNumberOfResults: 500,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 49128,
            numUniqueCombinationsPassedConstraints: 1106,
            numStatBlocksEjected: 41,
            numUniqueCombinationsEjected: 128,
            numUniqueCombinationsBelowLowerBound: 16,
            numStatBlocksShiftedDuringInsertion: 54772,
            numDuplicateRaceStatBlocksRemoved: 1052,
            numUniqueStatBlockAfterRacePass: 30,
            numUniqueStatBlockAfterFeatPass: 4,
            numFinalStatBlockCombinations: 1000,
        };

    let expectedStatBlocks = 
        vec![
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 15, 13, 10, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 12, 13, 8, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 12, 13, 8, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 15, 12, 10, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 13, 12, 8, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 13, 12, 8, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 15, 12, 11, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 12, 12, 9, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 12, 12, 9, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [10, 15, 11, 10, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 13, 11, 8, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 13, 11, 8, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 15, 11, 11, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 13, 11, 9, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 13, 11, 9, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 15, 11, 12, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 12, 11, 10, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 12, 11, 10, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 15, 10, 10, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 13, 10, 8, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 13, 10, 8, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [10, 15, 10, 11, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 13, 10, 9, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 13, 10, 9, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 15, 10, 12, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 13, 10, 10, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 13, 10, 10, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 15, 10, 11, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 14, 10, 8, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 14, 10, 8, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 15, 10, 13, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 12, 10, 11, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 12, 10, 11, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 15, 9, 10, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 13, 9, 8, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 13, 9, 8, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 15, 9, 11, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 13, 9, 9, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 13, 9, 9, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [10, 15, 9, 12, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 13, 9, 10, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 13, 9, 10, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [10, 15, 9, 11, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 14, 9, 8, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 14, 9, 8, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 15, 9, 13, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 13, 9, 11, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 13, 9, 11, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 15, 9, 12, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 14, 9, 9, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 14, 9, 9, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 15, 8, 10, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 13, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 13, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 15, 8, 11, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 13, 8, 9, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 13, 8, 9, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 15, 8, 12, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 13, 8, 10, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 13, 8, 10, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 15, 8, 11, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 14, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 14, 8, 8, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [10, 15, 8, 13, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 13, 8, 11, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 13, 8, 11, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [10, 15, 8, 12, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 14, 8, 9, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 14, 8, 9, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 15, 8, 13, 15, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 14, 8, 10, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 14, 8, 10, 15, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 14, 14, 10, 15, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 12, 14, 8, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 12, 14, 8, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 14, 14, 11, 15, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 11, 14, 9, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 11, 14, 9, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 14, 13, 10, 15, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 12, 13, 8, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 12, 13, 8, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [10, 14, 13, 11, 15, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 12, 13, 9, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 12, 13, 9, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 14, 13, 12, 15, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 12, 13, 10, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 12, 13, 10, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 14, 13, 13, 15, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 11, 13, 11, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 11, 13, 11, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 14, 12, 10, 15, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 12, 12, 8, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 12, 12, 8, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 14, 12, 11, 15, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 12, 12, 9, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 12, 12, 9, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [10, 14, 12, 12, 15, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 12, 12, 10, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 12, 12, 10, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 14, 12, 13, 15, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 12, 12, 11, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 12, 12, 11, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 14, 11, 10, 15, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 12, 11, 8, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 12, 11, 8, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 14, 11, 11, 15, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 12, 11, 9, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 12, 11, 9, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 14, 11, 12, 15, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 12, 11, 10, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 12, 11, 10, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [10, 14, 11, 13, 15, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 12, 11, 11, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 12, 11, 11, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 14, 10, 10, 15, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 12, 10, 8, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 12, 10, 8, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 14, 10, 11, 15, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 12, 10, 9, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 12, 10, 9, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 14, 10, 12, 15, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 12, 10, 10, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 12, 10, 10, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 14, 10, 13, 15, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 12, 10, 11, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 12, 10, 11, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 14, 9, 11, 15, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 12, 9, 9, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 12, 9, 9, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 14, 9, 10, 15, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 9, 8, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 9, 8, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 14, 9, 12, 15, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 12, 9, 10, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 12, 9, 10, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 14, 9, 13, 15, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 12, 9, 11, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 12, 9, 11, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 14, 8, 10, 15, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 8, 8, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 8, 8, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 14, 8, 12, 15, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 12, 8, 10, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 12, 8, 10, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 14, 8, 11, 15, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 8, 9, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 8, 9, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 14, 8, 13, 15, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 12, 8, 11, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 12, 8, 11, 15, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [10, 13, 15, 10, 15, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 11, 15, 8, 15, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 11, 15, 8, 15, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 13, 15, 11, 15, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 11, 15, 9, 15, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 11, 15, 9, 15, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 13, 14, 10, 15, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 11, 14, 8, 15, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 11, 14, 8, 15, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 13, 14, 11, 15, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 11, 14, 9, 15, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 11, 14, 9, 15, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 13, 13, 10, 15, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 11, 13, 8, 15, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 11, 13, 8, 15, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 13, 13, 11, 15, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 11, 13, 9, 15, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 11, 13, 9, 15, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 13, 12, 11, 15, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 11, 12, 9, 15, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 11, 12, 9, 15, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 13, 12, 10, 15, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 10, 12, 8, 15, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 10, 12, 8, 15, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 13, 11, 10, 15, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 11, 8, 15, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 11, 8, 15, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 13, 11, 11, 15, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 10, 11, 9, 15, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 10, 11, 9, 15, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 13, 10, 11, 15, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 10, 9, 15, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 10, 9, 15, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 13, 10, 10, 15, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 10, 10, 8, 15, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 10, 10, 8, 15, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 13, 9, 10, 15, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 9, 8, 15, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 9, 8, 15, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 13, 9, 11, 15, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 10, 9, 9, 15, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 10, 9, 9, 15, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 13, 8, 11, 15, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 8, 9, 15, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 8, 9, 15, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 12, 15, 10, 15, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 10, 15, 8, 15, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 10, 15, 8, 15, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 12, 15, 11, 15, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 10, 15, 9, 15, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 10, 15, 9, 15, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 12, 14, 10, 15, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 10, 14, 8, 15, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 10, 14, 8, 15, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 12, 14, 11, 15, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 10, 14, 9, 15, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 10, 14, 9, 15, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 12, 13, 10, 15, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 10, 13, 8, 15, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 10, 13, 8, 15, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 12, 13, 11, 15, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 9, 13, 9, 15, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 9, 13, 9, 15, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 12, 12, 11, 15, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 10, 12, 9, 15, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 10, 12, 9, 15, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 12, 12, 10, 15, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 9, 12, 8, 15, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 9, 12, 8, 15, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 12, 11, 10, 15, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 10, 11, 8, 15, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 10, 11, 8, 15, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 12, 11, 11, 15, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 9, 11, 9, 15, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 9, 11, 9, 15, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 12, 10, 11, 15, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 10, 10, 9, 15, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 10, 10, 9, 15, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 15, 14, 10, 14, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 12, 14, 8, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 12, 14, 8, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [10, 15, 13, 10, 14, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 13, 13, 8, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 13, 13, 8, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 15, 13, 11, 14, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 13, 13, 9, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 13, 13, 9, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 15, 13, 12, 14, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 12, 13, 10, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 12, 13, 10, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 15, 12, 10, 14, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 13, 12, 8, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 13, 12, 8, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [10, 15, 12, 11, 14, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 13, 12, 9, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 13, 12, 9, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 15, 12, 12, 14, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 13, 12, 10, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 13, 12, 10, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 15, 12, 11, 14, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 14, 12, 8, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 14, 12, 8, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 15, 12, 13, 14, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 12, 12, 11, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 12, 12, 11, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 15, 11, 10, 14, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 13, 11, 8, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 13, 11, 8, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 15, 11, 11, 14, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 13, 11, 9, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 13, 11, 9, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [10, 15, 11, 12, 14, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 13, 11, 10, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 13, 11, 10, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [10, 15, 11, 11, 14, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 14, 11, 8, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 14, 11, 8, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 15, 11, 13, 14, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 13, 11, 11, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 13, 11, 11, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 15, 11, 12, 14, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 14, 11, 9, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 14, 11, 9, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 15, 10, 10, 14, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 13, 10, 8, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 13, 10, 8, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 15, 10, 11, 14, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 13, 10, 9, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 13, 10, 9, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 15, 10, 12, 14, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 13, 10, 10, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 13, 10, 10, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 15, 10, 11, 14, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 14, 10, 8, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 14, 10, 8, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [10, 15, 10, 13, 14, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 13, 10, 11, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 13, 10, 11, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [10, 15, 10, 12, 14, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 14, 10, 9, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 14, 10, 9, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 15, 10, 13, 14, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 14, 10, 10, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 14, 10, 10, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 15, 9, 10, 14, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 13, 9, 8, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 13, 9, 8, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 15, 9, 11, 14, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 13, 9, 9, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 13, 9, 9, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 15, 9, 12, 14, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 13, 9, 10, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 13, 9, 10, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 15, 9, 11, 14, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 14, 9, 8, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 14, 9, 8, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 15, 9, 13, 14, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 13, 9, 11, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 13, 9, 11, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 15, 9, 12, 14, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 14, 9, 9, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 14, 9, 9, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [10, 15, 9, 13, 14, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 14, 9, 10, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 14, 9, 10, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 15, 8, 11, 14, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 13, 8, 9, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 13, 8, 9, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 15, 8, 10, 14, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 8, 8, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 8, 8, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 15, 8, 12, 14, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 13, 8, 10, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 13, 8, 10, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 15, 8, 11, 14, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 14, 8, 8, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 14, 8, 8, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 15, 8, 13, 14, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 13, 8, 11, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 13, 8, 11, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 15, 8, 12, 14, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 14, 8, 9, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 14, 8, 9, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 15, 8, 13, 14, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 14, 8, 10, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 14, 8, 10, 14, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 14, 15, 10, 14, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 12, 15, 8, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 12, 15, 8, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 14, 15, 11, 14, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 11, 15, 9, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 11, 15, 9, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 14, 14, 10, 14, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 12, 14, 8, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 12, 14, 8, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [10, 14, 14, 11, 14, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 12, 14, 9, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 12, 14, 9, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 14, 14, 12, 14, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 12, 14, 10, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 12, 14, 10, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 14, 14, 13, 14, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 11, 14, 11, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 11, 14, 11, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 14, 13, 10, 14, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 12, 13, 8, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 12, 13, 8, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 14, 13, 11, 14, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 12, 13, 9, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 12, 13, 9, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 14, 13, 12, 14, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 12, 13, 10, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 12, 13, 10, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [10, 14, 13, 13, 14, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 12, 13, 11, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 12, 13, 11, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 14, 12, 10, 14, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 12, 12, 8, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 12, 12, 8, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 14, 12, 11, 14, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 12, 12, 9, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 12, 12, 9, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 14, 12, 12, 14, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 12, 12, 10, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 12, 12, 10, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 14, 12, 13, 14, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 12, 12, 11, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 12, 12, 11, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 14, 11, 11, 14, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 12, 11, 9, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 12, 11, 9, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 14, 11, 10, 14, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 11, 8, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 11, 8, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 14, 11, 12, 14, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 12, 11, 10, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 12, 11, 10, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 14, 11, 13, 14, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 12, 11, 11, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 12, 11, 11, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 14, 10, 10, 14, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 10, 8, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 10, 8, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 14, 10, 12, 14, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 12, 10, 10, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 12, 10, 10, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 14, 10, 11, 14, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 10, 9, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 10, 9, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 14, 10, 13, 14, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 12, 10, 11, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 12, 10, 11, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 14, 9, 11, 14, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 9, 9, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 9, 9, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 14, 9, 10, 14, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 9, 8, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 9, 8, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 14, 9, 13, 14, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 12, 9, 11, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 12, 9, 11, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 14, 9, 12, 14, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 9, 10, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 9, 10, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 14, 8, 10, 14, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 8, 8, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 8, 8, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 14, 8, 12, 14, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 8, 10, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 8, 10, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 14, 8, 11, 14, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 8, 9, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 8, 9, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 14, 8, 13, 14, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 8, 11, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 8, 11, 14, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 13, 15, 10, 14, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 11, 15, 8, 14, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 11, 15, 8, 14, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 13, 15, 11, 14, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 11, 15, 9, 14, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 11, 15, 9, 14, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 13, 14, 10, 14, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 11, 14, 8, 14, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 11, 14, 8, 14, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 13, 14, 11, 14, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 11, 14, 9, 14, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 11, 14, 9, 14, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 13, 13, 10, 14, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 13, 8, 14, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 13, 8, 14, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 13, 13, 11, 14, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 10, 13, 9, 14, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 10, 13, 9, 14, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 13, 12, 11, 14, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 12, 9, 14, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 12, 9, 14, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 13, 12, 10, 14, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 8, 14, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 8, 14, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 13, 11, 10, 14, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 11, 8, 14, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 11, 8, 14, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 13, 11, 11, 14, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 10, 11, 9, 14, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 10, 11, 9, 14, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 13, 10, 11, 14, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 10, 9, 14, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 10, 9, 14, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 12, 15, 10, 14, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 10, 15, 8, 14, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 10, 15, 8, 14, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 12, 15, 11, 14, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 10, 15, 9, 14, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 10, 15, 9, 14, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 12, 14, 10, 14, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 10, 14, 8, 14, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 10, 14, 8, 14, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 12, 14, 11, 14, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 9, 14, 9, 14, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 9, 14, 9, 14, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 12, 13, 10, 14, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 8, 14, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 8, 14, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 12, 13, 11, 14, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 9, 14, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 9, 13, 9, 14, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 12, 12, 11, 14, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 9, 14, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 10, 12, 9, 14, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 15, 15, 10, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 12, 15, 8, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 12, 15, 8, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [10, 15, 14, 10, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 13, 14, 8, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 13, 14, 8, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 15, 14, 11, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 13, 14, 9, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 13, 14, 9, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 15, 14, 12, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 12, 14, 10, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 12, 14, 10, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 15, 13, 10, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 13, 13, 8, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 13, 13, 8, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 15, 13, 11, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 13, 13, 9, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 13, 13, 9, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [10, 15, 13, 12, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 13, 13, 10, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 13, 13, 10, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [10, 15, 13, 11, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 14, 13, 8, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 14, 13, 8, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 15, 13, 13, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 13, 13, 11, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 13, 13, 11, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 15, 13, 12, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 14, 13, 9, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 14, 13, 9, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 15, 12, 10, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 13, 12, 8, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 13, 12, 8, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 15, 12, 11, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 13, 12, 9, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 13, 12, 9, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 15, 12, 12, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 13, 12, 10, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 13, 12, 10, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 15, 12, 11, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 14, 12, 8, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 14, 12, 8, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [10, 15, 12, 13, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 13, 12, 11, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 13, 12, 11, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [10, 15, 12, 12, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 14, 12, 9, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 14, 12, 9, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 15, 12, 13, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 14, 12, 10, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 14, 12, 10, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 15, 11, 10, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 13, 11, 8, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 13, 11, 8, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 15, 11, 11, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 13, 11, 9, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 13, 11, 9, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 15, 11, 12, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 13, 11, 10, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 13, 11, 10, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 15, 11, 11, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 14, 11, 8, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 14, 11, 8, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 15, 11, 13, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 13, 11, 11, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 13, 11, 11, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 15, 11, 12, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 14, 11, 9, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 14, 11, 9, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [10, 15, 11, 13, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 14, 11, 10, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 14, 11, 10, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 15, 10, 11, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 13, 10, 9, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 13, 10, 9, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 15, 10, 10, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 10, 8, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 10, 8, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 15, 10, 12, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 13, 10, 10, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 13, 10, 10, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 15, 10, 11, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 14, 10, 8, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 14, 10, 8, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 15, 10, 13, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 13, 10, 11, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 13, 10, 11, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 15, 10, 12, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 14, 10, 9, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 14, 10, 9, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 15, 10, 13, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 14, 10, 10, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 14, 10, 10, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 15, 9, 10, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 13, 9, 8, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 13, 9, 8, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 15, 9, 12, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 13, 9, 10, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 13, 9, 10, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 15, 9, 11, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 14, 9, 8, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 14, 9, 8, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 15, 9, 13, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 13, 9, 11, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 13, 9, 11, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 15, 9, 12, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 14, 9, 9, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 14, 9, 9, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 15, 9, 13, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 14, 9, 10, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 14, 9, 10, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 15, 8, 11, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 13, 8, 9, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 13, 8, 9, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 15, 8, 10, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 8, 8, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 8, 8, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 15, 8, 13, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 13, 8, 11, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 13, 8, 11, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 15, 8, 12, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 14, 8, 9, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 14, 8, 9, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 15, 8, 13, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 14, 8, 10, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 14, 8, 10, 13, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 14, 15, 10, 13, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 12, 15, 8, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 12, 15, 8, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [10, 14, 15, 11, 13, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 12, 15, 9, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 12, 15, 9, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 14, 15, 12, 13, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 12, 15, 10, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 12, 15, 10, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 14, 15, 13, 13, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 11, 15, 11, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 11, 15, 11, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 14, 14, 10, 13, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 12, 14, 8, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 12, 14, 8, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 14, 14, 11, 13, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 12, 14, 9, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 12, 14, 9, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 14, 14, 12, 13, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 12, 14, 10, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 12, 14, 10, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [10, 14, 14, 13, 13, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 12, 14, 11, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 12, 14, 11, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 14, 13, 11, 13, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 12, 13, 9, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 12, 13, 9, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 14, 13, 10, 13, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 13, 8, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 13, 8, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 14, 13, 12, 13, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 12, 13, 10, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 12, 13, 10, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 14, 13, 13, 13, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 12, 13, 11, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 12, 13, 11, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 14, 12, 10, 13, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 12, 8, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 12, 8, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 14, 12, 12, 13, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 12, 12, 10, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 12, 12, 10, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 14, 12, 11, 13, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 12, 9, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 12, 9, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 14, 12, 13, 13, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 12, 12, 11, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 12, 12, 11, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 14, 11, 11, 13, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 11, 9, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 11, 9, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 14, 11, 10, 13, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 11, 8, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 11, 8, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 14, 11, 13, 13, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 12, 11, 11, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 12, 11, 11, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 14, 11, 12, 13, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 11, 10, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 11, 10, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 14, 10, 10, 13, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 10, 8, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 10, 8, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 14, 10, 12, 13, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 10, 10, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 10, 10, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 14, 10, 11, 13, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 10, 9, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 10, 9, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 14, 10, 13, 13, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 10, 11, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 10, 11, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 14, 9, 11, 13, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 9, 9, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 9, 9, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 14, 9, 13, 13, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 9, 11, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 9, 11, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 14, 9, 12, 13, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 9, 10, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 9, 10, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 14, 8, 12, 13, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 8, 10, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 8, 10, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 14, 8, 13, 13, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 8, 11, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 8, 11, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 13, 15, 10, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 11, 15, 8, 13, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 11, 15, 8, 13, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 13, 15, 11, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 11, 15, 9, 13, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 11, 15, 9, 13, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 13, 14, 10, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 14, 8, 13, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 14, 8, 13, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 13, 14, 11, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 10, 14, 9, 13, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 10, 14, 9, 13, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 13, 13, 10, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 13, 8, 13, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 13, 8, 13, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 13, 13, 11, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 9, 13, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 9, 13, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 13, 12, 11, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 12, 9, 13, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 12, 9, 13, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 12, 15, 10, 13, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 10, 15, 8, 13, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 10, 15, 8, 13, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 12, 15, 11, 13, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 9, 15, 9, 13, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 9, 15, 9, 13, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 12, 14, 10, 13, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 10, 14, 8, 13, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 10, 14, 8, 13, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 12, 14, 11, 13, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 9, 14, 9, 13, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 9, 14, 9, 13, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 15, 15, 10, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 13, 15, 8, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 13, 15, 8, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 15, 15, 11, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 12, 15, 9, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 12, 15, 9, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 15, 14, 10, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 13, 14, 8, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 13, 14, 8, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [10, 15, 14, 11, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 13, 14, 9, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 13, 14, 9, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 15, 14, 12, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 13, 14, 10, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 13, 14, 10, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 15, 14, 11, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 14, 14, 8, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 14, 14, 8, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 15, 14, 13, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 12, 14, 11, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 12, 14, 11, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 15, 13, 10, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 13, 13, 8, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 13, 13, 8, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 15, 13, 11, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 13, 13, 9, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 13, 13, 9, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 15, 13, 12, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 13, 13, 10, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 13, 13, 10, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 15, 13, 11, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 14, 13, 8, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 14, 13, 8, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [10, 15, 13, 13, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 13, 13, 11, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 13, 13, 11, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [10, 15, 13, 12, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 14, 13, 9, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 14, 13, 9, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 15, 13, 13, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 14, 13, 10, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 14, 13, 10, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 15, 12, 10, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 13, 12, 8, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 13, 12, 8, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 15, 12, 11, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 13, 12, 9, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 13, 12, 9, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 15, 12, 12, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 13, 12, 10, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 13, 12, 10, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 15, 12, 11, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 14, 12, 8, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 14, 12, 8, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 15, 12, 13, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 13, 12, 11, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 13, 12, 11, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 15, 12, 12, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 14, 12, 9, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 14, 12, 9, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [10, 15, 12, 13, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 14, 12, 10, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 14, 12, 10, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 15, 11, 11, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 13, 11, 9, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 13, 11, 9, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 15, 11, 10, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 11, 8, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 11, 8, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 15, 11, 12, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 13, 11, 10, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 13, 11, 10, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 15, 11, 11, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 14, 11, 8, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 14, 11, 8, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 15, 11, 13, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 13, 11, 11, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 13, 11, 11, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 15, 11, 12, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 14, 11, 9, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 14, 11, 9, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 15, 11, 13, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 14, 11, 10, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 14, 11, 10, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 15, 10, 10, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 13, 10, 8, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 13, 10, 8, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 15, 10, 12, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 13, 10, 10, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 13, 10, 10, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 15, 10, 11, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 14, 10, 8, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 14, 10, 8, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 15, 10, 13, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 13, 10, 11, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 13, 10, 11, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 15, 10, 12, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 14, 10, 9, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 14, 10, 9, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 15, 10, 13, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 14, 10, 10, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 14, 10, 10, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 15, 9, 11, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 13, 9, 9, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 13, 9, 9, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 15, 9, 10, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 9, 8, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 9, 8, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 15, 9, 13, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 13, 9, 11, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 13, 9, 11, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 15, 9, 12, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 14, 9, 9, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 14, 9, 9, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 15, 9, 13, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 14, 9, 10, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 14, 9, 10, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 15, 8, 10, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 13, 8, 8, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 13, 8, 8, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 15, 8, 12, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 13, 8, 10, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 13, 8, 10, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 15, 8, 11, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 14, 8, 8, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 14, 8, 8, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 15, 8, 13, 12, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 14, 8, 10, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 14, 8, 10, 12, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 14, 15, 10, 12, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 12, 15, 8, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 12, 15, 8, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 14, 15, 11, 12, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 12, 15, 9, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 12, 15, 9, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [10, 14, 15, 12, 12, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 12, 15, 10, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 12, 15, 10, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 14, 15, 13, 12, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 12, 15, 11, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 12, 15, 11, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 14, 14, 10, 12, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 12, 14, 8, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 12, 14, 8, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 14, 14, 11, 12, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 12, 14, 9, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 12, 14, 9, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 14, 14, 12, 12, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 12, 14, 10, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 12, 14, 10, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 14, 14, 13, 12, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 12, 14, 11, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 12, 14, 11, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 14, 13, 10, 12, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 13, 8, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 13, 8, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 14, 13, 12, 12, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 12, 13, 10, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 12, 13, 10, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 14, 13, 11, 12, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 13, 9, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 13, 9, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 14, 13, 13, 12, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 12, 13, 11, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 12, 13, 11, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 14, 12, 11, 12, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 12, 9, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 12, 9, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 14, 12, 10, 12, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 12, 8, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 12, 8, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 14, 12, 13, 12, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 12, 12, 11, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 12, 12, 11, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 14, 12, 12, 12, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 12, 10, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 12, 10, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 14, 11, 10, 12, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 11, 8, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 11, 8, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 14, 11, 12, 12, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 11, 10, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 11, 10, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 14, 11, 11, 12, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 11, 9, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 11, 9, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 14, 11, 13, 12, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 11, 11, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 11, 11, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 14, 10, 11, 12, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 10, 9, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 10, 9, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 14, 10, 13, 12, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 10, 11, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 10, 11, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 14, 10, 12, 12, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 10, 10, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 10, 10, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 14, 9, 12, 12, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 9, 10, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 9, 10, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 14, 9, 13, 12, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 9, 11, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 9, 11, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 14, 8, 13, 12, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 8, 11, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 8, 11, 12, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 13, 15, 11, 12, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 11, 15, 9, 12, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 11, 15, 9, 12, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 13, 15, 10, 12, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 10, 15, 8, 12, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 10, 15, 8, 12, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 13, 14, 11, 12, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 14, 9, 12, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 14, 9, 12, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 13, 14, 10, 12, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 10, 14, 8, 12, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 10, 14, 8, 12, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 13, 13, 11, 12, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 13, 9, 12, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 13, 9, 12, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 12, 15, 11, 12, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 10, 15, 9, 12, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 10, 15, 9, 12, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 12, 15, 10, 12, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 9, 15, 8, 12, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 9, 15, 8, 12, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 12, 14, 11, 12, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 10, 14, 9, 12, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 10, 14, 9, 12, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [10, 15, 15, 10, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 13, 15, 8, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 13, 15, 8, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 15, 15, 11, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 13, 15, 9, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 13, 15, 9, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 15, 15, 12, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 12, 15, 10, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 12, 15, 10, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 15, 14, 10, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 13, 14, 8, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 13, 14, 8, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 15, 14, 11, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 13, 14, 9, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 13, 14, 9, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [10, 15, 14, 12, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 13, 14, 10, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 13, 14, 10, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [10, 15, 14, 11, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 14, 14, 8, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 14, 14, 8, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 15, 14, 13, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 13, 14, 11, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 13, 14, 11, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 15, 14, 12, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 14, 14, 9, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 14, 14, 9, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 15, 13, 10, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 13, 13, 8, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 13, 13, 8, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 15, 13, 11, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 13, 13, 9, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 13, 13, 9, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 15, 13, 12, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 13, 13, 10, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 13, 13, 10, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 15, 13, 11, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 14, 13, 8, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 14, 13, 8, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 15, 13, 13, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 13, 13, 11, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 13, 13, 11, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 15, 13, 12, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 14, 13, 9, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 14, 13, 9, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [10, 15, 13, 13, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 14, 13, 10, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 14, 13, 10, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 15, 12, 11, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 13, 12, 9, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 13, 12, 9, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 15, 12, 10, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 12, 8, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 12, 8, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 15, 12, 12, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 13, 12, 10, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 13, 12, 10, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 15, 12, 11, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 14, 12, 8, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 14, 12, 8, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 15, 12, 13, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 13, 12, 11, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 13, 12, 11, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 15, 12, 12, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 14, 12, 9, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 14, 12, 9, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 15, 12, 13, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 14, 12, 10, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 14, 12, 10, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 15, 11, 10, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 13, 11, 8, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 13, 11, 8, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 15, 11, 12, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 13, 11, 10, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 13, 11, 10, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 15, 11, 11, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 14, 11, 8, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 14, 11, 8, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 15, 11, 13, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 13, 11, 11, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 13, 11, 11, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 15, 11, 12, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 14, 11, 9, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 14, 11, 9, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 15, 11, 13, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 14, 11, 10, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 14, 11, 10, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 15, 10, 11, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 13, 10, 9, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 13, 10, 9, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 15, 10, 10, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 10, 8, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 10, 8, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 15, 10, 13, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 13, 10, 11, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 13, 10, 11, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 15, 10, 12, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 14, 10, 9, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 14, 10, 9, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 15, 10, 13, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 14, 10, 10, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 14, 10, 10, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 15, 9, 10, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 13, 9, 8, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 13, 9, 8, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 15, 9, 12, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 13, 9, 10, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 13, 9, 10, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 15, 9, 11, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 14, 9, 8, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 14, 9, 8, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 15, 9, 13, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 14, 9, 10, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 14, 9, 10, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 15, 8, 11, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 13, 8, 9, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 13, 8, 9, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 15, 8, 13, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 13, 8, 11, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 13, 8, 11, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 15, 8, 12, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 14, 8, 9, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 14, 8, 9, 11, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 14, 15, 10, 11, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 12, 15, 8, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 12, 15, 8, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 14, 15, 11, 11, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 12, 15, 9, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 12, 15, 9, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 14, 15, 12, 11, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 12, 15, 10, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 12, 15, 10, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [10, 14, 15, 13, 11, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 12, 15, 11, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 12, 15, 11, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 14, 14, 11, 11, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 12, 14, 9, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 12, 14, 9, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 14, 14, 10, 11, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 14, 8, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 14, 8, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 14, 14, 12, 11, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 12, 14, 10, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 12, 14, 10, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 14, 14, 13, 11, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 12, 14, 11, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 12, 14, 11, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 14, 13, 11, 11, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 13, 9, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 13, 9, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 14, 13, 10, 11, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 13, 8, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 13, 8, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 14, 13, 13, 11, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 12, 13, 11, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 12, 13, 11, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 14, 13, 12, 11, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 13, 10, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 13, 10, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 14, 12, 10, 11, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 12, 8, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 12, 8, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 14, 12, 12, 11, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 12, 10, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 12, 10, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 14, 12, 11, 11, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 12, 9, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 12, 9, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 14, 12, 13, 11, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 12, 11, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 12, 11, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 14, 11, 11, 11, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 11, 9, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 11, 9, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 14, 11, 13, 11, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 11, 11, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 11, 11, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 14, 11, 12, 11, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 11, 10, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 11, 10, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 14, 10, 12, 11, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 10, 10, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 10, 10, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 14, 10, 13, 11, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 10, 11, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 10, 11, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 14, 9, 13, 11, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 9, 11, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 9, 11, 11, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 13, 15, 10, 11, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 15, 8, 11, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 15, 8, 11, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 13, 15, 11, 11, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 10, 15, 9, 11, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 10, 15, 9, 11, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 13, 14, 10, 11, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 14, 8, 11, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 14, 8, 11, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 13, 14, 11, 11, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 10, 14, 9, 11, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 10, 14, 9, 11, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 12, 15, 10, 11, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 10, 15, 8, 11, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 10, 15, 8, 11, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 12, 15, 11, 11, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 9, 15, 9, 11, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 9, 15, 9, 11, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 15, 15, 10, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 13, 15, 8, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 13, 15, 8, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [10, 15, 15, 11, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 13, 15, 9, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 13, 15, 9, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 15, 15, 12, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 13, 15, 10, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 13, 15, 10, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 15, 15, 11, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 14, 15, 8, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 14, 15, 8, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [8, 15, 15, 13, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 12, 15, 11, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 12, 15, 11, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 15, 14, 10, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 13, 14, 8, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 13, 14, 8, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 15, 14, 11, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 13, 14, 9, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 13, 14, 9, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 15, 14, 12, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 13, 14, 10, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 13, 14, 10, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 15, 14, 11, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 14, 14, 8, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 14, 14, 8, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [10, 15, 14, 13, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 13, 14, 11, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 13, 14, 11, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [10, 15, 14, 12, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 14, 14, 9, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 14, 14, 9, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 15, 14, 13, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 14, 14, 10, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 14, 14, 10, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 15, 13, 11, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 13, 13, 9, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 13, 13, 9, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 15, 13, 10, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 13, 8, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 13, 8, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 15, 13, 12, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 13, 13, 10, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 13, 13, 10, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 15, 13, 11, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 14, 13, 8, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 14, 13, 8, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 15, 13, 13, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 13, 13, 11, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 13, 13, 11, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 15, 13, 12, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 14, 13, 9, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 14, 13, 9, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 15, 13, 13, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 14, 13, 10, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 14, 13, 10, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 15, 12, 10, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 13, 12, 8, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 13, 12, 8, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 15, 12, 12, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 13, 12, 10, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 13, 12, 10, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 15, 12, 11, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 14, 12, 8, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 14, 12, 8, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 15, 12, 13, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 13, 12, 11, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 13, 12, 11, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 15, 12, 12, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 14, 12, 9, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 14, 12, 9, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 15, 12, 13, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 14, 12, 10, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 14, 12, 10, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 15, 11, 11, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 13, 11, 9, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 13, 11, 9, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 15, 11, 10, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 11, 8, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 11, 8, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 15, 11, 13, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 13, 11, 11, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 13, 11, 11, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 15, 11, 12, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 14, 11, 9, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 14, 11, 9, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 15, 11, 13, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 14, 11, 10, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 14, 11, 10, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 15, 10, 10, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 13, 10, 8, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 13, 10, 8, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 15, 10, 12, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 13, 10, 10, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 13, 10, 10, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 15, 10, 11, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 14, 10, 8, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 14, 10, 8, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 15, 10, 13, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 14, 10, 10, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 14, 10, 10, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 15, 9, 11, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 13, 9, 9, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 13, 9, 9, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 15, 9, 13, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 13, 9, 11, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 13, 9, 11, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 15, 9, 12, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 14, 9, 9, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 14, 9, 9, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 15, 8, 12, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 13, 8, 10, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 13, 8, 10, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 15, 8, 11, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 14, 8, 8, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 14, 8, 8, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 15, 8, 13, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 14, 8, 10, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 14, 8, 10, 10, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 14, 15, 10, 10, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 12, 15, 8, 10, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 12, 15, 8, 10, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 14, 15, 11, 10, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 12, 15, 9, 10, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 12, 15, 9, 10, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 14, 15, 12, 10, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 12, 15, 10, 10, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 12, 15, 10, 10, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 14, 15, 13, 10, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 12, 15, 11, 10, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 12, 15, 11, 10, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 14, 14, 10, 10, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 14, 8, 10, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 14, 8, 10, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 14, 14, 12, 10, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 12, 14, 10, 10, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 12, 14, 10, 10, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 14, 14, 11, 10, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 14, 9, 10, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 14, 9, 10, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 14, 14, 13, 10, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 12, 14, 11, 10, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 12, 14, 11, 10, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 14, 13, 10, 10, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 13, 8, 10, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 13, 8, 10, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 14, 13, 12, 10, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 13, 10, 10, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 13, 10, 10, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 14, 13, 11, 10, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 13, 9, 10, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 13, 9, 10, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 14, 13, 13, 10, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 13, 11, 10, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 13, 11, 10, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 14, 12, 11, 10, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 12, 9, 10, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 12, 9, 10, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 14, 12, 13, 10, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 12, 11, 10, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 12, 11, 10, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 14, 12, 12, 10, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 12, 10, 10, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 12, 10, 10, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 14, 11, 12, 10, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 11, 10, 10, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 11, 10, 10, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 14, 11, 13, 10, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 11, 11, 10, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 11, 11, 10, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 14, 10, 13, 10, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 10, 11, 10, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 10, 11, 10, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 13, 15, 11, 10, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 15, 9, 10, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 15, 9, 10, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 13, 15, 10, 10, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 10, 15, 8, 10, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 10, 15, 8, 10, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 13, 14, 11, 10, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 14, 9, 10, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 14, 9, 10, 13],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 12, 15, 11, 10, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 10, 15, 9, 10, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 10, 15, 9, 10, 12],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 15, 15, 10, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 13, 15, 8, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 13, 15, 8, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 15, 15, 11, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 13, 15, 9, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 13, 15, 9, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [10, 15, 15, 12, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 13, 15, 10, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 13, 15, 10, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [10, 15, 15, 11, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 14, 15, 8, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 14, 15, 8, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 15, 15, 13, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 13, 15, 11, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 13, 15, 11, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [9, 15, 15, 12, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 14, 15, 9, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 14, 15, 9, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 15, 14, 10, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 13, 14, 8, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 13, 14, 8, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 15, 14, 11, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 13, 14, 9, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 13, 14, 9, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 15, 14, 12, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 13, 14, 10, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 13, 14, 10, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 15, 14, 11, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 14, 14, 8, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 14, 14, 8, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 15, 14, 13, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 13, 14, 11, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 13, 14, 11, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [11, 15, 14, 12, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 14, 14, 9, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [10, 14, 14, 9, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [10, 15, 14, 13, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 14, 14, 10, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [9, 14, 14, 10, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 15, 13, 10, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 13, 13, 8, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 13, 13, 8, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 15, 13, 12, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 13, 13, 10, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 13, 13, 10, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 15, 13, 11, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 14, 13, 8, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 14, 13, 8, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 15, 13, 13, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 13, 13, 11, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 13, 13, 11, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 15, 13, 12, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 14, 13, 9, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 14, 13, 9, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 15, 13, 13, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 14, 13, 10, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 14, 13, 10, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 15, 12, 11, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 13, 12, 9, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 13, 12, 9, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 15, 12, 10, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 12, 8, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 12, 8, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 15, 12, 13, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 13, 12, 11, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 13, 12, 11, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 15, 12, 12, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 14, 12, 9, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 14, 12, 9, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 15, 12, 13, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 14, 12, 10, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 14, 12, 10, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 15, 11, 10, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 13, 11, 8, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 13, 11, 8, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 15, 11, 12, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 13, 11, 10, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 13, 11, 10, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 15, 11, 11, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 14, 11, 8, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 14, 11, 8, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 15, 11, 13, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 14, 11, 10, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 14, 11, 10, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 15, 10, 11, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 13, 10, 9, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 13, 10, 9, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 15, 10, 13, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 13, 10, 11, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 13, 10, 11, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 15, 10, 12, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 14, 10, 9, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 14, 10, 9, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 15, 9, 12, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 13, 9, 10, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 13, 9, 10, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 15, 9, 11, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 14, 9, 8, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 14, 9, 8, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 15, 9, 13, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 14, 9, 10, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 14, 9, 10, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 15, 8, 13, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 13, 8, 11, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 13, 8, 11, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 15, 8, 12, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 14, 8, 9, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 14, 8, 9, 9, 15],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 14, 15, 11, 9, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 12, 15, 9, 9, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 12, 15, 9, 9, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 14, 15, 10, 9, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 15, 8, 9, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 15, 8, 9, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 14, 15, 12, 9, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 12, 15, 10, 9, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [12, 12, 15, 10, 9, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 14, 15, 13, 9, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 12, 15, 11, 9, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [11, 12, 15, 11, 9, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 14, 14, 11, 9, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 14, 9, 9, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 14, 9, 9, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 14, 14, 10, 9, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 14, 8, 9, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 14, 8, 9, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 14, 14, 13, 9, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 12, 14, 11, 9, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [13, 12, 14, 11, 9, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 14, 14, 12, 9, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 14, 10, 9, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 11, 14, 10, 9, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 14, 13, 11, 9, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 13, 9, 9, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 13, 9, 9, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 14, 13, 13, 9, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 13, 11, 9, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [14, 12, 13, 11, 9, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 14, 13, 12, 9, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 13, 10, 9, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 13, 10, 9, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 14, 12, 12, 9, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 12, 10, 9, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 12, 10, 9, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 14, 12, 13, 9, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 12, 11, 9, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 11, 12, 11, 9, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 2, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 14, 11, 13, 9, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Gnome_Deep,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 11, 11, 9, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Gnome_Forest,
                        myFeats: vec![
                            FadeAway,
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [15, 12, 11, 11, 9, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
        ];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

