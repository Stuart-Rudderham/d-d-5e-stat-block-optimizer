use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test32" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Weighted,
        statOrdering: [
            Intelligence,
            Constitution,
            Dexterity,
            Wisdom,
            Strength,
            Charisma,
        ],
        isReversed: true,
        availableRaces: vec![
            Aasimar_Protector,
            Aasimar_Scourge,
            Bugbear,
            CustomLineage,
            Changling,
            Dwarf_Grey,
            Dwarf_Hill,
            Dwarf_MarkOfWarding,
            Dwarf_Mountain,
            Elf_Drow,
            Elf_High,
            Elf_Wood,
            Firbolg,
            Genasi_Water,
            Gnome_MarkOfScribing,
            Gnome_Rock,
            Goblin,
            HalfElf,
            HalfElf_MarkOfDetection,
            HalfElf_MarkOfStorm,
            Halfling_Ghostwise,
            Halfling_Lightfoot,
            Halfling_MarkOfHospitality,
            Halfling_Stout,
            HalfOrc,
            Hobgoblin,
            Human,
            Human_MarkOfFinding,
            Human_MarkOfHandling,
            Human_MarkOfMaking,
            Human_MarkOfPassage,
            Human_MarkOfSentinel,
            Human_Variant,
            Kenku,
            Lizardfolk,
            Orc_Eberron,
            Shifter_BeastHide,
            Shifter_LongTooth,
            Shifter_SwiftStride,
            Shifter_WildHunt,
            Tiefling,
            Tortle,
            Triton,
            Warforged,
        ],
        requiredFeats: vec![
            DualWielder,
            Slasher,
        ],
        constraints: vec![],
        numberOfASIs: 0,
        doAllRacesHaveFreeFeat: true,
        canRearrangeRaceModifiers: true,
        maxNumberOfResults: 23,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 442152,
            numUniqueCombinationsPassedConstraints: 442152,
            numStatBlocksEjected: 265,
            numUniqueCombinationsEjected: 268,
            numUniqueCombinationsBelowLowerBound: 441388,
            numStatBlocksShiftedDuringInsertion: 3062,
            numDuplicateRaceStatBlocksRemoved: 478,
            numUniqueStatBlockAfterRacePass: 147,
            numUniqueStatBlockAfterFeatPass: 36,
            numFinalStatBlockCombinations: 46,
        };

    let expectedStatBlocks = 
        vec![
            RankedStatBlock {
                rank: 600,
                statBlock: [13, 13, 13, 13, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [10, 13, 13, 13, 13, 13],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [11, 12, 13, 13, 13, 13],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1320,
                statBlock: [13, 13, 11, 11, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [10, 13, 11, 11, 13, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [11, 12, 11, 11, 13, 15],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1320,
                statBlock: [15, 13, 11, 11, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [15, 10, 11, 11, 13, 13],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [15, 11, 10, 11, 13, 13],
                        myRaceModifiers:     [0, 1, 1, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1320,
                statBlock: [13, 13, 11, 11, 15, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [10, 13, 11, 11, 15, 13],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [11, 12, 11, 11, 15, 13],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1320,
                statBlock: [13, 15, 11, 11, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [10, 15, 11, 11, 13, 13],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [11, 15, 10, 11, 13, 13],
                        myRaceModifiers:     [1, 0, 1, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1320,
                statBlock: [13, 11, 13, 11, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [10, 11, 13, 11, 13, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [11, 10, 13, 11, 13, 15],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1320,
                statBlock: [15, 11, 13, 11, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 11, 13, 13],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [15, 9, 12, 11, 13, 13],
                        myRaceModifiers:     [0, 1, 1, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1320,
                statBlock: [13, 11, 13, 11, 15, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [10, 11, 13, 11, 15, 13],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [11, 10, 13, 11, 15, 13],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1320,
                statBlock: [13, 13, 13, 11, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [10, 13, 13, 11, 11, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [11, 12, 13, 11, 11, 15],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1320,
                statBlock: [15, 13, 13, 11, 11, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 11, 11, 13],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [15, 11, 12, 11, 11, 13],
                        myRaceModifiers:     [0, 1, 1, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1320,
                statBlock: [11, 13, 13, 11, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [8, 13, 13, 11, 13, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [9, 12, 13, 11, 13, 15],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1320,
                statBlock: [15, 13, 13, 11, 13, 11],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [15, 10, 13, 11, 13, 11],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [15, 11, 12, 11, 13, 11],
                        myRaceModifiers:     [0, 1, 1, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1320,
                statBlock: [11, 13, 13, 11, 15, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [8, 13, 13, 11, 15, 13],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [9, 12, 13, 11, 15, 13],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1320,
                statBlock: [13, 13, 13, 11, 15, 11],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [10, 13, 13, 11, 15, 11],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [11, 12, 13, 11, 15, 11],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1320,
                statBlock: [13, 15, 13, 11, 11, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [10, 15, 13, 11, 11, 13],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [11, 15, 12, 11, 11, 13],
                        myRaceModifiers:     [1, 0, 1, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1320,
                statBlock: [11, 15, 13, 11, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [8, 15, 13, 11, 13, 13],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [9, 15, 12, 11, 13, 13],
                        myRaceModifiers:     [1, 0, 1, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1320,
                statBlock: [13, 15, 13, 11, 13, 11],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [10, 15, 13, 11, 13, 11],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [11, 15, 12, 11, 13, 11],
                        myRaceModifiers:     [1, 0, 1, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1320,
                statBlock: [13, 11, 15, 11, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [10, 11, 15, 11, 13, 13],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [11, 10, 15, 11, 13, 13],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1320,
                statBlock: [13, 13, 15, 11, 11, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [10, 13, 15, 11, 11, 13],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [11, 12, 15, 11, 11, 13],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1320,
                statBlock: [11, 13, 15, 11, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [8, 13, 15, 11, 13, 13],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [9, 12, 15, 11, 13, 13],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1320,
                statBlock: [13, 13, 15, 11, 13, 11],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [10, 13, 15, 11, 13, 11],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [11, 12, 15, 11, 13, 11],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1320,
                statBlock: [13, 11, 11, 13, 13, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [10, 11, 11, 13, 13, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [11, 10, 11, 13, 13, 15],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1320,
                statBlock: [15, 11, 11, 13, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [15, 8, 11, 13, 13, 13],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Human_Variant,
                        myFeats: vec![
                            DualWielder,
                            Slasher,
                        ],
                        myBasePointBuyArray: [15, 9, 10, 13, 13, 13],
                        myRaceModifiers:     [0, 1, 1, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
        ];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

