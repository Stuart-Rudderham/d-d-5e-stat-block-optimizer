#![allow(non_upper_case_globals)]

use crate::dnd::statblock::*;

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq, Clone, Copy, Hash)]
pub enum CharacterRace
{
    Aarakocra,
    Aasimar_Fallen,
    Aasimar_Protector,
    Aasimar_Scourge,
    Bugbear,
    CustomLineage,
    Changling,
    DragonBorn,
    Dwarf_Grey,
    Dwarf_Hill,
    Dwarf_MarkOfWarding,
    Dwarf_Mountain,
    Elf_Drow,
    Elf_High,
    Elf_MarkOfShadow,
    Elf_Wood,
    Firbolg,
    Genasi_Air,
    Genasi_Earth,
    Genasi_Fire,
    Genasi_Water,
    Gnome_Deep,
    Gnome_Forest,
    Gnome_MarkOfScribing,
    Gnome_Rock,
    Goblin,
    Goliath,
    HalfElf,
    HalfElf_MarkOfDetection,
    HalfElf_MarkOfStorm,
    Halfling_Ghostwise,
    Halfling_Lightfoot,
    Halfling_MarkOfHealing,
    Halfling_MarkOfHospitality,
    Halfling_Stout,
    HalfOrc,
    HalfOrc_MarkOfFinding,
    Hobgoblin,
    Human,
    Human_MarkOfFinding,
    Human_MarkOfHandling,
    Human_MarkOfMaking,
    Human_MarkOfPassage,
    Human_MarkOfSentinel,
    Human_Variant,
    Kalashtar,
    Kenku,
    Kobold,
    Lizardfolk,
    Orc,
    Orc_Eberron,
    Shifter_BeastHide,
    Shifter_LongTooth,
    Shifter_SwiftStride,
    Shifter_WildHunt,
    Tabaxi,
    Tiefling,
    Tiefling_Feral,
    Tortle,
    Triton,
    Warforged,
    YuanTiPureblood,
}

pub fn GenerateRaceModifierArrays(aRace : CharacterRace, canRearrangeRaceModifiers : bool) -> Vec<StatBlock>
{
    let raceModifiers = match aRace
    {
        //                                                 | Str | Dex | Con | Int | Wis | Cha |
        CharacterRace::Aarakocra                  => vec![ [  0  ,  2  ,  0  ,  0  ,  1  ,  0  ] ],
        CharacterRace::Aasimar_Fallen             => vec![ [  1  ,  0  ,  0  ,  0  ,  0  ,  2  ] ],
        CharacterRace::Aasimar_Protector          => vec![ [  0  ,  0  ,  0  ,  0  ,  1  ,  2  ] ],
        CharacterRace::Aasimar_Scourge            => vec![ [  0  ,  0  ,  1  ,  0  ,  0  ,  2  ] ],
        CharacterRace::Bugbear                    => vec![ [  2  ,  1  ,  0  ,  0  ,  0  ,  0  ] ],
        CharacterRace::Changling                  => vec![ [  0  ,  0  ,  0  ,  0  ,  1  ,  2  ],   // Changlings get +1 to a non-Charisma stat.
                                                           [  0  ,  0  ,  0  ,  1  ,  0  ,  2  ],
                                                           [  0  ,  0  ,  1  ,  0  ,  0  ,  2  ],
                                                           [  0  ,  1  ,  0  ,  0  ,  0  ,  2  ],
                                                           [  1  ,  0  ,  0  ,  0  ,  0  ,  2  ] ],
        CharacterRace::CustomLineage              => vec![ [  0  ,  0  ,  0  ,  0  ,  0  ,  2  ],   // A Custom Lineage gives you +2 to a single stat.
                                                           [  0  ,  0  ,  0  ,  0  ,  2  ,  0  ],
                                                           [  0  ,  0  ,  0  ,  2  ,  0  ,  0  ],
                                                           [  0  ,  0  ,  2  ,  0  ,  0  ,  0  ],
                                                           [  0  ,  2  ,  0  ,  0  ,  0  ,  0  ],
                                                           [  2  ,  0  ,  0  ,  0  ,  0  ,  0  ] ],
        CharacterRace::DragonBorn                 => vec![ [  2  ,  0  ,  0  ,  0  ,  0  ,  1  ] ],
        CharacterRace::Dwarf_Grey                 => vec![ [  1  ,  0  ,  2  ,  0  ,  0  ,  0  ] ],
        CharacterRace::Dwarf_Hill                 => vec![ [  0  ,  0  ,  2  ,  0  ,  1  ,  0  ] ],
        CharacterRace::Dwarf_MarkOfWarding        => vec![ [  0  ,  0  ,  2  ,  1  ,  0  ,  0  ] ],
        CharacterRace::Dwarf_Mountain             => vec![ [  2  ,  0  ,  2  ,  0  ,  0  ,  0  ] ],
        CharacterRace::Elf_Drow                   => vec![ [  0  ,  2  ,  0  ,  0  ,  0  ,  1  ] ],
        CharacterRace::Elf_High                   => vec![ [  0  ,  2  ,  0  ,  1  ,  0  ,  0  ] ],
        CharacterRace::Elf_MarkOfShadow           => vec![ [  0  ,  2  ,  0  ,  0  ,  0  ,  1  ] ],
        CharacterRace::Elf_Wood                   => vec![ [  0  ,  2  ,  0  ,  0  ,  1  ,  0  ] ],
        CharacterRace::Firbolg                    => vec![ [  1  ,  0  ,  0  ,  0  ,  2  ,  0  ] ],
        CharacterRace::Genasi_Air                 => vec![ [  0  ,  1  ,  2  ,  0  ,  0  ,  0  ] ],
        CharacterRace::Genasi_Earth               => vec![ [  1  ,  0  ,  2  ,  0  ,  0  ,  0  ] ],
        CharacterRace::Genasi_Fire                => vec![ [  0  ,  0  ,  2  ,  1  ,  0  ,  0  ] ],
        CharacterRace::Genasi_Water               => vec![ [  0  ,  0  ,  2  ,  0  ,  1  ,  0  ] ],
        CharacterRace::Gnome_Deep                 => vec![ [  0  ,  1  ,  0  ,  2  ,  0  ,  0  ] ],
        CharacterRace::Gnome_Forest               => vec![ [  0  ,  1  ,  0  ,  2  ,  0  ,  0  ] ],
        CharacterRace::Gnome_MarkOfScribing       => vec![ [  0  ,  0  ,  0  ,  2  ,  0  ,  1  ] ],
        CharacterRace::Gnome_Rock                 => vec![ [  0  ,  0  ,  1  ,  2  ,  0  ,  0  ] ],
        CharacterRace::Goblin                     => vec![ [  0  ,  2  ,  1  ,  0  ,  0  ,  0  ] ],
        CharacterRace::Goliath                    => vec![ [  2  ,  0  ,  1  ,  0  ,  0  ,  0  ] ],
        CharacterRace::HalfElf                    => vec![ [  0  ,  0  ,  0  ,  1  ,  1  ,  2  ],   // Half-Elves get +1 to any two different non-Charisma stats.
                                                           [  0  ,  0  ,  1  ,  0  ,  1  ,  2  ],
                                                           [  0  ,  0  ,  1  ,  1  ,  0  ,  2  ],
                                                           [  0  ,  1  ,  0  ,  0  ,  1  ,  2  ],
                                                           [  0  ,  1  ,  0  ,  1  ,  0  ,  2  ],
                                                           [  0  ,  1  ,  1  ,  0  ,  0  ,  2  ],
                                                           [  1  ,  0  ,  0  ,  0  ,  1  ,  2  ],
                                                           [  1  ,  0  ,  0  ,  1  ,  0  ,  2  ],
                                                           [  1  ,  0  ,  1  ,  0  ,  0  ,  2  ],
                                                           [  1  ,  1  ,  0  ,  0  ,  0  ,  2  ] ],
        CharacterRace::HalfElf_MarkOfDetection    => vec![ [  0  ,  0  ,  0  ,  0  ,  2  ,  1  ],   // Mark of Detection Half-Elves get +1 to a non-Wisdom stat.
                                                           [  0  ,  0  ,  0  ,  1  ,  2  ,  0  ],
                                                           [  0  ,  0  ,  1  ,  0  ,  2  ,  0  ],
                                                           [  0  ,  1  ,  0  ,  0  ,  2  ,  0  ],
                                                           [  1  ,  0  ,  0  ,  0  ,  2  ,  0  ] ],
        CharacterRace::HalfElf_MarkOfStorm        => vec![ [  0  ,  1  ,  0  ,  0  ,  0  ,  2  ] ],
        CharacterRace::Halfling_Ghostwise         => vec![ [  0  ,  2  ,  0  ,  0  ,  1  ,  0  ] ],
        CharacterRace::Halfling_Lightfoot         => vec![ [  0  ,  2  ,  0  ,  0  ,  0  ,  1  ] ],
        CharacterRace::Halfling_MarkOfHealing     => vec![ [  0  ,  2  ,  0  ,  0  ,  0  ,  1  ] ],
        CharacterRace::Halfling_MarkOfHospitality => vec![ [  0  ,  2  ,  0  ,  0  ,  1  ,  0  ] ],
        CharacterRace::Halfling_Stout             => vec![ [  0  ,  2  ,  1  ,  0  ,  0  ,  0  ] ],
        CharacterRace::HalfOrc                    => vec![ [  2  ,  0  ,  1  ,  0  ,  0  ,  0  ] ],
        CharacterRace::HalfOrc_MarkOfFinding      => vec![ [  0  ,  0  ,  1  ,  0  ,  2  ,  0  ] ],
        CharacterRace::Hobgoblin                  => vec![ [  0  ,  0  ,  2  ,  1  ,  0  ,  0  ] ],
        CharacterRace::Human                      => vec![ [  1  ,  1  ,  1  ,  1  ,  1  ,  1  ] ],
        CharacterRace::Human_MarkOfFinding        => vec![ [  0  ,  0  ,  1  ,  0  ,  2  ,  0  ] ],
        CharacterRace::Human_MarkOfHandling       => vec![ [  0  ,  0  ,  0  ,  0  ,  2  ,  1  ],   // Mark of Handling Humans get +1 to a non-Wisdom stat.
                                                           [  0  ,  0  ,  0  ,  1  ,  2  ,  0  ],
                                                           [  0  ,  0  ,  1  ,  0  ,  2  ,  0  ],
                                                           [  0  ,  1  ,  0  ,  0  ,  2  ,  0  ],
                                                           [  1  ,  0  ,  0  ,  0  ,  2  ,  0  ] ],
        CharacterRace::Human_MarkOfMaking         => vec![ [  0  ,  0  ,  0  ,  2  ,  0  ,  1  ],   // Mark of Making Humans get +1 to a non-Intelligence stat.
                                                           [  0  ,  0  ,  0  ,  2  ,  1  ,  0  ],
                                                           [  0  ,  0  ,  1  ,  2  ,  0  ,  0  ],
                                                           [  0  ,  1  ,  0  ,  2  ,  0  ,  0  ],
                                                           [  1  ,  0  ,  0  ,  2  ,  0  ,  0  ] ],
        CharacterRace::Human_MarkOfPassage        => vec![ [  0  ,  2  ,  0  ,  0  ,  0  ,  1  ],   // Mark of Passage Humans get +1 to a non-Dexterity stat.
                                                           [  0  ,  2  ,  0  ,  0  ,  1  ,  0  ],
                                                           [  0  ,  2  ,  0  ,  1  ,  0  ,  0  ],
                                                           [  0  ,  2  ,  1  ,  0  ,  0  ,  0  ],
                                                           [  1  ,  2  ,  0  ,  0  ,  0  ,  0  ] ],
        CharacterRace::Human_MarkOfSentinel       => vec![ [  0  ,  0  ,  2  ,  0  ,  1  ,  0  ] ],
        CharacterRace::Human_Variant              => vec![ [  0  ,  0  ,  0  ,  0  ,  1  ,  1  ],   // Variant Humans get +1 to any two different stats.
                                                           [  0  ,  0  ,  0  ,  1  ,  0  ,  1  ],
                                                           [  0  ,  0  ,  0  ,  1  ,  1  ,  0  ],
                                                           [  0  ,  0  ,  1  ,  0  ,  0  ,  1  ],
                                                           [  0  ,  0  ,  1  ,  0  ,  1  ,  0  ],
                                                           [  0  ,  0  ,  1  ,  1  ,  0  ,  0  ],
                                                           [  0  ,  1  ,  0  ,  0  ,  0  ,  1  ],
                                                           [  0  ,  1  ,  0  ,  0  ,  1  ,  0  ],
                                                           [  0  ,  1  ,  0  ,  1  ,  0  ,  0  ],
                                                           [  0  ,  1  ,  1  ,  0  ,  0  ,  0  ],
                                                           [  1  ,  0  ,  0  ,  0  ,  0  ,  1  ],
                                                           [  1  ,  0  ,  0  ,  0  ,  1  ,  0  ],
                                                           [  1  ,  0  ,  0  ,  1  ,  0  ,  0  ],
                                                           [  1  ,  0  ,  1  ,  0  ,  0  ,  0  ],
                                                           [  1  ,  1  ,  0  ,  0  ,  0  ,  0  ] ],
        CharacterRace::Kalashtar                  => vec![ [  0  ,  0  ,  0  ,  0  ,  2  ,  1  ] ],
        CharacterRace::Kenku                      => vec![ [  0  ,  2  ,  0  ,  0  ,  1  ,  0  ] ],
        CharacterRace::Kobold                     => vec![ [ -2  ,  2  ,  0  ,  0  ,  0  ,  0  ] ],
        CharacterRace::Lizardfolk                 => vec![ [  0  ,  0  ,  2  ,  0  ,  1  ,  0  ] ],
        CharacterRace::Orc                        => vec![ [  2  ,  0  ,  1  , -2  ,  0  ,  0  ] ],
        CharacterRace::Orc_Eberron                => vec![ [  2  ,  0  ,  1  ,  0  ,  0  ,  0  ] ],
        CharacterRace::Shifter_BeastHide          => vec![ [  1  ,  0  ,  2  ,  0  ,  0  ,  0  ] ],
        CharacterRace::Shifter_LongTooth          => vec![ [  2  ,  1  ,  0  ,  0  ,  0  ,  0  ] ],
        CharacterRace::Shifter_SwiftStride        => vec![ [  0  ,  2  ,  0  ,  0  ,  0  ,  1  ] ],
        CharacterRace::Shifter_WildHunt           => vec![ [  0  ,  1  ,  0  ,  0  ,  2  ,  0  ] ],
        CharacterRace::Tabaxi                     => vec![ [  0  ,  2  ,  0  ,  0  ,  0  ,  1  ] ],
        CharacterRace::Tiefling                   => vec![ [  0  ,  0  ,  0  ,  1  ,  0  ,  2  ] ],
        CharacterRace::Tiefling_Feral             => vec![ [  0  ,  2  ,  0  ,  1  ,  0  ,  0  ] ],
        CharacterRace::Tortle                     => vec![ [  2  ,  0  ,  0  ,  0  ,  1  ,  0  ] ],
        CharacterRace::Triton                     => vec![ [  1  ,  0  ,  1  ,  0  ,  0  ,  1  ] ],
        CharacterRace::Warforged                  => vec![ [  0  ,  0  ,  2  ,  0  ,  0  ,  1  ],   // Warforged get +1 to a non-Constitution stat.
                                                           [  0  ,  0  ,  2  ,  0  ,  1  ,  0  ],
                                                           [  0  ,  0  ,  2  ,  1  ,  0  ,  0  ],
                                                           [  0  ,  1  ,  2  ,  0  ,  0  ,  0  ],
                                                           [  1  ,  0  ,  2  ,  0  ,  0  ,  0  ] ],
        CharacterRace::YuanTiPureblood            => vec![ [  0  ,  0  ,  0  ,  1  ,  0  ,  2  ] ],
    };

    if canRearrangeRaceModifiers
    {
        // For races that have only one choice for stat modifiers that's all we
        // can use to generate permutations.
        //
        // For races that have more that one choice, so far every race is setup
        // like "+2 to StatX, +1 to any stat that isn't StatX" so it doesn't matter
        // which choice we use to generate the permutations from, they are already
        // permutations of each other.
        let baseStatBlock = *raceModifiers.first().unwrap();

        GenerateAllUniquePermutationsOfStatBlock(baseStatBlock)
    }
    else
    {
        raceModifiers
    }
}

#[test]
fn TestGenerateRaceModifierArrays()
{
    use itertools::Itertools;

    // Humans have no flexibility in their stat assignment, so nothing changes.
    assert_eq!( GenerateRaceModifierArrays(CharacterRace::Human, false)
              , GenerateRaceModifierArrays(CharacterRace::Human, true ) );

    // Variant humans have complete flexibility in their stat assignment already, so nothing changes.
    assert_eq!( GenerateRaceModifierArrays(CharacterRace::Human_Variant, false)
              , GenerateRaceModifierArrays(CharacterRace::Human_Variant, true ) );

    // Tritons have the lowest non-zero amount of rearranging allowed, so testing them is easiest.
    assert_eq!( GenerateRaceModifierArrays(CharacterRace::Triton, false), vec!
    [
        [ 1, 0, 1, 0, 0, 1 ]
    ]);

    assert_eq!( GenerateRaceModifierArrays(CharacterRace::Triton, true), vec!
    [
        [ 0, 0, 0, 1, 1, 1 ],
        [ 0, 0, 1, 0, 1, 1 ],
        [ 0, 0, 1, 1, 0, 1 ],
        [ 0, 0, 1, 1, 1, 0 ],
        [ 0, 1, 0, 0, 1, 1 ],
        [ 0, 1, 0, 1, 0, 1 ],
        [ 0, 1, 0, 1, 1, 0 ],
        [ 0, 1, 1, 0, 0, 1 ],
        [ 0, 1, 1, 0, 1, 0 ],
        [ 0, 1, 1, 1, 0, 0 ],
        [ 1, 0, 0, 0, 1, 1 ],
        [ 1, 0, 0, 1, 0, 1 ],
        [ 1, 0, 0, 1, 1, 0 ],
        [ 1, 0, 1, 0, 0, 1 ],
        [ 1, 0, 1, 0, 1, 0 ],
        [ 1, 0, 1, 1, 0, 0 ],
        [ 1, 1, 0, 0, 0, 1 ],
        [ 1, 1, 0, 0, 1, 0 ],
        [ 1, 1, 0, 1, 0, 0 ],
        [ 1, 1, 1, 0, 0, 0 ],
    ]);

    // For races that have more than one choice for stat modifiers, make sure
    // that any choice we use to generate permutations gives the same result.
    //
    // The actual function always just picks the first choice, so we need to
    // make sure that's actually a valid assumption.
    for race in RaceList.iter()
    {
        let raceModifierArrays = GenerateRaceModifierArrays(*race, false);

        for (a, b) in raceModifierArrays.iter()
                                        .map(|raceModifierArray| GenerateAllUniquePermutationsOfStatBlock(*raceModifierArray) )
                                        .circular_tuple_windows()
        {
            assert_eq!(a, b);
        }
    }
}

pub fn GetNumStartingFeats( aRace : CharacterRace, doAllRacesHaveFreeFeat : bool) -> u8
{
    // Everyone starting with a free feat is a homebrew rule. We've decided
    // that it stacks with races that already give a free feat.
    let addition = if doAllRacesHaveFreeFeat { 1 } else { 0 };

    addition + match aRace
    {
        CharacterRace::CustomLineage => 1,
        CharacterRace::Human_Variant => 1,
        _                            => 0,
    }
}

// This duplication is kind of gross, it would be nice if we could generate it automatically.
// If we could iterate over all possible enum values that would also eliminate the need for this variable.
pub static RaceList: [CharacterRace; 62] =
    [
        CharacterRace::Aarakocra                 ,
        CharacterRace::Aasimar_Fallen            ,
        CharacterRace::Aasimar_Protector         ,
        CharacterRace::Aasimar_Scourge           ,
        CharacterRace::Bugbear                   ,
        CharacterRace::Changling                 ,
        CharacterRace::CustomLineage             ,
        CharacterRace::DragonBorn                ,
        CharacterRace::Dwarf_Grey                ,
        CharacterRace::Dwarf_Hill                ,
        CharacterRace::Dwarf_MarkOfWarding       ,
        CharacterRace::Dwarf_Mountain            ,
        CharacterRace::Elf_Drow                  ,
        CharacterRace::Elf_High                  ,
        CharacterRace::Elf_MarkOfShadow          ,
        CharacterRace::Elf_Wood                  ,
        CharacterRace::Firbolg                   ,
        CharacterRace::Genasi_Air                ,
        CharacterRace::Genasi_Earth              ,
        CharacterRace::Genasi_Fire               ,
        CharacterRace::Genasi_Water              ,
        CharacterRace::Gnome_Deep                ,
        CharacterRace::Gnome_Forest              ,
        CharacterRace::Gnome_MarkOfScribing      ,
        CharacterRace::Gnome_Rock                ,
        CharacterRace::Goblin                    ,
        CharacterRace::Goliath                   ,
        CharacterRace::HalfElf                   ,
        CharacterRace::HalfElf_MarkOfDetection   ,
        CharacterRace::HalfElf_MarkOfStorm       ,
        CharacterRace::Halfling_Ghostwise        ,
        CharacterRace::Halfling_Lightfoot        ,
        CharacterRace::Halfling_MarkOfHealing    ,
        CharacterRace::Halfling_MarkOfHospitality,
        CharacterRace::Halfling_Stout            ,
        CharacterRace::HalfOrc                   ,
        CharacterRace::HalfOrc_MarkOfFinding     ,
        CharacterRace::Hobgoblin                 ,
        CharacterRace::Human                     ,
        CharacterRace::Human_MarkOfFinding       ,
        CharacterRace::Human_MarkOfHandling      ,
        CharacterRace::Human_MarkOfMaking        ,
        CharacterRace::Human_MarkOfPassage       ,
        CharacterRace::Human_MarkOfSentinel      ,
        CharacterRace::Human_Variant             ,
        CharacterRace::Kalashtar                 ,
        CharacterRace::Kenku                     ,
        CharacterRace::Kobold                    ,
        CharacterRace::Lizardfolk                ,
        CharacterRace::Orc                       ,
        CharacterRace::Orc_Eberron               ,
        CharacterRace::Shifter_BeastHide         ,
        CharacterRace::Shifter_LongTooth         ,
        CharacterRace::Shifter_SwiftStride       ,
        CharacterRace::Shifter_WildHunt          ,
        CharacterRace::Tabaxi                    ,
        CharacterRace::Tiefling                  ,
        CharacterRace::Tiefling_Feral            ,
        CharacterRace::Tortle                    ,
        CharacterRace::Triton                    ,
        CharacterRace::Warforged                 ,
        CharacterRace::YuanTiPureblood           ,
    ];
