use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test07" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Flat,
        statOrdering: [
            Charisma,
            Intelligence,
            Wisdom,
            Dexterity,
            Constitution,
            Strength,
        ],
        isReversed: false,
        availableRaces: vec![
            Aarakocra,
            Aasimar_Fallen,
            Aasimar_Protector,
            Aasimar_Scourge,
            Bugbear,
            Changling,
            CustomLineage,
            DragonBorn,
            Dwarf_Grey,
            Dwarf_Hill,
            Dwarf_MarkOfWarding,
            Dwarf_Mountain,
            Elf_Drow,
            Elf_High,
            Elf_MarkOfShadow,
            Elf_Wood,
            Firbolg,
            Genasi_Air,
            Genasi_Earth,
            Genasi_Fire,
            Genasi_Water,
            Gnome_Deep,
            Gnome_Forest,
            Gnome_MarkOfScribing,
            Gnome_Rock,
            Goblin,
            Goliath,
            HalfElf,
            HalfElf_MarkOfDetection,
            HalfElf_MarkOfStorm,
            Halfling_Ghostwise,
            Halfling_Lightfoot,
            Halfling_MarkOfHealing,
            Halfling_MarkOfHospitality,
            Halfling_Stout,
            HalfOrc,
            HalfOrc_MarkOfFinding,
            Hobgoblin,
            Human,
            Human_MarkOfFinding,
            Human_MarkOfHandling,
            Human_MarkOfMaking,
            Human_MarkOfPassage,
            Human_MarkOfSentinel,
            Human_Variant,
            Kalashtar,
            Kenku,
            Kobold,
            Lizardfolk,
            Orc,
            Orc_Eberron,
            Shifter_BeastHide,
            Shifter_LongTooth,
            Shifter_SwiftStride,
            Shifter_WildHunt,
            Tabaxi,
            Tiefling,
            Tiefling_Feral,
            Tortle,
            Triton,
            Warforged,
            YuanTiPureblood,
        ],
        requiredFeats: vec![
            Observant,
            TavernBrawler,
        ],
        constraints: vec![
            BinCmp_StatValue_Constant(
                BinaryCompare {
                    lhsValue: GetStatValue(
                        Strength,
                    ),
                    binCmp: GE,
                    rhsValue: GetConstant(
                        -100,
                    ),
                },
            ),
            BinCmp_StatValue_Constant(
                BinaryCompare {
                    lhsValue: GetStatValue(
                        Dexterity,
                    ),
                    binCmp: GE,
                    rhsValue: GetConstant(
                        -99,
                    ),
                },
            ),
            BinCmp_StatValue_Constant(
                BinaryCompare {
                    lhsValue: GetStatValue(
                        Constitution,
                    ),
                    binCmp: GE,
                    rhsValue: GetConstant(
                        -98,
                    ),
                },
            ),
            BinCmp_StatValue_Constant(
                BinaryCompare {
                    lhsValue: GetStatValue(
                        Intelligence,
                    ),
                    binCmp: GE,
                    rhsValue: GetConstant(
                        -97,
                    ),
                },
            ),
            BinCmp_StatValue_Constant(
                BinaryCompare {
                    lhsValue: GetStatValue(
                        Wisdom,
                    ),
                    binCmp: GE,
                    rhsValue: GetConstant(
                        -96,
                    ),
                },
            ),
            BinCmp_StatValue_Constant(
                BinaryCompare {
                    lhsValue: GetStatValue(
                        Charisma,
                    ),
                    binCmp: GE,
                    rhsValue: GetConstant(
                        -95,
                    ),
                },
            ),
            BinCmp_StatValue_Constant(
                BinaryCompare {
                    lhsValue: GetStatValue(
                        Strength,
                    ),
                    binCmp: LT,
                    rhsValue: GetConstant(
                        100,
                    ),
                },
            ),
            BinCmp_StatValue_Constant(
                BinaryCompare {
                    lhsValue: GetStatValue(
                        Dexterity,
                    ),
                    binCmp: LT,
                    rhsValue: GetConstant(
                        99,
                    ),
                },
            ),
            BinCmp_StatValue_Constant(
                BinaryCompare {
                    lhsValue: GetStatValue(
                        Constitution,
                    ),
                    binCmp: LT,
                    rhsValue: GetConstant(
                        98,
                    ),
                },
            ),
            BinCmp_StatValue_Constant(
                BinaryCompare {
                    lhsValue: GetStatValue(
                        Intelligence,
                    ),
                    binCmp: LT,
                    rhsValue: GetConstant(
                        97,
                    ),
                },
            ),
            BinCmp_StatValue_Constant(
                BinaryCompare {
                    lhsValue: GetStatValue(
                        Wisdom,
                    ),
                    binCmp: LT,
                    rhsValue: GetConstant(
                        96,
                    ),
                },
            ),
            BinCmp_StatValue_Constant(
                BinaryCompare {
                    lhsValue: GetStatValue(
                        Charisma,
                    ),
                    binCmp: LT,
                    rhsValue: GetConstant(
                        96,
                    ),
                },
            ),
            BinCmp_StatValue_StatValue(
                BinaryCompare {
                    lhsValue: GetStatValue(
                        Strength,
                    ),
                    binCmp: EQ,
                    rhsValue: GetStatValue(
                        Strength,
                    ),
                },
            ),
            BinCmp_StatValue_StatValue(
                BinaryCompare {
                    lhsValue: GetStatValue(
                        Dexterity,
                    ),
                    binCmp: EQ,
                    rhsValue: GetStatValue(
                        Dexterity,
                    ),
                },
            ),
            BinCmp_StatValue_StatValue(
                BinaryCompare {
                    lhsValue: GetStatValue(
                        Constitution,
                    ),
                    binCmp: EQ,
                    rhsValue: GetStatValue(
                        Constitution,
                    ),
                },
            ),
            BinCmp_StatValue_StatValue(
                BinaryCompare {
                    lhsValue: GetStatValue(
                        Intelligence,
                    ),
                    binCmp: EQ,
                    rhsValue: GetStatValue(
                        Intelligence,
                    ),
                },
            ),
            BinCmp_StatValue_StatValue(
                BinaryCompare {
                    lhsValue: GetStatValue(
                        Wisdom,
                    ),
                    binCmp: EQ,
                    rhsValue: GetStatValue(
                        Wisdom,
                    ),
                },
            ),
            BinCmp_StatValue_StatValue(
                BinaryCompare {
                    lhsValue: GetStatValue(
                        Charisma,
                    ),
                    binCmp: EQ,
                    rhsValue: GetStatValue(
                        Charisma,
                    ),
                },
            ),
            BinCmp_StatModifier_Constant(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Strength,
                    ),
                    binCmp: GE,
                    rhsValue: GetConstant(
                        -100,
                    ),
                },
            ),
            BinCmp_StatModifier_Constant(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Dexterity,
                    ),
                    binCmp: GE,
                    rhsValue: GetConstant(
                        -99,
                    ),
                },
            ),
            BinCmp_StatModifier_Constant(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Constitution,
                    ),
                    binCmp: GE,
                    rhsValue: GetConstant(
                        -98,
                    ),
                },
            ),
            BinCmp_StatModifier_Constant(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Intelligence,
                    ),
                    binCmp: GE,
                    rhsValue: GetConstant(
                        -97,
                    ),
                },
            ),
            BinCmp_StatModifier_Constant(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Wisdom,
                    ),
                    binCmp: GE,
                    rhsValue: GetConstant(
                        -96,
                    ),
                },
            ),
            BinCmp_StatModifier_Constant(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Charisma,
                    ),
                    binCmp: GE,
                    rhsValue: GetConstant(
                        -95,
                    ),
                },
            ),
            BinCmp_StatModifier_Constant(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Strength,
                    ),
                    binCmp: LT,
                    rhsValue: GetConstant(
                        100,
                    ),
                },
            ),
            BinCmp_StatModifier_Constant(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Dexterity,
                    ),
                    binCmp: LT,
                    rhsValue: GetConstant(
                        99,
                    ),
                },
            ),
            BinCmp_StatModifier_Constant(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Constitution,
                    ),
                    binCmp: LT,
                    rhsValue: GetConstant(
                        98,
                    ),
                },
            ),
            BinCmp_StatModifier_Constant(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Intelligence,
                    ),
                    binCmp: LT,
                    rhsValue: GetConstant(
                        97,
                    ),
                },
            ),
            BinCmp_StatModifier_Constant(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Wisdom,
                    ),
                    binCmp: LT,
                    rhsValue: GetConstant(
                        96,
                    ),
                },
            ),
            BinCmp_StatModifier_Constant(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Charisma,
                    ),
                    binCmp: LT,
                    rhsValue: GetConstant(
                        96,
                    ),
                },
            ),
            BinCmp_StatModifier_StatModifier(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Strength,
                    ),
                    binCmp: EQ,
                    rhsValue: GetStatModifier(
                        Strength,
                    ),
                },
            ),
            BinCmp_StatModifier_StatModifier(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Dexterity,
                    ),
                    binCmp: EQ,
                    rhsValue: GetStatModifier(
                        Dexterity,
                    ),
                },
            ),
            BinCmp_StatModifier_StatModifier(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Constitution,
                    ),
                    binCmp: EQ,
                    rhsValue: GetStatModifier(
                        Constitution,
                    ),
                },
            ),
            BinCmp_StatModifier_StatModifier(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Intelligence,
                    ),
                    binCmp: EQ,
                    rhsValue: GetStatModifier(
                        Intelligence,
                    ),
                },
            ),
            BinCmp_StatModifier_StatModifier(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Wisdom,
                    ),
                    binCmp: EQ,
                    rhsValue: GetStatModifier(
                        Wisdom,
                    ),
                },
            ),
            BinCmp_StatModifier_StatModifier(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Charisma,
                    ),
                    binCmp: EQ,
                    rhsValue: GetStatModifier(
                        Charisma,
                    ),
                },
            ),
        ],
        numberOfASIs: 3,
        doAllRacesHaveFreeFeat: false,
        canRearrangeRaceModifiers: false,
        maxNumberOfResults: 26,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 19654503,
            numUniqueCombinationsPassedConstraints: 19512915,
            numStatBlocksEjected: 523,
            numUniqueCombinationsEjected: 657,
            numUniqueCombinationsBelowLowerBound: 19512296,
            numStatBlocksShiftedDuringInsertion: 7215,
            numDuplicateRaceStatBlocksRemoved: 28,
            numUniqueStatBlockAfterRacePass: 65,
            numUniqueStatBlockAfterFeatPass: 204,
            numFinalStatBlockCombinations: 26,
        };

    let expectedStatBlocks = 
        vec![
            RankedStatBlock {
                rank: 12,
                statBlock: [10, 14, 14, 16, 14, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            Observant,
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [8, 13, 13, 13, 13, 14],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 12,
                statBlock: [12, 14, 12, 16, 14, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            Observant,
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [10, 13, 11, 13, 13, 14],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 12,
                statBlock: [14, 14, 10, 16, 14, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            Observant,
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [12, 13, 9, 13, 13, 14],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 12,
                statBlock: [12, 12, 14, 16, 14, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            Observant,
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [10, 11, 13, 13, 13, 14],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 12,
                statBlock: [14, 12, 12, 16, 14, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            Observant,
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [12, 11, 11, 13, 13, 14],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 12,
                statBlock: [14, 10, 14, 16, 14, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            Observant,
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [12, 9, 13, 13, 13, 14],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 12,
                statBlock: [12, 14, 14, 16, 12, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            Observant,
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [10, 13, 13, 13, 11, 14],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 12,
                statBlock: [14, 14, 12, 16, 12, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            Observant,
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [12, 13, 11, 13, 11, 14],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 12,
                statBlock: [14, 12, 14, 16, 12, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            Observant,
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [12, 11, 13, 13, 11, 14],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 12,
                statBlock: [14, 14, 14, 16, 10, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            Observant,
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [12, 13, 13, 13, 9, 14],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 12,
                statBlock: [12, 14, 14, 15, 14, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            Observant,
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [10, 13, 13, 13, 13, 13],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 12,
                statBlock: [14, 14, 12, 15, 14, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            Observant,
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [12, 13, 11, 13, 13, 13],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 12,
                statBlock: [14, 12, 14, 15, 14, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            Observant,
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [12, 11, 13, 13, 13, 13],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 12,
                statBlock: [14, 14, 14, 15, 12, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            Observant,
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [12, 13, 13, 13, 11, 13],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 12,
                statBlock: [10, 14, 14, 14, 16, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            Observant,
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [8, 13, 13, 13, 13, 14],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 0, 1, 1],
                        myFeatModifiers:     [1, 0, 0, 0, 1, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 12,
                statBlock: [12, 14, 12, 14, 16, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            Observant,
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [10, 13, 11, 13, 13, 14],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 0, 1, 1],
                        myFeatModifiers:     [1, 0, 0, 0, 1, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 12,
                statBlock: [14, 14, 10, 14, 16, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            Observant,
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [12, 13, 9, 13, 13, 14],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 0, 1, 1],
                        myFeatModifiers:     [1, 0, 0, 0, 1, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 12,
                statBlock: [12, 12, 14, 14, 16, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            Observant,
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [10, 11, 13, 13, 13, 14],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 0, 1, 1],
                        myFeatModifiers:     [1, 0, 0, 0, 1, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 12,
                statBlock: [14, 12, 12, 14, 16, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            Observant,
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [12, 11, 11, 13, 13, 14],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 0, 1, 1],
                        myFeatModifiers:     [1, 0, 0, 0, 1, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 12,
                statBlock: [14, 10, 14, 14, 16, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            Observant,
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [12, 9, 13, 13, 13, 14],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 0, 1, 1],
                        myFeatModifiers:     [1, 0, 0, 0, 1, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 12,
                statBlock: [12, 14, 14, 14, 15, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            Observant,
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [10, 13, 13, 13, 13, 13],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [1, 0, 0, 0, 1, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 12,
                statBlock: [14, 14, 12, 14, 15, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            Observant,
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [12, 13, 11, 13, 13, 13],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [1, 0, 0, 0, 1, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 12,
                statBlock: [14, 12, 14, 14, 15, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            Observant,
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [12, 11, 13, 13, 13, 13],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [1, 0, 0, 0, 1, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 12,
                statBlock: [10, 14, 16, 14, 14, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            Observant,
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [9, 13, 13, 12, 13, 14],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 1, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 1, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 12,
                statBlock: [12, 14, 15, 14, 14, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            Observant,
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [11, 13, 13, 12, 13, 13],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 1, 1, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 12,
                statBlock: [13, 14, 14, 14, 14, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![
                            Observant,
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [11, 13, 13, 12, 13, 13],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [1, 0, 0, 1, 0, 0],
                    },
                ],
            },
        ];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

