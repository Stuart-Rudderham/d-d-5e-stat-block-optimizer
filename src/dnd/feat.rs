#![allow(non_upper_case_globals)]

use crate::hash_container_typedefs::*;
use crate::dnd::race::*;
use crate::dnd::statblock::*;
use crate::dnd::asi::*;
use itertools::iproduct;
use itertools::Itertools;
use maplit::btreeset;
use more_asserts::*;
use std::collections::BTreeSet;
use std::convert::TryInto;
use std::iter;

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq, Hash, Clone, Copy)]
pub enum Feat
{
    AberrantDragonMark  ,
    Actor               ,
    Alert               ,
    ArtificerInitiate   ,
    Athlete             ,
    BountifulLuck       ,
    Charger             ,
    Chef                ,
    CrossbowExpert      ,
    Crusher             ,
    DefensiveDuelist    ,
    DragonFear          ,
    DragonHide          ,
    DrowHighMagic       ,
    DualWielder         ,
    DungeonDelver       ,
    Durable             ,
    DwarvenFortitude    ,
    EldritchAdept       ,
    ElementalAdept      ,
    ElvenAccuracy       ,
    FadeAway            ,
    FeyTeleportation    ,
    FeyTouched          ,
    FightingInitiate    ,
    FlamesOfPhlegethos  ,
    Grappler            ,
    GreatWeaponMaster   ,
    Gunner              ,
    Healer              ,
    HeavilyArmored      ,
    HeavyArmorMaster    ,
    InfernalConstitution,
    InspiringLeader     ,
    KeenMind            ,
    LightlyArmored      ,
    Linguist            ,
    Lucky               ,
    MageSlayer          ,
    MagicInitiate       ,
    MartialAdept        ,
    MediumArmorMaster   ,
    MetamagicAdept      ,
    Mobile              ,
    ModeratelyArmored   ,
    MountedCombatant    ,
    Observant           ,
    OrcishFury          ,
    Piercer             ,
    Poisoner            ,
    PolearmMaster       ,
    Prodigy             ,
    Resilient_Cha       , // These are separate
    Resilient_Con       , // feats since the
    Resilient_Dex       , // stat improved also
    Resilient_Int       , // affects proficiency
    Resilient_Str       , // in the relevant
    Resilient_Wis       , // saving throw.
    RevenantBlade       ,
    RitualCaster        ,
    SavageAttacker      ,
    SecondChance        ,
    Sentinel            ,
    ShadowTouched       ,
    Sharpshooter        ,
    ShieldMaster        ,
    Skilled             ,
    SkillExpert         ,
    Skulker             ,
    Slasher             ,
    SpellSniper         ,
    SquatNimbleness     ,
    TavernBrawler       ,
    Telekinetic         ,
    Telepathic          ,
    Tough               ,
    WarCaster           ,
    WeaponMaster        ,
    WoodElfMagic        ,
    NonStatGivingFeat   , // A placeholder for feats that don't give stat increases.
}

pub fn GenerateFeatStatArrays(aFeat : Feat) -> Vec<StatBlock>
{
    use Feat::*;

    //                         | Str | Dex | Con | Int | Wis | Cha |
    const Zero   : StatBlock = [ 0   , 0   , 0   , 0   , 0   , 0   ];
    const OneStr : StatBlock = [ 1   , 0   , 0   , 0   , 0   , 0   ];
    const OneDex : StatBlock = [ 0   , 1   , 0   , 0   , 0   , 0   ];
    const OneCon : StatBlock = [ 0   , 0   , 1   , 0   , 0   , 0   ];
    const OneInt : StatBlock = [ 0   , 0   , 0   , 1   , 0   , 0   ];
    const OneWis : StatBlock = [ 0   , 0   , 0   , 0   , 1   , 0   ];
    const OneCha : StatBlock = [ 0   , 0   , 0   , 0   , 0   , 1   ];

    match aFeat
    {
        AberrantDragonMark    => vec![ OneCon ],
        Actor                 => vec![ OneCha ],
        Alert                 => vec![ Zero ],
        ArtificerInitiate     => vec![ Zero ],
        Athlete               => vec![ OneStr, OneDex ],
        BountifulLuck         => vec![ Zero ],
        Charger               => vec![ Zero ],
        Chef                  => vec![ OneCon, OneCha ],
        CrossbowExpert        => vec![ Zero ],
        Crusher               => vec![ OneStr, OneCon ],
        DefensiveDuelist      => vec![ Zero ],
        DragonFear            => vec![ OneStr, OneCon, OneCha ],
        DragonHide            => vec![ OneStr, OneCon, OneCha ],
        DrowHighMagic         => vec![ Zero ],
        DualWielder           => vec![ Zero ],
        DungeonDelver         => vec![ Zero ],
        Durable               => vec![ OneCon ],
        DwarvenFortitude      => vec![ OneCon ],
        EldritchAdept         => vec![ Zero ],
        ElementalAdept        => vec![ Zero ],
        ElvenAccuracy         => vec![ OneDex, OneInt, OneWis, OneCha ],
        FadeAway              => vec![ OneDex, OneInt ],
        FeyTeleportation      => vec![ OneInt, OneCha ],
        FeyTouched            => vec![ OneInt, OneWis, OneCha ],
        FightingInitiate      => vec![ Zero ],
        FlamesOfPhlegethos    => vec![ OneInt, OneCha ],
        Grappler              => vec![ Zero ],
        GreatWeaponMaster     => vec![ Zero ],
        Gunner                => vec![ OneDex ],
        Healer                => vec![ Zero ],
        HeavilyArmored        => vec![ OneStr ],
        HeavyArmorMaster      => vec![ OneStr ],
        InfernalConstitution  => vec![ OneCon ],
        InspiringLeader       => vec![ Zero ],
        KeenMind              => vec![ OneInt ],
        LightlyArmored        => vec![ OneStr, OneDex ],
        Linguist              => vec![ OneInt ],
        Lucky                 => vec![ Zero ],
        MageSlayer            => vec![ Zero ],
        MagicInitiate         => vec![ Zero ],
        MartialAdept          => vec![ Zero ],
        MediumArmorMaster     => vec![ Zero ],
        MetamagicAdept        => vec![ Zero ],
        Mobile                => vec![ Zero ],
        ModeratelyArmored     => vec![ OneStr, OneDex ],
        MountedCombatant      => vec![ Zero ],
        Observant             => vec![ OneInt, OneWis ],
        OrcishFury            => vec![ OneStr, OneCon ],
        Piercer               => vec![ OneStr, OneDex ],
        Poisoner              => vec![ Zero ],
        PolearmMaster         => vec![ Zero ],
        Prodigy               => vec![ Zero ],
        Resilient_Cha         => vec![ OneCha ],
        Resilient_Con         => vec![ OneCon ],
        Resilient_Dex         => vec![ OneDex ],
        Resilient_Int         => vec![ OneInt ],
        Resilient_Str         => vec![ OneStr ],
        Resilient_Wis         => vec![ OneWis ],
        RevenantBlade         => vec![ OneStr, OneDex ],
        RitualCaster          => vec![ Zero ],
        SavageAttacker        => vec![ Zero ],
        SecondChance          => vec![ OneDex, OneCon, OneCha ],
        Sentinel              => vec![ Zero ],
        ShadowTouched         => vec![ OneInt, OneWis, OneCha ],
        Sharpshooter          => vec![ Zero ],
        ShieldMaster          => vec![ Zero ],
        Skilled               => vec![ Zero ],
        SkillExpert           => vec![ OneStr, OneDex, OneCon, OneInt, OneWis, OneCha ],
        Skulker               => vec![ Zero ],
        Slasher               => vec![ OneStr, OneDex ],
        SpellSniper           => vec![ Zero ],
        SquatNimbleness       => vec![ OneStr, OneDex ],
        TavernBrawler         => vec![ OneStr, OneCon ],
        Telekinetic           => vec![ OneInt, OneWis, OneCha ],
        Telepathic            => vec![ OneInt, OneWis, OneCha ],
        Tough                 => vec![ Zero ],
        WarCaster             => vec![ Zero ],
        WeaponMaster          => vec![ OneStr, OneDex ],
        WoodElfMagic          => vec![ Zero ],
        NonStatGivingFeat     => vec![ Zero ],
    }
}

pub fn IsFeatValidForRace( aFeat : Feat, aCharacterRace : CharacterRace ) -> bool
{
    use Feat::*;
    use CharacterRace::*;

    let AnyOf = |raceList : &[CharacterRace]|
    {
        raceList.iter().any(|x| *x == aCharacterRace)
    };

    let NoneOf = |raceList : &[CharacterRace]|
    {
        !AnyOf(raceList)
    };

    match aFeat
    {
        AberrantDragonMark   => NoneOf(&[Dwarf_MarkOfWarding, Elf_MarkOfShadow, Gnome_MarkOfScribing, HalfElf_MarkOfDetection, HalfElf_MarkOfStorm, Halfling_MarkOfHealing, Halfling_MarkOfHospitality, HalfOrc_MarkOfFinding, Human_MarkOfFinding, Human_MarkOfHandling, Human_MarkOfMaking, Human_MarkOfPassage, Human_MarkOfSentinel]),
        Actor                => true,
        Alert                => true,
        ArtificerInitiate    => true,
        Athlete              => true,
        BountifulLuck        => AnyOf(&[Halfling_Ghostwise, Halfling_Lightfoot, Halfling_MarkOfHealing, Halfling_MarkOfHospitality, Halfling_Stout]),
        Charger              => true,
        Chef                 => true,
        CrossbowExpert       => true,
        Crusher              => true,
        DefensiveDuelist     => true,
        DragonFear           => AnyOf(&[DragonBorn]),
        DragonHide           => AnyOf(&[DragonBorn]),
        DrowHighMagic        => AnyOf(&[Elf_Drow]),
        DualWielder          => true,
        DungeonDelver        => true,
        Durable              => true,
        DwarvenFortitude     => AnyOf(&[Dwarf_Grey, Dwarf_Hill, Dwarf_MarkOfWarding, Dwarf_Mountain]),
        EldritchAdept        => true,
        ElementalAdept       => true,
        ElvenAccuracy        => AnyOf(&[Elf_Drow, Elf_High, Elf_MarkOfShadow, Elf_Wood, HalfElf, HalfElf_MarkOfDetection, HalfElf_MarkOfStorm]),
        FadeAway             => AnyOf(&[Gnome_Deep, Gnome_Forest, Gnome_MarkOfScribing, Gnome_Rock]),
        FeyTeleportation     => AnyOf(&[Elf_High]),
        FeyTouched           => true,
        FightingInitiate     => true,
        FlamesOfPhlegethos   => AnyOf(&[Tiefling, Tiefling_Feral]),
        Grappler             => true,
        GreatWeaponMaster    => true,
        Gunner               => true,
        Healer               => true,
        HeavilyArmored       => true,
        HeavyArmorMaster     => true,
        InfernalConstitution => AnyOf(&[Tiefling, Tiefling_Feral]),
        InspiringLeader      => true,
        KeenMind             => true,
        LightlyArmored       => true,
        Linguist             => true,
        Lucky                => true,
        MageSlayer           => true,
        MagicInitiate        => true,
        MartialAdept         => true,
        MediumArmorMaster    => true,
        MetamagicAdept       => true,
        Mobile               => true,
        ModeratelyArmored    => true,
        MountedCombatant     => true,
        Observant            => true,
        OrcishFury           => AnyOf(&[HalfOrc, HalfOrc_MarkOfFinding]),
        Piercer              => true,
        Poisoner             => true,
        PolearmMaster        => true,
        Prodigy              => AnyOf(&[HalfElf, HalfElf_MarkOfDetection, HalfElf_MarkOfStorm, HalfOrc, HalfOrc_MarkOfFinding, Human, Human_MarkOfFinding, Human_MarkOfHandling, Human_MarkOfMaking, Human_MarkOfPassage, Human_MarkOfSentinel, Human_Variant]),
        Resilient_Cha        => true,
        Resilient_Con        => true,
        Resilient_Dex        => true,
        Resilient_Int        => true,
        Resilient_Str        => true,
        Resilient_Wis        => true,
        RevenantBlade        => AnyOf(&[Elf_Drow, Elf_High, Elf_MarkOfShadow, Elf_Wood]),
        RitualCaster         => true,
        SavageAttacker       => true,
        SecondChance         => AnyOf(&[Halfling_Ghostwise, Halfling_Lightfoot, Halfling_MarkOfHealing, Halfling_MarkOfHospitality, Halfling_Stout]),
        Sentinel             => true,
        ShadowTouched        => true,
        Sharpshooter         => true,
        ShieldMaster         => true,
        Skilled              => true,
        SkillExpert          => true,
        Skulker              => true,
        Slasher              => true,
        SpellSniper          => true,
        SquatNimbleness      => AnyOf(&[Dwarf_Grey, Dwarf_Hill, Dwarf_MarkOfWarding, Dwarf_Mountain, Gnome_Deep, Gnome_Forest, Gnome_MarkOfScribing, Gnome_Rock, Goblin, Halfling_Ghostwise, Halfling_Lightfoot, Halfling_MarkOfHealing, Halfling_MarkOfHospitality, Halfling_Stout, Kobold]),
        TavernBrawler        => true,
        Telekinetic          => true,
        Telepathic           => true,
        Tough                => true,
        WarCaster            => true,
        WeaponMaster         => true,
        WoodElfMagic         => AnyOf(&[Elf_Wood]),
        NonStatGivingFeat    => true,
    }
}

fn GenerateCombinedFeatStatArrays( someFeats : &[Feat] ) -> Vec<StatBlock>
{
    // Given a set of feats, return the set of unique stat blocks
    // calculated from the combination of all the feats.
    let mut result = vec![ [0, 0, 0, 0, 0, 0] ];

    for &aFeat in someFeats
    {
        let featModifiers = GenerateFeatStatArrays(aFeat);

        result = iproduct!(result, featModifiers)
                     .map(|(i, j)| CombineStatBlocks2(i, j) )
                     .collect::<Vec<_>>();
    }

    result.sort();
    result.dedup();

    result
}

// For a given stat block, what are a bunch of different choices
// for feats that, when combined, could give that stat block.
type StatBlockToFeatChoices = DeterministicHashMap<StatBlock, BTreeSet<Vec<Feat>>>;

// Index is: how many extra feats were used for the feat choice selection.
fn GenerateExtraFeatList( someRequiredFeats : &[Feat], maxNumExtraFeats : u8 ) -> Vec<StatBlockToFeatChoices>
{
    //
    // This function assumes that a character can't take the same feat twice.
    //
    // There are two edge cases we don't handle properly, neither of which we care enough
    // about to pay the cost of complicating the code.
    //
    //     1) Elemental Adept is the one feat that should be allowed to be taken twice
    //
    //     2) Resilient shouldn't be able to be taken multiple times but internally it's stored as
    //        multiple feats so we allow it to be. This is because the stat choice affects other
    //        things about the feat (i.e. saving throw) so a user might want a specific choice.
    //

    let possibleFeatsToAdd =
    {
        let mut result : DeterministicHashMap<StatBlock, Vec<Feat>> = DeterministicHashMap::default();

        // Add "NonStatGivingFeat" to represent all the feats that don't give a stat increase.
        //
        // This special case is kind of gross but it eliminates what would otherwise be a huge
        // amount of pointless duplication and noise.
        for &feat in FeatList.iter().chain( iter::once(&Feat::NonStatGivingFeat) )
        {
            let featStatArrays = GenerateFeatStatArrays(feat);

            if feat != Feat::NonStatGivingFeat
            {
                if someRequiredFeats.contains(&feat)
                {
                    // This feat is already required and we can't have duplicates.
                    continue;
                }

                if featStatArrays.iter().copied().all(IsZeroStatBlock)
                {
                    // We only care about feats that give stats here, except
                    // for the special case of NonStatGivingFeat.
                    continue;
                }
            }

            for featStatArray in featStatArrays.into_iter()
            {
                result.entry(featStatArray).or_default().push(feat);
            }
        }

        result
    };

    let mut result = vec![];

    // We start with just the required feats...
    {
        let startingSet = GenerateCombinedFeatStatArrays(someRequiredFeats)
                              .into_iter()
                              .map( |statBlock|
                              {
                                  let mut sortedRequiredFeats = someRequiredFeats.to_vec();
                                  sortedRequiredFeats.sort();

                                  (statBlock, btreeset!(sortedRequiredFeats))
                              })
                              .collect::<StatBlockToFeatChoices>();

        result.push( startingSet );
    }

    // ...and then add on extra feats from there, one by one.
    for i in 1..=maxNumExtraFeats
    {
        // Build on the previous results.
        let previous = &result[usize::from(i - 1)];

        let mut resultForExtraFeat = StatBlockToFeatChoices::default();

        // For every choice of N - 1 feats...
        for (existingStatBlock, listOfListOfFeatsToGetExistingStatBlock) in previous.iter()
        {
            // For every choice of a new feat...
            for (newStatBlock, featsToGetNewStatBlock) in possibleFeatsToAdd.iter()
            {
                let mut existingFeatsWithNewFeat = BTreeSet::new();

                // For all combinations of the N - 1 feats...
                for existingFeats in listOfListOfFeatsToGetExistingStatBlock.iter()
                {
                    // Add in the new feat...
                    for &feat in featsToGetNewStatBlock.iter()
                    {
                        if feat != Feat::NonStatGivingFeat
                        {
                            if existingFeats.contains(&feat)
                            {
                                // Can't have duplicates.
                                continue;
                            }
                        }

                        let mut allFeats = existingFeats.clone();
                        allFeats.push(feat);

                        allFeats.sort();

                        //
                        // We're using a BTreeSet here to eliminate duplicates and keep a deterministic order.
                        //
                        // For example, we would have duplicates in the case of:
                        //     1) in the previous choice we picked feat A and now we're picking feat B
                        //     2) in the previous choice we picked feat B and now we're picking feat A
                        //
                        existingFeatsWithNewFeat.insert(allFeats);
                    }
                }

                if !existingFeatsWithNewFeat.is_empty()
                {
                    let combinedStatBlock = CombineStatBlocks2(*newStatBlock, *existingStatBlock);

                    resultForExtraFeat.entry(combinedStatBlock).or_default().append(&mut existingFeatsWithNewFeat);
                }
            }
        }

        result.push(resultForExtraFeat);
    }

    // Sanity check.
    {
        // Should have entries for everything.
        // The "+ 1" is since we always have the 0th element.
        assert_eq!(result.len(), usize::from(maxNumExtraFeats) + 1);

        for (numFreeFeats, possibleStatBlocks) in result.iter().enumerate()
        {
            let mut sawTwoNonStatGivingFeats = false;

            for (_, listOfListOfFeats) in possibleStatBlocks.iter()
            {
                for listOfFeats in listOfListOfFeats.iter()
                {
                    // For a given number of free feats used, all choices should have the same length.
                    assert_eq!(listOfFeats.len(), usize::from(numFreeFeats) + someRequiredFeats.len());

                    // The required feats are, well, required, so they better be there.
                    for requiredFeat in someRequiredFeats
                    {
                        assert!( listOfFeats.contains(requiredFeat) );
                    }

                    for (&featA, &featB) in listOfFeats.iter().tuple_windows()
                    {
                        // Make sure it's sorted.
                        assert_le!( featA, featB );

                        // Make sure there are no duplicates, other than NonStatGivingFeat since it's special.
                        if (featA != Feat::NonStatGivingFeat) && (featB != Feat::NonStatGivingFeat)
                        {
                            assert_ne!(featA, featB);
                        }

                        if (featA == Feat::NonStatGivingFeat) && (featB == Feat::NonStatGivingFeat)
                        {
                            sawTwoNonStatGivingFeats = true;
                        }
                    }
                }
            }

            // Make sure that we can have duplicates of NonStatGivingFeat, since it's special.
            if numFreeFeats >= 2
            {
                assert!( sawTwoNonStatGivingFeats, "{:?}", possibleStatBlocks );
            }
        }
    }

    result
}

type NumFreeFeats = u8;
type NumASIs = u8;
type AllPossibleFeatChoices = DeterministicHashMap<NumFreeFeats, (NumASIs, StatBlockToFeatChoices)>;

pub fn GenerateFeatChoiceList( someRequiredFeats : &[Feat], totalAvailableASIs : NumASIs, possibleNumFreeFeats : &BTreeSet<NumFreeFeats> ) -> AllPossibleFeatChoices
{
    let numRequiredFeats : u8 = someRequiredFeats.len().try_into().unwrap();

    // Must have at least one possibility, even if it's 0 free feats.
    assert_ne!(possibleNumFreeFeats.len(), 0);
    let maxNumFreeFeats = *possibleNumFreeFeats.iter().max().unwrap();

    let maxNumExtraFreeFeatsAvailable = if numRequiredFeats >= maxNumFreeFeats
    {
        0
    }
    else
    {
        maxNumFreeFeats - numRequiredFeats
    };

    let extraFreeFeatsAvailable = GenerateExtraFeatList(someRequiredFeats, maxNumExtraFreeFeatsAvailable);

    let mut result = AllPossibleFeatChoices::default();

    // For all possible amounts of free feats....
    for &numFreeFeats in possibleNumFreeFeats.iter()
    {
        if let Some((numRemainingASIs, numRemainingFreeFeats)) = GetNumRemainingAfterRequiredFeatPurchace(totalAvailableASIs, numRequiredFeats, numFreeFeats)
        {
            let statBlockToFeatChoices = &extraFreeFeatsAvailable[usize::from(numRemainingFreeFeats)];

            result.entry(numFreeFeats).and_modify(|(existingNumRemainingASIs, existingStatBlockToFeatChoices)|
            {
                assert_eq!(numRemainingASIs, *existingNumRemainingASIs);

                // Merge in all the new data.
                for (statBlock, listOfListOfFeats) in statBlockToFeatChoices
                {
                    let entry = existingStatBlockToFeatChoices.entry(*statBlock).or_default();

                    for featList in listOfListOfFeats
                    {
                        // BTreeSet will ensure we don't have duplicate feat lists.
                        entry.insert(featList.clone());
                    }
                }
            })
            .or_insert_with(|| (numRemainingASIs, statBlockToFeatChoices.clone()) );
        }
    }

    result
}

// This duplication is kind of gross, it would be nice if we could generate it automatically.
// If we could iterate over all possible enum values that would also eliminate the need for this variable.
//
// NonStatGivingFeat is intentionally left off the list as it isn't a real feat.
pub static FeatList: [Feat; 79] =
    [
        Feat::AberrantDragonMark,
        Feat::Actor,
        Feat::Alert,
        Feat::ArtificerInitiate,
        Feat::Athlete,
        Feat::BountifulLuck,
        Feat::Charger,
        Feat::Chef,
        Feat::CrossbowExpert,
        Feat::Crusher,
        Feat::DefensiveDuelist,
        Feat::DragonFear,
        Feat::DragonHide,
        Feat::DrowHighMagic,
        Feat::DualWielder,
        Feat::DungeonDelver,
        Feat::Durable,
        Feat::DwarvenFortitude,
        Feat::EldritchAdept,
        Feat::ElementalAdept,
        Feat::ElvenAccuracy,
        Feat::FadeAway,
        Feat::FeyTeleportation,
        Feat::FeyTouched,
        Feat::FightingInitiate,
        Feat::FlamesOfPhlegethos,
        Feat::Grappler,
        Feat::GreatWeaponMaster,
        Feat::Gunner,
        Feat::Healer,
        Feat::HeavilyArmored,
        Feat::HeavyArmorMaster,
        Feat::InfernalConstitution,
        Feat::InspiringLeader,
        Feat::KeenMind,
        Feat::LightlyArmored,
        Feat::Linguist,
        Feat::Lucky,
        Feat::MageSlayer,
        Feat::MagicInitiate,
        Feat::MartialAdept,
        Feat::MediumArmorMaster,
        Feat::MetamagicAdept,
        Feat::Mobile,
        Feat::ModeratelyArmored,
        Feat::MountedCombatant,
        Feat::Observant,
        Feat::OrcishFury,
        Feat::Piercer,
        Feat::Poisoner,
        Feat::PolearmMaster,
        Feat::Prodigy,
        Feat::Resilient_Cha,
        Feat::Resilient_Con,
        Feat::Resilient_Dex,
        Feat::Resilient_Int,
        Feat::Resilient_Str,
        Feat::Resilient_Wis,
        Feat::RevenantBlade,
        Feat::RitualCaster,
        Feat::SavageAttacker,
        Feat::SecondChance,
        Feat::Sentinel,
        Feat::ShadowTouched,
        Feat::Sharpshooter,
        Feat::ShieldMaster,
        Feat::Skilled,
        Feat::SkillExpert,
        Feat::Skulker,
        Feat::Slasher,
        Feat::SpellSniper,
        Feat::SquatNimbleness,
        Feat::TavernBrawler,
        Feat::Telekinetic,
        Feat::Telepathic,
        Feat::Tough,
        Feat::WarCaster,
        Feat::WeaponMaster,
        Feat::WoodElfMagic,
    ];

#[cfg(test)]
mod tests
{
    use super::*;

    fn GetRequiredFeatsList() -> Vec<Vec<Feat>>
    {
        vec!
        [
            vec![],                                                                     // No feats.
            vec![Feat::Actor            ],                                              // 1 stat giving feat.
            vec![Feat::Alert            ],                                              // 1 specific non-stat-giving feat.
            vec![Feat::NonStatGivingFeat],                                              // 1 generic non-stat-giving feat.
            vec![Feat::FadeAway          , Feat::DungeonDelver],                        // 2 feats.
            vec![Feat::Crusher           , Feat::FeyTouched    , Feat::ElvenAccuracy],  // 3 feats.
        ]
    }

    #[test]
    fn TestGenerateExtraFeatList()
    {
        for requiredFeats in GetRequiredFeatsList()
        {
            // Most sanity checks will be run in the generation function itself.
            let zero  = GenerateExtraFeatList( &requiredFeats, 0 );
            let one   = GenerateExtraFeatList( &requiredFeats, 1 );
            let two   = GenerateExtraFeatList( &requiredFeats, 2 );
            let three = GenerateExtraFeatList( &requiredFeats, 3 );

            // Calculating with more free feats shouldn't change the earlier data.
            assert_eq!( three[2],  two[2] );
            assert_eq!( three[1],  two[1] );
            assert_eq!( three[0],  two[0] );
            assert_eq!(   two[1],  one[1] );
            assert_eq!(   two[0],  one[0] );
            assert_eq!(   one[0], zero[0] );
        }

        // Check a specific, hardcoded example.
        let actualData = GenerateExtraFeatList(&[Feat::Actor], 1);

        // All the ".iter().cloned().collect()" stuff is so that we can build up a
        // DeterministicHashMap object, since maplit doesn't provide easy macros for that.
        let expectedData : Vec<StatBlockToFeatChoices> = vec!
        [
            // 0 extra feats.
            [
                ([0, 0, 0, 0, 0, 1], btreeset!
                [
                    vec![Feat::Actor],
                ]),
            ]
            .iter().cloned().collect(),

            // 1 extra feat.
            [
                ([0, 0, 0, 0, 0, 1], btreeset!
                [
                    vec![Feat::Actor, Feat::NonStatGivingFeat],
                ]),

                ([0, 0, 0, 0, 0, 2], btreeset!
                [
                    vec![Feat::Actor, Feat::Chef],
                    vec![Feat::Actor, Feat::DragonFear],
                    vec![Feat::Actor, Feat::DragonHide],
                    vec![Feat::Actor, Feat::ElvenAccuracy],
                    vec![Feat::Actor, Feat::FeyTeleportation],
                    vec![Feat::Actor, Feat::FeyTouched],
                    vec![Feat::Actor, Feat::FlamesOfPhlegethos],
                    vec![Feat::Actor, Feat::Resilient_Cha],
                    vec![Feat::Actor, Feat::SecondChance],
                    vec![Feat::Actor, Feat::ShadowTouched],
                    vec![Feat::Actor, Feat::SkillExpert],
                    vec![Feat::Actor, Feat::Telekinetic],
                    vec![Feat::Actor, Feat::Telepathic],
                ]),

                ([0, 0, 0, 0, 1, 1], btreeset!
                [
                    vec![Feat::Actor, Feat::ElvenAccuracy],
                    vec![Feat::Actor, Feat::FeyTouched],
                    vec![Feat::Actor, Feat::Observant],
                    vec![Feat::Actor, Feat::Resilient_Wis],
                    vec![Feat::Actor, Feat::ShadowTouched],
                    vec![Feat::Actor, Feat::SkillExpert],
                    vec![Feat::Actor, Feat::Telekinetic],
                    vec![Feat::Actor, Feat::Telepathic],
                ]),

                ([0, 0, 0, 1, 0, 1], btreeset!
                [
                    vec![Feat::Actor, Feat::ElvenAccuracy],
                    vec![Feat::Actor, Feat::FadeAway],
                    vec![Feat::Actor, Feat::FeyTeleportation],
                    vec![Feat::Actor, Feat::FeyTouched],
                    vec![Feat::Actor, Feat::FlamesOfPhlegethos],
                    vec![Feat::Actor, Feat::KeenMind],
                    vec![Feat::Actor, Feat::Linguist],
                    vec![Feat::Actor, Feat::Observant],
                    vec![Feat::Actor, Feat::Resilient_Int],
                    vec![Feat::Actor, Feat::ShadowTouched],
                    vec![Feat::Actor, Feat::SkillExpert],
                    vec![Feat::Actor, Feat::Telekinetic],
                    vec![Feat::Actor, Feat::Telepathic],
                ]),

                ([0, 0, 1, 0, 0, 1], btreeset!
                [
                    vec![Feat::AberrantDragonMark, Feat::Actor],
                    vec![Feat::Actor             , Feat::Chef],
                    vec![Feat::Actor             , Feat::Crusher],
                    vec![Feat::Actor             , Feat::DragonFear],
                    vec![Feat::Actor             , Feat::DragonHide],
                    vec![Feat::Actor             , Feat::Durable],
                    vec![Feat::Actor             , Feat::DwarvenFortitude],
                    vec![Feat::Actor             , Feat::InfernalConstitution],
                    vec![Feat::Actor             , Feat::OrcishFury],
                    vec![Feat::Actor             , Feat::Resilient_Con],
                    vec![Feat::Actor             , Feat::SecondChance],
                    vec![Feat::Actor             , Feat::SkillExpert],
                    vec![Feat::Actor             , Feat::TavernBrawler],
                ]),

                ([0, 1, 0, 0, 0, 1], btreeset!
                [
                    vec![Feat::Actor, Feat::Athlete],
                    vec![Feat::Actor, Feat::ElvenAccuracy],
                    vec![Feat::Actor, Feat::FadeAway],
                    vec![Feat::Actor, Feat::Gunner],
                    vec![Feat::Actor, Feat::LightlyArmored],
                    vec![Feat::Actor, Feat::ModeratelyArmored],
                    vec![Feat::Actor, Feat::Piercer],
                    vec![Feat::Actor, Feat::Resilient_Dex],
                    vec![Feat::Actor, Feat::RevenantBlade],
                    vec![Feat::Actor, Feat::SecondChance],
                    vec![Feat::Actor, Feat::SkillExpert],
                    vec![Feat::Actor, Feat::Slasher],
                    vec![Feat::Actor, Feat::SquatNimbleness],
                    vec![Feat::Actor, Feat::WeaponMaster],
                ]),

                ([1, 0, 0, 0, 0, 1], btreeset!
                [
                    vec![Feat::Actor, Feat::Athlete],
                    vec![Feat::Actor, Feat::Crusher],
                    vec![Feat::Actor, Feat::DragonFear],
                    vec![Feat::Actor, Feat::DragonHide],
                    vec![Feat::Actor, Feat::HeavilyArmored],
                    vec![Feat::Actor, Feat::HeavyArmorMaster],
                    vec![Feat::Actor, Feat::LightlyArmored],
                    vec![Feat::Actor, Feat::ModeratelyArmored],
                    vec![Feat::Actor, Feat::OrcishFury],
                    vec![Feat::Actor, Feat::Piercer],
                    vec![Feat::Actor, Feat::Resilient_Str],
                    vec![Feat::Actor, Feat::RevenantBlade],
                    vec![Feat::Actor, Feat::SkillExpert],
                    vec![Feat::Actor, Feat::Slasher],
                    vec![Feat::Actor, Feat::SquatNimbleness],
                    vec![Feat::Actor, Feat::TavernBrawler],
                    vec![Feat::Actor, Feat::WeaponMaster],
                ]),
            ]
            .iter().cloned().collect(),
        ];

        assert_eq!( expectedData, actualData );
    }

    #[test]
    fn TestGenerateFeatChoiceList()
    {
        let possibleNumFreeFeatsList = vec!
        [
            btreeset!(0),
            btreeset!(2),
            btreeset!(0, 1),
            btreeset!(0, 2),
            btreeset!(0, 1, 2),
            btreeset!(0, 2, 4),
        ];

        for requiredFeats in GetRequiredFeatsList().iter()
        {
            for numASIs in 0..=4
            {
                for possibleNumFreeFeats in possibleNumFreeFeatsList.iter()
                {
                    // All sanity checks will be run in the generation function itself.
                    GenerateFeatChoiceList( requiredFeats, numASIs, &possibleNumFreeFeats );
                }
            }
        }

        // Check a specific, hardcoded example.
        let actualData = GenerateFeatChoiceList( &[Feat::Actor], 1, &btreeset!(0, 2) );

        // All the ".iter().cloned().collect()" stuff is so that we can build up a
        // DeterministicHashMap object, since maplit doesn't provide easy macros for that.
        let expectedData : AllPossibleFeatChoices =
        [
            // Started with 0 free feats, 0 ASIs remaining
            (0, (0,
            [
                ([0, 0, 0, 0, 0, 1], btreeset!
                [
                    vec![Feat::Actor]
                ]),
            ].iter().cloned().collect())),

            // Started with 2 free feats, 1 ASI remaining.
            (2, (1,
            [
                ([0, 0, 0, 0, 0, 1], btreeset!
                [
                    vec![Feat::Actor, Feat::NonStatGivingFeat],
                ]),

                ([0, 0, 0, 0, 0, 2], btreeset!
                [
                    vec![Feat::Actor, Feat::Chef],
                    vec![Feat::Actor, Feat::DragonFear],
                    vec![Feat::Actor, Feat::DragonHide],
                    vec![Feat::Actor, Feat::ElvenAccuracy],
                    vec![Feat::Actor, Feat::FeyTeleportation],
                    vec![Feat::Actor, Feat::FeyTouched],
                    vec![Feat::Actor, Feat::FlamesOfPhlegethos],
                    vec![Feat::Actor, Feat::Resilient_Cha],
                    vec![Feat::Actor, Feat::SecondChance],
                    vec![Feat::Actor, Feat::ShadowTouched],
                    vec![Feat::Actor, Feat::SkillExpert],
                    vec![Feat::Actor, Feat::Telekinetic],
                    vec![Feat::Actor, Feat::Telepathic],
                ]),

                ([0, 0, 0, 0, 1, 1], btreeset!
                [
                    vec![Feat::Actor, Feat::ElvenAccuracy],
                    vec![Feat::Actor, Feat::FeyTouched],
                    vec![Feat::Actor, Feat::Observant],
                    vec![Feat::Actor, Feat::Resilient_Wis],
                    vec![Feat::Actor, Feat::ShadowTouched],
                    vec![Feat::Actor, Feat::SkillExpert],
                    vec![Feat::Actor, Feat::Telekinetic],
                    vec![Feat::Actor, Feat::Telepathic],
                ]),

                ([0, 0, 0, 1, 0, 1], btreeset!
                [
                    vec![Feat::Actor, Feat::ElvenAccuracy],
                    vec![Feat::Actor, Feat::FadeAway],
                    vec![Feat::Actor, Feat::FeyTeleportation],
                    vec![Feat::Actor, Feat::FeyTouched],
                    vec![Feat::Actor, Feat::FlamesOfPhlegethos],
                    vec![Feat::Actor, Feat::KeenMind],
                    vec![Feat::Actor, Feat::Linguist],
                    vec![Feat::Actor, Feat::Observant],
                    vec![Feat::Actor, Feat::Resilient_Int],
                    vec![Feat::Actor, Feat::ShadowTouched],
                    vec![Feat::Actor, Feat::SkillExpert],
                    vec![Feat::Actor, Feat::Telekinetic],
                    vec![Feat::Actor, Feat::Telepathic],
                ]),

                ([0, 0, 1, 0, 0, 1], btreeset!
                [
                    vec![Feat::AberrantDragonMark, Feat::Actor],
                    vec![Feat::Actor             , Feat::Chef],
                    vec![Feat::Actor             , Feat::Crusher],
                    vec![Feat::Actor             , Feat::DragonFear],
                    vec![Feat::Actor             , Feat::DragonHide],
                    vec![Feat::Actor             , Feat::Durable],
                    vec![Feat::Actor             , Feat::DwarvenFortitude],
                    vec![Feat::Actor             , Feat::InfernalConstitution],
                    vec![Feat::Actor             , Feat::OrcishFury],
                    vec![Feat::Actor             , Feat::Resilient_Con],
                    vec![Feat::Actor             , Feat::SecondChance],
                    vec![Feat::Actor             , Feat::SkillExpert],
                    vec![Feat::Actor             , Feat::TavernBrawler],
                ]),

                ([0, 1, 0, 0, 0, 1], btreeset!
                [
                    vec![Feat::Actor, Feat::Athlete],
                    vec![Feat::Actor, Feat::ElvenAccuracy],
                    vec![Feat::Actor, Feat::FadeAway],
                    vec![Feat::Actor, Feat::Gunner],
                    vec![Feat::Actor, Feat::LightlyArmored],
                    vec![Feat::Actor, Feat::ModeratelyArmored],
                    vec![Feat::Actor, Feat::Piercer],
                    vec![Feat::Actor, Feat::Resilient_Dex],
                    vec![Feat::Actor, Feat::RevenantBlade],
                    vec![Feat::Actor, Feat::SecondChance],
                    vec![Feat::Actor, Feat::SkillExpert],
                    vec![Feat::Actor, Feat::Slasher],
                    vec![Feat::Actor, Feat::SquatNimbleness],
                    vec![Feat::Actor, Feat::WeaponMaster],
                ]),

                ([1, 0, 0, 0, 0, 1], btreeset!
                [
                    vec![Feat::Actor, Feat::Athlete],
                    vec![Feat::Actor, Feat::Crusher],
                    vec![Feat::Actor, Feat::DragonFear],
                    vec![Feat::Actor, Feat::DragonHide],
                    vec![Feat::Actor, Feat::HeavilyArmored],
                    vec![Feat::Actor, Feat::HeavyArmorMaster],
                    vec![Feat::Actor, Feat::LightlyArmored],
                    vec![Feat::Actor, Feat::ModeratelyArmored],
                    vec![Feat::Actor, Feat::OrcishFury],
                    vec![Feat::Actor, Feat::Piercer],
                    vec![Feat::Actor, Feat::Resilient_Str],
                    vec![Feat::Actor, Feat::RevenantBlade],
                    vec![Feat::Actor, Feat::SkillExpert],
                    vec![Feat::Actor, Feat::Slasher],
                    vec![Feat::Actor, Feat::SquatNimbleness],
                    vec![Feat::Actor, Feat::TavernBrawler],
                    vec![Feat::Actor, Feat::WeaponMaster],
                ]),
            ].iter().cloned().collect())),
        ]
        .iter().cloned().collect();

        assert_eq!( expectedData, actualData );
    }

    #[test]
    fn CombinedFeatStatArrays()
    {
        // Empty feat list.
        assert_eq!
        (
            GenerateCombinedFeatStatArrays( &[] ),
            [ [0, 0, 0, 0, 0, 0] ]
        );

        // Feat that gives no stats.
        assert_eq!
        (
            GenerateCombinedFeatStatArrays( &[Feat::Alert] ),
            [ [0, 0, 0, 0, 0, 0] ]
        );

        // Feat that gives one possible stat boost.
        assert_eq!
        (
            GenerateCombinedFeatStatArrays( &[Feat::KeenMind] ),
            [ [0, 0, 0, 1, 0, 0] ]
        );

        // Feat that gives two possible stat boost.
        assert_eq!
        (
            GenerateCombinedFeatStatArrays( &[Feat::Athlete] ),
            [ [0, 1, 0, 0, 0, 0] ,
              [1, 0, 0, 0, 0, 0] ]
        );

        // Two feats, one that gives no stat boosts.
        assert_eq!
        (
            GenerateCombinedFeatStatArrays( &[Feat::Alert, Feat::Athlete] ),
            [ [0, 1, 0, 0, 0, 0] ,
              [1, 0, 0, 0, 0, 0] ]
        );

        // Two feats, each giving multiple stat boosts.
        assert_eq!
        (
            GenerateCombinedFeatStatArrays( &[Feat::Athlete, Feat::Observant] ),
            [ [0, 1, 0, 0, 1, 0] ,
              [0, 1, 0, 1, 0, 0] ,
              [1, 0, 0, 0, 1, 0] ,
              [1, 0, 0, 1, 0, 0] ]
        );

        // Ensure the order of the feats doesn't matter.
        assert_eq!
        (
            GenerateCombinedFeatStatArrays( &[Feat::Observant, Feat::Athlete] ),
            [ [0, 1, 0, 0, 1, 0] ,
              [0, 1, 0, 1, 0, 0] ,
              [1, 0, 0, 0, 1, 0] ,
              [1, 0, 0, 1, 0, 0] ]
        );

        // Two feats, each giving the same possible stat boosts.
        assert_eq!
        (
            GenerateCombinedFeatStatArrays( &[Feat::LightlyArmored, Feat::Athlete] ),
            [ [0, 2, 0, 0, 0, 0] ,
              [1, 1, 0, 0, 0, 0] ,
              [2, 0, 0, 0, 0, 0] ]
        );

        // Three feats, each giving multiple possible stat boosts.
        assert_eq!
        (
            GenerateCombinedFeatStatArrays( &[Feat::Athlete, Feat::TavernBrawler, Feat::Observant] ),
            [ [0, 1, 1, 0, 1, 0] ,
              [0, 1, 1, 1, 0, 0] ,
              [1, 0, 1, 0, 1, 0] ,
              [1, 0, 1, 1, 0, 0] ,
              [1, 1, 0, 0, 1, 0] ,
              [1, 1, 0, 1, 0, 0] ,
              [2, 0, 0, 0, 1, 0] ,
              [2, 0, 0, 1, 0, 0] ]
        );
    }

    #[test]
    fn RacialFeats()
    {
        // Just test some random combinations.
        assert!(  IsFeatValidForRace( Feat::Athlete             , CharacterRace::Orc                     ) );
        assert!(  IsFeatValidForRace( Feat::OrcishFury          , CharacterRace::HalfOrc                 ) );
        assert!( !IsFeatValidForRace( Feat::OrcishFury          , CharacterRace::Orc                     ) );
        assert!( !IsFeatValidForRace( Feat::OrcishFury          , CharacterRace::Human                   ) );
        assert!(  IsFeatValidForRace( Feat::InfernalConstitution, CharacterRace::Tiefling_Feral          ) );
        assert!( !IsFeatValidForRace( Feat::InfernalConstitution, CharacterRace::Halfling_Lightfoot      ) );
        assert!(  IsFeatValidForRace( Feat::AberrantDragonMark  , CharacterRace::Human_Variant           ) );
        assert!( !IsFeatValidForRace( Feat::AberrantDragonMark  , CharacterRace::Human_MarkOfSentinel    ) );
        assert!(  IsFeatValidForRace( Feat::Prodigy             , CharacterRace::Human_Variant           ) );
        assert!(  IsFeatValidForRace( Feat::Prodigy             , CharacterRace::Human                   ) );
        assert!(  IsFeatValidForRace( Feat::Prodigy             , CharacterRace::HalfElf                 ) );
        assert!(  IsFeatValidForRace( Feat::Prodigy             , CharacterRace::HalfElf_MarkOfDetection ) );
        assert!(  IsFeatValidForRace( Feat::Prodigy             , CharacterRace::HalfOrc                 ) );
        assert!(  IsFeatValidForRace( Feat::Prodigy             , CharacterRace::HalfOrc_MarkOfFinding   ) );
        assert!( !IsFeatValidForRace( Feat::Prodigy             , CharacterRace::Elf_High                ) );
    }
}
