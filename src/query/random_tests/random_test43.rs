use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test43" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Weighted,
        statOrdering: [
            Charisma,
            Intelligence,
            Dexterity,
            Constitution,
            Strength,
            Wisdom,
        ],
        isReversed: true,
        availableRaces: vec![
            Aarakocra,
            Aasimar_Fallen,
            Aasimar_Protector,
            Bugbear,
            CustomLineage,
            Changling,
            DragonBorn,
            Dwarf_Grey,
            Dwarf_Hill,
            Dwarf_MarkOfWarding,
            Dwarf_Mountain,
            Elf_Drow,
            Elf_High,
            Elf_MarkOfShadow,
            Elf_Wood,
            Firbolg,
            Genasi_Air,
            Genasi_Fire,
            Genasi_Water,
            Gnome_Deep,
            Gnome_Forest,
            Gnome_MarkOfScribing,
            Gnome_Rock,
            Goblin,
            Goliath,
            HalfElf,
            HalfElf_MarkOfDetection,
            HalfElf_MarkOfStorm,
            Halfling_Ghostwise,
            Halfling_Lightfoot,
            Halfling_MarkOfHospitality,
            Halfling_Stout,
            HalfOrc,
            HalfOrc_MarkOfFinding,
            Hobgoblin,
            Human,
            Human_MarkOfFinding,
            Human_MarkOfHandling,
            Human_MarkOfMaking,
            Human_MarkOfPassage,
            Human_MarkOfSentinel,
            Human_Variant,
            Kalashtar,
            Kenku,
            Kobold,
            Lizardfolk,
            Orc,
            Orc_Eberron,
            Shifter_BeastHide,
            Shifter_LongTooth,
            Shifter_SwiftStride,
            Shifter_WildHunt,
            Tabaxi,
            Tiefling,
            Tiefling_Feral,
            Tortle,
            Triton,
            Warforged,
            YuanTiPureblood,
        ],
        requiredFeats: vec![
            Slasher,
            TavernBrawler,
        ],
        constraints: vec![],
        numberOfASIs: 2,
        doAllRacesHaveFreeFeat: false,
        canRearrangeRaceModifiers: true,
        maxNumberOfResults: 11,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 14541205,
            numUniqueCombinationsPassedConstraints: 14539220,
            numStatBlocksEjected: 233,
            numUniqueCombinationsEjected: 1681,
            numUniqueCombinationsBelowLowerBound: 14537893,
            numStatBlocksShiftedDuringInsertion: 1341,
            numDuplicateRaceStatBlocksRemoved: 770,
            numUniqueStatBlockAfterRacePass: 297,
            numUniqueStatBlockAfterFeatPass: 922,
            numFinalStatBlockCombinations: 12,
        };

    let expectedStatBlocks = 
        vec![
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 13, 13, 13, 13, 10],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Slasher,
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [9, 13, 13, 13, 15, 10],
                        myRaceModifiers:     [2, 0, 0, 0, -2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [2, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 13, 13, 12, 13, 11],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Slasher,
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [9, 13, 13, 12, 15, 11],
                        myRaceModifiers:     [2, 0, 0, 0, -2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [2, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 12, 13, 13, 13, 11],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Slasher,
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [9, 12, 13, 13, 15, 11],
                        myRaceModifiers:     [2, 0, 0, 0, -2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [2, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 13, 12, 13, 13, 11],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Slasher,
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [9, 13, 12, 13, 15, 11],
                        myRaceModifiers:     [2, 0, 0, 0, -2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [2, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [12, 13, 13, 13, 13, 11],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Slasher,
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [8, 13, 13, 13, 15, 11],
                        myRaceModifiers:     [2, 0, 0, 0, -2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [2, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 13, 13, 13, 12, 11],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Slasher,
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [9, 13, 13, 15, 12, 11],
                        myRaceModifiers:     [2, 0, 0, -2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [2, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 13, 13, 13, 13, 11],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Slasher,
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [10, 13, 14, 13, 13, 11],
                        myRaceModifiers:     [2, 0, -2, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![
                            Slasher,
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [9, 12, 13, 13, 15, 11],
                        myRaceModifiers:     [2, 1, 0, 0, -2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [2, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 13, 13, 11, 13, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Slasher,
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [9, 13, 13, 11, 15, 12],
                        myRaceModifiers:     [2, 0, 0, 0, -2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [2, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 11, 13, 13, 13, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Slasher,
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [9, 11, 13, 13, 15, 12],
                        myRaceModifiers:     [2, 0, 0, 0, -2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [2, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 13, 11, 13, 13, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Slasher,
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [9, 13, 11, 13, 15, 12],
                        myRaceModifiers:     [2, 0, 0, 0, -2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [2, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [11, 13, 13, 13, 13, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Slasher,
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [8, 12, 13, 13, 15, 12],
                        myRaceModifiers:     [2, 0, 0, 0, -2, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 1, 0, 0, 0, 0],
                    },
                ],
            },
        ];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

