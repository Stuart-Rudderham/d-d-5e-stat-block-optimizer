use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test36" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Unranked,
        statOrdering: [
            Charisma,
            Strength,
            Intelligence,
            Wisdom,
            Constitution,
            Dexterity,
        ],
        isReversed: false,
        availableRaces: vec![
            Aarakocra,
            Aasimar_Fallen,
            Aasimar_Protector,
            Aasimar_Scourge,
            Changling,
            Dwarf_Grey,
            Dwarf_Hill,
            Dwarf_MarkOfWarding,
            Dwarf_Mountain,
            Elf_High,
            Elf_MarkOfShadow,
            Firbolg,
            Genasi_Air,
            Genasi_Earth,
            Genasi_Fire,
            Genasi_Water,
            Gnome_Deep,
            Gnome_MarkOfScribing,
            Goblin,
            HalfElf,
            HalfElf_MarkOfDetection,
            HalfElf_MarkOfStorm,
            Halfling_Lightfoot,
            Halfling_MarkOfHealing,
            Halfling_Stout,
            HalfOrc,
            HalfOrc_MarkOfFinding,
            Hobgoblin,
            Human,
            Human_MarkOfFinding,
            Human_MarkOfHandling,
            Human_MarkOfPassage,
            Kalashtar,
            Kobold,
            Orc,
            Orc_Eberron,
            Shifter_BeastHide,
            Shifter_LongTooth,
            Shifter_SwiftStride,
            Tabaxi,
            Tiefling,
            Tortle,
            Triton,
            Warforged,
            YuanTiPureblood,
        ],
        requiredFeats: vec![],
        constraints: vec![],
        numberOfASIs: 1,
        doAllRacesHaveFreeFeat: true,
        canRearrangeRaceModifiers: false,
        maxNumberOfResults: 19,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 12419393,
            numUniqueCombinationsPassedConstraints: 12419393,
            numStatBlocksEjected: 1261,
            numUniqueCombinationsEjected: 12030,
            numUniqueCombinationsBelowLowerBound: 12418087,
            numStatBlocksShiftedDuringInsertion: 11453,
            numDuplicateRaceStatBlocksRemoved: 9,
            numUniqueStatBlockAfterRacePass: 40,
            numUniqueStatBlockAfterFeatPass: 191,
            numFinalStatBlockCombinations: 178,
        };

    let expectedStatBlocks = 
        vec![
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 8, 8, 16, 8, 20],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            ElvenAccuracy,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 8, 8, 15, 10, 20],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 14, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 14, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            ElvenAccuracy,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 14, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 14, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 14, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 14, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 14, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 14, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 14, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 8, 9, 15, 9, 20],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [15, 8, 9, 14, 9, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 8, 9, 14, 9, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            ElvenAccuracy,
                        ],
                        myBasePointBuyArray: [15, 8, 9, 14, 9, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 9, 14, 9, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [15, 8, 9, 14, 9, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 9, 14, 9, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 8, 9, 14, 9, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 8, 9, 14, 9, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 8, 9, 14, 9, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 9, 8, 15, 9, 20],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [15, 9, 8, 14, 9, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 9, 8, 14, 9, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            ElvenAccuracy,
                        ],
                        myBasePointBuyArray: [15, 9, 8, 14, 9, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 9, 8, 14, 9, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [15, 9, 8, 14, 9, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 9, 8, 14, 9, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 9, 8, 14, 9, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 9, 8, 14, 9, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 9, 8, 14, 9, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 8, 8, 15, 9, 20],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            ElvenAccuracy,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 8, 10, 15, 8, 20],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [15, 8, 10, 14, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 8, 10, 14, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            ElvenAccuracy,
                        ],
                        myBasePointBuyArray: [15, 8, 10, 14, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 10, 14, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [15, 8, 10, 14, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 10, 14, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 8, 10, 14, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 8, 10, 14, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 8, 10, 14, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 9, 9, 15, 8, 20],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [15, 9, 9, 14, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 9, 9, 14, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            ElvenAccuracy,
                        ],
                        myBasePointBuyArray: [15, 9, 9, 14, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 9, 9, 14, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [15, 9, 9, 14, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 9, 9, 14, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 9, 9, 14, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 9, 9, 14, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 9, 9, 14, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 8, 9, 15, 8, 20],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            ElvenAccuracy,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 10, 8, 15, 8, 20],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [15, 10, 8, 14, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 10, 8, 14, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            ElvenAccuracy,
                        ],
                        myBasePointBuyArray: [15, 10, 8, 14, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 10, 8, 14, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [15, 10, 8, 14, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 10, 8, 14, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 10, 8, 14, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 10, 8, 14, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 10, 8, 14, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 9, 8, 15, 8, 20],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            ElvenAccuracy,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 1, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 8, 8, 15, 8, 20],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Aasimar_Fallen,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Aasimar_Fallen,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Aasimar_Fallen,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Aasimar_Fallen,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Aasimar_Fallen,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Aasimar_Fallen,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Aasimar_Fallen,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Aasimar_Fallen,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Changling,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 8, 8, 14, 12, 20],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 13, 12, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 13, 12, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            ElvenAccuracy,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 13, 12, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 13, 12, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 13, 12, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 13, 12, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 13, 12, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 13, 12, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 13, 12, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 8, 9, 14, 11, 20],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [15, 8, 9, 13, 11, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 8, 9, 13, 11, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            ElvenAccuracy,
                        ],
                        myBasePointBuyArray: [15, 8, 9, 13, 11, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 9, 13, 11, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [15, 8, 9, 13, 11, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 9, 13, 11, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 8, 9, 13, 11, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 8, 9, 13, 11, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 8, 9, 13, 11, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 9, 8, 14, 11, 20],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [15, 9, 8, 13, 11, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 9, 8, 13, 11, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            ElvenAccuracy,
                        ],
                        myBasePointBuyArray: [15, 9, 8, 13, 11, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 9, 8, 13, 11, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [15, 9, 8, 13, 11, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 9, 8, 13, 11, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 9, 8, 13, 11, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 9, 8, 13, 11, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 9, 8, 13, 11, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 8, 8, 14, 11, 20],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 14, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 14, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            ElvenAccuracy,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 14, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 14, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 14, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 14, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 14, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 14, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 14, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 0, 1, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 8, 10, 14, 10, 20],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [15, 8, 10, 13, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 8, 10, 13, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            ElvenAccuracy,
                        ],
                        myBasePointBuyArray: [15, 8, 10, 13, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 10, 13, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [15, 8, 10, 13, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 10, 13, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 8, 10, 13, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 8, 10, 13, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 8, 10, 13, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 9, 9, 14, 10, 20],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [15, 9, 9, 13, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 9, 9, 13, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            ElvenAccuracy,
                        ],
                        myBasePointBuyArray: [15, 9, 9, 13, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 9, 9, 13, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [15, 9, 9, 13, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 9, 9, 13, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 9, 9, 13, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 9, 9, 13, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 9, 9, 13, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 8, 9, 14, 10, 20],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 14, 10, 15],
                        myRaceModifiers:     [1, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 14, 10, 15],
                        myRaceModifiers:     [1, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            ElvenAccuracy,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 14, 10, 15],
                        myRaceModifiers:     [1, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 14, 10, 15],
                        myRaceModifiers:     [1, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 14, 10, 15],
                        myRaceModifiers:     [1, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 14, 10, 15],
                        myRaceModifiers:     [1, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 14, 10, 15],
                        myRaceModifiers:     [1, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 14, 10, 15],
                        myRaceModifiers:     [1, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 14, 10, 15],
                        myRaceModifiers:     [1, 0, 1, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 10, 8, 14, 10, 20],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [15, 10, 8, 13, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [15, 10, 8, 13, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            ElvenAccuracy,
                        ],
                        myBasePointBuyArray: [15, 10, 8, 13, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [15, 10, 8, 13, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [15, 10, 8, 13, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [15, 10, 8, 13, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [15, 10, 8, 13, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [15, 10, 8, 13, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [15, 10, 8, 13, 10, 15],
                        myRaceModifiers:     [1, 0, 0, 1, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 2],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                ],
            },
        ];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

