use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test12" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Weighted,
        statOrdering: [
            Dexterity,
            Intelligence,
            Strength,
            Constitution,
            Wisdom,
            Charisma,
        ],
        isReversed: false,
        availableRaces: vec![
            Aarakocra,
            Aasimar_Fallen,
            Aasimar_Protector,
            Aasimar_Scourge,
            Bugbear,
            Changling,
            CustomLineage,
            DragonBorn,
            Dwarf_Grey,
            Dwarf_Hill,
            Dwarf_MarkOfWarding,
            Dwarf_Mountain,
            Elf_Drow,
            Elf_High,
            Elf_MarkOfShadow,
            Elf_Wood,
            Firbolg,
            Genasi_Air,
            Genasi_Earth,
            Genasi_Fire,
            Genasi_Water,
            Gnome_Deep,
            Gnome_Forest,
            Gnome_MarkOfScribing,
            Gnome_Rock,
            Goblin,
            Goliath,
            HalfElf,
            HalfElf_MarkOfDetection,
            HalfElf_MarkOfStorm,
            Halfling_Ghostwise,
            Halfling_Lightfoot,
            Halfling_MarkOfHealing,
            Halfling_MarkOfHospitality,
            Halfling_Stout,
            HalfOrc,
            HalfOrc_MarkOfFinding,
            Hobgoblin,
            Human,
            Human_MarkOfFinding,
            Human_MarkOfHandling,
            Human_MarkOfMaking,
            Human_MarkOfPassage,
            Human_MarkOfSentinel,
            Human_Variant,
            Kalashtar,
            Kenku,
            Kobold,
            Lizardfolk,
            Orc,
            Orc_Eberron,
            Shifter_BeastHide,
            Shifter_LongTooth,
            Shifter_SwiftStride,
            Shifter_WildHunt,
            Tabaxi,
            Tiefling,
            Tiefling_Feral,
            Tortle,
            Triton,
            Warforged,
            YuanTiPureblood,
        ],
        requiredFeats: vec![
            Athlete,
        ],
        constraints: vec![
            UnCmp_StatModifier(
                UnaryCompare {
                    value: GetStatModifier(
                        Dexterity,
                    ),
                    unCmp: IsEven,
                },
            ),
        ],
        numberOfASIs: 1,
        doAllRacesHaveFreeFeat: false,
        canRearrangeRaceModifiers: true,
        maxNumberOfResults: 10,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 8395854,
            numUniqueCombinationsPassedConstraints: 4197466,
            numStatBlocksEjected: 135,
            numUniqueCombinationsEjected: 135,
            numUniqueCombinationsBelowLowerBound: 4197321,
            numStatBlocksShiftedDuringInsertion: 751,
            numDuplicateRaceStatBlocksRemoved: 0,
            numUniqueStatBlockAfterRacePass: 297,
            numUniqueStatBlockAfterFeatPass: 529,
            numFinalStatBlockCombinations: 10,
        };

    let expectedStatBlocks = 
        vec![
            RankedStatBlock {
                rank: 1002102,
                statBlock: [20, 14, 12, 14, 8, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 14, 12, 14, 8, 8],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1002102,
                statBlock: [20, 14, 8, 14, 12, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 14, 8, 14, 12, 8],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1002102,
                statBlock: [20, 14, 8, 14, 8, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 14, 8, 14, 8, 12],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1002102,
                statBlock: [20, 14, 14, 12, 8, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 14, 14, 12, 8, 8],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1002102,
                statBlock: [20, 14, 8, 12, 14, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 14, 8, 12, 14, 8],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1002102,
                statBlock: [20, 14, 8, 12, 8, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 14, 8, 12, 8, 14],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1002102,
                statBlock: [20, 14, 14, 8, 12, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 14, 14, 8, 12, 8],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1002102,
                statBlock: [20, 14, 14, 8, 8, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 14, 14, 8, 8, 12],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1002102,
                statBlock: [20, 14, 12, 8, 14, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 14, 12, 8, 14, 8],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1002102,
                statBlock: [20, 14, 12, 8, 8, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 14, 12, 8, 8, 14],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
        ];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

