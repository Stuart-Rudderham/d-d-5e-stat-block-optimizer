#![allow(non_snake_case)]
#![allow(deprecated)]
#![allow(non_camel_case_types)]

use crate::dnd::stat::*;
use crate::dnd::race::*;
use crate::dnd::feat::*;

use crate::constraint::*;
use crate::constraint::compare_binary::*;
use crate::constraint::compare_unary::*;
use crate::constraint::value::*;

pub mod read_from_file;

use std::num::ParseIntError;
use std::str;

use nom::*;

#[derive(Debug, Copy, Clone)]
enum ErrorCode
{
    Constraint = 1          ,
    BinaryCompare           ,
    UnaryCompare            ,
    StatVal                 ,
    StatMod                 ,
    Constant                ,
    MetaStatValueBinCmp     ,
    MetaStatValueUnCmp      ,
    MetaStatModifierBinCmp  ,
    MetaStatModifierUnCmp   ,
    BinCmp                  ,
    UnCmp                   ,
    CharacterRace           ,
    Feat                    ,
}

fn Parse_BinCmp(input : &[u8]) -> IResult<&[u8], BinCmp>
{
    error!
    (
        input,
        ErrorKind::Custom( ErrorCode::BinCmp as u32 ),
        alt!
        (
            // A bug in nom? This does not give the same behavior when replaced by "alt_complete!".
            complete!( preceded!(tag!("=="), value!(BinCmp::EQ) ) ) |
            complete!( preceded!(tag!("!="), value!(BinCmp::NE) ) ) |
            complete!( preceded!(tag!(">="), value!(BinCmp::GE) ) ) |
            complete!( preceded!(tag!(">" ), value!(BinCmp::GT) ) ) | // ">" must be checked after ">=" because it is a prefix.
            complete!( preceded!(tag!("<="), value!(BinCmp::LE) ) ) |
            complete!( preceded!(tag!("<" ), value!(BinCmp::LT) ) )   // "<" must be checked after "<=" because it is a prefix.
        )
    )
}

fn Parse_UnCmp(input : &[u8]) -> IResult<&[u8], UnCmp>
{
    error!
    (
        input,
        ErrorKind::Custom( ErrorCode::UnCmp as u32 ),
        alt!
        (
            // A bug in nom? This does not give the same behavior when replaced by "alt_complete!".
            complete!( preceded!(tag!("is even"), value!(UnCmp::IsEven) ) ) |
            complete!( preceded!(tag!("is odd" ), value!(UnCmp::IsOdd ) ) ) |
            complete!( preceded!(tag!("is max" ), value!(UnCmp::IsMax ) ) ) |
            complete!( preceded!(tag!("is min" ), value!(UnCmp::IsMin ) ) )
        )
    )
}

pub trait ParseConstraintValue : Sized + GetConstraintValue
{
    fn Parse(input : &[u8]) -> IResult<&[u8], Self>;
}

impl ParseConstraintValue for GetConstant
{
    fn Parse(input : &[u8]) -> IResult<&[u8], Self>
    {
        error!
        (
            input,
            ErrorKind::Custom( ErrorCode::Constant as u32 ),
            map_res!
            (
                pair!
                (
                    opt!( complete!(one_of!("-+")) ),
                    map_res!( digit, str::from_utf8 )
                ),
                |(sign, digits) : (Option<char>, &str)| -> Result<GetConstant, ParseIntError>
                {
                    // This could probably be done without making a copy.
                    let mut s = String::with_capacity(digits.len() + 1);

                    if let Some(c) = sign
                    {
                        s.push(c);
                    }

                    s.push_str( digits );

                    let value = s.parse::<i8>()?;

                    Ok( GetConstant(value) )
                }
            )
        )
    }
}

impl ParseConstraintValue for GetStatValue
{
    fn Parse(input : &[u8]) -> IResult<&[u8], Self>
    {
        error!
        (
            input,
            ErrorKind::Custom( ErrorCode::StatVal as u32 ),
            alt!
            (
                // A bug in nom? This does not give the same behavior when replaced by "alt_complete!".
                complete!( preceded!(tag!("str"), value!(GetStatValue(Stat::Strength    )) ) ) |
                complete!( preceded!(tag!("dex"), value!(GetStatValue(Stat::Dexterity   )) ) ) |
                complete!( preceded!(tag!("con"), value!(GetStatValue(Stat::Constitution)) ) ) |
                complete!( preceded!(tag!("int"), value!(GetStatValue(Stat::Intelligence)) ) ) |
                complete!( preceded!(tag!("wis"), value!(GetStatValue(Stat::Wisdom      )) ) ) |
                complete!( preceded!(tag!("cha"), value!(GetStatValue(Stat::Charisma    )) ) )
            )
        )
    }
}

impl ParseConstraintValue for GetStatModifier
{
    fn Parse(input : &[u8]) -> IResult<&[u8], Self>
    {
        error!
        (
            input,
            ErrorKind::Custom( ErrorCode::StatMod as u32),
            alt!
            (
                // A bug in nom? This does not give the same behavior when replaced by "alt_complete!".
                complete!( preceded!(tag!("strmod"), value!(GetStatModifier(Stat::Strength    )) ) ) |
                complete!( preceded!(tag!("dexmod"), value!(GetStatModifier(Stat::Dexterity   )) ) ) |
                complete!( preceded!(tag!("conmod"), value!(GetStatModifier(Stat::Constitution)) ) ) |
                complete!( preceded!(tag!("intmod"), value!(GetStatModifier(Stat::Intelligence)) ) ) |
                complete!( preceded!(tag!("wismod"), value!(GetStatModifier(Stat::Wisdom      )) ) ) |
                complete!( preceded!(tag!("chamod"), value!(GetStatModifier(Stat::Charisma    )) ) )
            )
        )
    }
}

/*
impl ParseConstraintValue for GetMetaStatValueBinCmp
{
    fn Parse(input : &[u8]) -> IResult<&[u8], Self>
    {
        error!
        (
            input,
            ErrorKind::Custom( ErrorCode::MetaStatValueBinCmp as u32 ),
            call!( |_| { IResult::Error(Err::Code(ErrorKind::Tag)) } )
        )
    }
}

impl ParseConstraintValue for GetMetaStatValueUnCmp
{
    fn Parse(input : &[u8]) -> IResult<&[u8], Self>
    {
        error!
        (
            input,
            ErrorKind::Custom( ErrorCode::MetaStatValueUnCmp as u32 ),
            call!( |_| { IResult::Error(Err::Code(ErrorKind::Tag)) } )
        )
    }
}

impl ParseConstraintValue for GetMetaStatModifierBinCmp
{
    fn Parse(input : &[u8]) -> IResult<&[u8], Self>
    {
        error!
        (
            input,
            ErrorKind::Custom( ErrorCode::MetaStatModifierBinCmp as u32 ),
            call!( |_| { IResult::Error(Err::Code(ErrorKind::Tag)) } )
        )
    }
}

impl ParseConstraintValue for GetMetaStatModifierUnCmp
{
    fn Parse(input : &[u8]) -> IResult<&[u8], Self>
    {
        error!
        (
            input,
            ErrorKind::Custom( ErrorCode::MetaStatModifierUnCmp as u32 ),
            call!( |_| { IResult::Error(Err::Code(ErrorKind::Tag)) } )
        )
    }
}
*/

fn Parse_BinaryCompare<V1: ParseConstraintValue, V2: ParseConstraintValue>( input : &[u8] ) -> IResult<&[u8], BinaryCompare<V1, V2>>
{
    fn Helper<V1: ParseConstraintValue, V2: ParseConstraintValue>( input0 : &[u8] ) -> IResult<&[u8], BinaryCompare<V1, V2>>
    {
        let (input1, _     ) = try_parse!(input0, take_while!( is_space ) );
        let (input2, value1) = try_parse!(input1, V1::Parse               );
        let (input3, _     ) = try_parse!(input2, take_while!( is_space ) );
        let (input4, binCmp) = try_parse!(input3, Parse_BinCmp            );
        let (input5, _     ) = try_parse!(input4, take_while!( is_space ) );
        let (input6, value2) = try_parse!(input5, V2::Parse               );
        let (input7, _     ) = try_parse!(input6, take_while!( is_space ) );
        let (input8, _     ) = try_parse!(input7, eof                     );

        IResult::Done(input8, BinaryCompare::New(value1, binCmp, value2) )
    }

    error!
    (
        input,
        ErrorKind::Custom( ErrorCode::BinaryCompare as u32 ),
        Helper
    )
}

fn Parse_UnaryCompare<V: ParseConstraintValue>( input : &[u8] ) -> IResult<&[u8], UnaryCompare<V>>
{
    fn Helper<V: ParseConstraintValue>( input0 : &[u8] ) -> IResult<&[u8], UnaryCompare<V>>
    {
        let (input1, _    ) = try_parse!(input0, take_while!( is_space )  );
        let (input2, value) = try_parse!(input1, V::Parse                 );
        let (input3, _    ) = try_parse!(input2, take_while1!( is_space ) ); // NOTE: Must have at least one space so that "stris even" doesn't parse.
        let (input4, unCmp) = try_parse!(input3, Parse_UnCmp              );
        let (input5, _    ) = try_parse!(input4, take_while!( is_space )  );
        let (input6, _    ) = try_parse!(input5, eof                      );

        IResult::Done(input6, UnaryCompare::New(value, unCmp) )
    };

    error!
    (
        input,
        ErrorKind::Custom( ErrorCode::UnaryCompare as u32 ),
        Helper
    )
}

fn Parse_Constraint( input : &[u8] ) -> IResult<&[u8], Constraint>
{
    // If we use the alt! macro it throws away all the errors we so carefully generated.
    // Instead we test each possible constraint manually, and if they all fail then merge
    // the sperate errors lists together into one big error list.
    //
    // An example of using the alt! macro is below.
    //
    //     return error!
    //     (
    //         input,
    //         ErrorKind::Custom( ErrorCode::Constraint as u32 ),
    //         alt!
    //         (
    //             input,
    //             map!( Parse_BinaryCompare::<GetStatValue   , GetStatValue>, |x| Constraint::BinCmp_StatValue_StatValue   (x) ) |
    //             map!( Parse_BinaryCompare::<GetStatModifier, GetStatValue>, |x| Constraint::BinCmp_StatModifier_StatValue(x) ) |
    //             .
    //             .
    //             .
    //         )
    //     )

    // This is a macro that is kind of the opposite of "try!".
    macro_rules! ReturnOnSuccessfulParse
    {
        ( $x:expr ) =>
        {{
            // Execute the parsing expression.
            let ret : IResult<&[u8], Constraint> = $x;

            match ret
            {
                IResult::Done(..) =>
                {
                    // If it succeeded then no need to continue, we can just
                    // early exit.
                    return ret;
                },

                IResult::Error(e) =>
                {
                    // If there was an error then keep it since we might need
                    // it for printing later.
                    e
                },

                IResult::Incomplete(_) =>
                {
                    // Something has gone wrong with our parsers, they should
                    // always return an Error since we will never have partial
                    // input data.
                    panic!("Should never get an IResult::Incomplete from parsing! Input was \"{:?}\"", input);
                },
            }
        }};
    }

    let e01 =  ReturnOnSuccessfulParse!( map!( input, Parse_BinaryCompare::< GetStatValue                  , GetConstant               >, |x| Constraint::BinCmp_StatValue_Constant                             (x) ) );
    let e02 =  ReturnOnSuccessfulParse!( map!( input, Parse_BinaryCompare::< GetStatModifier               , GetConstant               >, |x| Constraint::BinCmp_StatModifier_Constant                          (x) ) );
    let e03 =  ReturnOnSuccessfulParse!( map!( input, Parse_BinaryCompare::< GetStatValue                  , GetStatValue              >, |x| Constraint::BinCmp_StatValue_StatValue                            (x) ) );
    let e04 =  ReturnOnSuccessfulParse!( map!( input, Parse_BinaryCompare::< GetStatModifier               , GetStatModifier           >, |x| Constraint::BinCmp_StatModifier_StatModifier                      (x) ) );
  //let e05 =  ReturnOnSuccessfulParse!( map!( input, Parse_BinaryCompare::< GetMetaStatValueBinCmp        , GetConstant               >, |x| Constraint::BinCmp_MetaStatValueBinCmp_Constant                   (x) ) );
  //let e06 =  ReturnOnSuccessfulParse!( map!( input, Parse_BinaryCompare::< GetMetaStatValueUnCmp         , GetConstant               >, |x| Constraint::BinCmp_MetaStatValueUnCmp_Constant                    (x) ) );
  //let e07 =  ReturnOnSuccessfulParse!( map!( input, Parse_BinaryCompare::< GetMetaStatModifierBinCmp     , GetConstant               >, |x| Constraint::BinCmp_MetaStatModifierBinCmp_Constant                (x) ) );
  //let e08 =  ReturnOnSuccessfulParse!( map!( input, Parse_BinaryCompare::< GetMetaStatModifierUnCmp      , GetConstant               >, |x| Constraint::BinCmp_MetaStatModifierUnCmp_Constant                 (x) ) );
  //let e09 =  ReturnOnSuccessfulParse!( map!( input, Parse_BinaryCompare::< GetMetaStatValueBinCmp        , GetMetaStatValueBinCmp    >, |x| Constraint::BinCmp_MetaStatValueBinCmp_MetaStatValueBinCmp        (x) ) );
  //let e10 =  ReturnOnSuccessfulParse!( map!( input, Parse_BinaryCompare::< GetMetaStatValueUnCmp         , GetMetaStatValueBinCmp    >, |x| Constraint::BinCmp_MetaStatValueUnCmp_MetaStatValueBinCmp         (x) ) );
  //let e11 =  ReturnOnSuccessfulParse!( map!( input, Parse_BinaryCompare::< GetMetaStatModifierBinCmp     , GetMetaStatValueBinCmp    >, |x| Constraint::BinCmp_MetaStatModifierBinCmp_MetaStatValueBinCmp     (x) ) );
  //let e12 =  ReturnOnSuccessfulParse!( map!( input, Parse_BinaryCompare::< GetMetaStatModifierUnCmp      , GetMetaStatValueBinCmp    >, |x| Constraint::BinCmp_MetaStatModifierUnCmp_MetaStatValueBinCmp      (x) ) );
  //let e13 =  ReturnOnSuccessfulParse!( map!( input, Parse_BinaryCompare::< GetMetaStatValueBinCmp        , GetMetaStatValueUnCmp     >, |x| Constraint::BinCmp_MetaStatValueBinCmp_MetaStatValueUnCmp         (x) ) );
  //let e14 =  ReturnOnSuccessfulParse!( map!( input, Parse_BinaryCompare::< GetMetaStatValueUnCmp         , GetMetaStatValueUnCmp     >, |x| Constraint::BinCmp_MetaStatValueUnCmp_MetaStatValueUnCmp          (x) ) );
  //let e15 =  ReturnOnSuccessfulParse!( map!( input, Parse_BinaryCompare::< GetMetaStatModifierBinCmp     , GetMetaStatValueUnCmp     >, |x| Constraint::BinCmp_MetaStatModifierBinCmp_MetaStatValueUnCmp      (x) ) );
  //let e16 =  ReturnOnSuccessfulParse!( map!( input, Parse_BinaryCompare::< GetMetaStatModifierUnCmp      , GetMetaStatValueUnCmp     >, |x| Constraint::BinCmp_MetaStatModifierUnCmp_MetaStatValueUnCmp       (x) ) );
  //let e17 =  ReturnOnSuccessfulParse!( map!( input, Parse_BinaryCompare::< GetMetaStatValueBinCmp        , GetMetaStatModifierBinCmp >, |x| Constraint::BinCmp_MetaStatValueBinCmp_MetaStatModifierBinCmp     (x) ) );
  //let e18 =  ReturnOnSuccessfulParse!( map!( input, Parse_BinaryCompare::< GetMetaStatValueUnCmp         , GetMetaStatModifierBinCmp >, |x| Constraint::BinCmp_MetaStatValueUnCmp_MetaStatModifierBinCmp      (x) ) );
  //let e19 =  ReturnOnSuccessfulParse!( map!( input, Parse_BinaryCompare::< GetMetaStatModifierBinCmp     , GetMetaStatModifierBinCmp >, |x| Constraint::BinCmp_MetaStatModifierBinCmp_MetaStatModifierBinCmp  (x) ) );
  //let e20 =  ReturnOnSuccessfulParse!( map!( input, Parse_BinaryCompare::< GetMetaStatModifierUnCmp      , GetMetaStatModifierBinCmp >, |x| Constraint::BinCmp_MetaStatModifierUnCmp_MetaStatModifierBinCmp   (x) ) );
  //let e21 =  ReturnOnSuccessfulParse!( map!( input, Parse_BinaryCompare::< GetMetaStatValueBinCmp        , GetMetaStatModifierUnCmp  >, |x| Constraint::BinCmp_MetaStatValueBinCmp_MetaStatModifierUnCmp      (x) ) );
  //let e22 =  ReturnOnSuccessfulParse!( map!( input, Parse_BinaryCompare::< GetMetaStatValueUnCmp         , GetMetaStatModifierUnCmp  >, |x| Constraint::BinCmp_MetaStatValueUnCmp_MetaStatModifierUnCmp       (x) ) );
  //let e23 =  ReturnOnSuccessfulParse!( map!( input, Parse_BinaryCompare::< GetMetaStatModifierBinCmp     , GetMetaStatModifierUnCmp  >, |x| Constraint::BinCmp_MetaStatModifierBinCmp_MetaStatModifierUnCmp   (x) ) );
  //let e24 =  ReturnOnSuccessfulParse!( map!( input, Parse_BinaryCompare::< GetMetaStatModifierUnCmp      , GetMetaStatModifierUnCmp  >, |x| Constraint::BinCmp_MetaStatModifierUnCmp_MetaStatModifierUnCmp    (x) ) );
    let e25 =  ReturnOnSuccessfulParse!( map!( input, Parse_UnaryCompare ::< GetStatValue                                              >, |x| Constraint::UnCmp_StatValue                                       (x) ) );
    let e26 =  ReturnOnSuccessfulParse!( map!( input, Parse_UnaryCompare ::< GetStatModifier                                           >, |x| Constraint::UnCmp_StatModifier                                    (x) ) );
  //let e27 =  ReturnOnSuccessfulParse!( map!( input, Parse_UnaryCompare ::< GetMetaStatValueBinCmp                                    >, |x| Constraint::UnCmp_MetaStatValueBinCmp                             (x) ) );
  //let e28 =  ReturnOnSuccessfulParse!( map!( input, Parse_UnaryCompare ::< GetMetaStatValueUnCmp                                     >, |x| Constraint::UnCmp_MetaStatValueUnCmp                              (x) ) );
  //let e29 =  ReturnOnSuccessfulParse!( map!( input, Parse_UnaryCompare ::< GetMetaStatModifierBinCmp                                 >, |x| Constraint::UnCmp_MetaStatModifierBinCmp                          (x) ) );
  //let e30 =  ReturnOnSuccessfulParse!( map!( input, Parse_UnaryCompare ::< GetMetaStatModifierUnCmp                                  >, |x| Constraint::UnCmp_MetaStatModifierUnCmp                           (x) ) );

    let baseError = Err::Position( ErrorKind::Custom(ErrorCode::Constraint as u32), input);

    fn MergeErrors<P:Clone,E:Clone>( first : &Err<P,E>, rest : &[&Err<P,E>] ) -> Err<P,E>
    {
        if rest.is_empty()
        {
            return first.clone();
        }

        let (firstRest, restRest) = rest.split_first().unwrap();

        match *first
        {
            // Replace terminal with link to start of next error.
            Err::Code        (ref i                 ) => Err::Node        (i.clone(),            Box::new(MergeErrors(firstRest, restRest)) ),
            Err::Position    (ref i, ref p          ) => Err::NodePosition(i.clone(), p.clone(), Box::new(MergeErrors(firstRest, restRest)) ),

            // Copy link into merged list.
            Err::Node        (ref i,        ref next) => Err::Node        (i.clone(),            Box::new(MergeErrors(next     , rest    )) ),
            Err::NodePosition(ref i, ref p, ref next) => Err::NodePosition(i.clone(), p.clone(), Box::new(MergeErrors(next     , rest    )) ),
        }
    }

    IResult::Error
    (
        MergeErrors
        (
            &baseError,
            &[
                &e01, &e02, &e03, &e04,// &e05, &e06,
              //&e07, &e08, &e09, &e10, &e11, &e12,
              //&e13, &e14, &e15, &e16, &e17, &e18,
              //&e19, &e20, &e21, &e22, &e23, &e24,
                &e25, &e26,// &e27, &e28, &e29, &e30,
            ]
        )
    )
}

fn Parse_CharacterRace(input : &[u8]) -> IResult<&[u8], CharacterRace>
{
    error!
    (
        input,
        ErrorKind::Custom( ErrorCode::CharacterRace as u32),

        // NOTE: We explictly check for EOF, unlike most other parsers.
        // This means that we can't compose this parser with other ones.
        terminated!
        (
            alt!
            (
                // A bug in nom? This does not give the same behavior when replaced by "alt_complete!".
                complete!( preceded!(tag!("Aarakocra"                 ), value!(CharacterRace::Aarakocra                 )) ) |
                complete!( preceded!(tag!("Aasimar_Fallen"            ), value!(CharacterRace::Aasimar_Fallen            )) ) |
                complete!( preceded!(tag!("Aasimar_Protector"         ), value!(CharacterRace::Aasimar_Protector         )) ) |
                complete!( preceded!(tag!("Aasimar_Scourge"           ), value!(CharacterRace::Aasimar_Scourge           )) ) |
                complete!( preceded!(tag!("Bugbear"                   ), value!(CharacterRace::Bugbear                   )) ) |
                complete!( preceded!(tag!("Changling"                 ), value!(CharacterRace::Changling                 )) ) |
                complete!( preceded!(tag!("CustomLineage"             ), value!(CharacterRace::CustomLineage             )) ) |
                complete!( preceded!(tag!("DragonBorn"                ), value!(CharacterRace::DragonBorn                )) ) |
                complete!( preceded!(tag!("Dwarf_Grey"                ), value!(CharacterRace::Dwarf_Grey                )) ) |
                complete!( preceded!(tag!("Dwarf_Hill"                ), value!(CharacterRace::Dwarf_Hill                )) ) |
                complete!( preceded!(tag!("Dwarf_MarkOfWarding"       ), value!(CharacterRace::Dwarf_MarkOfWarding       )) ) |
                complete!( preceded!(tag!("Dwarf_Mountain"            ), value!(CharacterRace::Dwarf_Mountain            )) ) |
                complete!( preceded!(tag!("Elf_Drow"                  ), value!(CharacterRace::Elf_Drow                  )) ) |
                complete!( preceded!(tag!("Elf_High"                  ), value!(CharacterRace::Elf_High                  )) ) |
                complete!( preceded!(tag!("Elf_MarkOfShadow"          ), value!(CharacterRace::Elf_MarkOfShadow          )) ) |
                complete!( preceded!(tag!("Elf_Wood"                  ), value!(CharacterRace::Elf_Wood                  )) ) |
                complete!( preceded!(tag!("Firbolg"                   ), value!(CharacterRace::Firbolg                   )) ) |
                complete!( preceded!(tag!("Genasi_Air"                ), value!(CharacterRace::Genasi_Air                )) ) |
                complete!( preceded!(tag!("Genasi_Earth"              ), value!(CharacterRace::Genasi_Earth              )) ) |
                complete!( preceded!(tag!("Genasi_Fire"               ), value!(CharacterRace::Genasi_Fire               )) ) |
                complete!( preceded!(tag!("Genasi_Water"              ), value!(CharacterRace::Genasi_Water              )) ) |
                complete!( preceded!(tag!("Gnome_Deep"                ), value!(CharacterRace::Gnome_Deep                )) ) |
                complete!( preceded!(tag!("Gnome_Forest"              ), value!(CharacterRace::Gnome_Forest              )) ) |
                complete!( preceded!(tag!("Gnome_MarkOfScribing"      ), value!(CharacterRace::Gnome_MarkOfScribing      )) ) |
                complete!( preceded!(tag!("Gnome_Rock"                ), value!(CharacterRace::Gnome_Rock                )) ) |
                complete!( preceded!(tag!("Goblin"                    ), value!(CharacterRace::Goblin                    )) ) |
                complete!( preceded!(tag!("Goliath"                   ), value!(CharacterRace::Goliath                   )) ) |
                complete!( preceded!(tag!("HalfElf_MarkOfDetection"   ), value!(CharacterRace::HalfElf_MarkOfDetection   )) ) |
                complete!( preceded!(tag!("HalfElf_MarkOfStorm"       ), value!(CharacterRace::HalfElf_MarkOfStorm       )) ) |
                complete!( preceded!(tag!("HalfElf"                   ), value!(CharacterRace::HalfElf                   )) ) | // "HalfElf" must be checked after because it is a prefix of the previous entries.
                complete!( preceded!(tag!("Halfling_Ghostwise"        ), value!(CharacterRace::Halfling_Ghostwise        )) ) |
                complete!( preceded!(tag!("Halfling_Lightfoot"        ), value!(CharacterRace::Halfling_Lightfoot        )) ) |
                complete!( preceded!(tag!("Halfling_MarkOfHealing"    ), value!(CharacterRace::Halfling_MarkOfHealing    )) ) |
                complete!( preceded!(tag!("Halfling_MarkOfHospitality"), value!(CharacterRace::Halfling_MarkOfHospitality)) ) |
                complete!( preceded!(tag!("Halfling_Stout"            ), value!(CharacterRace::Halfling_Stout            )) ) |
                complete!( preceded!(tag!("HalfOrc_MarkOfFinding"     ), value!(CharacterRace::HalfOrc_MarkOfFinding     )) ) |
                complete!( preceded!(tag!("HalfOrc"                   ), value!(CharacterRace::HalfOrc                   )) ) | // "HalfOrc" must be checked after because it is a prefix of the previous entries.
                complete!( preceded!(tag!("Hobgoblin"                 ), value!(CharacterRace::Hobgoblin                 )) ) |
                complete!( preceded!(tag!("Human_MarkOfFinding"       ), value!(CharacterRace::Human_MarkOfFinding       )) ) |
                complete!( preceded!(tag!("Human_MarkOfHandling"      ), value!(CharacterRace::Human_MarkOfHandling      )) ) |
                complete!( preceded!(tag!("Human_MarkOfMaking"        ), value!(CharacterRace::Human_MarkOfMaking        )) ) |
                complete!( preceded!(tag!("Human_MarkOfPassage"       ), value!(CharacterRace::Human_MarkOfPassage       )) ) |
                complete!( preceded!(tag!("Human_MarkOfSentinel"      ), value!(CharacterRace::Human_MarkOfSentinel      )) ) |
                complete!( preceded!(tag!("Human_Variant"             ), value!(CharacterRace::Human_Variant             )) ) |
                complete!( preceded!(tag!("Human"                     ), value!(CharacterRace::Human                     )) ) | // "Human" must be checked after because it is a prefix of the previous entries.
                complete!( preceded!(tag!("Kalashtar"                 ), value!(CharacterRace::Kalashtar                 )) ) |
                complete!( preceded!(tag!("Kenku"                     ), value!(CharacterRace::Kenku                     )) ) |
                complete!( preceded!(tag!("Kobold"                    ), value!(CharacterRace::Kobold                    )) ) |
                complete!( preceded!(tag!("Lizardfolk"                ), value!(CharacterRace::Lizardfolk                )) ) |
                complete!( preceded!(tag!("Orc_Eberron"               ), value!(CharacterRace::Orc_Eberron               )) ) |
                complete!( preceded!(tag!("Orc"                       ), value!(CharacterRace::Orc                       )) ) | // "Orc" must be checked after because it is a prefix of the previous entries.
                complete!( preceded!(tag!("Shifter_BeastHide"         ), value!(CharacterRace::Shifter_BeastHide         )) ) |
                complete!( preceded!(tag!("Shifter_LongTooth"         ), value!(CharacterRace::Shifter_LongTooth         )) ) |
                complete!( preceded!(tag!("Shifter_SwiftStride"       ), value!(CharacterRace::Shifter_SwiftStride       )) ) |
                complete!( preceded!(tag!("Shifter_WildHunt"          ), value!(CharacterRace::Shifter_WildHunt          )) ) |
                complete!( preceded!(tag!("Tabaxi"                    ), value!(CharacterRace::Tabaxi                    )) ) |
                complete!( preceded!(tag!("Tiefling_Feral"            ), value!(CharacterRace::Tiefling_Feral            )) ) |
                complete!( preceded!(tag!("Tiefling"                  ), value!(CharacterRace::Tiefling                  )) ) | // "Tiefling" must be checked after because it is a prefix of the previous entries.
                complete!( preceded!(tag!("Tortle"                    ), value!(CharacterRace::Tortle                    )) ) |
                complete!( preceded!(tag!("Triton"                    ), value!(CharacterRace::Triton                    )) ) |
                complete!( preceded!(tag!("Warforged"                 ), value!(CharacterRace::Warforged                 )) ) |
                complete!( preceded!(tag!("YuanTiPureblood"           ), value!(CharacterRace::YuanTiPureblood           )) )
            ),
            eof
        )
    )
}

fn Parse_Feat(input : &[u8]) -> IResult<&[u8], Feat>
{
    error!
    (
        input,
        ErrorKind::Custom( ErrorCode::Feat as u32),

        // NOTE: We explictly check for EOF, unlike most other parsers.
        // This means that we can't compose this parser with other ones.
        terminated!
        (
            alt!
            (
                // A bug in nom? This does not give the same behavior when replaced by "alt_complete!".
                complete!( preceded!(tag!("AberrantDragonMark"  ), value!(Feat::AberrantDragonMark  )) ) |
                complete!( preceded!(tag!("Actor"               ), value!(Feat::Actor               )) ) |
                complete!( preceded!(tag!("Alert"               ), value!(Feat::Alert               )) ) |
                complete!( preceded!(tag!("ArtificerInitiate"   ), value!(Feat::ArtificerInitiate   )) ) |
                complete!( preceded!(tag!("Athlete"             ), value!(Feat::Athlete             )) ) |
                complete!( preceded!(tag!("BountifulLuck"       ), value!(Feat::BountifulLuck       )) ) |
                complete!( preceded!(tag!("Charger"             ), value!(Feat::Charger             )) ) |
                complete!( preceded!(tag!("Chef"                ), value!(Feat::Chef                )) ) |
                complete!( preceded!(tag!("CrossbowExpert"      ), value!(Feat::CrossbowExpert      )) ) |
                complete!( preceded!(tag!("Crusher"             ), value!(Feat::Crusher             )) ) |
                complete!( preceded!(tag!("DefensiveDuelist"    ), value!(Feat::DefensiveDuelist    )) ) |
                complete!( preceded!(tag!("DragonFear"          ), value!(Feat::DragonFear          )) ) |
                complete!( preceded!(tag!("DragonHide"          ), value!(Feat::DragonHide          )) ) |
                complete!( preceded!(tag!("DrowHighMagic"       ), value!(Feat::DrowHighMagic       )) ) |
                complete!( preceded!(tag!("DualWielder"         ), value!(Feat::DualWielder         )) ) |
                complete!( preceded!(tag!("DungeonDelver"       ), value!(Feat::DungeonDelver       )) ) |
                complete!( preceded!(tag!("Durable"             ), value!(Feat::Durable             )) ) |
                complete!( preceded!(tag!("DwarvenFortitude"    ), value!(Feat::DwarvenFortitude    )) ) |
                complete!( preceded!(tag!("EldritchAdept"       ), value!(Feat::EldritchAdept       )) ) |
                complete!( preceded!(tag!("ElementalAdept"      ), value!(Feat::ElementalAdept      )) ) |
                complete!( preceded!(tag!("ElvenAccuracy"       ), value!(Feat::ElvenAccuracy       )) ) |
                complete!( preceded!(tag!("FadeAway"            ), value!(Feat::FadeAway            )) ) |
                complete!( preceded!(tag!("FeyTeleportation"    ), value!(Feat::FeyTeleportation    )) ) |
                complete!( preceded!(tag!("FeyTouched"          ), value!(Feat::FeyTouched          )) ) |
                complete!( preceded!(tag!("FightingInitiate"    ), value!(Feat::FightingInitiate    )) ) |
                complete!( preceded!(tag!("FlamesOfPhlegethos"  ), value!(Feat::FlamesOfPhlegethos  )) ) |
                complete!( preceded!(tag!("Grappler"            ), value!(Feat::Grappler            )) ) |
                complete!( preceded!(tag!("GreatWeaponMaster"   ), value!(Feat::GreatWeaponMaster   )) ) |
                complete!( preceded!(tag!("Gunner"              ), value!(Feat::Gunner              )) ) |
                complete!( preceded!(tag!("Healer"              ), value!(Feat::Healer              )) ) |
                complete!( preceded!(tag!("HeavilyArmored"      ), value!(Feat::HeavilyArmored      )) ) |
                complete!( preceded!(tag!("HeavyArmorMaster"    ), value!(Feat::HeavyArmorMaster    )) ) |
                complete!( preceded!(tag!("InfernalConstitution"), value!(Feat::InfernalConstitution)) ) |
                complete!( preceded!(tag!("InspiringLeader"     ), value!(Feat::InspiringLeader     )) ) |
                complete!( preceded!(tag!("KeenMind"            ), value!(Feat::KeenMind            )) ) |
                complete!( preceded!(tag!("LightlyArmored"      ), value!(Feat::LightlyArmored      )) ) |
                complete!( preceded!(tag!("Linguist"            ), value!(Feat::Linguist            )) ) |
                complete!( preceded!(tag!("Lucky"               ), value!(Feat::Lucky               )) ) |
                complete!( preceded!(tag!("MageSlayer"          ), value!(Feat::MageSlayer          )) ) |
                complete!( preceded!(tag!("MagicInitiate"       ), value!(Feat::MagicInitiate       )) ) |
                complete!( preceded!(tag!("MartialAdept"        ), value!(Feat::MartialAdept        )) ) |
                complete!( preceded!(tag!("MediumArmorMaster"   ), value!(Feat::MediumArmorMaster   )) ) |
                complete!( preceded!(tag!("MetamagicAdept"      ), value!(Feat::MetamagicAdept      )) ) |
                complete!( preceded!(tag!("Mobile"              ), value!(Feat::Mobile              )) ) |
                complete!( preceded!(tag!("ModeratelyArmored"   ), value!(Feat::ModeratelyArmored   )) ) |
                complete!( preceded!(tag!("MountedCombatant"    ), value!(Feat::MountedCombatant    )) ) |
                complete!( preceded!(tag!("Observant"           ), value!(Feat::Observant           )) ) |
                complete!( preceded!(tag!("OrcishFury"          ), value!(Feat::OrcishFury          )) ) |
                complete!( preceded!(tag!("Piercer"             ), value!(Feat::Piercer             )) ) |
                complete!( preceded!(tag!("Poisoner"            ), value!(Feat::Poisoner            )) ) |
                complete!( preceded!(tag!("PolearmMaster"       ), value!(Feat::PolearmMaster       )) ) |
                complete!( preceded!(tag!("Prodigy"             ), value!(Feat::Prodigy             )) ) |
                complete!( preceded!(tag!("Resilient_Cha"       ), value!(Feat::Resilient_Cha       )) ) |
                complete!( preceded!(tag!("Resilient_Con"       ), value!(Feat::Resilient_Con       )) ) |
                complete!( preceded!(tag!("Resilient_Dex"       ), value!(Feat::Resilient_Dex       )) ) |
                complete!( preceded!(tag!("Resilient_Int"       ), value!(Feat::Resilient_Int       )) ) |
                complete!( preceded!(tag!("Resilient_Str"       ), value!(Feat::Resilient_Str       )) ) |
                complete!( preceded!(tag!("Resilient_Wis"       ), value!(Feat::Resilient_Wis       )) ) |
                complete!( preceded!(tag!("RevenantBlade"       ), value!(Feat::RevenantBlade       )) ) |
                complete!( preceded!(tag!("RitualCaster"        ), value!(Feat::RitualCaster        )) ) |
                complete!( preceded!(tag!("SavageAttacker"      ), value!(Feat::SavageAttacker      )) ) |
                complete!( preceded!(tag!("SecondChance"        ), value!(Feat::SecondChance        )) ) |
                complete!( preceded!(tag!("Sentinel"            ), value!(Feat::Sentinel            )) ) |
                complete!( preceded!(tag!("ShadowTouched"       ), value!(Feat::ShadowTouched       )) ) |
                complete!( preceded!(tag!("Sharpshooter"        ), value!(Feat::Sharpshooter        )) ) |
                complete!( preceded!(tag!("ShieldMaster"        ), value!(Feat::ShieldMaster        )) ) |
                complete!( preceded!(tag!("Skilled"             ), value!(Feat::Skilled             )) ) |
                complete!( preceded!(tag!("SkillExpert"         ), value!(Feat::SkillExpert         )) ) |
                complete!( preceded!(tag!("Skulker"             ), value!(Feat::Skulker             )) ) |
                complete!( preceded!(tag!("Slasher"             ), value!(Feat::Slasher             )) ) |
                complete!( preceded!(tag!("SpellSniper"         ), value!(Feat::SpellSniper         )) ) |
                complete!( preceded!(tag!("SquatNimbleness"     ), value!(Feat::SquatNimbleness     )) ) |
                complete!( preceded!(tag!("TavernBrawler"       ), value!(Feat::TavernBrawler       )) ) |
                complete!( preceded!(tag!("Telekinetic"         ), value!(Feat::Telekinetic         )) ) |
                complete!( preceded!(tag!("Telepathic"          ), value!(Feat::Telepathic          )) ) |
                complete!( preceded!(tag!("Tough"               ), value!(Feat::Tough               )) ) |
                complete!( preceded!(tag!("WarCaster"           ), value!(Feat::WarCaster           )) ) |
                complete!( preceded!(tag!("WeaponMaster"        ), value!(Feat::WeaponMaster        )) ) |
                complete!( preceded!(tag!("WoodElfMagic"        ), value!(Feat::WoodElfMagic        )) )
            ),
            eof
        )
    )
}

pub fn FormatErrorMessage<I>(input : &[u8], res : &IResult<&[u8], I> ) -> String
{
    let mut ret = String::new();

    {
        let mut FormatSpecificErrorMessage = |errorKind, p|
        {
            // The below error message strings have hacky extra space added to get
            // some better error formatting.
            //
            // We know that BinaryCompare and UnaryCompare are always the start of
            // a distinct error chain. So add a newline at the beginning to make it
            // clear that the error chains are seperate.
            //
            // The other error messages are indented manually because we know that
            // they always appear in the middle of an error chain.
            //
            // Hopefully these assumption doesn't become false in the future!

            let errorMessageOption = match errorKind
            {
                ErrorKind::Custom(x) =>
                {
                    // Have to do this weird "if guard" matching because we can't
                    // cast the enum to u32 in the match arms normally.
                    match x
                    {
                        x if x == ErrorCode::Constraint                 as u32 => Some( "\nFailed to parse Constraint                     |" ),
                        x if x == ErrorCode::BinaryCompare              as u32 => Some( "\n    Failed to parse BinaryCompare              |" ),
                        x if x == ErrorCode::UnaryCompare               as u32 => Some( "\n    Failed to parse UnaryCompare               |" ),
                        x if x == ErrorCode::StatVal                    as u32 => Some(   "        Failed to parse StatValue              |" ),
                        x if x == ErrorCode::StatMod                    as u32 => Some(   "        Failed to parse StatModifier           |" ),
                        x if x == ErrorCode::Constant                   as u32 => Some(   "        Failed to parse Constant               |" ),
                        x if x == ErrorCode::MetaStatValueBinCmp        as u32 => Some(   "        Failed to parse MetaStatValueBinCmp    |" ),
                        x if x == ErrorCode::MetaStatValueUnCmp         as u32 => Some(   "        Failed to parse MetaStatValueUnCmp     |" ),
                        x if x == ErrorCode::MetaStatModifierBinCmp     as u32 => Some(   "        Failed to parse MetaStatModifierBinCmp |" ),
                        x if x == ErrorCode::MetaStatModifierUnCmp      as u32 => Some(   "        Failed to parse MetaStatModifierUnCmp  |" ),
                        x if x == ErrorCode::BinCmp                     as u32 => Some(   "        Failed to parse BinCmp                 |" ),
                        x if x == ErrorCode::UnCmp                      as u32 => Some(   "        Failed to parse UnCmp                  |" ),

                        x if x == ErrorCode::CharacterRace              as u32 => Some( "\nFailed to parse CharacterRace                  |" ),
                        x if x == ErrorCode::Feat                       as u32 => Some( "\nFailed to parse Feat                           |" ),

                        // We want to make sure no custom error codes are ignored.
                        _                                                      => panic!("Unknown custom error code {:?}", x),
                    }
                },

                ErrorKind::Eof                                                 => Some(   "        Leftover input data                    |" ),

                // We only care about specific errors.
                _ => None,
            };

            // Print the span of input that goes with the error.
            if let Some(errorMessage) = errorMessageOption
            {
                let (o1, o2) = slice_to_offsets(input, p);

                ret.push_str( format!("{:35} \"{}\"\n", errorMessage, String::from_utf8_lossy(&input[o1 .. o2])).as_str() );
            }
        };

        // Print out any parsing errors that have a span of input data.
        if let IResult::Error(ref e) = *res
        {
            let mut err = e.clone();

            loop
            {
                match err
                {
                    // Reached the end of the error linked-list.
                    Err::Code        (_                    ) => {                                              break      ; },
                    Err::Position    (errorKind, span      ) => { FormatSpecificErrorMessage(errorKind, span); break      ; },

                    // Continue along the error linked-list.
                    Err::Node        (_        ,       next) => {                                              err = *next; },
                    Err::NodePosition(errorKind, span, next) => { FormatSpecificErrorMessage(errorKind, span); err = *next; },
                }
            }
        }
    }


    ret
}

#[cfg(test)]
mod tests
{
    use super::*;
    use super::Parse_BinCmp;
    use super::Parse_UnCmp;
    use super::Parse_Constraint;
    use super::Parse_CharacterRace;
    use super::Parse_Feat;

    use std::i8;

    #[test]
    fn ParsingBinCmp()
    {
        // Testing for success.
        assert_eq!( Parse_BinCmp( &b"==" [..] ), IResult::Done( &b"" [..], BinCmp::EQ ) );
        assert_eq!( Parse_BinCmp( &b"==a"[..] ), IResult::Done( &b"a"[..], BinCmp::EQ ) );
        assert_eq!( Parse_BinCmp( &b"==="[..] ), IResult::Done( &b"="[..], BinCmp::EQ ) );

        assert_eq!( Parse_BinCmp( &b"!=" [..] ), IResult::Done( &b"" [..], BinCmp::NE ) );
        assert_eq!( Parse_BinCmp( &b"!=a"[..] ), IResult::Done( &b"a"[..], BinCmp::NE ) );
        assert_eq!( Parse_BinCmp( &b"!=="[..] ), IResult::Done( &b"="[..], BinCmp::NE ) );

        assert_eq!( Parse_BinCmp( &b">"  [..] ), IResult::Done( &b"" [..], BinCmp::GT ) );
        assert_eq!( Parse_BinCmp( &b">a" [..] ), IResult::Done( &b"a"[..], BinCmp::GT ) );

        assert_eq!( Parse_BinCmp( &b">=" [..] ), IResult::Done( &b"" [..], BinCmp::GE ) );
        assert_eq!( Parse_BinCmp( &b">=a"[..] ), IResult::Done( &b"a"[..], BinCmp::GE ) );
        assert_eq!( Parse_BinCmp( &b">=="[..] ), IResult::Done( &b"="[..], BinCmp::GE ) );

        assert_eq!( Parse_BinCmp( &b"<"  [..] ), IResult::Done( &b"" [..], BinCmp::LT ) );
        assert_eq!( Parse_BinCmp( &b"<a" [..] ), IResult::Done( &b"a"[..], BinCmp::LT ) );

        assert_eq!( Parse_BinCmp( &b"<=" [..] ), IResult::Done( &b"" [..], BinCmp::LE ) );
        assert_eq!( Parse_BinCmp( &b"<=a"[..] ), IResult::Done( &b"a"[..], BinCmp::LE ) );
        assert_eq!( Parse_BinCmp( &b"<=="[..] ), IResult::Done( &b"="[..], BinCmp::LE ) );

        // Testing for failure.
        assert!( Parse_BinCmp( &b""   [..] ).is_err() );
        assert!( Parse_BinCmp( &b"a"  [..] ).is_err() );
        assert!( Parse_BinCmp( &b"="  [..] ).is_err() );
        assert!( Parse_BinCmp( &b"!"  [..] ).is_err() );
        assert!( Parse_BinCmp( &b"!>="[..] ).is_err() );
        assert!( Parse_BinCmp( &b"=>" [..] ).is_err() );

        assert!( Parse_BinCmp( &b" =="[..] ).is_err() );
        assert!( Parse_BinCmp( &b" !="[..] ).is_err() );
        assert!( Parse_BinCmp( &b" >" [..] ).is_err() );
        assert!( Parse_BinCmp( &b" >="[..] ).is_err() );
        assert!( Parse_BinCmp( &b" <" [..] ).is_err() );
        assert!( Parse_BinCmp( &b" <="[..] ).is_err() );
    }

    #[test]
    fn ParsingUnCmp()
    {
        // Testing for success.
        assert_eq!( Parse_UnCmp( &b"is even" [..] ), IResult::Done( &b"" [..], UnCmp::IsEven ) );
        assert_eq!( Parse_UnCmp( &b"is evenQ"[..] ), IResult::Done( &b"Q"[..], UnCmp::IsEven ) );

        assert_eq!( Parse_UnCmp( &b"is odd"  [..] ), IResult::Done( &b"" [..], UnCmp::IsOdd  ) );
        assert_eq!( Parse_UnCmp( &b"is oddQ" [..] ), IResult::Done( &b"Q"[..], UnCmp::IsOdd  ) );

        assert_eq!( Parse_UnCmp( &b"is max"  [..] ), IResult::Done( &b"" [..], UnCmp::IsMax  ) );
        assert_eq!( Parse_UnCmp( &b"is maxQ" [..] ), IResult::Done( &b"Q"[..], UnCmp::IsMax  ) );

        assert_eq!( Parse_UnCmp( &b"is min"  [..] ), IResult::Done( &b"" [..], UnCmp::IsMin  ) );
        assert_eq!( Parse_UnCmp( &b"is minQ" [..] ), IResult::Done( &b"Q"[..], UnCmp::IsMin  ) );

        // Testing for failure.
        assert!( Parse_UnCmp( &b""        [..] ).is_err() );
        assert!( Parse_UnCmp( &b"i"       [..] ).is_err() );
        assert!( Parse_UnCmp( &b"is"      [..] ).is_err() );
        assert!( Parse_UnCmp( &b"is "     [..] ).is_err() );
        assert!( Parse_UnCmp( &b"is Q"    [..] ).is_err() );

        assert!( Parse_UnCmp( &b"is e"    [..] ).is_err() );
        assert!( Parse_UnCmp( &b"is ev"   [..] ).is_err() );
        assert!( Parse_UnCmp( &b"is eve"  [..] ).is_err() );
        assert!( Parse_UnCmp( &b"is eveQ" [..] ).is_err() );
        assert!( Parse_UnCmp( &b" is even"[..] ).is_err() );

        assert!( Parse_UnCmp( &b"is o"    [..] ).is_err() );
        assert!( Parse_UnCmp( &b"is od"   [..] ).is_err() );
        assert!( Parse_UnCmp( &b"is odQ"  [..] ).is_err() );
        assert!( Parse_UnCmp( &b" is odd" [..] ).is_err() );

        assert!( Parse_UnCmp( &b"is m"    [..] ).is_err() );

        assert!( Parse_UnCmp( &b"is ma"   [..] ).is_err() );
        assert!( Parse_UnCmp( &b"is maQ"  [..] ).is_err() );
        assert!( Parse_UnCmp( &b" is max" [..] ).is_err() );

        assert!( Parse_UnCmp( &b"is mi"   [..] ).is_err() );
        assert!( Parse_UnCmp( &b"is miQ"  [..] ).is_err() );
        assert!( Parse_UnCmp( &b" is min" [..] ).is_err() );
    }

    #[test]
    fn ParsingGetConstant()
    {
        // Testing for success.
        // Can replace with inclusive range syntax "..." when/if that gets stabilized.
        let mut i = i8::MIN;

        loop
        {
            assert_eq!( GetConstant::Parse( i.to_string().as_bytes() ), IResult::Done( &b""[..], GetConstant(i) ) );

            if i == i8::MAX
            {
                break;
            }
            else
            {
                i += 1;
            }
        }

        // Testing for failure.
        let underflowStr = (i8::MIN as i16 - 1).to_string();
        let overflowStr  = (i8::MAX as i16 + 1).to_string();

        assert!( GetConstant::Parse( underflowStr.as_bytes() ).is_err() );
        assert!( GetConstant::Parse( overflowStr .as_bytes() ).is_err() );

        assert!( GetConstant::Parse( &b""   [..] ).is_err() );
        assert!( GetConstant::Parse( &b" "  [..] ).is_err() );
        assert!( GetConstant::Parse( &b"-"  [..] ).is_err() );
        assert!( GetConstant::Parse( &b"+"  [..] ).is_err() );
        assert!( GetConstant::Parse( &b"a"  [..] ).is_err() );
        assert!( GetConstant::Parse( &b"a1" [..] ).is_err() );
        assert!( GetConstant::Parse( &b"--1"[..] ).is_err() );
        assert!( GetConstant::Parse( &b"++1"[..] ).is_err() );
        assert!( GetConstant::Parse( &b"-+1"[..] ).is_err() );
        assert!( GetConstant::Parse( &b"+-1"[..] ).is_err() );
        assert!( GetConstant::Parse( &b" 1" [..] ).is_err() );
    }

    #[test]
    fn ParsingGetStatValue()
    {
        // Testing for success.
        assert_eq!( GetStatValue::Parse( &b"str" [..] ), IResult::Done( &b"" [..], GetStatValue(Stat::Strength    ) ) );
        assert_eq!( GetStatValue::Parse( &b"strQ"[..] ), IResult::Done( &b"Q"[..], GetStatValue(Stat::Strength    ) ) );
        assert_eq!( GetStatValue::Parse( &b"dex" [..] ), IResult::Done( &b"" [..], GetStatValue(Stat::Dexterity   ) ) );
        assert_eq!( GetStatValue::Parse( &b"dexQ"[..] ), IResult::Done( &b"Q"[..], GetStatValue(Stat::Dexterity   ) ) );
        assert_eq!( GetStatValue::Parse( &b"con" [..] ), IResult::Done( &b"" [..], GetStatValue(Stat::Constitution) ) );
        assert_eq!( GetStatValue::Parse( &b"conQ"[..] ), IResult::Done( &b"Q"[..], GetStatValue(Stat::Constitution) ) );
        assert_eq!( GetStatValue::Parse( &b"int" [..] ), IResult::Done( &b"" [..], GetStatValue(Stat::Intelligence) ) );
        assert_eq!( GetStatValue::Parse( &b"intQ"[..] ), IResult::Done( &b"Q"[..], GetStatValue(Stat::Intelligence) ) );
        assert_eq!( GetStatValue::Parse( &b"wis" [..] ), IResult::Done( &b"" [..], GetStatValue(Stat::Wisdom      ) ) );
        assert_eq!( GetStatValue::Parse( &b"wisQ"[..] ), IResult::Done( &b"Q"[..], GetStatValue(Stat::Wisdom      ) ) );
        assert_eq!( GetStatValue::Parse( &b"cha" [..] ), IResult::Done( &b"" [..], GetStatValue(Stat::Charisma    ) ) );
        assert_eq!( GetStatValue::Parse( &b"chaQ"[..] ), IResult::Done( &b"Q"[..], GetStatValue(Stat::Charisma    ) ) );

        // Testing for failure.
        assert!( GetStatValue::Parse( &b""    [..] ).is_err() );
        assert!( GetStatValue::Parse( &b" "   [..] ).is_err() );

        assert!( GetStatValue::Parse( &b"a"   [..] ).is_err() );
        assert!( GetStatValue::Parse( &b"aa"  [..] ).is_err() );
        assert!( GetStatValue::Parse( &b"aaa" [..] ).is_err() );
        assert!( GetStatValue::Parse( &b"aaaa"[..] ).is_err() );

        assert!( GetStatValue::Parse( &b"s"   [..] ).is_err() );
        assert!( GetStatValue::Parse( &b"st"  [..] ).is_err() );
        assert!( GetStatValue::Parse( &b"stq" [..] ).is_err() );
        assert!( GetStatValue::Parse( &b" str"[..] ).is_err() );

        assert!( GetStatValue::Parse( &b"d"   [..] ).is_err() );
        assert!( GetStatValue::Parse( &b"de"  [..] ).is_err() );
        assert!( GetStatValue::Parse( &b"deq" [..] ).is_err() );
        assert!( GetStatValue::Parse( &b" dex"[..] ).is_err() );

        assert!( GetStatValue::Parse( &b"c"   [..] ).is_err() );
        assert!( GetStatValue::Parse( &b"co"  [..] ).is_err() );
        assert!( GetStatValue::Parse( &b"coq" [..] ).is_err() );
        assert!( GetStatValue::Parse( &b" con"[..] ).is_err() );

        assert!( GetStatValue::Parse( &b"i"   [..] ).is_err() );
        assert!( GetStatValue::Parse( &b"in"  [..] ).is_err() );
        assert!( GetStatValue::Parse( &b"inq" [..] ).is_err() );
        assert!( GetStatValue::Parse( &b" int"[..] ).is_err() );

        assert!( GetStatValue::Parse( &b"w"   [..] ).is_err() );
        assert!( GetStatValue::Parse( &b"wi"  [..] ).is_err() );
        assert!( GetStatValue::Parse( &b"wiq" [..] ).is_err() );
        assert!( GetStatValue::Parse( &b" wis"[..] ).is_err() );

        assert!( GetStatValue::Parse( &b"c"   [..] ).is_err() );
        assert!( GetStatValue::Parse( &b"ch"  [..] ).is_err() );
        assert!( GetStatValue::Parse( &b"chq" [..] ).is_err() );
        assert!( GetStatValue::Parse( &b" cha"[..] ).is_err() );
    }

    #[test]
    fn ParsingGetStatModifier()
    {
        // Testing for success.
        assert_eq!( GetStatModifier::Parse( &b"strmod" [..] ), IResult::Done( &b"" [..], GetStatModifier(Stat::Strength    ) ) );
        assert_eq!( GetStatModifier::Parse( &b"strmodQ"[..] ), IResult::Done( &b"Q"[..], GetStatModifier(Stat::Strength    ) ) );
        assert_eq!( GetStatModifier::Parse( &b"dexmod" [..] ), IResult::Done( &b"" [..], GetStatModifier(Stat::Dexterity   ) ) );
        assert_eq!( GetStatModifier::Parse( &b"dexmodQ"[..] ), IResult::Done( &b"Q"[..], GetStatModifier(Stat::Dexterity   ) ) );
        assert_eq!( GetStatModifier::Parse( &b"conmod" [..] ), IResult::Done( &b"" [..], GetStatModifier(Stat::Constitution) ) );
        assert_eq!( GetStatModifier::Parse( &b"conmodQ"[..] ), IResult::Done( &b"Q"[..], GetStatModifier(Stat::Constitution) ) );
        assert_eq!( GetStatModifier::Parse( &b"intmod" [..] ), IResult::Done( &b"" [..], GetStatModifier(Stat::Intelligence) ) );
        assert_eq!( GetStatModifier::Parse( &b"intmodQ"[..] ), IResult::Done( &b"Q"[..], GetStatModifier(Stat::Intelligence) ) );
        assert_eq!( GetStatModifier::Parse( &b"wismod" [..] ), IResult::Done( &b"" [..], GetStatModifier(Stat::Wisdom      ) ) );
        assert_eq!( GetStatModifier::Parse( &b"wismodQ"[..] ), IResult::Done( &b"Q"[..], GetStatModifier(Stat::Wisdom      ) ) );
        assert_eq!( GetStatModifier::Parse( &b"chamod" [..] ), IResult::Done( &b"" [..], GetStatModifier(Stat::Charisma    ) ) );
        assert_eq!( GetStatModifier::Parse( &b"chamodQ"[..] ), IResult::Done( &b"Q"[..], GetStatModifier(Stat::Charisma    ) ) );

        // Testing for failure.
        assert!( GetStatModifier::Parse( &b""       [..] ).is_err() );
        assert!( GetStatModifier::Parse( &b" "      [..] ).is_err() );

        assert!( GetStatModifier::Parse( &b"a"      [..] ).is_err() );
        assert!( GetStatModifier::Parse( &b"aa"     [..] ).is_err() );
        assert!( GetStatModifier::Parse( &b"aaa"    [..] ).is_err() );
        assert!( GetStatModifier::Parse( &b"aaaa"   [..] ).is_err() );

        assert!( GetStatModifier::Parse( &b"s"      [..] ).is_err() );
        assert!( GetStatModifier::Parse( &b"st"     [..] ).is_err() );
        assert!( GetStatModifier::Parse( &b"str"    [..] ).is_err() );
        assert!( GetStatModifier::Parse( &b"strm"   [..] ).is_err() );
        assert!( GetStatModifier::Parse( &b" strmod"[..] ).is_err() );

        assert!( GetStatModifier::Parse( &b"d"      [..] ).is_err() );
        assert!( GetStatModifier::Parse( &b"de"     [..] ).is_err() );
        assert!( GetStatModifier::Parse( &b"dex"    [..] ).is_err() );
        assert!( GetStatModifier::Parse( &b"dexm"   [..] ).is_err() );
        assert!( GetStatModifier::Parse( &b" dexmod"[..] ).is_err() );

        assert!( GetStatModifier::Parse( &b"c"      [..] ).is_err() );
        assert!( GetStatModifier::Parse( &b"co"     [..] ).is_err() );
        assert!( GetStatModifier::Parse( &b"con"    [..] ).is_err() );
        assert!( GetStatModifier::Parse( &b"conm"   [..] ).is_err() );
        assert!( GetStatModifier::Parse( &b" conmod"[..] ).is_err() );

        assert!( GetStatModifier::Parse( &b"i"      [..] ).is_err() );
        assert!( GetStatModifier::Parse( &b"in"     [..] ).is_err() );
        assert!( GetStatModifier::Parse( &b"int"    [..] ).is_err() );
        assert!( GetStatModifier::Parse( &b"intm"   [..] ).is_err() );
        assert!( GetStatModifier::Parse( &b" intmod"[..] ).is_err() );

        assert!( GetStatModifier::Parse( &b"w"      [..] ).is_err() );
        assert!( GetStatModifier::Parse( &b"wi"     [..] ).is_err() );
        assert!( GetStatModifier::Parse( &b"wis"    [..] ).is_err() );
        assert!( GetStatModifier::Parse( &b"wism"   [..] ).is_err() );
        assert!( GetStatModifier::Parse( &b" wismod"[..] ).is_err() );

        assert!( GetStatModifier::Parse( &b"c"      [..] ).is_err() );
        assert!( GetStatModifier::Parse( &b"ch"     [..] ).is_err() );
        assert!( GetStatModifier::Parse( &b"cha"    [..] ).is_err() );
        assert!( GetStatModifier::Parse( &b"chm"    [..] ).is_err() );
        assert!( GetStatModifier::Parse( &b" chamod"[..] ).is_err() );
    }

    #[test]
    fn ParsingConstraint()
    {
        // Testing success for BinCmp_StatValue_Constant
        assert_eq!( Parse_Constraint( &b"str==-2"        [..] ), IResult::Done(&b""[..], Constraint::BinCmp_StatValue_Constant( BinaryCompare::New(GetStatValue(Stat::Strength    ), BinCmp::EQ, GetConstant(-2)) )) );
        assert_eq!( Parse_Constraint( &b"dex  !=  -1"    [..] ), IResult::Done(&b""[..], Constraint::BinCmp_StatValue_Constant( BinaryCompare::New(GetStatValue(Stat::Dexterity   ), BinCmp::NE, GetConstant(-1)) )) );
        assert_eq!( Parse_Constraint( &b"  con>=-0  "    [..] ), IResult::Done(&b""[..], Constraint::BinCmp_StatValue_Constant( BinaryCompare::New(GetStatValue(Stat::Constitution), BinCmp::GE, GetConstant( 0)) )) );
        assert_eq!( Parse_Constraint( &b"  int  >  +0  " [..] ), IResult::Done(&b""[..], Constraint::BinCmp_StatValue_Constant( BinaryCompare::New(GetStatValue(Stat::Intelligence), BinCmp::GT, GetConstant( 0)) )) );
        assert_eq!( Parse_Constraint( &b"wis <= +1"      [..] ), IResult::Done(&b""[..], Constraint::BinCmp_StatValue_Constant( BinaryCompare::New(GetStatValue(Stat::Wisdom      ), BinCmp::LE, GetConstant( 1)) )) );
        assert_eq!( Parse_Constraint( &b"cha < +2"       [..] ), IResult::Done(&b""[..], Constraint::BinCmp_StatValue_Constant( BinaryCompare::New(GetStatValue(Stat::Charisma    ), BinCmp::LT, GetConstant( 2)) )) );

        // Testing success for BinCmp_StatModifier_Constant
        assert_eq!( Parse_Constraint( &b"strmod==-2"      [..] ), IResult::Done(&b""[..], Constraint::BinCmp_StatModifier_Constant( BinaryCompare::New(GetStatModifier(Stat::Strength    ), BinCmp::EQ, GetConstant(-2)) )) );
        assert_eq!( Parse_Constraint( &b"dexmod  !=  -1"  [..] ), IResult::Done(&b""[..], Constraint::BinCmp_StatModifier_Constant( BinaryCompare::New(GetStatModifier(Stat::Dexterity   ), BinCmp::NE, GetConstant(-1)) )) );
        assert_eq!( Parse_Constraint( &b"  conmod>=0  "   [..] ), IResult::Done(&b""[..], Constraint::BinCmp_StatModifier_Constant( BinaryCompare::New(GetStatModifier(Stat::Constitution), BinCmp::GE, GetConstant( 0)) )) );
        assert_eq!( Parse_Constraint( &b"  intmod  >  0  "[..] ), IResult::Done(&b""[..], Constraint::BinCmp_StatModifier_Constant( BinaryCompare::New(GetStatModifier(Stat::Intelligence), BinCmp::GT, GetConstant( 0)) )) );
        assert_eq!( Parse_Constraint( &b"wismod <= 1"     [..] ), IResult::Done(&b""[..], Constraint::BinCmp_StatModifier_Constant( BinaryCompare::New(GetStatModifier(Stat::Wisdom      ), BinCmp::LE, GetConstant( 1)) )) );
        assert_eq!( Parse_Constraint( &b"chamod < 2"      [..] ), IResult::Done(&b""[..], Constraint::BinCmp_StatModifier_Constant( BinaryCompare::New(GetStatModifier(Stat::Charisma    ), BinCmp::LT, GetConstant( 2)) )) );

        // Testing success for BinCmp_StatValue_StatValue
        assert_eq!( Parse_Constraint( &b"str==cha"       [..] ), IResult::Done(&b""[..], Constraint::BinCmp_StatValue_StatValue( BinaryCompare::New(GetStatValue(Stat::Strength    ), BinCmp::EQ, GetStatValue(Stat::Charisma    )) )) );
        assert_eq!( Parse_Constraint( &b"dex  !=  wis"   [..] ), IResult::Done(&b""[..], Constraint::BinCmp_StatValue_StatValue( BinaryCompare::New(GetStatValue(Stat::Dexterity   ), BinCmp::NE, GetStatValue(Stat::Wisdom      )) )) );
        assert_eq!( Parse_Constraint( &b"  con>=int  "   [..] ), IResult::Done(&b""[..], Constraint::BinCmp_StatValue_StatValue( BinaryCompare::New(GetStatValue(Stat::Constitution), BinCmp::GE, GetStatValue(Stat::Intelligence)) )) );
        assert_eq!( Parse_Constraint( &b"  int  >  con  "[..] ), IResult::Done(&b""[..], Constraint::BinCmp_StatValue_StatValue( BinaryCompare::New(GetStatValue(Stat::Intelligence), BinCmp::GT, GetStatValue(Stat::Constitution)) )) );
        assert_eq!( Parse_Constraint( &b"wis <= dex"     [..] ), IResult::Done(&b""[..], Constraint::BinCmp_StatValue_StatValue( BinaryCompare::New(GetStatValue(Stat::Wisdom      ), BinCmp::LE, GetStatValue(Stat::Dexterity   )) )) );
        assert_eq!( Parse_Constraint( &b"cha < str"      [..] ), IResult::Done(&b""[..], Constraint::BinCmp_StatValue_StatValue( BinaryCompare::New(GetStatValue(Stat::Charisma    ), BinCmp::LT, GetStatValue(Stat::Strength    )) )) );

        // Testing success for BinCmp_StatModifier_StatModifier
        assert_eq!( Parse_Constraint( &b"strmod==chamod"       [..] ), IResult::Done(&b""[..], Constraint::BinCmp_StatModifier_StatModifier( BinaryCompare::New(GetStatModifier(Stat::Strength    ), BinCmp::EQ, GetStatModifier(Stat::Charisma    )) )) );
        assert_eq!( Parse_Constraint( &b"dexmod  !=  wismod"   [..] ), IResult::Done(&b""[..], Constraint::BinCmp_StatModifier_StatModifier( BinaryCompare::New(GetStatModifier(Stat::Dexterity   ), BinCmp::NE, GetStatModifier(Stat::Wisdom      )) )) );
        assert_eq!( Parse_Constraint( &b"  conmod>=intmod  "   [..] ), IResult::Done(&b""[..], Constraint::BinCmp_StatModifier_StatModifier( BinaryCompare::New(GetStatModifier(Stat::Constitution), BinCmp::GE, GetStatModifier(Stat::Intelligence)) )) );
        assert_eq!( Parse_Constraint( &b"  intmod  >  conmod  "[..] ), IResult::Done(&b""[..], Constraint::BinCmp_StatModifier_StatModifier( BinaryCompare::New(GetStatModifier(Stat::Intelligence), BinCmp::GT, GetStatModifier(Stat::Constitution)) )) );
        assert_eq!( Parse_Constraint( &b"wismod <= dexmod"     [..] ), IResult::Done(&b""[..], Constraint::BinCmp_StatModifier_StatModifier( BinaryCompare::New(GetStatModifier(Stat::Wisdom      ), BinCmp::LE, GetStatModifier(Stat::Dexterity   )) )) );
        assert_eq!( Parse_Constraint( &b"chamod < strmod"      [..] ), IResult::Done(&b""[..], Constraint::BinCmp_StatModifier_StatModifier( BinaryCompare::New(GetStatModifier(Stat::Charisma    ), BinCmp::LT, GetStatModifier(Stat::Strength    )) )) );

        // Testing success for UnCmp_StatValue
        assert_eq!( Parse_Constraint( &b"  str is min  "  [..] ), IResult::Done(&b""[..], Constraint::UnCmp_StatValue( UnaryCompare::New(GetStatValue(Stat::Strength    ), UnCmp::IsMin ) )) );
        assert_eq!( Parse_Constraint( &b"dex  is odd"     [..] ), IResult::Done(&b""[..], Constraint::UnCmp_StatValue( UnaryCompare::New(GetStatValue(Stat::Dexterity   ), UnCmp::IsOdd ) )) );
        assert_eq!( Parse_Constraint( &b"  con  is even  "[..] ), IResult::Done(&b""[..], Constraint::UnCmp_StatValue( UnaryCompare::New(GetStatValue(Stat::Constitution), UnCmp::IsEven) )) );
        assert_eq!( Parse_Constraint( &b"int is odd"      [..] ), IResult::Done(&b""[..], Constraint::UnCmp_StatValue( UnaryCompare::New(GetStatValue(Stat::Intelligence), UnCmp::IsOdd ) )) );
        assert_eq!( Parse_Constraint( &b"wis is even"     [..] ), IResult::Done(&b""[..], Constraint::UnCmp_StatValue( UnaryCompare::New(GetStatValue(Stat::Wisdom      ), UnCmp::IsEven) )) );
        assert_eq!( Parse_Constraint( &b"cha is max"      [..] ), IResult::Done(&b""[..], Constraint::UnCmp_StatValue( UnaryCompare::New(GetStatValue(Stat::Charisma    ), UnCmp::IsMax ) )) );

        // Testing success for UnCmp_StatModifier
        assert_eq!( Parse_Constraint( &b"  strmod is even  " [..] ), IResult::Done(&b""[..], Constraint::UnCmp_StatModifier( UnaryCompare::New(GetStatModifier(Stat::Strength    ), UnCmp::IsEven) )) );
        assert_eq!( Parse_Constraint( &b"dexmod  is min"     [..] ), IResult::Done(&b""[..], Constraint::UnCmp_StatModifier( UnaryCompare::New(GetStatModifier(Stat::Dexterity   ), UnCmp::IsMin ) )) );
        assert_eq!( Parse_Constraint( &b"  conmod  is even  "[..] ), IResult::Done(&b""[..], Constraint::UnCmp_StatModifier( UnaryCompare::New(GetStatModifier(Stat::Constitution), UnCmp::IsEven) )) );
        assert_eq!( Parse_Constraint( &b"intmod is odd"      [..] ), IResult::Done(&b""[..], Constraint::UnCmp_StatModifier( UnaryCompare::New(GetStatModifier(Stat::Intelligence), UnCmp::IsOdd ) )) );
        assert_eq!( Parse_Constraint( &b"wismod is max"      [..] ), IResult::Done(&b""[..], Constraint::UnCmp_StatModifier( UnaryCompare::New(GetStatModifier(Stat::Wisdom      ), UnCmp::IsMax ) )) );
        assert_eq!( Parse_Constraint( &b"chamod is odd"      [..] ), IResult::Done(&b""[..], Constraint::UnCmp_StatModifier( UnaryCompare::New(GetStatModifier(Stat::Charisma    ), UnCmp::IsOdd ) )) );

        // Testing for failure.
        assert!( Parse_Constraint( &b""                     [..] ).is_err() );
        assert!( Parse_Constraint( &b" "                    [..] ).is_err() );
        assert!( Parse_Constraint( &b"a"                    [..] ).is_err() );
        assert!( Parse_Constraint( &b"s"                    [..] ).is_err() );
        assert!( Parse_Constraint( &b"st"                   [..] ).is_err() );
        assert!( Parse_Constraint( &b"str"                  [..] ).is_err() );
        assert!( Parse_Constraint( &b"strm"                 [..] ).is_err() );
        assert!( Parse_Constraint( &b"strmo"                [..] ).is_err() );
        assert!( Parse_Constraint( &b"strmod"               [..] ).is_err() );
        assert!( Parse_Constraint( &b"strmod "              [..] ).is_err() );
        assert!( Parse_Constraint( &b"strmod ="             [..] ).is_err() );
        assert!( Parse_Constraint( &b"strmod =="            [..] ).is_err() );
        assert!( Parse_Constraint( &b"strmod == "           [..] ).is_err() );
        assert!( Parse_Constraint( &b"strmod == +"          [..] ).is_err() );
        assert!( Parse_Constraint( &b"strstrmod == 10"      [..] ).is_err() );
        assert!( Parse_Constraint( &b"str == 1000"          [..] ).is_err() );
        assert!( Parse_Constraint( &b"dexmod > -1000"       [..] ).is_err() );
        assert!( Parse_Constraint( &b"stris even"           [..] ).is_err() );
        assert!( Parse_Constraint( &b"str  is  even"        [..] ).is_err() );
        assert!( Parse_Constraint( &b"is max dex"           [..] ).is_err() );
        assert!( Parse_Constraint( &b"conmonis max dex"     [..] ).is_err() );
        assert!( Parse_Constraint( &b"conmon is min dex"    [..] ).is_err() );
        assert!( Parse_Constraint( &b"cha = 10"             [..] ).is_err() );
        assert!( Parse_Constraint( &b"cha === 10"           [..] ).is_err() );
        assert!( Parse_Constraint( &b"wis===10wis==10"      [..] ).is_err() );
        assert!( Parse_Constraint( &b"conmod is even 10"    [..] ).is_err() );
        assert!( Parse_Constraint( &b"intmodisodd"          [..] ).is_err() );
        assert!( Parse_Constraint( &b"dex > str > int"      [..] ).is_err() );
        assert!( Parse_Constraint( &b"chamod is > intmod"   [..] ).is_err() );
        assert!( Parse_Constraint( &b"is odd chamod"        [..] ).is_err() );
        assert!( Parse_Constraint( &b"< 10 int"             [..] ).is_err() );
        assert!( Parse_Constraint( &b"int <> wis  "         [..] ).is_err() );
        assert!( Parse_Constraint( &b"int <> wis  1"        [..] ).is_err() );
        assert!( Parse_Constraint( &b"a != 10"              [..] ).is_err() );
        assert!( Parse_Constraint( &b"str > x"              [..] ).is_err() );
        assert!( Parse_Constraint( &b"wismod <= dex"        [..] ).is_err() );
        assert!( Parse_Constraint( &b"cha < strmod"         [..] ).is_err() );
        assert!( Parse_Constraint( &b"10 == dex"            [..] ).is_err() );
        assert!( Parse_Constraint( &b"3 != conmod"          [..] ).is_err() );
        assert!( Parse_Constraint( &b"wis <= +1  a"         [..] ).is_err() );
        assert!( Parse_Constraint( &b"3 != conmod"          [..] ).is_err() );
    }

    #[test]
    fn ParsingCharacterRace()
    {
        // Testing for success.
        for race in RaceList.iter()
        {
            let raceStr = format!("{:?}" , race);

            assert_eq!( Parse_CharacterRace( raceStr.as_bytes() ), IResult::Done( &b""[..], *race ) );
        }

        // Testing for failure.
        for race in RaceList.iter()
        {
            let raceStr = format!("{:?}Q", race);

            assert!( Parse_CharacterRace( raceStr.as_bytes() ).is_err() );
        }

        assert!( Parse_CharacterRace( &b""          [..] ).is_err() );
        assert!( Parse_CharacterRace( &b" "         [..] ).is_err() );
        assert!( Parse_CharacterRace( &b"!"         [..] ).is_err() );
        assert!( Parse_CharacterRace( &b"A"         [..] ).is_err() );
        assert!( Parse_CharacterRace( &b"Aa"        [..] ).is_err() );
        assert!( Parse_CharacterRace( &b"Aar"       [..] ).is_err() );
        assert!( Parse_CharacterRace( &b"Aara"      [..] ).is_err() );
        assert!( Parse_CharacterRace( &b"Aarak"     [..] ).is_err() );
        assert!( Parse_CharacterRace( &b"Aarako"    [..] ).is_err() );
        assert!( Parse_CharacterRace( &b"Aarakoc"   [..] ).is_err() );
        assert!( Parse_CharacterRace( &b"Aarakocr"  [..] ).is_err() );
        assert!( Parse_CharacterRace( &b" Aarakocra"[..] ).is_err() );
    }

    #[test]
    fn ParsingFeat()
    {
        // Testing for success.
        for feat in FeatList.iter()
        {
            let featStr = format!("{:?}" , feat);

            assert_eq!( Parse_Feat( featStr.as_bytes() ), IResult::Done( &b""[..], *feat ) );
        }

        // Testing for failure.
        for feat in FeatList.iter()
        {
            let featStr = format!("{:?}Q", feat);

            assert!( Parse_Feat( featStr.as_bytes() ).is_err() );
        }

        assert!( Parse_Feat( &b""                 [..] ).is_err() );
        assert!( Parse_Feat( &b" "                [..] ).is_err() );
        assert!( Parse_Feat( &b"!"                [..] ).is_err() );
        assert!( Parse_Feat( &b"A"                [..] ).is_err() );
        assert!( Parse_Feat( &b"Ac"               [..] ).is_err() );
        assert!( Parse_Feat( &b"Act"              [..] ).is_err() );
        assert!( Parse_Feat( &b"Acto"             [..] ).is_err() );
        assert!( Parse_Feat( &b" Actor"           [..] ).is_err() );
        assert!( Parse_Feat( &b"NonStatGivingFeat"[..] ).is_err() );
    }
}
