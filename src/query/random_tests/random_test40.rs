use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test40" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Weighted,
        statOrdering: [
            Constitution,
            Intelligence,
            Wisdom,
            Dexterity,
            Charisma,
            Strength,
        ],
        isReversed: true,
        availableRaces: vec![
            Aasimar_Protector,
            Aasimar_Scourge,
            Bugbear,
            CustomLineage,
            Dwarf_Grey,
            Dwarf_Hill,
            Dwarf_MarkOfWarding,
            Dwarf_Mountain,
            Elf_Drow,
            Elf_High,
            Firbolg,
            Genasi_Earth,
            Genasi_Fire,
            Gnome_MarkOfScribing,
            Gnome_Rock,
            HalfElf,
            Halfling_Ghostwise,
            Human,
            Human_MarkOfHandling,
            Human_MarkOfPassage,
            Kalashtar,
            Kobold,
            Lizardfolk,
            Orc_Eberron,
            Shifter_BeastHide,
            Shifter_LongTooth,
            Shifter_SwiftStride,
            Shifter_WildHunt,
            Tiefling_Feral,
            Triton,
            Warforged,
        ],
        requiredFeats: vec![],
        constraints: vec![
            BinCmp_StatModifier_StatModifier(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Constitution,
                    ),
                    binCmp: LE,
                    rhsValue: GetStatModifier(
                        Intelligence,
                    ),
                },
            ),
        ],
        numberOfASIs: 1,
        doAllRacesHaveFreeFeat: true,
        canRearrangeRaceModifiers: true,
        maxNumberOfResults: 21,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 39989145,
            numUniqueCombinationsPassedConstraints: 23409718,
            numStatBlocksEjected: 397,
            numUniqueCombinationsEjected: 32370,
            numUniqueCombinationsBelowLowerBound: 23408609,
            numStatBlocksShiftedDuringInsertion: 4261,
            numDuplicateRaceStatBlocksRemoved: 2589,
            numUniqueStatBlockAfterRacePass: 162,
            numUniqueStatBlockAfterFeatPass: 615,
            numFinalStatBlockCombinations: 81,
        };

    let expectedStatBlocks = 
        vec![
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 13, 10, 13, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [9, 13, 10, 13, 13, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 13, 11, 12, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [9, 13, 11, 12, 13, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 13, 11, 13, 12, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [9, 13, 11, 13, 12, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 12, 11, 13, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [9, 12, 11, 13, 13, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 13, 11, 13, 13, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [9, 13, 11, 13, 15, 12],
                        myRaceModifiers:     [2, 0, 0, 0, -2, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [12, 13, 11, 13, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [8, 13, 11, 13, 13, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 13, 11, 13, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            AberrantDragonMark,
                        ],
                        myBasePointBuyArray: [9, 13, 10, 13, 13, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [9, 13, 11, 13, 15, 12],
                        myRaceModifiers:     [2, 0, 0, 0, -2, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 13, 11, 13, 13, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [9, 13, 10, 13, 13, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Crusher,
                        ],
                        myBasePointBuyArray: [8, 13, 11, 13, 13, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Durable,
                        ],
                        myBasePointBuyArray: [9, 13, 10, 13, 13, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [9, 13, 11, 12, 13, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Gunner,
                        ],
                        myBasePointBuyArray: [9, 12, 11, 13, 13, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            HeavilyArmored,
                        ],
                        myBasePointBuyArray: [8, 13, 11, 13, 13, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            HeavyArmorMaster,
                        ],
                        myBasePointBuyArray: [8, 13, 11, 13, 13, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            KeenMind,
                        ],
                        myBasePointBuyArray: [9, 13, 11, 12, 13, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 13, 11, 13, 13, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [9, 13, 11, 12, 13, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            ModeratelyArmored,
                        ],
                        myBasePointBuyArray: [8, 13, 11, 13, 13, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [9, 13, 11, 12, 13, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Piercer,
                        ],
                        myBasePointBuyArray: [8, 13, 11, 13, 13, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [9, 13, 11, 13, 15, 12],
                        myRaceModifiers:     [2, 0, 0, 0, -2, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Resilient_Con,
                        ],
                        myBasePointBuyArray: [9, 13, 10, 13, 13, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Resilient_Dex,
                        ],
                        myBasePointBuyArray: [9, 12, 11, 13, 13, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Resilient_Int,
                        ],
                        myBasePointBuyArray: [9, 13, 11, 12, 13, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Resilient_Str,
                        ],
                        myBasePointBuyArray: [8, 13, 11, 13, 13, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Resilient_Wis,
                        ],
                        myBasePointBuyArray: [9, 13, 11, 13, 12, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [9, 13, 11, 12, 13, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 13, 11, 13, 13, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Slasher,
                        ],
                        myBasePointBuyArray: [8, 13, 11, 13, 13, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            SquatNimbleness,
                        ],
                        myBasePointBuyArray: [8, 13, 11, 13, 13, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [8, 13, 11, 13, 13, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [9, 13, 11, 12, 13, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [9, 13, 11, 12, 13, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            WeaponMaster,
                        ],
                        myBasePointBuyArray: [8, 13, 11, 13, 13, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [10, 13, 11, 13, 13, 14],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [1, 0, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 13, 12, 13, 11, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [9, 13, 12, 13, 11, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 11, 12, 13, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [9, 11, 12, 13, 13, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 13, 12, 13, 13, 11],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [9, 13, 12, 13, 15, 11],
                        myRaceModifiers:     [2, 0, 0, 0, -2, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [11, 13, 12, 13, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [8, 12, 12, 13, 13, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [1, 1, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 13, 13, 12, 11, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [9, 13, 13, 12, 11, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 11, 13, 12, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [9, 11, 13, 12, 13, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 13, 13, 12, 13, 11],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [9, 13, 13, 12, 15, 11],
                        myRaceModifiers:     [2, 0, 0, 0, -2, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [11, 13, 13, 12, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [8, 12, 13, 12, 13, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [1, 1, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 13, 13, 13, 10, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [9, 13, 13, 13, 10, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 12, 13, 13, 11, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [9, 12, 13, 13, 11, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 13, 13, 13, 11, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [9, 13, 13, 15, 11, 12],
                        myRaceModifiers:     [2, 0, 0, -2, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [12, 13, 13, 13, 11, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [8, 13, 13, 13, 11, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 13, 13, 13, 11, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            AberrantDragonMark,
                        ],
                        myBasePointBuyArray: [9, 13, 12, 13, 11, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Actor,
                        ],
                        myBasePointBuyArray: [9, 13, 13, 15, 11, 12],
                        myRaceModifiers:     [2, 0, 0, -2, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 13, 13, 13, 11, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Chef,
                        ],
                        myBasePointBuyArray: [9, 13, 12, 13, 11, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Crusher,
                        ],
                        myBasePointBuyArray: [8, 13, 13, 13, 11, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Durable,
                        ],
                        myBasePointBuyArray: [9, 13, 12, 13, 11, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            FeyTouched,
                        ],
                        myBasePointBuyArray: [9, 13, 13, 12, 11, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Gunner,
                        ],
                        myBasePointBuyArray: [9, 12, 13, 13, 11, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            HeavilyArmored,
                        ],
                        myBasePointBuyArray: [8, 13, 13, 13, 11, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            HeavyArmorMaster,
                        ],
                        myBasePointBuyArray: [8, 13, 13, 13, 11, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            KeenMind,
                        ],
                        myBasePointBuyArray: [9, 13, 13, 12, 11, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            LightlyArmored,
                        ],
                        myBasePointBuyArray: [8, 13, 13, 13, 11, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Linguist,
                        ],
                        myBasePointBuyArray: [9, 13, 13, 12, 11, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            ModeratelyArmored,
                        ],
                        myBasePointBuyArray: [8, 13, 13, 13, 11, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Observant,
                        ],
                        myBasePointBuyArray: [9, 13, 13, 12, 11, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Piercer,
                        ],
                        myBasePointBuyArray: [8, 13, 13, 13, 11, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Resilient_Cha,
                        ],
                        myBasePointBuyArray: [9, 13, 13, 15, 11, 12],
                        myRaceModifiers:     [2, 0, 0, -2, 0, 0],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 1],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Resilient_Con,
                        ],
                        myBasePointBuyArray: [9, 13, 12, 13, 11, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 1, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Resilient_Dex,
                        ],
                        myBasePointBuyArray: [9, 12, 13, 13, 11, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Resilient_Int,
                        ],
                        myBasePointBuyArray: [9, 13, 13, 12, 11, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Resilient_Str,
                        ],
                        myBasePointBuyArray: [8, 13, 13, 13, 11, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Resilient_Wis,
                        ],
                        myBasePointBuyArray: [9, 13, 13, 13, 10, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 1, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            ShadowTouched,
                        ],
                        myBasePointBuyArray: [9, 13, 13, 12, 11, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            SkillExpert,
                        ],
                        myBasePointBuyArray: [8, 13, 13, 13, 11, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Slasher,
                        ],
                        myBasePointBuyArray: [8, 13, 13, 13, 11, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            SquatNimbleness,
                        ],
                        myBasePointBuyArray: [8, 13, 13, 13, 11, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            TavernBrawler,
                        ],
                        myBasePointBuyArray: [8, 13, 13, 13, 11, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Telekinetic,
                        ],
                        myBasePointBuyArray: [9, 13, 13, 12, 11, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            Telepathic,
                        ],
                        myBasePointBuyArray: [9, 13, 13, 12, 11, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 1, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            WeaponMaster,
                        ],
                        myBasePointBuyArray: [8, 13, 13, 13, 11, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [10, 13, 13, 13, 11, 14],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [1, 0, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 510,
                statBlock: [13, 11, 13, 13, 12, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![
                            NonStatGivingFeat,
                        ],
                        myBasePointBuyArray: [9, 11, 13, 13, 12, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 0, -2],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
        ];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

