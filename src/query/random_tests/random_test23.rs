use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test23" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Unranked,
        statOrdering: [
            Constitution,
            Charisma,
            Strength,
            Dexterity,
            Intelligence,
            Wisdom,
        ],
        isReversed: false,
        availableRaces: vec![
            Aarakocra,
            Aasimar_Fallen,
            Aasimar_Scourge,
            Bugbear,
            CustomLineage,
            Changling,
            DragonBorn,
            Dwarf_Grey,
            Dwarf_Hill,
            Dwarf_MarkOfWarding,
            Dwarf_Mountain,
            Elf_Drow,
            Elf_High,
            Elf_MarkOfShadow,
            Elf_Wood,
            Firbolg,
            Genasi_Air,
            Genasi_Earth,
            Genasi_Fire,
            Gnome_Forest,
            Goblin,
            Goliath,
            HalfElf,
            HalfElf_MarkOfDetection,
            HalfElf_MarkOfStorm,
            Halfling_Ghostwise,
            Halfling_Lightfoot,
            Halfling_MarkOfHealing,
            Halfling_MarkOfHospitality,
            Halfling_Stout,
            HalfOrc_MarkOfFinding,
            Human,
            Human_MarkOfHandling,
            Human_MarkOfMaking,
            Human_MarkOfPassage,
            Human_MarkOfSentinel,
            Kalashtar,
            Kenku,
            Kobold,
            Lizardfolk,
            Orc,
            Orc_Eberron,
            Shifter_LongTooth,
            Shifter_SwiftStride,
            Tabaxi,
            Tiefling_Feral,
            Tortle,
            Triton,
            Warforged,
            YuanTiPureblood,
        ],
        requiredFeats: vec![
            HeavilyArmored,
        ],
        constraints: vec![
            BinCmp_StatModifier_StatModifier(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Charisma,
                    ),
                    binCmp: NE,
                    rhsValue: GetStatModifier(
                        Constitution,
                    ),
                },
            ),
        ],
        numberOfASIs: 0,
        doAllRacesHaveFreeFeat: false,
        canRearrangeRaceModifiers: true,
        maxNumberOfResults: 21,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 73692,
            numUniqueCombinationsPassedConstraints: 58530,
            numStatBlocksEjected: 197,
            numUniqueCombinationsEjected: 197,
            numUniqueCombinationsBelowLowerBound: 58312,
            numStatBlocksShiftedDuringInsertion: 3669,
            numDuplicateRaceStatBlocksRemoved: 0,
            numUniqueStatBlockAfterRacePass: 282,
            numUniqueStatBlockAfterFeatPass: 6,
            numFinalStatBlockCombinations: 21,
        };

    let expectedStatBlocks = 
        vec![
            RankedStatBlock {
                rank: 0,
                statBlock: [16, 8, 17, 8, 8, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            HeavilyArmored,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 8, 15],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 10, 17, 8, 8, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            HeavilyArmored,
                        ],
                        myBasePointBuyArray: [14, 10, 15, 8, 8, 15],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 9, 17, 9, 8, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            HeavilyArmored,
                        ],
                        myBasePointBuyArray: [14, 9, 15, 9, 8, 15],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 9, 17, 8, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            HeavilyArmored,
                        ],
                        myBasePointBuyArray: [14, 9, 15, 8, 9, 15],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 8, 17, 10, 8, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            HeavilyArmored,
                        ],
                        myBasePointBuyArray: [14, 8, 15, 10, 8, 15],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 8, 17, 9, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            HeavilyArmored,
                        ],
                        myBasePointBuyArray: [14, 8, 15, 9, 9, 15],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [15, 8, 17, 8, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            HeavilyArmored,
                        ],
                        myBasePointBuyArray: [14, 8, 15, 8, 10, 15],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 12, 17, 8, 8, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            HeavilyArmored,
                        ],
                        myBasePointBuyArray: [13, 12, 15, 8, 8, 15],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 11, 17, 9, 8, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            HeavilyArmored,
                        ],
                        myBasePointBuyArray: [13, 11, 15, 9, 8, 15],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 11, 17, 8, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            HeavilyArmored,
                        ],
                        myBasePointBuyArray: [13, 11, 15, 8, 9, 15],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 10, 17, 10, 8, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            HeavilyArmored,
                        ],
                        myBasePointBuyArray: [13, 10, 15, 10, 8, 15],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 10, 17, 9, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            HeavilyArmored,
                        ],
                        myBasePointBuyArray: [13, 10, 15, 9, 9, 15],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 10, 17, 8, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            HeavilyArmored,
                        ],
                        myBasePointBuyArray: [13, 10, 15, 8, 10, 15],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 9, 17, 11, 8, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            HeavilyArmored,
                        ],
                        myBasePointBuyArray: [13, 9, 15, 11, 8, 15],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 9, 17, 10, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            HeavilyArmored,
                        ],
                        myBasePointBuyArray: [13, 9, 15, 10, 9, 15],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 9, 17, 9, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            HeavilyArmored,
                        ],
                        myBasePointBuyArray: [13, 9, 15, 9, 10, 15],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 9, 17, 8, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            HeavilyArmored,
                        ],
                        myBasePointBuyArray: [13, 9, 15, 8, 11, 15],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 8, 17, 12, 8, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            HeavilyArmored,
                        ],
                        myBasePointBuyArray: [13, 8, 15, 12, 8, 15],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 8, 17, 11, 9, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            HeavilyArmored,
                        ],
                        myBasePointBuyArray: [13, 8, 15, 11, 9, 15],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 8, 17, 10, 10, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            HeavilyArmored,
                        ],
                        myBasePointBuyArray: [13, 8, 15, 10, 10, 15],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 8, 17, 9, 11, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            HeavilyArmored,
                        ],
                        myBasePointBuyArray: [13, 8, 15, 9, 11, 15],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
        ];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

