use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test33" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Weighted,
        statOrdering: [
            Charisma,
            Intelligence,
            Constitution,
            Strength,
            Wisdom,
            Dexterity,
        ],
        isReversed: false,
        availableRaces: vec![
            Aarakocra,
            Aasimar_Protector,
            CustomLineage,
            Changling,
            Dwarf_MarkOfWarding,
            Dwarf_Mountain,
            Elf_Drow,
            Elf_Wood,
            Firbolg,
            Genasi_Air,
            Genasi_Earth,
            Genasi_Water,
            Gnome_MarkOfScribing,
            Goliath,
            HalfElf_MarkOfDetection,
            Halfling_Stout,
            HalfOrc_MarkOfFinding,
            Hobgoblin,
            Human,
            Human_MarkOfFinding,
            Human_MarkOfHandling,
            Human_Variant,
            Kenku,
            Orc,
            Orc_Eberron,
            Shifter_SwiftStride,
            Tiefling_Feral,
        ],
        requiredFeats: vec![
            Skulker,
        ],
        constraints: vec![
            BinCmp_StatValue_StatValue(
                BinaryCompare {
                    lhsValue: GetStatValue(
                        Charisma,
                    ),
                    binCmp: LT,
                    rhsValue: GetStatValue(
                        Constitution,
                    ),
                },
            ),
            UnCmp_StatValue(
                UnaryCompare {
                    value: GetStatValue(
                        Dexterity,
                    ),
                    unCmp: IsOdd,
                },
            ),
            UnCmp_StatModifier(
                UnaryCompare {
                    value: GetStatModifier(
                        Strength,
                    ),
                    unCmp: IsMin,
                },
            ),
        ],
        numberOfASIs: 2,
        doAllRacesHaveFreeFeat: true,
        canRearrangeRaceModifiers: true,
        maxNumberOfResults: 18,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 36161862,
            numUniqueCombinationsPassedConstraints: 1940472,
            numStatBlocksEjected: 355,
            numUniqueCombinationsEjected: 7047,
            numUniqueCombinationsBelowLowerBound: 1940097,
            numStatBlocksShiftedDuringInsertion: 3473,
            numDuplicateRaceStatBlocksRemoved: 0,
            numUniqueStatBlockAfterRacePass: 187,
            numUniqueStatBlockAfterFeatPass: 213,
            numFinalStatBlockCombinations: 18,
        };

    let expectedStatBlocks = 
        vec![
            RankedStatBlock {
                rank: 1101003,
                statBlock: [9, 9, 20, 14, 8, 18],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Dwarf_Mountain,
                        myFeats: vec![
                            Skulker,
                        ],
                        myBasePointBuyArray: [9, 9, 15, 14, 8, 15],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 2],
                        myASIs:              [0, 0, 3, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1101003,
                statBlock: [8, 9, 20, 14, 9, 18],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Dwarf_Mountain,
                        myFeats: vec![
                            Skulker,
                        ],
                        myBasePointBuyArray: [8, 9, 15, 14, 9, 15],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 2],
                        myASIs:              [0, 0, 3, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1101003,
                statBlock: [8, 9, 20, 9, 14, 18],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Dwarf_Mountain,
                        myFeats: vec![
                            Skulker,
                        ],
                        myBasePointBuyArray: [8, 9, 15, 9, 14, 15],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 2],
                        myASIs:              [0, 0, 3, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1101003,
                statBlock: [9, 9, 20, 8, 14, 18],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Dwarf_Mountain,
                        myFeats: vec![
                            Skulker,
                        ],
                        myBasePointBuyArray: [9, 9, 15, 8, 14, 15],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 2],
                        myASIs:              [0, 0, 3, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1101003,
                statBlock: [8, 15, 20, 8, 8, 18],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Dwarf_Mountain,
                        myFeats: vec![
                            Skulker,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 8, 8, 15],
                        myRaceModifiers:     [0, 0, 2, 0, 0, 2],
                        myASIs:              [0, 0, 3, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1101003,
                statBlock: [9, 9, 18, 20, 8, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Dwarf_Mountain,
                        myFeats: vec![
                            Skulker,
                        ],
                        myBasePointBuyArray: [9, 9, 15, 15, 8, 14],
                        myRaceModifiers:     [0, 0, 2, 2, 0, 0],
                        myASIs:              [0, 0, 1, 3, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1101003,
                statBlock: [8, 9, 18, 20, 9, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Dwarf_Mountain,
                        myFeats: vec![
                            Skulker,
                        ],
                        myBasePointBuyArray: [8, 9, 15, 15, 9, 14],
                        myRaceModifiers:     [0, 0, 2, 2, 0, 0],
                        myASIs:              [0, 0, 1, 3, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1101003,
                statBlock: [9, 9, 20, 18, 8, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Dwarf_Mountain,
                        myFeats: vec![
                            Skulker,
                        ],
                        myBasePointBuyArray: [9, 9, 15, 15, 8, 14],
                        myRaceModifiers:     [0, 0, 2, 2, 0, 0],
                        myASIs:              [0, 0, 3, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1101003,
                statBlock: [8, 9, 20, 18, 9, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Dwarf_Mountain,
                        myFeats: vec![
                            Skulker,
                        ],
                        myBasePointBuyArray: [8, 9, 15, 15, 9, 14],
                        myRaceModifiers:     [0, 0, 2, 2, 0, 0],
                        myASIs:              [0, 0, 3, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1101003,
                statBlock: [8, 9, 20, 9, 18, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Dwarf_Mountain,
                        myFeats: vec![
                            Skulker,
                        ],
                        myBasePointBuyArray: [8, 9, 15, 9, 15, 14],
                        myRaceModifiers:     [0, 0, 2, 0, 2, 0],
                        myASIs:              [0, 0, 3, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1101003,
                statBlock: [8, 9, 18, 9, 20, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Dwarf_Mountain,
                        myFeats: vec![
                            Skulker,
                        ],
                        myBasePointBuyArray: [8, 9, 15, 9, 15, 14],
                        myRaceModifiers:     [0, 0, 2, 0, 2, 0],
                        myASIs:              [0, 0, 1, 0, 3, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1101003,
                statBlock: [9, 9, 20, 8, 18, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Dwarf_Mountain,
                        myFeats: vec![
                            Skulker,
                        ],
                        myBasePointBuyArray: [9, 9, 15, 8, 15, 14],
                        myRaceModifiers:     [0, 0, 2, 0, 2, 0],
                        myASIs:              [0, 0, 3, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1101003,
                statBlock: [9, 9, 18, 8, 20, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Dwarf_Mountain,
                        myFeats: vec![
                            Skulker,
                        ],
                        myBasePointBuyArray: [9, 9, 15, 8, 15, 14],
                        myRaceModifiers:     [0, 0, 2, 0, 2, 0],
                        myASIs:              [0, 0, 1, 0, 3, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1101003,
                statBlock: [8, 9, 18, 20, 14, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Dwarf_Mountain,
                        myFeats: vec![
                            Skulker,
                        ],
                        myBasePointBuyArray: [8, 9, 15, 15, 14, 9],
                        myRaceModifiers:     [0, 0, 2, 2, 0, 0],
                        myASIs:              [0, 0, 1, 3, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1101003,
                statBlock: [8, 9, 14, 20, 18, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Dwarf_Mountain,
                        myFeats: vec![
                            Skulker,
                        ],
                        myBasePointBuyArray: [8, 9, 14, 15, 15, 9],
                        myRaceModifiers:     [0, 0, 0, 2, 2, 0],
                        myASIs:              [0, 0, 0, 3, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1101003,
                statBlock: [8, 9, 20, 18, 14, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Dwarf_Mountain,
                        myFeats: vec![
                            Skulker,
                        ],
                        myBasePointBuyArray: [8, 9, 15, 15, 14, 9],
                        myRaceModifiers:     [0, 0, 2, 2, 0, 0],
                        myASIs:              [0, 0, 3, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1101003,
                statBlock: [8, 9, 14, 18, 20, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Dwarf_Mountain,
                        myFeats: vec![
                            Skulker,
                        ],
                        myBasePointBuyArray: [8, 9, 14, 15, 15, 9],
                        myRaceModifiers:     [0, 0, 0, 2, 2, 0],
                        myASIs:              [0, 0, 0, 1, 3, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 1101003,
                statBlock: [8, 9, 20, 14, 18, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Dwarf_Mountain,
                        myFeats: vec![
                            Skulker,
                        ],
                        myBasePointBuyArray: [8, 9, 15, 14, 15, 9],
                        myRaceModifiers:     [0, 0, 2, 0, 2, 0],
                        myASIs:              [0, 0, 3, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
        ];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

