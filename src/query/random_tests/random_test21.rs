use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test21" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Flat,
        statOrdering: [
            Charisma,
            Wisdom,
            Intelligence,
            Constitution,
            Dexterity,
            Strength,
        ],
        isReversed: true,
        availableRaces: vec![
            Aarakocra,
            Aasimar_Fallen,
            Aasimar_Protector,
            Aasimar_Scourge,
            Bugbear,
            CustomLineage,
            Changling,
            DragonBorn,
            Dwarf_Grey,
            Dwarf_MarkOfWarding,
            Dwarf_Mountain,
            Elf_High,
            Elf_Wood,
            Genasi_Earth,
            Genasi_Fire,
            Genasi_Water,
            Gnome_Deep,
            Gnome_MarkOfScribing,
            Gnome_Rock,
            Goblin,
            Goliath,
            HalfElf,
            HalfElf_MarkOfStorm,
            Halfling_Ghostwise,
            Halfling_Lightfoot,
            Halfling_MarkOfHealing,
            Halfling_MarkOfHospitality,
            Halfling_Stout,
            HalfOrc,
            HalfOrc_MarkOfFinding,
            Human,
            Human_MarkOfFinding,
            Human_MarkOfHandling,
            Human_MarkOfMaking,
            Human_MarkOfPassage,
            Human_MarkOfSentinel,
            Human_Variant,
            Kalashtar,
            Kenku,
            Kobold,
            Lizardfolk,
            Orc,
            Orc_Eberron,
            Shifter_BeastHide,
            Shifter_LongTooth,
            Shifter_SwiftStride,
            Shifter_WildHunt,
            Tabaxi,
            Tiefling,
            Tiefling_Feral,
            Tortle,
            Triton,
            Warforged,
            YuanTiPureblood,
        ],
        requiredFeats: vec![],
        constraints: vec![],
        numberOfASIs: 1,
        doAllRacesHaveFreeFeat: false,
        canRearrangeRaceModifiers: false,
        maxNumberOfResults: 19,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 5917093,
            numUniqueCombinationsPassedConstraints: 5917093,
            numStatBlocksEjected: 313,
            numUniqueCombinationsEjected: 2008,
            numUniqueCombinationsBelowLowerBound: 5916760,
            numStatBlocksShiftedDuringInsertion: 2677,
            numDuplicateRaceStatBlocksRemoved: 0,
            numUniqueStatBlockAfterRacePass: 65,
            numUniqueStatBlockAfterFeatPass: 91,
            numFinalStatBlockCombinations: 19,
        };

    let expectedStatBlocks = 
        vec![
            RankedStatBlock {
                rank: 3,
                statBlock: [13, 17, 15, 9, 9, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 15, 15, 8, 8, 8],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 1, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [13, 17, 9, 15, 9, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 15, 8, 15, 8, 8],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 1, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [13, 11, 15, 15, 9, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 8, 15, 15, 8, 8],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 1, 0, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [7, 17, 15, 15, 9, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [1, 0, 0, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [13, 17, 9, 9, 15, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 15, 8, 8, 15, 8],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 1, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [13, 11, 15, 9, 15, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 8, 15, 8, 15, 8],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 1, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [7, 17, 15, 9, 15, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 8, 15, 8],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [1, 0, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [13, 11, 9, 15, 15, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 8, 8, 15, 15, 8],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 1, 1, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [7, 17, 9, 15, 15, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 8, 15, 15, 8],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [1, 0, 1, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [7, 11, 15, 15, 15, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 8, 15, 15, 15, 8],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [1, 1, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [13, 17, 15, 9, 8, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 15, 15, 8, 8, 8],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [13, 17, 9, 15, 8, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 15, 8, 15, 8, 8],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 1, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [13, 11, 15, 15, 8, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 8, 15, 15, 8, 8],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 1, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [7, 17, 15, 15, 8, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [8, 15, 15, 15, 8, 8],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [1, 0, 0, 0, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [13, 17, 15, 8, 9, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 15, 15, 8, 8, 8],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 1, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [13, 17, 15, 9, 9, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [14, 15, 15, 8, 9, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [1, 0, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [17, 15, 9, 13, 9, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Orc,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 15, 8, 15, 8, 8],
                        myRaceModifiers:     [2, 0, 1, -2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 1, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [13, 17, 8, 15, 9, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [15, 15, 8, 15, 8, 8],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 1, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 3,
                statBlock: [13, 17, 9, 15, 9, 9],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Kobold,
                        myFeats: vec![],
                        myBasePointBuyArray: [14, 15, 8, 15, 9, 9],
                        myRaceModifiers:     [-2, 2, 0, 0, 0, 0],
                        myASIs:              [1, 0, 1, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
        ];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

