use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test44" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Unranked,
        statOrdering: [
            Constitution,
            Intelligence,
            Charisma,
            Dexterity,
            Strength,
            Wisdom,
        ],
        isReversed: true,
        availableRaces: vec![
            Aasimar_Fallen,
            Aasimar_Scourge,
            Dwarf_Grey,
            Elf_Wood,
            Genasi_Earth,
            Gnome_MarkOfScribing,
            Halfling_Lightfoot,
            Halfling_MarkOfHospitality,
            Human_MarkOfPassage,
            Human_Variant,
            Kenku,
            Lizardfolk,
            Orc,
            Orc_Eberron,
            Tabaxi,
            Tiefling,
            Tiefling_Feral,
            Triton,
            YuanTiPureblood,
        ],
        requiredFeats: vec![
            BountifulLuck,
        ],
        constraints: vec![
            UnCmp_StatValue(
                UnaryCompare {
                    value: GetStatValue(
                        Constitution,
                    ),
                    unCmp: IsMax,
                },
            ),
            BinCmp_StatValue_Constant(
                BinaryCompare {
                    lhsValue: GetStatValue(
                        Wisdom,
                    ),
                    binCmp: LE,
                    rhsValue: GetConstant(
                        14,
                    ),
                },
            ),
            UnCmp_StatValue(
                UnaryCompare {
                    value: GetStatValue(
                        Wisdom,
                    ),
                    unCmp: IsOdd,
                },
            ),
            BinCmp_StatValue_Constant(
                BinaryCompare {
                    lhsValue: GetStatValue(
                        Dexterity,
                    ),
                    binCmp: LE,
                    rhsValue: GetConstant(
                        12,
                    ),
                },
            ),
        ],
        numberOfASIs: 1,
        doAllRacesHaveFreeFeat: false,
        canRearrangeRaceModifiers: true,
        maxNumberOfResults: 23,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 368460,
            numUniqueCombinationsPassedConstraints: 16526,
            numStatBlocksEjected: 148,
            numUniqueCombinationsEjected: 322,
            numUniqueCombinationsBelowLowerBound: 16112,
            numStatBlocksShiftedDuringInsertion: 1986,
            numDuplicateRaceStatBlocksRemoved: 460,
            numUniqueStatBlockAfterRacePass: 185,
            numUniqueStatBlockAfterFeatPass: 30,
            numFinalStatBlockCombinations: 46,
        };

    let expectedStatBlocks = 
        vec![
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 12, 14, 8, 13, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [14, 10, 14, 8, 12, 14],
                        myRaceModifiers:     [0, 2, 0, 0, 1, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHospitality,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [14, 10, 14, 8, 12, 14],
                        myRaceModifiers:     [0, 2, 0, 0, 1, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 11, 14, 9, 13, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [14, 9, 14, 8, 13, 14],
                        myRaceModifiers:     [0, 2, 0, 1, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHospitality,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [14, 9, 14, 8, 13, 14],
                        myRaceModifiers:     [0, 2, 0, 1, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 12, 14, 9, 13, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [12, 11, 14, 9, 13, 14],
                        myRaceModifiers:     [2, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHospitality,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [12, 11, 14, 9, 13, 14],
                        myRaceModifiers:     [2, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 12, 14, 10, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [14, 10, 14, 9, 13, 13],
                        myRaceModifiers:     [0, 2, 0, 1, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHospitality,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [14, 10, 14, 9, 13, 13],
                        myRaceModifiers:     [0, 2, 0, 1, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 10, 14, 10, 13, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [14, 8, 14, 9, 13, 14],
                        myRaceModifiers:     [0, 2, 0, 1, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHospitality,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [14, 8, 14, 9, 13, 14],
                        myRaceModifiers:     [0, 2, 0, 1, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 11, 14, 10, 13, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [12, 10, 14, 10, 13, 14],
                        myRaceModifiers:     [2, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHospitality,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [12, 10, 14, 10, 13, 14],
                        myRaceModifiers:     [2, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 12, 14, 10, 13, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [11, 11, 14, 10, 13, 14],
                        myRaceModifiers:     [2, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHospitality,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [11, 11, 14, 10, 13, 14],
                        myRaceModifiers:     [2, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 12, 14, 10, 11, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [14, 10, 14, 9, 11, 14],
                        myRaceModifiers:     [0, 2, 0, 1, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHospitality,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [14, 10, 14, 9, 11, 14],
                        myRaceModifiers:     [0, 2, 0, 1, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 12, 14, 10, 13, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [12, 12, 13, 10, 13, 14],
                        myRaceModifiers:     [2, 0, 1, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHospitality,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [12, 12, 13, 10, 13, 14],
                        myRaceModifiers:     [2, 0, 1, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 12, 14, 11, 13, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [14, 10, 14, 10, 13, 12],
                        myRaceModifiers:     [0, 2, 0, 1, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHospitality,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [14, 10, 14, 10, 13, 12],
                        myRaceModifiers:     [0, 2, 0, 1, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 11, 14, 11, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [14, 9, 14, 10, 13, 13],
                        myRaceModifiers:     [0, 2, 0, 1, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHospitality,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [14, 9, 14, 10, 13, 13],
                        myRaceModifiers:     [0, 2, 0, 1, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 12, 14, 11, 13, 13],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [12, 11, 14, 11, 13, 13],
                        myRaceModifiers:     [2, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHospitality,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [12, 11, 14, 11, 13, 13],
                        myRaceModifiers:     [2, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 9, 14, 11, 13, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [14, 8, 14, 9, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHospitality,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [14, 8, 14, 9, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 10, 14, 11, 13, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [12, 9, 14, 11, 13, 14],
                        myRaceModifiers:     [2, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHospitality,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [12, 9, 14, 11, 13, 14],
                        myRaceModifiers:     [2, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 11, 14, 11, 13, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [11, 10, 14, 11, 13, 14],
                        myRaceModifiers:     [2, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHospitality,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [11, 10, 14, 11, 13, 14],
                        myRaceModifiers:     [2, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 11, 14, 11, 11, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [14, 9, 14, 10, 11, 14],
                        myRaceModifiers:     [0, 2, 0, 1, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHospitality,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [14, 9, 14, 10, 11, 14],
                        myRaceModifiers:     [0, 2, 0, 1, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 11, 14, 11, 13, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [12, 11, 13, 11, 13, 14],
                        myRaceModifiers:     [2, 0, 1, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHospitality,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [12, 11, 13, 11, 13, 14],
                        myRaceModifiers:     [2, 0, 1, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [12, 12, 14, 11, 13, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [10, 11, 14, 11, 13, 14],
                        myRaceModifiers:     [2, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHospitality,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [10, 11, 14, 11, 13, 14],
                        myRaceModifiers:     [2, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [13, 12, 14, 11, 13, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [11, 12, 13, 11, 13, 14],
                        myRaceModifiers:     [2, 0, 1, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHospitality,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [11, 12, 13, 11, 13, 14],
                        myRaceModifiers:     [2, 0, 1, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 12, 14, 11, 11, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [12, 11, 14, 11, 11, 14],
                        myRaceModifiers:     [2, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHospitality,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [12, 11, 14, 11, 11, 14],
                        myRaceModifiers:     [2, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 12, 14, 12, 13, 11],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [14, 10, 14, 11, 13, 11],
                        myRaceModifiers:     [0, 2, 0, 1, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHospitality,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [14, 10, 14, 11, 13, 11],
                        myRaceModifiers:     [0, 2, 0, 1, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 11, 14, 12, 13, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [14, 9, 14, 11, 13, 12],
                        myRaceModifiers:     [0, 2, 0, 1, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHospitality,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [14, 9, 14, 11, 13, 12],
                        myRaceModifiers:     [0, 2, 0, 1, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 0,
                statBlock: [14, 12, 14, 12, 13, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Halfling_Lightfoot,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [12, 11, 14, 12, 13, 12],
                        myRaceModifiers:     [2, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: Halfling_MarkOfHospitality,
                        myFeats: vec![
                            BountifulLuck,
                        ],
                        myBasePointBuyArray: [12, 11, 14, 12, 13, 12],
                        myRaceModifiers:     [2, 1, 0, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
        ];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

