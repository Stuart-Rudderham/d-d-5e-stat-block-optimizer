#![allow(non_snake_case)]
#![allow(deprecated)]
#![allow(non_camel_case_types)]

use crate::dnd::race::*;
use crate::dnd::feat::*;

use crate::constraint::*;

use std::fs::File;
use std::fs;
use std::io::prelude::*;
use std::path::Path;
use std::str;

use super::Parse_Constraint;
use super::Parse_CharacterRace;
use super::Parse_Feat;
use super::FormatErrorMessage;

use nom;

fn ParseFromFile<T, F>(ParsingFn : F, aPath : &Path, someDefaultText : &str) -> Result<Vec<T>, String>
    where F : Fn(&[u8]) -> nom::IResult<&[u8], T>
{
    // Could remove all the ".map_err(|e| e.to_string())" by having the result error
    // be Box<Error> and making sure that all errors actually implement the Error trait.
    //
    // http://blog.burntsushi.net/rust-error-handling/#composing-custom-error-types

    // If the file doesn't exist then we have to create it first.
    if fs::metadata(aPath).is_err()
    {
        // Create any required folders along the path.
        if let Some(rootPath) = aPath.parent()
        {
            fs::create_dir_all(rootPath).map_err( |e| format!("Error creating {} ({})", aPath.to_str().unwrap(), e.to_string()) )?;
        }

        // Create the file.
        let mut tryCreateFile = File::create(aPath).map_err( |e| format!("Error creating {} ({})", aPath.to_str().unwrap(), e.to_string()) )?;

        // Write the data to it.
        write!(&mut tryCreateFile, "{}", someDefaultText).map_err( |e| format!("Error writing default data to {} ({})", aPath.to_str().unwrap(), e.to_string()) )?;

        // Then flush and close the file.
    }

    // Open the file.
    let mut file = File::open(aPath).map_err( |e| format!("Error opening {} ({})", aPath.to_str().unwrap(), e.to_string()) )?;

    // Read it all into memory.
    let mut fileContents = String::new();
    file.read_to_string(&mut fileContents).map_err( |e| format!("Error reading {} ({})", aPath.to_str().unwrap(), e.to_string()) )?;

    // And parse the data.
    ParseFromString( ParsingFn, fileContents.as_str() ).map_err( |e| format!("Error reading {}\n{}", aPath.to_str().unwrap(), e.to_string()) )
}

fn ParseFromString<T, F>(ParsingFn : F, aString : &str) -> Result<Vec<T>, String>
    where F : Fn(&[u8]) -> nom::IResult<&[u8], T>
{
    let mut results = vec![];

    for (index, line) in aString.lines().enumerate()
    {
        if line.is_empty() || line.starts_with("//")
        {
            continue;
        }

        let parsingResult = ParsingFn( line.as_bytes() );

        if let nom::IResult::Done(remaining, value) = parsingResult
        {
            // If we have leftover input one of the parsers is probably incorrect.
            assert_eq!( remaining, &b""[..] );

            results.push( value );
        }
        else
        {
            // Use "index + 1" because files start at line 1, not line 0.
            return Err( format!("Parsing error on line {}.\n{}", index + 1, FormatErrorMessage(line.as_bytes(), &parsingResult)) );
        }
    }

    Ok(results)
}

pub fn ReadConstraints( aPath : &Path ) -> Result<Vec<Constraint>, String>
{
    let defaultText =
"\
// This is the file where all the stat block constraints are specified.
//
// Lines that start with \"//\" are ignored as comments.
// Blank lines are also ignored.

// There are a two different types of constraints.
//
// 1) Constraints involving two values
//     The syntax is [Value] [Binary Operator] [Value]
//
//     The possible binary operators are:
//
//         [ == | != | > | >= | < | <= ]
//
// 2) Constraints involving one value
//     The syntax is [Value] [Unary Operator]
//
//     The possible unary operators are:
//
//         [ is even | is odd | is max | is min ]

// The possible Values for either type of constraint are
//
// 1) Stat Value
//     The possible values are:
//
//         [ str | dex | con | int | wis | cha ]
//
// 2) Stat Modifier
//     The possible values are:
//
//         [ strmod | dexmod | conmod | intmod | wismod | chamod ]
//
// 3) Constant
//     The possible values range from -128 to 127, inclusive

// Some example constraints are below.

str is max
dexmod <= 1
con > 12
int == wis
chamod is odd
";

    ParseFromFile( Parse_Constraint, aPath, defaultText )
}

pub fn ReadRaces( aPath : &Path ) -> Result<Vec<CharacterRace>, String>
{
    let mut defaultText = String::new();

    defaultText.push_str
    (
"\
// This is the file where all the allowed character races are specified.
//
// Lines that start with \"//\" are ignored as comments.
// Blank lines are also ignored.
//
// The list of all possible races is below.

"
    );

    for race in RaceList.iter()
    {
        defaultText.push_str( &format!("{:?}\n", race)[..] );
    }

    ParseFromFile( Parse_CharacterRace, aPath, &defaultText[..] )
}

pub fn ReadFeats( aPath : &Path ) -> Result<Vec<Feat>, String>
{
    let mut defaultText = String::new();

    defaultText.push_str
    (
"\
// This is the file where all the *required* feats specified.
//
// Lines that start with \"//\" are ignored as comments.
// Blank lines are also ignored.
//
// The list of all possible feats is below.

"
    );

    for feat in FeatList.iter()
    {
        defaultText.push_str( &format!("//{:?}\n", feat)[..] );
    }

    ParseFromFile( Parse_Feat, aPath, &defaultText[..] )
}
