#![allow(non_snake_case)]
#![allow(deprecated)]
#![allow(non_camel_case_types)]

use crate::dnd::statblock::*;

pub mod compare_binary;
use self::compare_binary::*;

pub mod compare_unary;
use self::compare_unary::*;

pub mod value;
use self::value::*;

#[derive(Debug, Eq, PartialEq, Copy, Clone)]
pub enum Constraint
{
    BinCmp_StatValue_Constant                              ( BinaryCompare   < GetStatValue                  , GetConstant               > ), // Basic binary compares.
    BinCmp_StatModifier_Constant                           ( BinaryCompare   < GetStatModifier               , GetConstant               > ),
    BinCmp_StatValue_StatValue                             ( BinaryCompare   < GetStatValue                  , GetStatValue              > ),
    BinCmp_StatModifier_StatModifier                       ( BinaryCompare   < GetStatModifier               , GetStatModifier           > ),
  //BinCmp_MetaStatValueBinCmp_Constant                    ( BinaryCompare   < GetMetaStatValueBinCmp        , GetConstant               > ), // Meta binary compares.
  //BinCmp_MetaStatValueUnCmp_Constant                     ( BinaryCompare   < GetMetaStatValueUnCmp         , GetConstant               > ),
  //BinCmp_MetaStatModifierBinCmp_Constant                 ( BinaryCompare   < GetMetaStatModifierBinCmp     , GetConstant               > ),
  //BinCmp_MetaStatModifierUnCmp_Constant                  ( BinaryCompare   < GetMetaStatModifierUnCmp      , GetConstant               > ),
  //BinCmp_MetaStatValueBinCmp_MetaStatValueBinCmp         ( BinaryCompare   < GetMetaStatValueBinCmp        , GetMetaStatValueBinCmp    > ),
  //BinCmp_MetaStatValueUnCmp_MetaStatValueBinCmp          ( BinaryCompare   < GetMetaStatValueUnCmp         , GetMetaStatValueBinCmp    > ),
  //BinCmp_MetaStatModifierBinCmp_MetaStatValueBinCmp      ( BinaryCompare   < GetMetaStatModifierBinCmp     , GetMetaStatValueBinCmp    > ),
  //BinCmp_MetaStatModifierUnCmp_MetaStatValueBinCmp       ( BinaryCompare   < GetMetaStatModifierUnCmp      , GetMetaStatValueBinCmp    > ),
  //BinCmp_MetaStatValueBinCmp_MetaStatValueUnCmp          ( BinaryCompare   < GetMetaStatValueBinCmp        , GetMetaStatValueUnCmp     > ),
  //BinCmp_MetaStatValueUnCmp_MetaStatValueUnCmp           ( BinaryCompare   < GetMetaStatValueUnCmp         , GetMetaStatValueUnCmp     > ),
  //BinCmp_MetaStatModifierBinCmp_MetaStatValueUnCmp       ( BinaryCompare   < GetMetaStatModifierBinCmp     , GetMetaStatValueUnCmp     > ),
  //BinCmp_MetaStatModifierUnCmp_MetaStatValueUnCmp        ( BinaryCompare   < GetMetaStatModifierUnCmp      , GetMetaStatValueUnCmp     > ),
  //BinCmp_MetaStatValueBinCmp_MetaStatModifierBinCmp      ( BinaryCompare   < GetMetaStatValueBinCmp        , GetMetaStatModifierBinCmp > ),
  //BinCmp_MetaStatValueUnCmp_MetaStatModifierBinCmp       ( BinaryCompare   < GetMetaStatValueUnCmp         , GetMetaStatModifierBinCmp > ),
  //BinCmp_MetaStatModifierBinCmp_MetaStatModifierBinCmp   ( BinaryCompare   < GetMetaStatModifierBinCmp     , GetMetaStatModifierBinCmp > ),
  //BinCmp_MetaStatModifierUnCmp_MetaStatModifierBinCmp    ( BinaryCompare   < GetMetaStatModifierUnCmp      , GetMetaStatModifierBinCmp > ),
  //BinCmp_MetaStatValueBinCmp_MetaStatModifierUnCmp       ( BinaryCompare   < GetMetaStatValueBinCmp        , GetMetaStatModifierUnCmp  > ),
  //BinCmp_MetaStatValueUnCmp_MetaStatModifierUnCmp        ( BinaryCompare   < GetMetaStatValueUnCmp         , GetMetaStatModifierUnCmp  > ),
  //BinCmp_MetaStatModifierBinCmp_MetaStatModifierUnCmp    ( BinaryCompare   < GetMetaStatModifierBinCmp     , GetMetaStatModifierUnCmp  > ),
  //BinCmp_MetaStatModifierUnCmp_MetaStatModifierUnCmp     ( BinaryCompare   < GetMetaStatModifierUnCmp      , GetMetaStatModifierUnCmp  > ),
    UnCmp_StatValue                                        ( UnaryCompare    < GetStatValue                                              > ), // Basic unary compares.
    UnCmp_StatModifier                                     ( UnaryCompare    < GetStatModifier                                           > ),
  //UnCmp_MetaStatValueBinCmp                              ( UnaryCompare    < GetMetaStatValueBinCmp                                    > ), // Meta unary compares.
  //UnCmp_MetaStatValueUnCmp                               ( UnaryCompare    < GetMetaStatValueUnCmp                                     > ),
  //UnCmp_MetaStatModifierBinCmp                           ( UnaryCompare    < GetMetaStatModifierBinCmp                                 > ),
  //UnCmp_MetaStatModifierUnCmp                            ( UnaryCompare    < GetMetaStatModifierUnCmp                                  > ),
}

pub fn IsStatBlockValid(aConstraint : Constraint, aStatBlock : StatBlock ) -> bool
{
    match aConstraint
    {
        Constraint::BinCmp_StatValue_Constant                              (ref c) => c.IsStatBlockValid(aStatBlock),
        Constraint::BinCmp_StatModifier_Constant                           (ref c) => c.IsStatBlockValid(aStatBlock),
        Constraint::BinCmp_StatValue_StatValue                             (ref c) => c.IsStatBlockValid(aStatBlock),
        Constraint::BinCmp_StatModifier_StatModifier                       (ref c) => c.IsStatBlockValid(aStatBlock),
      //Constraint::BinCmp_MetaStatValueBinCmp_Constant                    (ref c) => c.IsStatBlockValid(aStatBlock),
      //Constraint::BinCmp_MetaStatValueUnCmp_Constant                     (ref c) => c.IsStatBlockValid(aStatBlock),
      //Constraint::BinCmp_MetaStatModifierBinCmp_Constant                 (ref c) => c.IsStatBlockValid(aStatBlock),
      //Constraint::BinCmp_MetaStatModifierUnCmp_Constant                  (ref c) => c.IsStatBlockValid(aStatBlock),
      //Constraint::BinCmp_MetaStatValueBinCmp_MetaStatValueBinCmp         (ref c) => c.IsStatBlockValid(aStatBlock),
      //Constraint::BinCmp_MetaStatValueUnCmp_MetaStatValueBinCmp          (ref c) => c.IsStatBlockValid(aStatBlock),
      //Constraint::BinCmp_MetaStatModifierBinCmp_MetaStatValueBinCmp      (ref c) => c.IsStatBlockValid(aStatBlock),
      //Constraint::BinCmp_MetaStatModifierUnCmp_MetaStatValueBinCmp       (ref c) => c.IsStatBlockValid(aStatBlock),
      //Constraint::BinCmp_MetaStatValueBinCmp_MetaStatValueUnCmp          (ref c) => c.IsStatBlockValid(aStatBlock),
      //Constraint::BinCmp_MetaStatValueUnCmp_MetaStatValueUnCmp           (ref c) => c.IsStatBlockValid(aStatBlock),
      //Constraint::BinCmp_MetaStatModifierBinCmp_MetaStatValueUnCmp       (ref c) => c.IsStatBlockValid(aStatBlock),
      //Constraint::BinCmp_MetaStatModifierUnCmp_MetaStatValueUnCmp        (ref c) => c.IsStatBlockValid(aStatBlock),
      //Constraint::BinCmp_MetaStatValueBinCmp_MetaStatModifierBinCmp      (ref c) => c.IsStatBlockValid(aStatBlock),
      //Constraint::BinCmp_MetaStatValueUnCmp_MetaStatModifierBinCmp       (ref c) => c.IsStatBlockValid(aStatBlock),
      //Constraint::BinCmp_MetaStatModifierBinCmp_MetaStatModifierBinCmp   (ref c) => c.IsStatBlockValid(aStatBlock),
      //Constraint::BinCmp_MetaStatModifierUnCmp_MetaStatModifierBinCmp    (ref c) => c.IsStatBlockValid(aStatBlock),
      //Constraint::BinCmp_MetaStatValueBinCmp_MetaStatModifierUnCmp       (ref c) => c.IsStatBlockValid(aStatBlock),
      //Constraint::BinCmp_MetaStatValueUnCmp_MetaStatModifierUnCmp        (ref c) => c.IsStatBlockValid(aStatBlock),
      //Constraint::BinCmp_MetaStatModifierBinCmp_MetaStatModifierUnCmp    (ref c) => c.IsStatBlockValid(aStatBlock),
      //Constraint::BinCmp_MetaStatModifierUnCmp_MetaStatModifierUnCmp     (ref c) => c.IsStatBlockValid(aStatBlock),
        Constraint::UnCmp_StatValue                                        (ref c) => c.IsStatBlockValid(aStatBlock),
        Constraint::UnCmp_StatModifier                                     (ref c) => c.IsStatBlockValid(aStatBlock),
      //Constraint::UnCmp_MetaStatValueBinCmp                              (ref c) => c.IsStatBlockValid(aStatBlock),
      //Constraint::UnCmp_MetaStatValueUnCmp                               (ref c) => c.IsStatBlockValid(aStatBlock),
      //Constraint::UnCmp_MetaStatModifierBinCmp                           (ref c) => c.IsStatBlockValid(aStatBlock),
      //Constraint::UnCmp_MetaStatModifierUnCmp                            (ref c) => c.IsStatBlockValid(aStatBlock),
    }
}

#[test]
fn TestConstraints()
{
    use crate::dnd::stat::*;

    // BinCmp_StatValue_Constant
    assert!( IsStatBlockValid(Constraint::BinCmp_StatValue_Constant( BinaryCompare::New( GetStatValue(Stat::Strength    ), BinCmp::EQ, GetConstant(15)) ), [15, 14, 13, 12, 11, 10]) == true  );
    assert!( IsStatBlockValid(Constraint::BinCmp_StatValue_Constant( BinaryCompare::New( GetStatValue(Stat::Dexterity   ), BinCmp::NE, GetConstant(14)) ), [15, 14, 13, 12, 11, 10]) == false );
    assert!( IsStatBlockValid(Constraint::BinCmp_StatValue_Constant( BinaryCompare::New( GetStatValue(Stat::Constitution), BinCmp::GT, GetConstant(12)) ), [15, 14, 13, 12, 11, 10]) == true  );
    assert!( IsStatBlockValid(Constraint::BinCmp_StatValue_Constant( BinaryCompare::New( GetStatValue(Stat::Intelligence), BinCmp::GE, GetConstant(13)) ), [15, 14, 13, 12, 11, 10]) == false );
    assert!( IsStatBlockValid(Constraint::BinCmp_StatValue_Constant( BinaryCompare::New( GetStatValue(Stat::Wisdom      ), BinCmp::LT, GetConstant(12)) ), [15, 14, 13, 12, 11, 10]) == true  );
    assert!( IsStatBlockValid(Constraint::BinCmp_StatValue_Constant( BinaryCompare::New( GetStatValue(Stat::Charisma    ), BinCmp::LE, GetConstant(9 )) ), [15, 14, 13, 12, 11, 10]) == false );

    // BinCmp_StatModifier_Constant
    assert!( IsStatBlockValid(Constraint::BinCmp_StatModifier_Constant( BinaryCompare::New( GetStatModifier(Stat::Strength    ), BinCmp::EQ, GetConstant(-2)) ), [ 6, 14, 13, 12, 11, 10]) == true  );
    assert!( IsStatBlockValid(Constraint::BinCmp_StatModifier_Constant( BinaryCompare::New( GetStatModifier(Stat::Dexterity   ), BinCmp::NE, GetConstant(-1)) ), [15,  9, 13, 12, 11, 10]) == false );
    assert!( IsStatBlockValid(Constraint::BinCmp_StatModifier_Constant( BinaryCompare::New( GetStatModifier(Stat::Constitution), BinCmp::GT, GetConstant(-0)) ), [15, 14, 13, 12, 11, 10]) == true  );
    assert!( IsStatBlockValid(Constraint::BinCmp_StatModifier_Constant( BinaryCompare::New( GetStatModifier(Stat::Intelligence), BinCmp::GE, GetConstant(-0)) ), [15, 14, 13,  8, 11, 10]) == false );
    assert!( IsStatBlockValid(Constraint::BinCmp_StatModifier_Constant( BinaryCompare::New( GetStatModifier(Stat::Wisdom      ), BinCmp::LT, GetConstant( 1)) ), [15, 14, 13, 12, 11, 10]) == true  );
    assert!( IsStatBlockValid(Constraint::BinCmp_StatModifier_Constant( BinaryCompare::New( GetStatModifier(Stat::Charisma    ), BinCmp::LE, GetConstant( 2)) ), [15, 14, 13, 12, 11, 17]) == false );

    // BinCmp_StatValue_StatValue
    assert!( IsStatBlockValid(Constraint::BinCmp_StatValue_StatValue( BinaryCompare::New( GetStatValue(Stat::Strength    ), BinCmp::EQ, GetStatValue(Stat::Charisma    )) ), [ 6, 14, 13, 12, 11,  6]) == true  );
    assert!( IsStatBlockValid(Constraint::BinCmp_StatValue_StatValue( BinaryCompare::New( GetStatValue(Stat::Dexterity   ), BinCmp::NE, GetStatValue(Stat::Wisdom      )) ), [15,  9, 13, 12,  9, 10]) == false );
    assert!( IsStatBlockValid(Constraint::BinCmp_StatValue_StatValue( BinaryCompare::New( GetStatValue(Stat::Constitution), BinCmp::GT, GetStatValue(Stat::Intelligence)) ), [15, 14, 13, 12, 11, 10]) == true  );
    assert!( IsStatBlockValid(Constraint::BinCmp_StatValue_StatValue( BinaryCompare::New( GetStatValue(Stat::Intelligence), BinCmp::GE, GetStatValue(Stat::Constitution)) ), [15, 14, 13, 12, 11, 10]) == false );
    assert!( IsStatBlockValid(Constraint::BinCmp_StatValue_StatValue( BinaryCompare::New( GetStatValue(Stat::Wisdom      ), BinCmp::LT, GetStatValue(Stat::Dexterity   )) ), [15, 14, 13, 12, 11, 10]) == true  );
    assert!( IsStatBlockValid(Constraint::BinCmp_StatValue_StatValue( BinaryCompare::New( GetStatValue(Stat::Charisma    ), BinCmp::LE, GetStatValue(Stat::Strength    )) ), [15, 14, 13, 12, 11, 17]) == false );

    // BinCmp_StatModifier_StatModifier
    assert!( IsStatBlockValid(Constraint::BinCmp_StatModifier_StatModifier( BinaryCompare::New( GetStatModifier(Stat::Strength    ), BinCmp::EQ, GetStatModifier(Stat::Charisma    )) ), [ 5, 14, 13, 12, 11,  4]) == true  );
    assert!( IsStatBlockValid(Constraint::BinCmp_StatModifier_StatModifier( BinaryCompare::New( GetStatModifier(Stat::Dexterity   ), BinCmp::NE, GetStatModifier(Stat::Wisdom      )) ), [15,  6, 13, 12,  7, 10]) == false );
    assert!( IsStatBlockValid(Constraint::BinCmp_StatModifier_StatModifier( BinaryCompare::New( GetStatModifier(Stat::Constitution), BinCmp::GT, GetStatModifier(Stat::Intelligence)) ), [15, 14, 16, 15, 11, 10]) == true  );
    assert!( IsStatBlockValid(Constraint::BinCmp_StatModifier_StatModifier( BinaryCompare::New( GetStatModifier(Stat::Intelligence), BinCmp::GE, GetStatModifier(Stat::Constitution)) ), [15, 14, 12, 10, 11, 10]) == false );
    assert!( IsStatBlockValid(Constraint::BinCmp_StatModifier_StatModifier( BinaryCompare::New( GetStatModifier(Stat::Wisdom      ), BinCmp::LT, GetStatModifier(Stat::Dexterity   )) ), [15, 16, 13, 12, 15, 10]) == true  );
    assert!( IsStatBlockValid(Constraint::BinCmp_StatModifier_StatModifier( BinaryCompare::New( GetStatModifier(Stat::Charisma    ), BinCmp::LE, GetStatModifier(Stat::Strength    )) ), [11, 14, 13, 12, 11, 12]) == false );

    // UnCmp_StatValue
    assert!( IsStatBlockValid(Constraint::UnCmp_StatValue( UnaryCompare::New( GetStatValue(Stat::Strength    ), UnCmp::IsEven ) ), [ 6, 14, 13, 12, 11,  4]) == true  );
    assert!( IsStatBlockValid(Constraint::UnCmp_StatValue( UnaryCompare::New( GetStatValue(Stat::Dexterity   ), UnCmp::IsEven ) ), [15, 25, 13, 12,  7, 10]) == false );
    assert!( IsStatBlockValid(Constraint::UnCmp_StatValue( UnaryCompare::New( GetStatValue(Stat::Constitution), UnCmp::IsOdd  ) ), [15, 14, 13, 15, 11, 10]) == true  );
    assert!( IsStatBlockValid(Constraint::UnCmp_StatValue( UnaryCompare::New( GetStatValue(Stat::Intelligence), UnCmp::IsOdd  ) ), [15, 14, 12, 10, 11, 10]) == false );
    assert!( IsStatBlockValid(Constraint::UnCmp_StatValue( UnaryCompare::New( GetStatValue(Stat::Wisdom      ), UnCmp::IsEven ) ), [15, 16, 13, 12, 18, 10]) == true  );
    assert!( IsStatBlockValid(Constraint::UnCmp_StatValue( UnaryCompare::New( GetStatValue(Stat::Charisma    ), UnCmp::IsOdd  ) ), [11, 14, 13, 12, 11, 12]) == false );

    assert!( IsStatBlockValid(Constraint::UnCmp_StatValue( UnaryCompare::New( GetStatValue(Stat::Strength    ), UnCmp::IsMax  ) ), [15, 14, 13, 15, 11, 15]) == true  );
    assert!( IsStatBlockValid(Constraint::UnCmp_StatValue( UnaryCompare::New( GetStatValue(Stat::Dexterity   ), UnCmp::IsMax  ) ), [15, 16, 13, 15, 11, 15]) == true  );
    assert!( IsStatBlockValid(Constraint::UnCmp_StatValue( UnaryCompare::New( GetStatValue(Stat::Constitution), UnCmp::IsMax  ) ), [15, 14, 15, 16, 14, 14]) == false );
    assert!( IsStatBlockValid(Constraint::UnCmp_StatValue( UnaryCompare::New( GetStatValue(Stat::Intelligence), UnCmp::IsMin  ) ), [15, 14, 13, 10, 11, 10]) == true  );
    assert!( IsStatBlockValid(Constraint::UnCmp_StatValue( UnaryCompare::New( GetStatValue(Stat::Wisdom      ), UnCmp::IsMin  ) ), [15, 14, 13, 15,  9, 10]) == true  );
    assert!( IsStatBlockValid(Constraint::UnCmp_StatValue( UnaryCompare::New( GetStatValue(Stat::Charisma    ), UnCmp::IsMin  ) ), [15, 14, 12, 10, 11, 12]) == false );

    // UnCmp_StatModifier
    assert!( IsStatBlockValid(Constraint::UnCmp_StatModifier( UnaryCompare::New( GetStatModifier(Stat::Strength    ), UnCmp::IsEven ) ), [10, 14, 13, 12, 11,  4]) == true  );
    assert!( IsStatBlockValid(Constraint::UnCmp_StatModifier( UnaryCompare::New( GetStatModifier(Stat::Dexterity   ), UnCmp::IsEven ) ), [15, 12, 13, 12,  7, 10]) == false );
    assert!( IsStatBlockValid(Constraint::UnCmp_StatModifier( UnaryCompare::New( GetStatModifier(Stat::Constitution), UnCmp::IsOdd  ) ), [15, 14, 13, 15, 11, 10]) == true  );
    assert!( IsStatBlockValid(Constraint::UnCmp_StatModifier( UnaryCompare::New( GetStatModifier(Stat::Intelligence), UnCmp::IsOdd  ) ), [15, 14, 12, 15, 11, 10]) == false );
    assert!( IsStatBlockValid(Constraint::UnCmp_StatModifier( UnaryCompare::New( GetStatModifier(Stat::Wisdom      ), UnCmp::IsEven ) ), [15, 16, 13, 12, 19, 10]) == true  );
    assert!( IsStatBlockValid(Constraint::UnCmp_StatModifier( UnaryCompare::New( GetStatModifier(Stat::Charisma    ), UnCmp::IsOdd  ) ), [11, 14, 13, 12, 11,  7]) == false );

    assert!( IsStatBlockValid(Constraint::UnCmp_StatModifier( UnaryCompare::New( GetStatModifier(Stat::Strength    ), UnCmp::IsMax  ) ), [17, 14, 13, 15, 11, 15]) == true  );
    assert!( IsStatBlockValid(Constraint::UnCmp_StatModifier( UnaryCompare::New( GetStatModifier(Stat::Dexterity   ), UnCmp::IsMax  ) ), [17, 16, 13, 15, 11, 15]) == true  );
    assert!( IsStatBlockValid(Constraint::UnCmp_StatModifier( UnaryCompare::New( GetStatModifier(Stat::Constitution), UnCmp::IsMax  ) ), [17, 14, 14, 14, 14, 14]) == false );
    assert!( IsStatBlockValid(Constraint::UnCmp_StatModifier( UnaryCompare::New( GetStatModifier(Stat::Intelligence), UnCmp::IsMin  ) ), [15, 14, 13, 11, 11, 10]) == true  );
    assert!( IsStatBlockValid(Constraint::UnCmp_StatModifier( UnaryCompare::New( GetStatModifier(Stat::Wisdom      ), UnCmp::IsMin  ) ), [15, 14, 13, 15,  9, 10]) == true  );
    assert!( IsStatBlockValid(Constraint::UnCmp_StatModifier( UnaryCompare::New( GetStatModifier(Stat::Charisma    ), UnCmp::IsMin  ) ), [15, 14, 12, 10, 11, 12]) == false );
}
