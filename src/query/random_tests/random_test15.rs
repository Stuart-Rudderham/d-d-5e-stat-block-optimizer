use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test15" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Weighted,
        statOrdering: [
            Charisma,
            Constitution,
            Dexterity,
            Strength,
            Intelligence,
            Wisdom,
        ],
        isReversed: false,
        availableRaces: vec![
            Aarakocra,
            Aasimar_Fallen,
            Aasimar_Protector,
            Aasimar_Scourge,
            Bugbear,
            Changling,
            CustomLineage,
            DragonBorn,
            Dwarf_Grey,
            Dwarf_Hill,
            Dwarf_MarkOfWarding,
            Dwarf_Mountain,
            Elf_Drow,
            Elf_High,
            Elf_MarkOfShadow,
            Elf_Wood,
            Firbolg,
            Genasi_Air,
            Genasi_Earth,
            Genasi_Fire,
            Genasi_Water,
            Gnome_Deep,
            Gnome_Forest,
            Gnome_MarkOfScribing,
            Gnome_Rock,
            Goblin,
            Goliath,
            HalfElf,
            HalfElf_MarkOfDetection,
            HalfElf_MarkOfStorm,
            Halfling_Ghostwise,
            Halfling_Lightfoot,
            Halfling_MarkOfHealing,
            Halfling_MarkOfHospitality,
            Halfling_Stout,
            HalfOrc,
            HalfOrc_MarkOfFinding,
            Hobgoblin,
            Human,
            Human_MarkOfFinding,
            Human_MarkOfHandling,
            Human_MarkOfMaking,
            Human_MarkOfPassage,
            Human_MarkOfSentinel,
            Human_Variant,
            Kalashtar,
            Kenku,
            Kobold,
            Lizardfolk,
            Orc,
            Orc_Eberron,
            Shifter_BeastHide,
            Shifter_LongTooth,
            Shifter_SwiftStride,
            Shifter_WildHunt,
            Tabaxi,
            Tiefling,
            Tiefling_Feral,
            Tortle,
            Triton,
            Warforged,
            YuanTiPureblood,
        ],
        requiredFeats: vec![
            Athlete,
        ],
        constraints: vec![
            BinCmp_StatModifier_StatModifier(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Intelligence,
                    ),
                    binCmp: NE,
                    rhsValue: GetStatModifier(
                        Constitution,
                    ),
                },
            ),
        ],
        numberOfASIs: 0,
        doAllRacesHaveFreeFeat: true,
        canRearrangeRaceModifiers: true,
        maxNumberOfResults: 29,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 6619998,
            numUniqueCombinationsPassedConstraints: 5321202,
            numStatBlocksEjected: 475,
            numUniqueCombinationsEjected: 856,
            numUniqueCombinationsBelowLowerBound: 5320661,
            numStatBlocksShiftedDuringInsertion: 7418,
            numDuplicateRaceStatBlocksRemoved: 0,
            numUniqueStatBlockAfterRacePass: 297,
            numUniqueStatBlockAfterFeatPass: 539,
            numFinalStatBlockCombinations: 46,
        };

    let expectedStatBlocks = 
        vec![
            RankedStatBlock {
                rank: 120003,
                statBlock: [8, 18, 16, 8, 8, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 8, 8, 15],
                        myRaceModifiers:     [0, 2, 1, 0, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 120003,
                statBlock: [18, 8, 16, 8, 8, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 8, 15],
                        myRaceModifiers:     [2, 0, 1, 0, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 120003,
                statBlock: [8, 18, 8, 16, 8, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 15, 8, 15, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 1, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 120003,
                statBlock: [18, 8, 8, 16, 8, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 8, 15],
                        myRaceModifiers:     [2, 0, 0, 1, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 120003,
                statBlock: [16, 18, 16, 8, 8, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 15, 15, 8, 8, 8],
                        myRaceModifiers:     [1, 2, 1, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 120003,
                statBlock: [8, 18, 16, 8, 16, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 15, 15, 8, 15, 8],
                        myRaceModifiers:     [0, 2, 1, 0, 1, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 120003,
                statBlock: [18, 16, 16, 8, 8, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 15, 15, 8, 8, 8],
                        myRaceModifiers:     [2, 1, 1, 0, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 120003,
                statBlock: [18, 8, 16, 8, 16, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 8, 15, 8, 15, 8],
                        myRaceModifiers:     [2, 0, 1, 0, 1, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 120003,
                statBlock: [16, 18, 8, 16, 8, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 15, 8, 15, 8, 8],
                        myRaceModifiers:     [1, 2, 0, 1, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 120003,
                statBlock: [8, 18, 8, 16, 16, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 15, 8, 15, 15, 8],
                        myRaceModifiers:     [0, 2, 0, 1, 1, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 120003,
                statBlock: [18, 16, 8, 16, 8, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 15, 8, 15, 8, 8],
                        myRaceModifiers:     [2, 1, 0, 1, 0, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 120003,
                statBlock: [18, 8, 8, 16, 16, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 8, 8, 15, 15, 8],
                        myRaceModifiers:     [2, 0, 0, 1, 1, 0],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 111102,
                statBlock: [12, 18, 14, 8, 8, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Dwarf_Mountain,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [12, 15, 14, 8, 8, 14],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [12, 15, 13, 8, 8, 15],
                        myRaceModifiers:     [0, 2, 1, 0, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 111102,
                statBlock: [8, 18, 14, 12, 8, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Dwarf_Mountain,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 15, 14, 12, 8, 14],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 15, 13, 12, 8, 15],
                        myRaceModifiers:     [0, 2, 1, 0, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 111102,
                statBlock: [8, 18, 14, 8, 12, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Dwarf_Mountain,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 15, 14, 8, 12, 14],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 15, 13, 8, 12, 15],
                        myRaceModifiers:     [0, 2, 1, 0, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 111102,
                statBlock: [18, 12, 14, 8, 8, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Dwarf_Mountain,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 12, 14, 8, 8, 14],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 12, 13, 8, 8, 15],
                        myRaceModifiers:     [2, 0, 1, 0, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 111102,
                statBlock: [18, 8, 14, 12, 8, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Dwarf_Mountain,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 8, 14, 12, 8, 14],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 12, 8, 15],
                        myRaceModifiers:     [2, 0, 1, 0, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 111102,
                statBlock: [18, 8, 14, 8, 12, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Dwarf_Mountain,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 8, 14, 8, 12, 14],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 8, 13, 8, 12, 15],
                        myRaceModifiers:     [2, 0, 1, 0, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 111102,
                statBlock: [14, 18, 12, 8, 8, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Dwarf_Mountain,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [14, 15, 12, 8, 8, 14],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [13, 15, 12, 8, 8, 15],
                        myRaceModifiers:     [1, 2, 0, 0, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 111102,
                statBlock: [8, 18, 12, 14, 8, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Dwarf_Mountain,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 15, 12, 14, 8, 14],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 15, 12, 13, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 1, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 111102,
                statBlock: [8, 18, 12, 8, 14, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Dwarf_Mountain,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 15, 12, 8, 14, 14],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 15, 12, 8, 13, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 1, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 111102,
                statBlock: [18, 14, 12, 8, 8, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Dwarf_Mountain,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 14, 12, 8, 8, 14],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 13, 12, 8, 8, 15],
                        myRaceModifiers:     [2, 1, 0, 0, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 111102,
                statBlock: [18, 8, 12, 14, 8, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Dwarf_Mountain,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 8, 12, 14, 8, 14],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 8, 12, 13, 8, 15],
                        myRaceModifiers:     [2, 0, 0, 1, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 111102,
                statBlock: [18, 8, 12, 8, 14, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Dwarf_Mountain,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 8, 12, 8, 14, 14],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 8, 12, 8, 13, 15],
                        myRaceModifiers:     [2, 0, 0, 0, 1, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 111102,
                statBlock: [14, 18, 8, 12, 8, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Dwarf_Mountain,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [14, 15, 8, 12, 8, 14],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [13, 15, 8, 12, 8, 15],
                        myRaceModifiers:     [1, 2, 0, 0, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 111102,
                statBlock: [12, 18, 8, 14, 8, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Dwarf_Mountain,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [12, 15, 8, 14, 8, 14],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [12, 15, 8, 13, 8, 15],
                        myRaceModifiers:     [0, 2, 0, 1, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 111102,
                statBlock: [8, 18, 8, 14, 12, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Dwarf_Mountain,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 15, 8, 14, 12, 14],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 15, 8, 13, 12, 15],
                        myRaceModifiers:     [0, 2, 0, 1, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 111102,
                statBlock: [8, 18, 8, 12, 14, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Dwarf_Mountain,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 15, 8, 12, 14, 14],
                        myRaceModifiers:     [0, 2, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [8, 15, 8, 12, 13, 15],
                        myRaceModifiers:     [0, 2, 0, 0, 1, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 1, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 111102,
                statBlock: [18, 14, 8, 12, 8, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Dwarf_Mountain,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 14, 8, 12, 8, 14],
                        myRaceModifiers:     [2, 0, 0, 0, 0, 2],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![
                            Athlete,
                        ],
                        myBasePointBuyArray: [15, 13, 8, 12, 8, 15],
                        myRaceModifiers:     [2, 1, 0, 0, 0, 1],
                        myASIs:              [0, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [1, 0, 0, 0, 0, 0],
                    },
                ],
            },
        ];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

