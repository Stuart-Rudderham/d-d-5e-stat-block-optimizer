#![allow(non_snake_case)]
#![allow(deprecated)]
#![allow(non_camel_case_types)]
#![allow(non_upper_case_globals)]

use crate::constraint::*;
use crate::dnd::asi::*;
use crate::dnd::feat::*;
use crate::dnd::pointbuy::*;
use crate::dnd::race::*;
use crate::dnd::statblock::*;
use crate::hash_container_typedefs::*;

use hprof;
use num_cpus;
use rayon;

use std::cmp::Ordering;
use std::convert::TryFrom;
use std::convert::TryInto;
use std::num::NonZeroUsize;
use more_asserts::*;

pub mod generate_random_tests;
pub mod random_tests_harness;
pub mod random_tests;

#[derive(Debug)]
pub struct QueryParameters
{
    pub rankingStyle              : RankingStyle,
    pub statOrdering              : StatOrdering,
    pub isReversed                : bool,
    pub availableRaces            : Vec<CharacterRace>,
    pub requiredFeats             : Vec<Feat>,
    pub constraints               : Vec<Constraint>,
    pub numberOfASIs              : u8,
    pub doAllRacesHaveFreeFeat    : bool,
    pub canRearrangeRaceModifiers : bool,
    pub maxNumberOfResults        : usize,
    pub optCustomPointBuyArray    : Option<StatBlock>,
}

#[derive(Debug, Default, Eq, PartialEq)]
pub struct QueryStats
{
    pub numUniqueCombinationsSeen               : u64,      // The total number of stat block combinations checked.
    pub numUniqueCombinationsPassedConstraints  : u64,      // The total number of stat block combinations that passed all the constraints.

    pub numStatBlocksEjected                    : u64,      // The number of stat blocks that were replaced with a better stat block.
    pub numUniqueCombinationsEjected            : u64,      // The number of combinations for all the stat blocks that were replaced.

    pub numUniqueCombinationsBelowLowerBound    : u64,      // The number of combinations that passed the constraints but worse than what we already had.

    pub numStatBlocksShiftedDuringInsertion     : u64,      // The number of stat blocks that needed to be shifted when inserting into the middle of the buffer.

    pub numDuplicateRaceStatBlocksRemoved       : u64,      // After generating results, how many combinations were removed for having the same race as another.

    pub numUniqueStatBlockAfterRacePass         : u64,      // Keep track of the size of the data so far after each pass, so we can see how it grows.
    pub numUniqueStatBlockAfterFeatPass         : u64,

    pub numFinalStatBlockCombinations           : u64,      // After the query is done, how many stat block combinations did we end up with.
}

pub type QueryProfiler = hprof::Profiler;

pub fn GetTotalTimeNS(aQueryProfiler : &QueryProfiler) -> u64
{
    aQueryProfiler.root().total_time.get()
}

pub fn GetTotalTimeS(aQueryProfiler : &QueryProfiler) -> f64
{
    GetTotalTimeNS(aQueryProfiler) as f64 / 1_000_000_000_f64
}

// Whitespace is added to help align the profile output and make it
// easier to read. This is a hacky workaround since the print function
// is in an external crate so I can't change it.
static ProfileTag_Total                  : &'static str = "Total                         ";
static ProfileTag_RunQuery               : &'static str =   "Run Query                    |---";
static ProfileTag_RacePass               : &'static str =     "Race Pass                  |    |---";
static ProfileTag_FeatChoiceGeneration   : &'static str =     "Feat Choice Generation     |    |---";
static ProfileTag_FeatPass               : &'static str =     "Feat Pass                  |    |---";
static ProfileTag_CombinePointBuyAndASIs : &'static str =     "Combine Point Buy and ASIs |    |---";
static ProfileTag_PointBuyAndAsiPass     : &'static str =     "Point Buy And ASI Pass     |    |---";
static ProfileTag_RemoveDuplicates       : &'static str =     "Remove Duplicates          |    |---";
static ProfileTag_SanityCheck            : &'static str =   "Sanity Check                 |---";

pub type QueryResult = (Vec<RankedStatBlock>, QueryStats, QueryProfiler);

fn GetChunkSizeHelper( aContainerSize : usize, aNumberOfCPUs : NonZeroUsize ) -> usize
{
    // Each CPU gets an (approximately) equal chunk of work.
    //
    // If the amount of work doesn't divide evenly we want to round up the chunk
    // size so that we don't end up with one extra tiny chunk at the end.
    (aContainerSize + aNumberOfCPUs.get() - 1) / aNumberOfCPUs.get()
}

fn GetChunkSize( aContainerSize : usize ) -> usize
{
    GetChunkSizeHelper( aContainerSize, NonZeroUsize::new(num_cpus::get()).unwrap() )
}

#[test]
fn TestGetChunkSizeHelper()
{
    for i in 0..=10
    {
        // If we only have 1 CPU then we should do everything in a single chunk.
        assert_eq!( GetChunkSizeHelper(i, NonZeroUsize::new(1).unwrap()), i );
    }

    // 2 CPUs.
    assert_eq!( GetChunkSizeHelper( 0, NonZeroUsize::new(2).unwrap()), 0 );
    assert_eq!( GetChunkSizeHelper( 1, NonZeroUsize::new(2).unwrap()), 1 );
    assert_eq!( GetChunkSizeHelper( 2, NonZeroUsize::new(2).unwrap()), 1 );
    assert_eq!( GetChunkSizeHelper( 3, NonZeroUsize::new(2).unwrap()), 2 );
    assert_eq!( GetChunkSizeHelper( 4, NonZeroUsize::new(2).unwrap()), 2 );
    assert_eq!( GetChunkSizeHelper( 5, NonZeroUsize::new(2).unwrap()), 3 );
    assert_eq!( GetChunkSizeHelper( 6, NonZeroUsize::new(2).unwrap()), 3 );
    assert_eq!( GetChunkSizeHelper( 7, NonZeroUsize::new(2).unwrap()), 4 );
    assert_eq!( GetChunkSizeHelper( 8, NonZeroUsize::new(2).unwrap()), 4 );
    assert_eq!( GetChunkSizeHelper( 9, NonZeroUsize::new(2).unwrap()), 5 );
    assert_eq!( GetChunkSizeHelper(10, NonZeroUsize::new(2).unwrap()), 5 );

    // 3 CPUs.
    assert_eq!( GetChunkSizeHelper( 0, NonZeroUsize::new(3).unwrap()), 0 );
    assert_eq!( GetChunkSizeHelper( 1, NonZeroUsize::new(3).unwrap()), 1 );
    assert_eq!( GetChunkSizeHelper( 2, NonZeroUsize::new(3).unwrap()), 1 );
    assert_eq!( GetChunkSizeHelper( 3, NonZeroUsize::new(3).unwrap()), 1 );
    assert_eq!( GetChunkSizeHelper( 4, NonZeroUsize::new(3).unwrap()), 2 );
    assert_eq!( GetChunkSizeHelper( 5, NonZeroUsize::new(3).unwrap()), 2 );
    assert_eq!( GetChunkSizeHelper( 6, NonZeroUsize::new(3).unwrap()), 2 );
    assert_eq!( GetChunkSizeHelper( 7, NonZeroUsize::new(3).unwrap()), 3 );
    assert_eq!( GetChunkSizeHelper( 8, NonZeroUsize::new(3).unwrap()), 3 );
    assert_eq!( GetChunkSizeHelper( 9, NonZeroUsize::new(3).unwrap()), 3 );
    assert_eq!( GetChunkSizeHelper(10, NonZeroUsize::new(3).unwrap()), 4 );

    // Way more CPUs than we need.
    assert_eq!( GetChunkSizeHelper(10, NonZeroUsize::new(300).unwrap()), 1 );
}

fn UniqueCartesianProduct( lhsStatBlocks : &[StatBlock], rhsStatBlocks : &[StatBlock] ) -> Vec<(StatBlock, (StatBlock, StatBlock))>
{
    fn Helper( lhsStatBlocks : &[StatBlock], rhsStatBlocks : &[StatBlock], serialThreshold : usize ) -> (Vec<(StatBlock, (StatBlock, StatBlock))>, DeterministicHashSet<StatBlock>)
    {
        if lhsStatBlocks.len() <= serialThreshold
        {
            // Serial case.

            let mut hashSet : DeterministicHashSet<StatBlock> = Default::default();

            let mut result = Vec::new();

            for lhsStatBlock in lhsStatBlocks
            {
                for rhsStatBlock in rhsStatBlocks
                {
                    let combinedStatBlock = CombineStatBlocks2(*lhsStatBlock, *rhsStatBlock);

                    if hashSet.insert(combinedStatBlock)
                    {
                        result.push( (combinedStatBlock, (*lhsStatBlock, *rhsStatBlock)) );
                    }
                }
            }

            // We include the set of combined stat blocks since the calling
            // function will need it to merge the two halves.
            (result, hashSet)
        }
        else
        {
            // Recursive case.

            let mid_point = lhsStatBlocks.len() / 2;
            let (lhsStatBlocks_FirstHalf, lhsStatBlocks_SecondHalf) = lhsStatBlocks.split_at(mid_point);

            // Handle each half (potentially) in parallel.
            let ((mut leftUnique, mut leftUniqueSet), (rightUnique, _)) = rayon::join( || Helper(lhsStatBlocks_FirstHalf , rhsStatBlocks, serialThreshold) ,
                                                                                       || Helper(lhsStatBlocks_SecondHalf, rhsStatBlocks, serialThreshold) );
            // Merge the right half into the left half, filtering out duplicates.
            for x in rightUnique
            {
                if leftUniqueSet.insert(x.0)
                {
                    leftUnique.push( x );
                }
            }

            (leftUnique, leftUniqueSet)
        }
    }

    let serialThreshold = GetChunkSize( lhsStatBlocks.len() );

    Helper( lhsStatBlocks, rhsStatBlocks, serialThreshold ).0
}

fn RunQuery( someParameters : &QueryParameters, someStats : &mut QueryStats, aProfiler : &QueryProfiler ) -> Vec<RankedStatBlock>
{
    let _guard = aProfiler.enter(ProfileTag_RunQuery);

    if someParameters.maxNumberOfResults == 0
    {
        return vec![];
    }

    if someParameters.availableRaces.is_empty()
    {
        return vec![];
    }

    type RaceStats = StatBlock;
    type FeatStats = StatBlock;

    type NumASIs = u8;

    type RacePassOutput =                               DeterministicHashMap<StatBlock, DeterministicHashMap< CharacterRace            ,  RaceStats            >>;
    type FeatPassOutput = DeterministicHashMap<NumASIs, DeterministicHashMap<StatBlock, DeterministicHashMap<(CharacterRace, Vec<Feat>), (RaceStats, FeatStats)>>>;

    // Race.
    let racePassOutput =
    {
        let _guard = aProfiler.enter(ProfileTag_RacePass);

        let mut workingSet = RacePassOutput::default();

        for race in someParameters.availableRaces.iter()
        {
            for raceModifiers in GenerateRaceModifierArrays(*race, someParameters.canRearrangeRaceModifiers)
            {
                let choicesMade = workingSet.entry(raceModifiers).or_default();

                choicesMade.entry(*race).or_insert_with(|| raceModifiers);
            }
        }

        workingSet
    };

    someStats.numUniqueStatBlockAfterRacePass = racePassOutput.len().try_into().unwrap();

    let featChoiceList =
    {
        let _guard = aProfiler.enter(ProfileTag_FeatChoiceGeneration);

        let possibleNumStartingFeats = racePassOutput.values().flat_map(|choices| choices.keys()).map(|race|
        {
            GetNumStartingFeats(*race, someParameters.doAllRacesHaveFreeFeat)
        })
        .collect();

        GenerateFeatChoiceList( &someParameters.requiredFeats, someParameters.numberOfASIs, &possibleNumStartingFeats )
    };

    // Feat.
    let featPassOutput =
    {
        let _guard = aProfiler.enter(ProfileTag_FeatPass);

        let mut workingSet = FeatPassOutput::default();

        for (currentModifiers, choicesMadeToGetCurrentModifers) in racePassOutput.into_iter()
        {
            for (race, raceModifiers) in choicesMadeToGetCurrentModifers.iter()
            {
                let numStartingFeats = GetNumStartingFeats(*race, someParameters.doAllRacesHaveFreeFeat);

                let optFeatChoicesForNumStartingFeats = featChoiceList.get(&numStartingFeats);
                if optFeatChoicesForNumStartingFeats.is_none()
                {
                    // Given our number of starting feats, we have no valid choices.
                    continue;
                }

                let (numRemainingASIs, featChoicesForNumStartingFeats) = optFeatChoicesForNumStartingFeats.unwrap();

                for (combinedFeatModifiers, listOfListOfFeats) in featChoicesForNumStartingFeats.iter()
                {
                    // The list of feat choices was calculated using all possible feats.
                    //
                    // Now that we're working with a specific race we need to get rid of
                    // any choices that aren't actually valid for that race.
                    //
                    // TODO: Possibly do it faster by not needing to clone the entire vector?
                    let raceFilteredListOfListOfFeats : Vec<Vec<Feat>> = listOfListOfFeats.iter().filter(|featList|
                    {
                        featList.iter().all(|feat| IsFeatValidForRace(*feat, *race))
                    })
                    .cloned()
                    .collect();

                    if raceFilteredListOfListOfFeats.is_empty()
                    {
                        continue;
                    }

                    let newModifiers = CombineStatBlocks2(currentModifiers, *combinedFeatModifiers);

                    let choicesMade = workingSet.entry(*numRemainingASIs).or_default()
                                                .entry( newModifiers    ).or_default();

                    let value = (*raceModifiers, *combinedFeatModifiers);

                    for featList in raceFilteredListOfListOfFeats.into_iter()
                    {
                        choicesMade.entry( (*race, featList) ).and_modify(|existingValue|
                        {
                            // If we have more than one choice for race + feat modifiers then take the "lowest"
                            // one. The choice doesn't really matter so long as it's deterministic. This isn't
                            // required for correctness (since each choice is perfectly valid) but it will prevent
                            // the change-detector unit tests from being triggered unnecessarily and it doesn't
                            // noticebly affect performance in a negative way.
                            if value < *existingValue
                            {
                                *existingValue = value;
                            }
                        })
                        .or_insert(value);
                    }
                }
            }
        }

        workingSet
    };

    someStats.numUniqueStatBlockAfterFeatPass = featPassOutput.values().map(|x| x.len()).sum::<usize>().try_into().unwrap();

    // Combine point buy and ASI.
    let pointBuyArraysCombinedWithASIs =
    {
        let _guard = aProfiler.enter(ProfileTag_CombinePointBuyAndASIs);

        // If a specific stat block isn't requested then we need to check all possible choices.
        let pointBuyArrays = someParameters.optCustomPointBuyArray.map_or_else(|| GenerateAllPointBuyArrays(), |statBlock| GenerateAllUniquePermutationsOfStatBlock(statBlock));

        let mut result : DeterministicHashMap<NumASIs, Vec<(StatBlock, (StatBlock, StatBlock))>> = Default::default();

        for numASIs in featPassOutput.keys()
        {
            let asiArrays = GenerateAsiArrays(*numASIs);

            //
            // If we have ASIs then it is possible that different stat allocation choices result
            // in the same combined stat block. Removing duplicates can result in *huge* savings.
            //
            //     | # of ASIs | # of stat blocks before | # of stat blocks after | delta      | % removed |
            //     +-----------+-------------------------+------------------------+------------+-----------+
            //     | 0         |    12,282               |  12,282                |          0 |   0%      |
            //     | 1         |   257,922               |  65,023                | -  192,899 | ~75%      |
            //     | 2         | 1,547,532               | 169,774                | -1,377,758 | ~89%      |
            //     | 3         | 5,674,284               | 347,308                | -5,326,976 | ~94%      |
            //
            let combined = UniqueCartesianProduct( &pointBuyArrays, &asiArrays );

            let previousValue = result.insert( *numASIs, combined );
            assert!( previousValue.is_none() );
        }

        result
    };

    // Point buy and ASI.
    let mut resultBuffer =
    {
        let _guard = aProfiler.enter(ProfileTag_PointBuyAndAsiPass);

        let mut resultBuffer : Vec<RankedStatBlock> = Vec::with_capacity(someParameters.maxNumberOfResults);

        for (numASIs, currentModifersList) in featPassOutput.into_iter()
        {
            let combinedPointBuyAndASIs = pointBuyArraysCombinedWithASIs.get(&numASIs).unwrap();

            for (currentModifiers, choicesMadeToGetCurrentModifers) in currentModifersList.into_iter()
            {
                for (combinedStatBlock, (pointBuyStatBlock, asiModifiers)) in combinedPointBuyAndASIs.iter()
                {
                    let aStatBlock = CombineStatBlocks2(currentModifiers, *combinedStatBlock);

                    someStats.numUniqueCombinationsSeen += 1;

                    if IsStatBlockAboveMaxValue(aStatBlock)
                    {
                        continue;
                    }

                    // Throw it away if it doesn't meet all the constraints.
                    if !someParameters.constraints.iter().all( |&aConstraint| IsStatBlockValid(aConstraint, aStatBlock) )
                    {
                        continue;
                    }

                    someStats.numUniqueCombinationsPassedConstraints += 1;

                    let optRankedStatBlock = GetOrInsertNewRankedStatBlockIfGoodEnough
                    (
                        &mut resultBuffer,
                        aStatBlock,
                        someParameters.rankingStyle,
                        someParameters.statOrdering,
                        someParameters.isReversed,
                        someStats
                    );

                    if let Some(rankedStatBlock) = optRankedStatBlock
                    {
                        // We know we're adding a bunch of new entries, might as well
                        // reserve space for them all at once to improve performance.
                        rankedStatBlock.combinations.reserve(choicesMadeToGetCurrentModifers.len());

                        for ((race, feats), (raceModifiers, featModifiers)) in choicesMadeToGetCurrentModifers.iter()
                        {
                            let combination =
                                CharacterStatBlock
                                {
                                    myRace              : *race,
                                    myFeats             : feats.clone(),
                                    myBasePointBuyArray : *pointBuyStatBlock,
                                    myRaceModifiers     : *raceModifiers,
                                    myASIs              : *asiModifiers,
                                    myFeatModifiers     : *featModifiers,
                                };

                            rankedStatBlock.combinations.push(combination);
                        }
                    }
                }
            }
        }

        resultBuffer
    };

    RemoveDuplicateStatBlocks( &mut resultBuffer, someStats, &aProfiler );

    someStats.numFinalStatBlockCombinations = resultBuffer
        .iter()
        .map(|rankedStatBlock| rankedStatBlock.combinations.len() )
        .sum::<usize>()
        .try_into()
        .unwrap();

    resultBuffer
}

pub fn GenerateAllValidCharacterStatBlocks(someParameters : &QueryParameters) -> QueryResult
{
    let queryProfiler = QueryProfiler::new("");

    queryProfiler.start_frame();

    let _guardTotal = queryProfiler.enter(ProfileTag_Total);

        let mut queryStats = QueryStats::default();

        let resultBuffer = RunQuery( &someParameters, &mut queryStats, &queryProfiler );

        SanityCheckQueryResults( &resultBuffer, &someParameters, &queryProfiler );

    drop(_guardTotal);

    queryProfiler.end_frame();

    (resultBuffer, queryStats, queryProfiler)
}

fn RemoveDuplicateStatBlocks(aQueryResult : &mut Vec<RankedStatBlock>, someStats : &mut QueryStats, aQueryProfiler : &QueryProfiler )
{
    let _guardRemoveDuplicates = aQueryProfiler.enter(ProfileTag_RemoveDuplicates);

    //
    // Remove duplicate stat blocks combinations that have the same race and feats.
    // They provide no extra information and just clutter the output.
    //
    // For example, a Variant Human with the Athlete feat could either:
    //
    //     Take +1 Strength from the Race, +1 Dexterity from the feat
    //         OR
    //     Take +1 Dexterity from the Race, +1 Strength from the feat
    //
    // and get the exact same result, the choice doesn't really matter.
    //
    for rankedStatBlock in aQueryResult
    {
        let oldSize = rankedStatBlock.combinations.len();

        rankedStatBlock.combinations.sort();
        rankedStatBlock.combinations.dedup_by(|i, j| (i.myRace == j.myRace) && (i.myFeats == j.myFeats) );

        let newSize = rankedStatBlock.combinations.len();

        let numRemoved = oldSize - newSize;

        someStats.numDuplicateRaceStatBlocksRemoved += u64::try_from(numRemoved).unwrap();
    }
}

fn SanityCheckQueryResults( aQueryResult : &[RankedStatBlock], someParameters : &QueryParameters, aQueryProfiler : &QueryProfiler )
{
    let _guardSanityCheck = aQueryProfiler.enter(ProfileTag_SanityCheck);

    let mut lastStatBlockRank = if someParameters.isReversed { StatBlockRank::min_value() } else { StatBlockRank::max_value() };

    // Check number of results.
    assert!( aQueryResult.len() <= someParameters.maxNumberOfResults );

    // Check stat blocks.
    for aRankedStatBlock in aQueryResult
    {
        // Check that the cached rank is correct.
        assert_eq!(aRankedStatBlock.rank, RankStatBlock(aRankedStatBlock.statBlock, someParameters.rankingStyle) );

        // Check sorted by rank.
        if someParameters.isReversed
        {
            assert!( aRankedStatBlock.rank >= lastStatBlockRank );
        }
        else
        {
            assert!( aRankedStatBlock.rank <= lastStatBlockRank );
        }

        lastStatBlockRank = aRankedStatBlock.rank;

        // Check constraints.
        for &aConstraint in &someParameters.constraints
        {
            assert!( IsStatBlockValid(aConstraint, aRankedStatBlock.statBlock) );
        }

        // Check max value.
        assert!( !IsStatBlockAboveMaxValue(aRankedStatBlock.statBlock) );

        // Should have at least one combination.
        assert!( !aRankedStatBlock.combinations.is_empty() );

        // Check combinations.
        for aCombination in &aRankedStatBlock.combinations
        {
            // Check race.
            assert!( someParameters.availableRaces.iter().any(|&aRace| aRace == aCombination.myRace) );

            // Check feats.
            let numRequiredFeats : u8 = someParameters.requiredFeats.len().try_into().unwrap();

            let numStartingFeats = GetNumStartingFeats(aCombination.myRace, someParameters.doAllRacesHaveFreeFeat);

            // We should never spend ASIs on feats if we don't have to.
            if someParameters.requiredFeats.is_empty() && numStartingFeats == 0
            {
                assert!( aCombination.myFeats.is_empty() );
            }

            // Make sure we have a generally appropriate number of feats.
            let minNumFeats = numRequiredFeats;
            let maxNumFeats = numRequiredFeats + numStartingFeats;

            assert_ge!( aCombination.myFeats.len(), minNumFeats.into() );
            assert_le!( aCombination.myFeats.len(), maxNumFeats.into() );

            // Check that all feats are something that the race can take!
            for aFeat in aCombination.myFeats.iter()
            {
                assert!( IsFeatValidForRace(*aFeat, aCombination.myRace) );
            }

            // Check stat block math.
            let finalStatBlock = CombineStatBlocks4
            (
                aCombination.myBasePointBuyArray   ,
                aCombination.myRaceModifiers       ,
                aCombination.myASIs                ,
                aCombination.myFeatModifiers
            );

            assert_eq!( aRankedStatBlock.statBlock, finalStatBlock );

            // Race should always increase at least one stat.
            assert!( aCombination.myRaceModifiers.iter().any(|&aValue| aValue > 0) );

            // ASIs should never reduce any stat.
            assert!( aCombination.myASIs.iter().all(|&aValue| aValue >= 0) );

            // Feats should never reduce any stat.
            assert!( aCombination.myFeatModifiers.iter().all(|&aValue| aValue >= 0) );

            // Check ASIs.
            let optNumRemaining = GetNumRemainingAfterRequiredFeatPurchace
            (
                someParameters.numberOfASIs,
                u8::try_from(someParameters.requiredFeats.len()).unwrap(),
                numStartingFeats
            );

            match optNumRemaining
            {
                Some((numRemainingASIs, _)) =>
                {
                    let asiSum : u8 = aCombination.myASIs.iter().sum::<i8>().try_into().unwrap();

                    assert_eq!( asiSum, numRemainingASIs * 2 );
                },
                None => unreachable!("How is this in the final result if there was nothing remaining?"),
            }
        }
    }
}

fn GetOrInsertNewRankedStatBlockIfGoodEnough<'a>( aResultBuffer  : &'a mut Vec<RankedStatBlock>
                                                , aStatBlock     : StatBlock
                                                , aRankingStyle  : RankingStyle
                                                , aStatOrdering  : StatOrdering
                                                , isReversed     : bool
                                                , someStats      : &mut QueryStats) -> Option<&'a mut RankedStatBlock>
{
    let Compare = |lhs, rhs|
    {
        let cmp = CompareStatBlock( lhs, rhs, aStatOrdering );

        if isReversed
        {
            cmp.reverse()
        }
        else
        {
            cmp
        }
    };

    let aStatBlockRank = RankStatBlock(aStatBlock, aRankingStyle);

    let isBufferFull = aResultBuffer.len() == aResultBuffer.capacity();

    // If the (sorted) buffer is full and the stat block is worse than our current
    // lower bound then we don't care about it.
    if isBufferFull
    {
        let cmpWithWorst =
        {
            let worstStatBlockSoFar = aResultBuffer.last().unwrap();

            Compare
            (
                (aStatBlockRank          , aStatBlock                   ),
                (worstStatBlockSoFar.rank, worstStatBlockSoFar.statBlock),
            )
        };

        if cmpWithWorst == Ordering::Less
        {
            someStats.numUniqueCombinationsBelowLowerBound += 1;
            return None;
        }
    }

    let index = aResultBuffer.binary_search_by( |probe|
    {
        // Buffer is in descending order (i.e. best to worst), so the probe is the second argument.
        Compare
        (
            (aStatBlockRank, aStatBlock     ),
            (probe.rank    , probe.statBlock),
        )
    });

    // Doesn't exist, add.
    if let Err(i) = index
    {
        // If the buffer was full we threw away any stat blocks below the lower
        // bound, so we should never want to insert past the end of the buffer.
        assert!( i < aResultBuffer.capacity() );

        // We want to add this stat block and the buffer is full so
        // throw away our current lower bound to make room.
        if isBufferFull
        {
            someStats.numStatBlocksEjected += 1;
            someStats.numUniqueCombinationsEjected += aResultBuffer.last().unwrap().combinations.len() as u64;

            aResultBuffer.pop();
        }

        someStats.numStatBlocksShiftedDuringInsertion += (aResultBuffer.len() - i) as u64;

        let oldLen = aResultBuffer.len();
        let oldCap = aResultBuffer.capacity();

        aResultBuffer.insert
        (
            i,
            RankedStatBlock
            {
                rank            : aStatBlockRank,
                statBlock       : aStatBlock,
                combinations    : vec![],
            }
        );

        let newLen = aResultBuffer.len();
        let newCap = aResultBuffer.capacity();

        // Sanity check, we should have enough space reserved already.
        assert!( newLen == oldLen + 1 );
        assert!( newCap == oldCap     );
    }

    let index = match index { Ok(i) | Err(i) => i };

    // Return the entry, whether it already existed or was newly added.
    Some( &mut aResultBuffer[index] )
}


#[test]
fn TestGetOrInsertNewRankedStatBlockIfGoodEnough()
{
    use crate::dnd::stat::*;

    let rankingStyle = RankingStyle::Flat;

    let statOrdering = [ Stat::Strength, Stat::Dexterity, Stat::Constitution, Stat::Intelligence, Stat::Wisdom, Stat::Charisma ];

    let inputStatBlocks_BestToWorst = vec!
    [
        [16, 10, 10, 10, 10, 10], // Rank 3
        [10, 12, 12, 12, 10, 10], // Rank 3
        [10, 10, 10, 10, 10, 16], // Rank 3
        [10, 10, 12, 10, 10, 10], // Rank 1
        [10, 10, 10, 10, 10, 10], // Rank 0
    ];

    for &isReversed in [false, true].iter()
    {
        for capacity in 1..=6
        {
            let mut queryStats = QueryStats::default();
            let mut rankedStatBlocks = Vec::with_capacity(capacity);

            // Try to add each stat block twice, first from best->worst and then from worst->best
            for inputStatBlock in inputStatBlocks_BestToWorst.iter().chain(inputStatBlocks_BestToWorst.iter().rev())
            {
                let optRankedStatBlock = GetOrInsertNewRankedStatBlockIfGoodEnough
                (
                    &mut rankedStatBlocks,
                    *inputStatBlock,
                    rankingStyle,
                    statOrdering,
                    isReversed,
                    &mut queryStats
                );

                if let Some(rankedStatBlock) = optRankedStatBlock
                {
                    // The actual data here doesn't matter, just the fact that an entry exists.
                    rankedStatBlock.combinations.push( CharacterStatBlock
                    {
                        myRace              : CharacterRace::Human,
                        myFeats             : vec![],
                        myBasePointBuyArray : [0, 0, 0, 0, 0, 0],
                        myRaceModifiers     : [0, 0, 0, 0, 0, 0],
                        myASIs              : [0, 0, 0, 0, 0, 0],
                        myFeatModifiers     : [0, 0, 0, 0, 0, 0],
                    });
                }
            }

            // Everything we ended up keeping should have 2 combinations
            // per entry, since we added each stat block twice.
            rankedStatBlocks.iter().map(|i| i.combinations.len() ).for_each(|n| assert_eq!(n, 2));

            let calculatedStatBlocks : Vec<_> = rankedStatBlocks.iter().map(|i| i.statBlock ).collect();

            let   expectedStatBlocks : Vec<_> = if isReversed { inputStatBlocks_BestToWorst.iter().rev().take(capacity).copied().collect() }  // Worse N stat blocks
                                                else          { inputStatBlocks_BestToWorst.iter()      .take(capacity).copied().collect() }; // Best N stat blocks

            assert_eq!( calculatedStatBlocks, expectedStatBlocks );

            // Make sure the stats are as expected.
            // This is the best way to make sure that the insertion algorithm behaved correctly.
            let TestStats = |numUniqueCombinationsBelowLowerBound, numStatBlocksEjected, numUniqueCombinationsEjected, numStatBlocksShiftedDuringInsertion|
            {
                assert_eq!( queryStats.numUniqueCombinationsBelowLowerBound, numUniqueCombinationsBelowLowerBound );
                assert_eq!( queryStats.numStatBlocksEjected                , numStatBlocksEjected );
                assert_eq!( queryStats.numUniqueCombinationsEjected        , numUniqueCombinationsEjected );
                assert_eq!( queryStats.numStatBlocksShiftedDuringInsertion , numStatBlocksShiftedDuringInsertion );
            };

            if isReversed
            {
                match capacity
                {
                    1 => TestStats(4, 4, 4, 0),
                    2 => TestStats(3, 3, 3, 1 + 1 + 1 + 1),
                    3 => TestStats(2, 2, 2, 1 + 2 + 2 + 2),
                    4 => TestStats(1, 1, 1, 1 + 2 + 3 + 3),
                    5 => TestStats(0, 0, 0, 1 + 2 + 3 + 4),
                    6 => TestStats(0, 0, 0, 1 + 2 + 3 + 4),
                    _ => panic!(""),
                };
            }
            else
            {
                match capacity
                {
                    1 => TestStats(8, 0, 0, 0),
                    2 => TestStats(6, 0, 0, 0),
                    3 => TestStats(4, 0, 0, 0),
                    4 => TestStats(2, 0, 0, 0),
                    5 => TestStats(0, 0, 0, 0),
                    6 => TestStats(0, 0, 0, 0),
                    _ => panic!(""),
                };
            }
        }
    }
}
