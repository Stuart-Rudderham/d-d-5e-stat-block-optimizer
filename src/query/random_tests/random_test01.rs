use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test01" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Unranked,
        statOrdering: [
            Dexterity,
            Strength,
            Intelligence,
            Charisma,
            Constitution,
            Wisdom,
        ],
        isReversed: true,
        availableRaces: vec![
            Aasimar_Scourge,
            CustomLineage,
            Changling,
            DragonBorn,
            Dwarf_MarkOfWarding,
            Elf_Wood,
            Gnome_MarkOfScribing,
            HalfElf_MarkOfDetection,
            HalfElf_MarkOfStorm,
            HalfOrc,
            Human_MarkOfHandling,
            Lizardfolk,
            Shifter_LongTooth,
            Tabaxi,
            Triton,
            Warforged,
            YuanTiPureblood,
        ],
        requiredFeats: vec![
            DrowHighMagic,
            HeavyArmorMaster,
        ],
        constraints: vec![],
        numberOfASIs: 3,
        doAllRacesHaveFreeFeat: false,
        canRearrangeRaceModifiers: true,
        maxNumberOfResults: 0,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 0,
            numUniqueCombinationsPassedConstraints: 0,
            numStatBlocksEjected: 0,
            numUniqueCombinationsEjected: 0,
            numUniqueCombinationsBelowLowerBound: 0,
            numStatBlocksShiftedDuringInsertion: 0,
            numDuplicateRaceStatBlocksRemoved: 0,
            numUniqueStatBlockAfterRacePass: 0,
            numUniqueStatBlockAfterFeatPass: 0,
            numFinalStatBlockCombinations: 0,
        };

    let expectedStatBlocks = 
        vec![];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

