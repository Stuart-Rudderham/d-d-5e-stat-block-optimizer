use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test46" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Flat,
        statOrdering: [
            Wisdom,
            Intelligence,
            Dexterity,
            Charisma,
            Strength,
            Constitution,
        ],
        isReversed: false,
        availableRaces: vec![
            Aarakocra,
            Aasimar_Protector,
            Aasimar_Scourge,
            Bugbear,
            CustomLineage,
            Changling,
            DragonBorn,
            Dwarf_Grey,
            Dwarf_Hill,
            Dwarf_MarkOfWarding,
            Elf_Drow,
            Elf_High,
            Elf_MarkOfShadow,
            Firbolg,
            Genasi_Air,
            Genasi_Earth,
            Gnome_Deep,
            Gnome_Forest,
            Gnome_MarkOfScribing,
            Gnome_Rock,
            Goliath,
            HalfElf,
            HalfElf_MarkOfDetection,
            HalfElf_MarkOfStorm,
            Halfling_Ghostwise,
            Halfling_Lightfoot,
            Halfling_MarkOfHealing,
            Halfling_MarkOfHospitality,
            Halfling_Stout,
            Human,
            Human_MarkOfFinding,
            Human_MarkOfHandling,
            Human_MarkOfMaking,
            Human_MarkOfPassage,
            Human_MarkOfSentinel,
            Kalashtar,
            Kenku,
            Lizardfolk,
            Orc,
            Shifter_BeastHide,
            Shifter_LongTooth,
            Shifter_SwiftStride,
            Shifter_WildHunt,
            Tiefling,
            Tiefling_Feral,
            Tortle,
            Triton,
            YuanTiPureblood,
        ],
        requiredFeats: vec![],
        constraints: vec![
            BinCmp_StatModifier_Constant(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Strength,
                    ),
                    binCmp: LE,
                    rhsValue: GetConstant(
                        3,
                    ),
                },
            ),
            BinCmp_StatModifier_Constant(
                BinaryCompare {
                    lhsValue: GetStatModifier(
                        Constitution,
                    ),
                    binCmp: LE,
                    rhsValue: GetConstant(
                        -1,
                    ),
                },
            ),
        ],
        numberOfASIs: 1,
        doAllRacesHaveFreeFeat: false,
        canRearrangeRaceModifiers: true,
        maxNumberOfResults: 28,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 15800589,
            numUniqueCombinationsPassedConstraints: 2170257,
            numStatBlocksEjected: 380,
            numUniqueCombinationsEjected: 646,
            numUniqueCombinationsBelowLowerBound: 2169787,
            numStatBlocksShiftedDuringInsertion: 5802,
            numDuplicateRaceStatBlocksRemoved: 5,
            numUniqueStatBlockAfterRacePass: 237,
            numUniqueStatBlockAfterFeatPass: 243,
            numFinalStatBlockCombinations: 28,
        };

    let expectedStatBlocks = 
        vec![
            RankedStatBlock {
                rank: 10,
                statBlock: [12, 14, 9, 14, 18, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![],
                        myBasePointBuyArray: [11, 13, 8, 13, 15, 13],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [14, 14, 9, 14, 18, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![],
                        myBasePointBuyArray: [13, 13, 8, 13, 15, 11],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [14, 12, 9, 14, 18, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![],
                        myBasePointBuyArray: [13, 11, 8, 13, 15, 13],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [14, 14, 9, 12, 18, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![],
                        myBasePointBuyArray: [13, 13, 8, 11, 15, 13],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [14, 14, 9, 14, 17, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![],
                        myBasePointBuyArray: [13, 13, 8, 13, 14, 13],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [12, 14, 9, 16, 16, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![],
                        myBasePointBuyArray: [11, 13, 8, 13, 15, 13],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [14, 14, 9, 16, 16, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![],
                        myBasePointBuyArray: [13, 13, 8, 13, 15, 11],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [14, 12, 9, 16, 16, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![],
                        myBasePointBuyArray: [13, 11, 8, 13, 15, 13],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [14, 14, 9, 15, 16, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![],
                        myBasePointBuyArray: [13, 13, 8, 13, 14, 13],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 1, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [12, 16, 9, 14, 16, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![],
                        myBasePointBuyArray: [11, 13, 8, 13, 15, 13],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [14, 16, 9, 14, 16, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![],
                        myBasePointBuyArray: [13, 13, 8, 13, 15, 11],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [14, 15, 9, 14, 16, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![],
                        myBasePointBuyArray: [13, 13, 8, 13, 14, 13],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 1, 0, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [12, 14, 9, 14, 16, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![],
                        myBasePointBuyArray: [11, 13, 8, 13, 13, 15],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [14, 14, 9, 14, 16, 15],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![],
                        myBasePointBuyArray: [13, 13, 8, 13, 13, 14],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [15, 14, 9, 14, 16, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![],
                        myBasePointBuyArray: [13, 13, 8, 13, 14, 13],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [1, 0, 0, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [14, 14, 9, 14, 16, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![],
                        myBasePointBuyArray: [11, 13, 8, 13, 15, 13],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [14, 14, 8, 14, 16, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: HalfElf,
                        myFeats: vec![],
                        myBasePointBuyArray: [13, 13, 8, 13, 13, 14],
                        myRaceModifiers:     [0, 1, 0, 1, 2, 0],
                        myASIs:              [1, 0, 0, 0, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [16, 14, 9, 14, 16, 12],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![],
                        myBasePointBuyArray: [13, 13, 8, 13, 15, 11],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [14, 12, 9, 14, 16, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![],
                        myBasePointBuyArray: [13, 11, 8, 13, 13, 15],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [16, 12, 9, 14, 16, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![],
                        myBasePointBuyArray: [13, 11, 8, 13, 15, 13],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [14, 16, 9, 12, 16, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![],
                        myBasePointBuyArray: [13, 13, 8, 11, 15, 13],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [14, 14, 9, 12, 16, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![],
                        myBasePointBuyArray: [13, 13, 8, 11, 13, 15],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 0, 2, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [16, 14, 9, 12, 16, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![],
                        myBasePointBuyArray: [13, 13, 8, 11, 15, 13],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [14, 14, 9, 16, 15, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![],
                        myBasePointBuyArray: [13, 13, 8, 13, 14, 13],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [14, 16, 9, 14, 15, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![],
                        myBasePointBuyArray: [13, 13, 8, 13, 14, 13],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 2, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [14, 14, 9, 14, 15, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![],
                        myBasePointBuyArray: [13, 13, 8, 13, 13, 14],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 0, 1, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [16, 14, 9, 14, 15, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![],
                        myBasePointBuyArray: [13, 13, 8, 13, 14, 13],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [2, 0, 0, 0, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 10,
                statBlock: [12, 14, 9, 18, 14, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: Human,
                        myFeats: vec![],
                        myBasePointBuyArray: [11, 13, 8, 15, 13, 13],
                        myRaceModifiers:     [1, 1, 1, 1, 1, 1],
                        myASIs:              [0, 0, 0, 2, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
        ];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

