use crate::query::*;
use crate::query::random_tests_harness::*;

#[allow(unused_imports)] use crate::dnd::race::*;
#[allow(unused_imports)] use crate::dnd::race::CharacterRace::*;

#[allow(unused_imports)] use crate::dnd::feat::*;
#[allow(unused_imports)] use crate::dnd::feat::Feat::*;

#[allow(unused_imports)] use crate::dnd::statblock::*;
#[allow(unused_imports)] use crate::dnd::statblock::RankingStyle::*;

#[allow(unused_imports)] use crate::dnd::stat::*;
#[allow(unused_imports)] use crate::dnd::stat::Stat::*;

#[allow(unused_imports)] use crate::constraint::*;
#[allow(unused_imports)] use crate::constraint::Constraint::*;

#[allow(unused_imports)] use crate::constraint::compare_binary::*;
#[allow(unused_imports)] use crate::constraint::compare_binary::BinCmp::*;

#[allow(unused_imports)] use crate::constraint::compare_unary::*;
#[allow(unused_imports)] use crate::constraint::compare_unary::UnCmp::*;

#[allow(unused_imports)] use crate::constraint::value::*;

#[test]
fn OffByOne()
{
    OffByOneTestHarness( GetQueryParameters )
}

#[test]
fn ExpectedOutput()
{
    ExpectedOutputTestHarness( GetQueryParameters, GetExpectedQueryResults )
}

#[cfg(feature = "benchmark")]
pub fn Benchmark()
{
    BenchmarkTestHarness( GetQueryParameters, "random_test38" )
}

fn GetQueryParameters() -> QueryParameters
{
    QueryParameters {
        rankingStyle: Weighted,
        statOrdering: [
            Intelligence,
            Constitution,
            Strength,
            Charisma,
            Wisdom,
            Dexterity,
        ],
        isReversed: false,
        availableRaces: vec![
            Aasimar_Fallen,
            Aasimar_Scourge,
            CustomLineage,
            DragonBorn,
            Elf_Wood,
            Genasi_Fire,
            Genasi_Water,
            Gnome_Forest,
            Halfling_Ghostwise,
            Halfling_MarkOfHealing,
            Halfling_Stout,
            HalfOrc,
            Hobgoblin,
            Human_MarkOfSentinel,
            Orc,
            Orc_Eberron,
            Shifter_BeastHide,
            Tiefling,
            Tortle,
            Triton,
            Warforged,
        ],
        requiredFeats: vec![
            MediumArmorMaster,
            Mobile,
        ],
        constraints: vec![
            UnCmp_StatValue(
                UnaryCompare {
                    value: GetStatValue(
                        Constitution,
                    ),
                    unCmp: IsEven,
                },
            ),
        ],
        numberOfASIs: 1,
        doAllRacesHaveFreeFeat: true,
        canRearrangeRaceModifiers: false,
        maxNumberOfResults: 17,
        optCustomPointBuyArray: None,
    }
}

#[cfg(test)]
fn GetExpectedQueryResults() -> QueryResult
{
    let expectedQueryStats = 
        QueryStats {
            numUniqueCombinationsSeen: 598932,
            numUniqueCombinationsPassedConstraints: 301051,
            numStatBlocksEjected: 94,
            numUniqueCombinationsEjected: 94,
            numUniqueCombinationsBelowLowerBound: 300940,
            numStatBlocksShiftedDuringInsertion: 819,
            numDuplicateRaceStatBlocksRemoved: 0,
            numUniqueStatBlockAfterRacePass: 23,
            numUniqueStatBlockAfterFeatPass: 23,
            numFinalStatBlockCombinations: 17,
        };

    let expectedStatBlocks = 
        vec![
            RankedStatBlock {
                rank: 111012,
                statBlock: [14, 8, 16, 18, 8, 10],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            MediumArmorMaster,
                            Mobile,
                        ],
                        myBasePointBuyArray: [14, 8, 15, 15, 8, 10],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 1, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 111012,
                statBlock: [14, 8, 16, 18, 10, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            MediumArmorMaster,
                            Mobile,
                        ],
                        myBasePointBuyArray: [14, 8, 15, 15, 10, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 1, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 111012,
                statBlock: [14, 10, 16, 18, 8, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            MediumArmorMaster,
                            Mobile,
                        ],
                        myBasePointBuyArray: [14, 10, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 1, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 111012,
                statBlock: [10, 8, 16, 18, 8, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            MediumArmorMaster,
                            Mobile,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 15, 8, 14],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 1, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 111012,
                statBlock: [10, 8, 16, 18, 14, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            MediumArmorMaster,
                            Mobile,
                        ],
                        myBasePointBuyArray: [10, 8, 15, 15, 14, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 1, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 111012,
                statBlock: [10, 14, 16, 18, 8, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            MediumArmorMaster,
                            Mobile,
                        ],
                        myBasePointBuyArray: [10, 14, 15, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 1, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 111012,
                statBlock: [8, 8, 16, 18, 10, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            MediumArmorMaster,
                            Mobile,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 15, 10, 14],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 1, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 111012,
                statBlock: [8, 10, 16, 18, 8, 14],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            MediumArmorMaster,
                            Mobile,
                        ],
                        myBasePointBuyArray: [8, 10, 15, 15, 8, 14],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 1, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 111012,
                statBlock: [8, 8, 16, 18, 14, 10],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            MediumArmorMaster,
                            Mobile,
                        ],
                        myBasePointBuyArray: [8, 8, 15, 15, 14, 10],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 1, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 111012,
                statBlock: [8, 14, 16, 18, 8, 10],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            MediumArmorMaster,
                            Mobile,
                        ],
                        myBasePointBuyArray: [8, 14, 15, 15, 8, 10],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 1, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 111012,
                statBlock: [8, 10, 16, 18, 14, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            MediumArmorMaster,
                            Mobile,
                        ],
                        myBasePointBuyArray: [8, 10, 15, 15, 14, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 1, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 111012,
                statBlock: [8, 14, 16, 18, 10, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            MediumArmorMaster,
                            Mobile,
                        ],
                        myBasePointBuyArray: [8, 14, 15, 15, 10, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 1, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 111012,
                statBlock: [16, 8, 14, 18, 8, 10],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            MediumArmorMaster,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 14, 15, 8, 10],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [1, 0, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 111012,
                statBlock: [16, 8, 14, 18, 10, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            MediumArmorMaster,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 8, 14, 15, 10, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [1, 0, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 111012,
                statBlock: [16, 10, 14, 18, 8, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            MediumArmorMaster,
                            Mobile,
                        ],
                        myBasePointBuyArray: [15, 10, 14, 15, 8, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [1, 0, 0, 1, 0, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 111012,
                statBlock: [10, 8, 14, 18, 8, 16],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            MediumArmorMaster,
                            Mobile,
                        ],
                        myBasePointBuyArray: [10, 8, 14, 15, 8, 15],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 1, 0, 1],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
            RankedStatBlock {
                rank: 111012,
                statBlock: [10, 8, 14, 18, 16, 8],
                combinations: vec![
                    CharacterStatBlock {
                        myRace: CustomLineage,
                        myFeats: vec![
                            MediumArmorMaster,
                            Mobile,
                        ],
                        myBasePointBuyArray: [10, 8, 14, 15, 15, 8],
                        myRaceModifiers:     [0, 0, 0, 2, 0, 0],
                        myASIs:              [0, 0, 0, 1, 1, 0],
                        myFeatModifiers:     [0, 0, 0, 0, 0, 0],
                    },
                ],
            },
        ];

    (expectedStatBlocks, expectedQueryStats, QueryProfiler::new(""))
}

